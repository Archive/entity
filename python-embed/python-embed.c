#include "config.h"

#ifdef USE_PYTHON

#include <glib.h>
#include <gmodule.h>
#include <stdio.h>
#include <stdlib.h>
#include <elements.h>
#include <xml-node.h>
#include <xml-tree.h>
#include <xml-parser.h>
#include <dlfcn.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include "edebug.h"
#include "xml-node.h"
#include "xml-tree.h"
#include <lang.h>
#include "erend.h"

#include <Python.h> 
#include <pythonrun.h> 
#include <graminit.h> 

#include "pyembed.h"

static XML_Node* current_python_calling_node;

/*Render the python node*/
void python_render(XML_Node* node)
{
  if (!node || !node->data)
    {
      g_warning("What?  why did i get this node then?\n");
      return;
    }
  PyRun_SimpleString(node->data->str);
}


static PyObject*
python_entity_get_parent(PyObject* self, PyObject* args)
{   
  XML_Node* node;
  PyObject* retobj = NULL;
  char* path;

  if(PyArg_Parse(args, "s", &path) )
  {
    /*Just in case path is relative?*/
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return retobj;

    node = node->parent;
    if(!node) return retobj;
    retobj = Py_BuildValue("s", xml_node_get_path(node) );
  }
  else
  {
    PyErr_SetString(PyExc_TypeError, "Usage: entity_get_parent(path)" );
  }
  return retobj;
}   


static PyObject* 
python_entity_find_parent (PyObject* self, PyObject* args)
{
  XML_Node* node;
  PyObject* retobj = NULL;
  char* path, *type;

  if(PyArg_Parse(args, "(ss)", &path, &type) )
  { 
    /*Just in case path is relative?*/
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return retobj;

    node = xml_node_find_parent (node, type);
    if(!node) return retobj;
    retobj = Py_BuildValue("s", xml_node_get_path(node) );
  }
  else
  {
    PyErr_SetString(PyExc_TypeError, "Usage: entity_find_parent(path, type)" );
  }
  return retobj;
}

static PyObject* 
python_entity_new_child(PyObject* self, PyObject* args)
{   
  XML_Node* node;
  XML_Node* parent;
  PyObject* retobj = NULL;
  char* path, *name, *tag;
  char* node_path;
  
  if(PyArg_Parse(args, "(sss)", &path, &tag, &name) )
  { 
    /*Just in case path is relative?*/
    parent = xml_node_fancy_find (current_python_calling_node, path);
    if(!parent) return retobj;

    node = xml_node_new_child (parent, tag, name, NULL);

    node_path = xml_node_get_path(node);
    retobj = Py_BuildValue("s", node_path );
  }
  else
  {
    PyErr_SetString(PyExc_TypeError, 
            "Usage: entity_new_child(path, tag, name)" );
  }
  return retobj;
}

static PyObject* 
python_entity_find(PyObject* self, PyObject* args)
{
  XML_Node* node;
  PyObject* retobj = NULL;
  char* path, *name;

  if(PyArg_Parse(args, "(ss)", &path, &name) )
  { 
    /*Just in case path is relative?*/
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return retobj;

    node = xml_node_find_by_name(node , name);
    if(!node) return retobj;

    retobj = Py_BuildValue("s", xml_node_get_path(node) );
  }
  else
  {
    PyErr_SetString(PyExc_TypeError, "Usage: entity_find(path, name)" );
  }
  return retobj;
}

static PyObject*
python_entity_set_xml(PyObject* self, PyObject* args)
{
  PyObject* retobj = NULL;
  char *path;
  char* data;
  XML_Node* node;

  if(PyArg_Parse(args, "(ss)", &path, &data) )
  {
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return Py_BuildValue("s", "");

    xml_parse_string (node, data);
    xml_tree_render (node);
  }
  else
  {
    PyErr_SetString(PyExc_TypeError, "Usage: entity_get_data(path, data)" );
  }
  Py_INCREF(Py_None);
  retobj = Py_None;
  return retobj;
}

static PyObject*
python_entity_get_xml(PyObject* self, PyObject* args)
{
  PyObject* retobj = NULL;
  char *path;
  char* data;
  XML_Node* node;

  if(PyArg_Parse(args, "s", &path) )
  {
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return Py_BuildValue("s", "");

    data = xml_tree_get_xml(node);
    if(data)
    {
      retobj = Py_BuildValue("s", data);
    }
    else
    {
      retobj = Py_BuildValue("s", "");
      /*
      Py_INCREF(Py_None);
      retobj = Py_None;
      */
    }
  }
  else
  {
    PyErr_SetString(PyExc_TypeError, "Usage: python_entity_get_xml(path)" );
  }
  return retobj;
}

static PyObject*
python_entity_set_data(PyObject* self, PyObject* args)
{
  PyObject* retobj = NULL;
  char *path;
  char* data;
  XML_Node* node;

  if(PyArg_Parse(args, "(ss)", &path, &data) )
  {
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return Py_BuildValue("s", "");

    xml_node_set_data(node, data);
  }
  else
  {
    PyErr_SetString(PyExc_TypeError, "Usage: entity_get_data(path, data)" );
  }
  Py_INCREF(Py_None);
  retobj = Py_None;
  return retobj;
}

static PyObject* 
python_entity_get_data(PyObject* self, PyObject* args)
{
  PyObject* retobj = NULL;
  char *path;
  GString* data;
  XML_Node* node;

  if(PyArg_Parse(args, "s", &path) )
  {
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return Py_BuildValue("s", "");

    data = xml_node_get_data(node);
    if(data)
    {
      retobj = Py_BuildValue("s", data->str);
    }
    else
    {
      retobj = Py_BuildValue("s", "");
      /*
      Py_INCREF(Py_None);
      retobj = Py_None;
      */
    }
  }
  else
  {
    PyErr_SetString(PyExc_TypeError, "Usage: entity_get_data(path)" );
  }
  return retobj;
}

static PyObject* 
python_entity_set_attr(PyObject* self, PyObject* args)
{
  PyObject* retobj = NULL;
  char *path, *attr, *value;
  XML_Node* node;

  if(PyArg_Parse(args, "(sss)", &path, &attr, &value) )
  {
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return retobj;

    xml_node_set_attr_str(node, attr, value);
  }

  Py_INCREF(Py_None);
  retobj = Py_None;

  return retobj;
}


static PyObject*
python_entity_get_attr(PyObject* self, PyObject* args)
{
  PyObject* retobj = NULL;
  char *path, *attr;
  char* value;
  XML_Node* node;

  if(PyArg_Parse(args, "(ss)", &path, &attr) )
  {
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return Py_BuildValue("s", "");

    value = xml_node_get_attr(node, attr);
    if(value)
    {
      retobj = Py_BuildValue("s", value);
    }
    else
    {
      retobj = Py_BuildValue("s", "");
      /*
      Py_INCREF(Py_None);
      retobj = Py_None;
      */
    }
  }
  else
  {
    PyErr_SetString(PyExc_TypeError, "Usage: entity_get_attr(path, attr)" );
  }
  return retobj;
}

static PyObject*
python_entity_is_attr_true(PyObject* self, PyObject* args)
{
  PyObject* retobj = NULL;
  char *path, *attr;
  char* value;
  XML_Node* node;

  if(PyArg_Parse(args, "(ss)", &path, &attr) )
  {
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return Py_BuildValue("s", "");

    value = xml_node_get_attr(node, attr);
    retobj = Py_BuildValue("i", erend_value_is_true(value));
  }
  else
  {
    PyErr_SetString(PyExc_TypeError, "Usage: entity_is_attr_true(path, attr)" );
  }
  return retobj;
}


static PyObject*
python_entity_ls(PyObject* self, PyObject* args)
{
  PyObject* retobj = NULL;
  PyObject* item = NULL;
  char *path;
  GSList* list, *flist;
  XML_Node* node;
  int i;

  if(PyArg_Parse(args, "(s)", &path) )
  {
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return Py_BuildValue("s", "");

    flist = list = xml_node_ls (node);

    retobj = PyTuple_New( g_slist_length(list) );

    for(i=0; list; i++)
    {
      path = (char*)list->data;
      item = Py_BuildValue("s", path);
      PyTuple_SetItem(retobj, i, item);
      list = list->next;
    }
    g_slist_free (flist);
  }
  else
  {
    PyErr_SetString(PyExc_TypeError, "Usage: entity_ls(path)" );
  }
  return retobj;
}

static PyObject*
python_entity_delete_node(PyObject* self, PyObject* args)
{
  PyObject* retobj = NULL;
  char *path;
  XML_Node* node;

  if(PyArg_Parse(args, "s", &path) )
  {
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return retobj;

    xml_tree_delete (node, TRUE);
  }
   
  Py_INCREF(Py_None);
  retobj = Py_None;

  return retobj;
}

static PyObject*
python_entity_delete_children(PyObject* self, PyObject* args)
{
  PyObject* retobj = NULL;
  char *path;
  XML_Node* node;

  if(PyArg_Parse(args, "s", &path) )
  {
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return retobj;

    xml_tree_delete_in (node, TRUE);
  }
   
  Py_INCREF(Py_None);
  retobj = Py_None;

  return retobj;
}

static PyObject*
python_entity_ioprint(PyObject* self, PyObject* args)
{
  int size;
  gchar *path;
  gchar *data;
  XML_Node *node;
  Element* element;
  ElementAttr* e_attr;
  void (*sendq)(XML_Node *node, gchar *data, gint send_size);
  PyObject* retobj = NULL;

  Py_INCREF(Py_None);
  retobj = Py_None;

  if(PyArg_Parse(args, "(ss#)", &path, &data, &size) )
  {
    node = xml_node_fancy_find (current_python_calling_node, path);

    if(!node) return retobj;

    if(g_str_equal(node->element, "queued-io") )
    {
      element = element_lookup_element(node);
      if(!element)  return retobj;
    
      e_attr = g_hash_table_lookup(element->attributes, "sendq");
      if(!e_attr)  return retobj;
    
      sendq = (void*) e_attr->set_attr_func;
      if(!sendq)  return retobj;
    
      sendq(node, data, size);
    }
  }
  return retobj;
}

static PyObject*
python_entity_get_set_attrs(PyObject* self, PyObject* args)
{ 
  PyObject* retobj = NULL;
  PyObject* item = NULL;
  char *path, *attr;
  GSList* list, *flist;
  XML_Node* node;
  int i;

  if(PyArg_Parse(args, "s", &path) )
  {
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return Py_BuildValue("s", "");
  
    flist = list = xml_node_attr_list (node);
    
    retobj = PyTuple_New( g_slist_length(list) );
    
    for(i=0; list; i++)
    {
      attr = (char*)list->data;
      item = Py_BuildValue("s", attr);
      PyTuple_SetItem(retobj, i, item);
      list = list->next;
    }
    g_slist_free (flist);
  } 
  else
  { 
    PyErr_SetString(PyExc_TypeError, "Usage: entity_get_set_attr(path)" );
  } 
  return retobj;
}   

static PyObject*
python_entity_get_all_attrs(PyObject* self, PyObject* args)
{
#if 0 /* Broken code. */
  PyObject* retobj = NULL;
  PyObject* item = NULL;
  char *path, *attr;
  GSList* list, *flist;
  XML_Node* node;
  int i;
  
  if(PyArg_Parse(args, "s", &path) )
  {
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return Py_BuildValue("s", "");
  
    flist = list = element_get_all_atts_from_node (node);
  
    retobj = PyTuple_New( g_slist_length(list) );
  
    for(i=0; list; i++)
    {
      attr = (char*)list->data;
      item = Py_BuildValue("s", attr);
      PyTuple_SetItem(retobj, i, item);
      list = list->next;
    }
    g_slist_free (flist);
  }
  else
  {
    PyErr_SetString(PyExc_TypeError, "Usage: entity_get_all_attr(path)" );
  }
  return retobj;
#endif /* busted code. */
} 

static PyObject*
python_entity_get_attr_info(PyObject* self, PyObject* args)
{ 
#if 0 /* Busted for now... */
  PyObject* retobj = NULL;
  PyObject* item = NULL;
  char *path, *attr;
  XML_Node* node;
  ElementAttr* e_attr;
    
  if(PyArg_Parse(args, "(ss)", &path, &attr) )
  {   
    node = xml_node_fancy_find (current_python_calling_node, path);
    if(!node) return Py_BuildValue("s", "");
      
    e_attr = element_get_attr_info_from_node (node, attr);
    if (!e_attr)
      return retobj;
    
    retobj = PyTuple_New(3);

    if (e_attr->description)
      item = Py_BuildValue("s", e_attr->description);
    else          
      item = Py_BuildValue("s", "");
    PyTuple_SetItem(retobj, 0, item);
                  
    if (e_attr->value_desc)
      item = Py_BuildValue("s", e_attr->value_desc);
    else          
      item = Py_BuildValue("s", "");
    PyTuple_SetItem(retobj, 1, item);
                  
    if (e_attr->possible_values)
      item = Py_BuildValue("s", e_attr->possible_values);
    else          
      item = Py_BuildValue("s", "");
    PyTuple_SetItem(retobj, 2, item);
  } 
  else
  {  
    PyErr_SetString(PyExc_TypeError,"Usage: entity_get_attr_info(path, attr)");
  }   
  return retobj;
#endif /* Broken code. */
}     



static struct PyMethodDef entity_methods[] =
{
  {"entity_set_attr",		python_entity_set_attr},
  {"entity_get_attr",		python_entity_get_attr},
  {"entity_find",		python_entity_find},
  {"entity_get_data",		python_entity_get_data},
  {"entity_get_parent",		python_entity_get_parent},
  {"entity_find_parent",	python_entity_find_parent},
  {"entity_set_xml",		python_entity_set_xml},
  {"entity_get_xml",		python_entity_get_xml},
  {"entity_set_data",		python_entity_set_data},
  {"entity_get_data",		python_entity_get_data},
  {"entity_get_attr",		python_entity_get_attr},
  {"entity_is_attr_true",	python_entity_is_attr_true},
  {"entity_ls",			python_entity_ls},
  {"entity_delete_node",	python_entity_delete_node},
  {"entity_delete_children",	python_entity_delete_children},
  {"entity_get_set_attrs",	python_entity_get_set_attrs},
  {"entity_get_all_attrs",	python_entity_get_all_attrs},
  {"entity_get_attr_info",	python_entity_get_attr_info},
  {"entity_ioprint",	        python_entity_ioprint},
  {"entity_new_child",	        python_entity_new_child},
  {NULL,			NULL}
};


void terminate_entity(int sig)
{
  edebug("python-emebed", "trying to die\n");
  exit(1);
}

/*Start up the interpreter.*/
void python_start(void)
{
  static struct sigaction siga;

  siga.sa_handler = SIG_DFL;
  siga.sa_flags = 0;

  /*signal(SIGINT, terminate_entity);*/

  Py_Initialize();


  edebug("python-embed", "python started");

  Py_InitModule("CEntity", entity_methods);
  PyRun_SimpleString("from CEntity import *");
  PyRun_SimpleString("import sys");
  /*PyRun_SimpleString("print sys.path");*/
  PyRun_SimpleString("sys.path.append('"ENTITYPY"')" );
  PyRun_SimpleString("from Entity import *");
  
  /* and restore SIGINT handler */
  sigaction(SIGINT, &siga, NULL);
}

static gint
python_function_execute (XML_Node *calling_node, gchar *function, GSList *args)
{
  PyObject node;
  PyObject *args_obj, *func=NULL, *item=NULL;
  PyObject *result;

  GSList *tmp, *objlist=NULL;
  LangArg *arg;

  char *path;
  int i = 0;

  current_python_calling_node = calling_node;

  func = Load_Attribute("__main__", function);
  if(!func)
  {
    g_warning("function (%s) doesn't exist in __main__", function);
    PyErr_Print();
    return FALSE;
  }
  objlist = g_slist_append(objlist, func);

  args_obj = PyTuple_New( g_slist_length(args) );
  objlist = g_slist_append(objlist, args_obj);

  for(tmp=args; tmp; tmp = tmp->next)
  {
    arg = (LangArg *) tmp->data;

    if(arg->type == LANG_NODE)
    {
      path = xml_node_get_path(arg->data);
      Run_Function("__main__", "enode", "O", &node, "(s)", path);
      g_free(path);
      item = Py_BuildValue("O", node);
      PyTuple_SetItem(args_obj, i, item);
    }
    else if(arg->type == LANG_STRING)
    {
      item = Py_BuildValue("s", (char*)arg->data);
      PyTuple_SetItem(args_obj, i, item);
    }
    else if(arg->type == LANG_INT)
    {
      item = Py_BuildValue("i", (char*)arg->intdata);
      PyTuple_SetItem(args_obj, i, item);
    }
    else if(arg->type == LANG_BINSTRING)
    {
      item = Py_BuildValue("s#", (char*)arg->data, arg->size);
      PyTuple_SetItem(args_obj, i, item);
    }
    i++;
    objlist = g_slist_append(objlist, item);
    lang_arg_free(arg);
  }

  result = PyEval_CallObject(func, args_obj);
  objlist = g_slist_append(objlist, result);

  for(tmp=objlist; tmp; tmp = tmp->next)
  {
    Py_XDECREF((PyObject*)tmp->data);
  }
  g_slist_free(objlist);

  if(!result)
  {
    g_warning("function (%s) didn't execute correctly", function);
    PyErr_Print();
    return FALSE;
  }

  return TRUE;
}

void python_init(void)
{
  Element* element;

  python_start();

  element = g_new0(Element, 1);
  element->render_func = python_render;
  element->tag = "python";
  element_register (element);

  lang_register("python", python_function_execute);
  lang_register("py", python_function_execute);
}

#endif USE_PYTHON










