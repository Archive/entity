
#include "pyembed.h"
#ifdef USE_PYTHON

/*****************************************************************************
 * RUN EMBEDDED CODE-STRINGS 
 * handles debugging, module (re)loading, namespaces, output conversions;
 * pbd.runeval returns a value: "eval(expr + '\n', globals, locals)";
 * pdb.run is just a statement: "exec cmd + '\n' in globals, locals"
 *****************************************************************************/


PyObject *
Debug_Codestr(StringModes mode, char *codestring, PyObject *moddict)
{
    PyObject *pdbfunc, *presult;
    char *pdbname = (mode == PY_EXPRESSION ? "runeval" : "run");

    pdbfunc = Load_Attribute("pdb", pdbname);   /* "pdb.run(stmt, d, d)" */
    if (pdbfunc == NULL)                        /* get func from pdb.py  */
        return NULL;                            /* and call it with args */
    else {                
        presult =  
            PyEval_CallFunction(pdbfunc, "(sOO)", codestring, moddict, moddict);
        Py_DECREF(pdbfunc);
        return presult;                         /* or use Run_Function.. */
    }
}


int
Run_Codestr(StringModes mode, char *code,      /* expr or stmt string */
            char *modname,                     /* loads module if needed */
            char *resfmt, void *cresult)       /* converts expr result to C */
{
    int parse_mode;                            /* "eval(code, d, d)", or */
    PyObject *module, *dict, *presult;         /* "exec code in d, d" */

    module = Load_Module(modname);             /* get module, init python */
    if (module == NULL)                        /* not incref'd */
        return -1;
    dict = PyModule_GetDict(module);           /* get dict namespace */
    if (dict == NULL)                          /* not incref'd */
        return -1;

    parse_mode = (mode == PY_EXPRESSION ? eval_input : file_input);
    if (PY_DEBUG) 
        presult = Debug_Codestr(mode, code, dict);            /* run in pdb */
    else 
        presult = PyRun_String(code, parse_mode, dict, dict); /* eval direct */
                                                              /* increfs res */
    if (mode == PY_STATEMENT) {
        int result = (presult == NULL? -1 : 0);          /* stmt: 'None' */
        Py_XDECREF(presult);                             /* ignore result */
        return result;
    }
    return Convert_Result(presult, resfmt, cresult);     /* expr val to C */
}

#endif /*USE_PYTHON*/
