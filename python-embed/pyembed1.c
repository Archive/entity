
#include "pyembed.h"

#ifdef USE_PYTHON

/*****************************************************************************
 * GET/SET MODULE-LEVEL (GLOBAL) PYTHON VARIABLES BY NAME
 * handles module (re)loading, input/output conversions;
 * useful for passing data to/from codestrings: no arguments
 * ex: make/load module + set-inputs + run-codestring + get-outputs
 *****************************************************************************/

#include <stdarg.h>

int
Convert_Result(PyObject *presult, char *resFormat, void *resTarget)
{
    if (presult == NULL)            /* error when run? */
        return -1;
    if (resTarget == NULL) {        /* NULL: ignore result */
        Py_DECREF(presult);         /* procedures return None */
        return 0;
    }
    if (! PyArg_Parse(presult, resFormat, resTarget)) { /* convert Python->C */
        Py_DECREF(presult);                             /* may not be a tuple */
        return -1;                                      /* error in convert? */
    }
    if (strcmp(resFormat, "O") != 0)       /* free object unless passed-out */
        Py_DECREF(presult);
    return 0;                              /* 0=success, -1=failure */
}                                          /* if 0: result in *resTarget  */


int
Get_Global(char *modname, char *varname, char *resfmt, void *cresult)
{
    PyObject *var;                                /* "x = modname.varname" */
    var =  Load_Attribute(modname, varname);      /* var is incref'd */
    return Convert_Result(var, resfmt, cresult);  /* convert output to C form */
}


int
Set_Global(char *modname, char *varname, char *valfmt, ... /* cval(s) */) 
{
    int result;
    PyObject *module, *val;                     /* "modname.varname = val" */
    va_list cvals;
    va_start(cvals, valfmt);                    /* C args after valfmt */

    module = Load_Module(modname);              /* get/load module */
    if (module == NULL) 
        return -1;
    val = Py_VaBuildValue(valfmt, cvals);       /* convert input to Python */
    va_end(cvals);
    if (val == NULL) 
        return -1;
    result = PyObject_SetAttrString(module, varname, val); 
    Py_DECREF(val);                             /* set global module var */
    return result;                              /* decref val: var owns it */
}                                               /* 0=success, varname set */

#endif USE_PYTHON
