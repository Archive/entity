
#include "pyembed.h"
#ifdef USE_PYTHON

/*****************************************************************************
 * RUN EMBEDDED MODULE-FUNCTIONS 
 * handles module (re)import, debugging, input/output conversions;  
 * note: also useful for calling classes at the top-level of a module 
 * to make Python instances: use class-name and 'O' result convert-code;
 * use argfmt="()" for no args, cresult='NULL' for no result (procedure);
 *****************************************************************************/

#include <stdarg.h>

PyObject *
Debug_Function(PyObject *func, PyObject *args)
{
    int oops, res;
    PyObject *presult;

    /* much magic: expand tuple at front */
    oops = _PyTuple_Resize(&args, (1 + PyTuple_Size(args)), 1); 
    oops |= PyTuple_SetItem(args, 0, func);   
    if (oops) 
        return NULL;                        /* "args = (funcobj,) + (arg,..)" */

    res = Run_Function(                     /* "pdb.runcall(funcobj, arg,..)" */
                 "pdb",  "runcall",         /* recursive run_function */
                 "O",    &presult,
                 "O",     args);            /* args already is a tuple */
    return (res != 0) ? NULL : presult;     /* errors in run_function? */
}                                           /* presult not yet decref'd */


int
Run_Function(char *modname, char *funcname,          /* load from module */
             char *resfmt,  void *cresult,           /* convert to c/c++ */
             char *argfmt,  ... /* arg, arg... */ )  /* convert to python */
{
    PyObject *func, *args, *presult;
    va_list argslist;
    va_start(argslist, argfmt);                   /* "modname.funcname(args)" */

    func = Load_Attribute(modname, funcname);     /* reload?; incref'd */
    if (func == NULL)                             /* func may be a class */
        return -1;
    args = Py_VaBuildValue(argfmt, argslist);     /* convert args to python */
    if (args == NULL) {                           /* args incref'd */
        Py_DECREF(func);
        return -1;
    }
    if (PY_DEBUG && strcmp(modname, "pdb") != 0)   /* debug this call? */
        presult = Debug_Function(func, args);      /* run in pdb; incref'd */
    else
        presult = PyEval_CallObject(func, args);   /* run function; incref'd */

    Py_DECREF(func);
    Py_DECREF(args);                                  /* result may be None */
    return Convert_Result(presult, resfmt, cresult);  /* convert result to C */
}

#endif /*USE_PYTHON*/
