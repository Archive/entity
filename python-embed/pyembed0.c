
#include "pyembed.h"

#ifdef USE_PYTHON

/*****************************************************************************
 * MODULE INTERFACE 
 * make/import/reload a python module by name
 *****************************************************************************/


int PY_RELOAD = 0;    /* reload modules dynamically? */
int PY_DEBUG  = 0;    /* debug embedded code with pdb? */

char *Init(char *modname) {
    Py_Initialize();                               /* init python if needed */
    return modname == NULL? "__main__" : modname;  /* default to '__main__' */     
}


int
Make_Dummy_Module(char *modname)     /* namespace for strings: if no file */
{                                    /* instead of sharing __main__ for all */
    PyObject *module, *dict;         /* note: __main__ is created in py_init */
    Py_Initialize();
    module = PyImport_AddModule(modname);    /* fetch or make, no load */
    if (module == NULL)                      /* module not incref'd */
        return -1;                  
    else {                                            /* module.__dict__ */
        dict = PyModule_GetDict(module);              /* ['__dummy__'] = None */
        PyDict_SetItemString(dict, "__dummy__", Py_None); 
        PyDict_SetItemString(dict, "__builtins__", PyEval_GetBuiltins());
        return 0;
    }
}


PyObject *
Load_Module(char *modname)            /* returns module object */
{
    /* 4 cases...
     * 1) module "__main__" has no file, and not prebuilt: fetch or make
     * 2) dummy modules have no files: don't try to reload them
     * 3) reload set and already loaded (on sys.modules): "reload()" before use
     * 4) not loaded yet, or loaded but reload off: "import" to fetch or load */

    PyObject *module, *result;                  
    modname = Init(modname);                        /* default to __main__ */
    module  = PyDict_GetItemString(                 /* in sys.modules dict? */
                PyImport_GetModuleDict(), modname);

    if (strcmp(modname, "__main__") == 0)           /* no file */
        return PyImport_AddModule(modname);         /* not incref'd */
    else
    if (module != NULL &&                                /* dummy: no file */
        PyDict_GetItemString(PyModule_GetDict(module), "__dummy__")) 
        return module;                                   /* not incref'd */
    else
    if (PY_RELOAD && module != NULL && PyModule_Check(module)) {
        module = PyImport_ReloadModule(module);   /* reload-file/run-code */
        Py_XDECREF(module);                       /* still on sys.modules */
        return module;                            /* not incref'd */
    }
    else {  
        module = PyImport_ImportModule(modname);  /* fetch or load module */
        Py_XDECREF(module);                       /* still on sys.modules */
        return module;                            /* not incref'd */
    }
}


PyObject *
Load_Attribute(char *modname, char *attrname)
{
    PyObject *module;                         /* fetch "module.attr" */
    modname = Init(modname);                  /* use before PyEval_CallObject */
    module  = Load_Module(modname);           /* not incref'd, may reload */
    if (module == NULL)
        return NULL;
    return PyObject_GetAttrString(module, attrname);  /* func, class, var,.. */
}                                                     /* caller must xdecref */


/* extra ops */
int 
Run_Command_Line(char *prompt)
{
    int res;               /* interact with python, in "__main__" */
    Py_Initialize();       /* in the program's "stdio" window     */
    if (prompt != NULL)
        printf("[%s <ctrl-d exits>]\n", prompt);
    res = PyRun_InteractiveLoop(stdin, "<stdin>");
    return res;
}

#endif /*USE_PYTHON*/
