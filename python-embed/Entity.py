import os
import sys
import regex
import string
from CEntity import *
from signal import *


def my_exit (sig, other):
	print 'exiting now'
	sys.exit(1)

signal(SIGINT, my_exit)

tag_name = regex.compile('^.*\..*$')

class enode:
	def __init__(self, path='//'):
		self.path = path

	def __setitem__(self, key, value):
		entity_set_attr(self.path, key, value)

	def __getitem__(self, key):
		value = entity_get_attr(self.path, key)
		return value


	def efind(self, path, name):
		return enode( entity_find(path, name) )

	def name(self):
		return self['name']

	def attrib(self, attribs ):
		if len(attribs) > 1:
			for key,value in attribs:
				print key + " " + value + " " + self.path
				self[key] = value
		else:
			print attribs[0]
			return self[attribs[0]]

	def get_data(self):
		return( entity_get_data(self.path) )

	def set_data(self, data=''):
		entity_set_data(self.path, data)

	def find_child(self, name=''):
		return( enode( entity_find(self.path, name) ) )

	def find_parent_type(self, type):
		return enode( entity_find_parent(self.path, type) )
	
	def children(self):
		pathlist = entity_ls(self.path)
		for x in pathlist:
			nodelist.append( enode(x) )
		return nodelist

	def append_data(self, data):
		self.set_data(self.get_data() + data)

	def get_xml(self):
		return entity_get_xml(self.path)
	
	def append_xml(self, xml=''):
		entity_set_xml(self.path, xml)

	def new_child(self, name):
		tag_name.match(line)
		(rtag, rname) = tag_name.group(1,2)
		return enode( entity_new_child(self.path, rtag, rname) )
	
	def is_attr_true(self, attr=''):
		return entity_is_attr_true(self.path, attr)

	def object(self):
		return enode( entity_find_parent(self.path, "object") )

	def parent(self):
		return enode( entity_get_parent(self.path) )

	def which_attribs(self):
		return entity_get_set_attr(self.path)

	def all_attribs(self):
		return entity_get_all_attr(self.path)
	
	def attrib_info(self, attr):
		return entity_attr_info(self.path, attr)

	def delete(self):
		entity_delete_node(self.path)

	def delete_children(self):
		entity_delete_children(self.path)

	def write(self, data=''):
		entity_ioprint(self.path, data)

