package Entity;

print ("Loading Entity.pl\n");

#/*-- 
#
# Entity::require (filename) 
# 
# Loads a file into the current namespace.
# This will load relative to the directory the current <app> was loaded from.
# 
# Note that you should NOT use the perl require() function, as it will only
# load a file once.. even if it is needed in multiple namespaces.
# This will break Entity applications when they try to load the perl file
# for the second time.
#
# filename: The name of the pl file to load.
#*/

sub require
{
    my ($file) = @_;
    my $obj;
    my $ret;
    my $filename;
    
    $obj = Enode::new ('Enode', "/");
    
    if ($file =~ /^\//) {
	$filename = $file;
    } else {
	my $parent_file = $obj->attrib ("__filename");
	$parent_file =~ s/(.*)\/.*/$1/;
	
	$filename = "$parent_file/$file";
    }
    
    my $ret = open (REQUIREFILE, $filename);

    if (!$ret) {
	print ("Error opening '$filename' for evaluation\n");
	return (0);
    }

    print ("PERL: loading $filename\n");
    
    my @code = <REQUIREFILE>;
    my $namespace = $obj->attrib ("__perl-namespace");
    #print ("Loading into $namespace\n");
    eval ("package $namespace; @code");
    print $@;
    close (REQUIREFILE);
    return (1);
}

# Rest is in perl-embed.c

1;

