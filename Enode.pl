package Enode;


# This lets us printout and veiw $node as the $node->{path}
# as long as we are using it as a string.
use overload 
    q("") => \&path;

# Used internally to export methods to other namespaces.
sub import
{
    my ($caller_package) = caller;

    foreach $method (@_) {
        *{"${caller_package}::${method}"} = *{"Enode::${method}"};
    }
}

# Create a new node given a path (this one is exported)
sub enode
{
    my ($path) = @_;
    return (Enode::new ('Enode', $path));
}

# Create a new node after finding (also exported)
sub efind
{
    my ($start_path, $name) = @_;
    my $fullpath = Entity::node_find_fancy ($start_path);
    my $path = Entity::node_find_child ($fullpath, $name);
    return (Enode::new('Enode', $path));
}

# Create a new enode object.
sub new
{
    my ($class, $path) = @_;
    my ($mytype, $myname);
    my $fullpath = Entity::node_find_fancy ($path);
    
    if (!defined ($fullpath)) {
	print ("Error creating new enode, path $path not found.\n");
	return (undef);
    }
    
    my $self = {};

    bless ($self, $class);
    
    $self->{path} = $fullpath;
    return ($self);
}

# Returns node type
sub type
{
    my $self = shift;
    my $mytype = (split m![/.]!, $self->{path})[-2];
    return ($mytype);
}

# Returns node basename
sub basename
{
    my ($self) = @_;
    
    my ($mytype, $myname) = (split m![/.]!, $self->{path})[-2..-1];
    return ($mytype . "." . $myname);
}

# Returns path of node (common interface)
sub path
{
    my $self = shift;
    return ($self->{path});
}

# Get/set attributes for a node
sub attrib
{
    my $self = shift;

    my ($attribute, $value, $i);
    
    # I _think_ this way is more efficient as it avoids excessive copies.
    # We are already copying all the attributes in when we call the method...
    
    if (@_ == 1) {
	$attribute = shift;
	return (Entity::node_get_attrib ($self->{path}, $attribute));
    } else {
	for ($i = 0; $i < @_; $i += 2) {
	    $attribute = $_[$i];
	    $value = $_[$i + 1];
	    
	    # TODO: one special case.. changing the name of the node changes
	    # its reference, so we have to update that
	    # if ($attribute eq "name")
	    Entity::node_set_attrib ($self->{path}, $attribute, $value);
	}
    }
}

# Set data of node
# We still need a data_append() and data_insert().
sub set_data
{
    my ($self, $data) = @_;
    Entity::node_set_data ($self->{path}, $data);
}

# Append data to a node (this needs to be fixed, its inefficient)
sub append_data
{
    my ($self, $data) = @_;
    #print ("appending $data to $self->{path}\n");
    my $temp = Entity::node_get_data ($self->{path});
    Entity::node_set_data ($self->{path}, $temp . $data);
}

# Get data of a node
sub get_data
{
    my $self = shift;
    return (Entity::node_get_data ($self->{path}));
}

# We'll make them all find_* I think..

# Find a child node given its name
sub find_child
{
    my ($self, $name) = @_; 
    my $foundpath = Entity::node_find_child ($self->{path}, $name);
    #print "found $foundpath\n";
    return new Enode ($foundpath);
}

# Return a list of attributes of various child nodes
sub find_children_attribs
{
    my $node = shift;
    my (@values, $name, $attrib);
    
    for ($i = 0; $i < @_; $i += 2) {
	$name = $_[$i];
	$attrib = $_[$i + 1];
	push @values, $node->find_child($name)->attrib($attrib);
    }
    
    return @values;
}

# Find a parent by type or by full name.  Either will work, just use it as eg:
# $node->find_parent ("object.foo") to find by name, or
# $node->find_parent ("object") to find by type.
sub find_parent
{
    my $self = shift;
    return enode( Entity::node_find_parent ($self->{path}, shift) );
}

#sub find_advanced
#{
#}

# Returns a list of all children (as enodes)
sub children
{
    my $self = shift;
    my (@nodes, $path);

    my @list = Entity::node_children($self->{path});
    foreach $path (@list) {
	push @nodes, enode ($path);
    }
    
    return @nodes;
}

# Returns xml string representing the tree
sub get_xml
{
    my $self = shift;
    return Entity::node_get_xml ($self->{path});
}

# For appending XML to a node (will be rendered in place).
sub append_xml
{
    my $self = shift;
    my $xml = shift;

    my %hash = (@_);
    
    if ( scalar (@_) ) {
        my $code .= "{ package EnodeXML;\n";
        while ( my ($key, $value) = each (%hash) )
        {
          $code .= '$'."$key = <<XMLPERL;\n$value\nXMLPERL\n".
                   "chop(".'$'."$key);\n";
        }
        $code .= "<<XMLPERL\n$xml\nXMLPERL\n} ";
        my $new_xml = eval $code;
        chop($new_xml);
        Entity::node_append_xml ($self->{path}, $new_xml);
    }
    else {
        Entity::node_append_xml ($self->{path}, $xml);
    }
}

# Programmatically create a new child XML Node.
sub new_child
{
    my $self = shift;
    my $name = shift;
    
    my ($rtag, $rname) = split('\.', $name);
    #print "$self, $name => ( $rtag . $rname ) \n"; 

    my $node = enode(Entity::node_new_child($self->{path}, $rtag, $rname, @_));

    #print "@_\n";
    #print "new = $node\n";

    ### Doesn't work right need to have there attribs set when renderered.
    #if (scalar(@_)) {
    #  $node->attrib(@_);
    #}

    return $node;
}

# Returns true/false for an attribute.
sub attrib_is_true
{
    my $self = shift;
    return Entity::node_attrib_is_true ($self->{path}, shift);
}

# Returns the current app
sub object
{
    my $self = shift;
    return enode(Entity::node_find_parent ($self->{path}, "object") );
}

# Returns the nodes parent
sub parent
{
    my $self = shift;
    my $parent = Entity::node_parent ($self->{path});

    return enode ($parent);
}

# Returns all attributes set in this node
sub get_set_attribs
{
    my $self = shift;
    return Entity::node_get_set_attribs ($self->{path});
}

sub set_all_attribs
{
    my $self = shift;
    my $setter = shift;

    #FIXME:  get the set="" tag out of the attrib.

    foreach my $attr ($self->get_set_attribs())
    {
        $setter->($self, $attr, $self->attrib($attr) );
    }
}

# Returns all supported attributes in this node
sub supported_attribs
{
    my $self = shift;
    return Entity::node_supported_attribs ($self->{path});
}

# Returns information about an attribute
sub attrib_info
{
    my $self = shift;
    return Entity::node_attrib_info ($self->{path}, shift);
}

# Deletes node and all children
sub delete
{
    my $self = shift;
    Entity::node_delete_tree ($self->{path});
}

# Deletes all children of this node.
sub delete_children
{
    my $self = shift;
    Entity::node_delete_tree_in ($self->{path});
}

# Call a perl function in another namespace (that of the enode)
sub call
{
    # This needs to be shifted off..
    my $self = shift;
    my $method = shift;
    my $namespace_path;
    
    if ($self->type () eq "object") {
	$namespace_path = $self->{path};
    } else {
	$namespace_path = Entity::node_find_parent ($self->{path}, "object");
    }
    my $namespace = Entity::node_get_attrib ($namespace_path, "__perl-namespace");
    
    # Jump to new object namespace
    my $old_namespace = Entity::get_reference_node ();
    Entity::set_reference_node ($self->{path});
    &{"${namespace}::${method}"} (@_);
    
    # And return to old namespace on completion.
    Entity::set_reference_node ($old_namespace);
}

# For io renderers, send data.
sub write
{
   my ($self, $data) = @_;
 
   Entity::node_ioprint ($self->{path}, $data);
}


return 1;

