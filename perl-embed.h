#ifndef __PERL_H__
#define __PERL_H__

void execute_perl_code (XML_Node * calling_node, gchar * code);

void
execute_perl_function (XML_Node * calling_node, gchar * function,
		       GSList * args);


void perl_init (void);


#endif /* __PERL_H__ */
