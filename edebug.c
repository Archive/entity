#include "config.h"
#include <glib.h>
#include <stdlib.h>
#include <stdarg.h>

static gint entity_debug = FALSE;
static gchar **debug_domains = NULL;

static gint			/*Freed MW */
edebug_check_domain (gchar * domain)
{
  gint i;

  if ((debug_domains[0]) && (g_str_equal (debug_domains[0], "all")))
    return (TRUE);

  for (i = 0; debug_domains[i] != NULL; i++)
    {
      if (g_str_equal (debug_domains[i], domain))
	return (TRUE);
    }

  return (FALSE);
}

void
edebug_enable (gchar * domains)
{
  entity_debug = TRUE;

  /*Not freed, called once per startup leak, but small. MW */
  debug_domains = g_strsplit (domains, ",", 255);
}

void				/*Freed MW */
edebug (gchar * domain, gchar * format, ...)
{
  va_list args;
  gchar *string;
  gint should_log;

  if (entity_debug == FALSE)
    return;

  g_return_if_fail (format != NULL);
  g_return_if_fail (domain != NULL);

  should_log = edebug_check_domain (domain);

  if (!should_log)
    return;

  va_start (args, format);
  string = g_strdup_vprintf (format, args);
  va_end (args);

  g_print ("Entity-%s: %s\n", domain, string);

  g_free (string);
}
