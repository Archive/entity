#!/usr/local/bin/entity

<object>
 <window border = "15" 
   position = "center"
   ondelete = "Entity::exit"
   title = "Some Cool App"
   width="300" height="400"
   name="windowone">
   <perl>
     sub scrolltest{
       my  $node = @_;
       ##print "$path\n";
       # $path = Entity::find ("/", "scrollwindow.sc1");
       $sw = enode("scrollwindow.sc1");
       
       # Entity::set_attr ($path, "x-scroll", "100");
     }
     sub xscrolled{
       my ( $node ) = @_;
       #print "$path\n";
       # $text = Entity::get_attr ($path, "x-scroll");
       $text = $node->attrib("x-scroll");
       print "x = $text\n";
     }
     sub yscrolled{
       my ( $node ) = @_;
       #print "$path\n";
       # $text = Entity::get_attr ($path, "y-scroll");
       $text = $node->attrib("y-scroll");
       print "y = $text\n";
     }
   </perl>

   <scrollwindow name="sc1" 
                 onx-scrolled="xscrolled"
                 ony-scrolled="yscrolled"
                 expand="true">
     <valign width="500" height="700">
     </valign>               
   </scrollwindow>


   <timer interval="200" action="scrolltest"/>
 </window>
</object>
