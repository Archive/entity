#!/usr/local/bin/entity

<object>

  <object>
    <window ondelete="entity:exit">
      <button label="nothing"/>
    </window>
  </object>
  
  <object>
    <window ondelete="entity:hide">
      <button label="hide window" onclick="entity:hide_window"/>
    </window>
  </object>

  <object>
    <window ondelete="entity:show_args">
      <button label="showargs to buttonpress" onbuttonpress="entity:show_args"/>
    </window>
  </object>

  <object>
    <window ondelete="entity:exit">
      <button label="delete me" onclick="entity:delete"/>
      <button label="nothing"/>
    </window>
  </object>

</object>
