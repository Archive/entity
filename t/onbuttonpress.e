#!/usr/local/bin/entity

<object>
  <window>
    <button label="nothing"/>
    <button label="delete me (onbuttonpress)" onbuttonpress="delete_me"/>
    <button label="delete me" onclick="delete_me"/>
  </window>
  <perl><![CDATA[
  sub delete_me
  {
    (shift)->delete();
  }
  ]]></perl>
</object>
