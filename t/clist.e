
<object>
  <window title="hi" width="400" height="300" ondelete="entity:exit">
   <button label="Set width 0" onclick="dowidth" />
   <button label="Set width 30" onclick="dowidth30" />
   <button label="Set width 60" onclick="dowidth60" />
   <hpane expand="true">
     <clist name="list" cols="3" maxcols="5" width="150" 
   	  selection-sytle="browse" expand="true" onselect="entity:show_args">
       <cl-row>
	  <cl-header title="Col 1"/>
	  <cl-header name="deleteme" width="20" title="Col 2"/>
	  <cl-header title="Col 3"/>
       </cl-row>
       <cl-row><string text="9r1c1"/></cl-row>
       <cl-row><string text="8r2c1"/><string text="r2c2"/></cl-row>
       <cl-row>
         <string text="r2c1"/><string text="r2c2"/><string text="r2c3"/>
       </cl-row>
     </clist>
     <text/>
   </hpane>
   <perl><![CDATA[
   my $clist = enode("clist.list");
   my $xml;

   $clist->attrib("frozen" => "true");
   for($i = 0; $i< 5000; $i++)
   {

   $xml = qq!<cl-row>
   		<string text="r2c1"/><string text="r2c2"/><string text="r2c3"/>
		</cl-row>
		!;
   #$xml x= 5000;
   $clist->append_xml($xml);
   }
   $clist->attrib("frozen" => "false");

   #enode("cl-header.deleteme")->delete();

   sub dowidth   { enode("cl-header.deleteme")->attrib("width" =>  "0"); }

   sub dowidth30 { enode("cl-header.deleteme")->attrib("width" => "30"); }

   sub dowidth60 { enode("cl-header.deleteme")->attrib("width" => "60"); }

   ]]></perl>
  </window>
</object>

<!-- 
   <cl-row visible="false"> 
     Hides this row. If it is the row with the <cl-header> 
           tags, the buttons a hidden. 
   <cl-header label="title" visible="false"/>  
      title - sets the title for this column.
      visible - sets the visiblity for this entire row.
      onclick - function to call when button clicked.
              - or if not set then titles cannot be selected.
      sort -  (de|a)ccend[:(no)case]
      sortattrib - attrib to sort on, text attrib by default.
      align - left|center|right|justify
-->
