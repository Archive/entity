#!/usr/local/bin/entity

<object title="Clock">
  <window ondelete="perl:Entity::exit" title="Clock" >
    <valign>
      <!-- Insure node lookups are working right by supplying fake
           label in different object -->
      <label name="time" text="fake! boo!"/>
      <object name="clock" dragable="true" expand="false">
	<label name="time" font="10x20" text = "00:00:00"></label>
        <perl>
	    sub update_clock
	      {
                my $node = shift;
		my $timelabel = enode("label.time");
	        print ("timelabel is $timelabel\n");
		
		my ($sec, $min, $hour, $mday) = localtime (time);
	        my $str = sprintf ("%02d:%02d:%02d", $hour, $min, $sec);

		$timelabel->attrib ("text" => $str);
	      }
	</perl>
      </object>
    </valign>
    <timer name="clock" interval="1000" action="call_update_clock"/>
    <perl>
      sub call_update_clock
        {
	  my $node = enode ("object.clock");
	  print ("object.clock path is $node\n");
	  $node->call ("update_clock", $node);
	  $node = enode ("label.time");
	  print ("local node $node\n");
	}
    </perl>
  </window>
</object>
