#!/usr/local/bin/entity

<object>
  <window ondelete = "Entity::exit" title = "Graph Test" width = "400" height = "300">
    <object expand = "true" fill = "true" dragable="true">
      <graph name="mygraph" expand = "true" fill="true">
        <graph-point x="1" y="30" size="10" type = "point" color="#123445"/>
	<graph-point x="2" y="20" size="10" type = "point" color="#123445"/>
	<graph-point x="3" y="10.4" size="10" type = "point" color="#123445"/>
	<graph-point name = "foo1" x="4" y="10" size="5" 
				type = "bar" color="#349090"/>
	<graph-point name = "foo2" x="5" y="20" size="10" 
				type = "bar" color="#342050"/>
	<graph-point name = "foo3" x="6" y="30" size="15" 
				type = "bar" color="#543090"/>
	<graph-point name = "foo4" x="7" y="40" size="20" 
				type = "bar" color="#549090"/>
      </graph>
      <perl>
        <![CDATA[
	
	$value = 2.0;
	$color = 1;
	$num = .001;
	sub set_graph
	  {

	    $value += 1.5;
	    
	    $color += 2;
	    if ($color > 99) {
	      $color = 0;
	    }
	    
	    $color_str = sprintf ("#2030%02d", $color);
	    
	    enode("graph-point.foo1")->attrib("y" => $value);
	    enode("graph-point.foo2")->attrib("y" => $value + 3);
	    enode("graph-point.foo3")->attrib("y" => $value + 5);
	    enode("graph-point.foo4")->attrib("y" => $value + 7);
	    
	    enode("graph-point.foo4")->attrib("color" => $color_str);
	    $point = '<graph-point x="$num" y="$num2"/>';
	    $num += $num/80;
	    $num2 += .1;

	    enode("graph.mygraph")->append_xml($point,  "num2" => $num2, 
							"num" => $num);

	    if ($value > 40) {
	      $value = 2.0
	    }
	  }
        ]]>
      </perl>
      <timer interval = "200" action = "set_graph"/>
    </object>
  </window>
</object>
