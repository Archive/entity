#!/usr/local/bin/entity

<object>
 <window border = "15" 
   position = "center"
   ondelete = "Entity::exit"
   title = "Some Cool App"
   name="windowone">

   <valign>
     <checkbox name = "mycheckbox" value="TRUE" ontoggle="checkboxtest">
	<label text="item 1"/>
     </checkbox>
   </valign>               

   <perl>
     sub checkboxtest{
       my ($node) = @_;
       print "$node\n";
       $isclicked = $node->attrib_is_true("selected");
       if($isclicked)
       {
         print "checkbox is set.\n";
       }
       else
       {
         print "checkbox is NOT set.\n";
       }
     }
   </perl>

 </window>
</object>
