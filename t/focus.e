#!/usr/local/bin/entity

<object>
  <window>
    <button name="me" label="me" onfocus="got_focus"/>
    <button name="you" label="you"/>

    <button label="focus you" onclick="focus_you"/>
  </window>

  <timer interval="10000" action="who_has_focus" />
  <timer interval="3600" action="focus_you" />

  <perl><![CDATA[
  sub who_has_focus
  {
    my $me = enode("button.me")->attrib("focused");
    my $you = enode("button.you")->attrib("focused");

    print "me focused? $me\n";
    print "you focused? $you\n";
  }
  sub focus_you
  {
    print "going to focus 'you'\n";
    enode("button.you")->attrib("focused" => "true");
  }
  sub got_focus
  {
    print shift, " got focus\n";
  }
  ]]></perl>
</object>

