#!/usr/local/bin/entity

<object>
 <window border = "15" 
   position = "center"
   ondelete = "Entity::exit"
   title = "Some Cool App"
   name="windowone">

   <halign>
     <dropdown name="dd" text="2" onenter="labelfun">
       <string text="1"/>
       <string text="2"/>
       <string text="3"/>
     </dropdown>
     <button label="go" onclick="dogo"/>
   </halign>               

<!--
<python>
def labelfun(node):
	print node['text']
	print node.path
def dogo(node):
	ddnode = enode('dropdown.dd')
	print ddnode['text']
</python>
-->

 <perl>
 sub labelfun
 {
   $node = shift;
   print $node,"\n";
 }
 sub dogo
 {
   $node = enode("dropdown.dd");
   print $node,"\n";
 }
 </perl>

 </window>
</object>
