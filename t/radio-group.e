#!/usr/local/bin/entity

<object>
  <window name="main">
  <button label="add" onclick="add_widgets"/>
  <button label="remove" onclick="remove_widgets"/>
  <valign name="main">
  </valign>

  <perl>
  <![CDATA[
  sub add_widgets
  {
    my $node = shift;
    my $valign = enode("valign.main");
    my $xml;
    $xml .= "<halign><radio-group><radio/><radio/></radio-group></halign>" x 5;
    
    $valign->append_xml($xml);
    print $node,"\n";
  }
  sub remove_widgets
  {
    my $valign = enode("valign.main"); 
    $valign->delete_children();
  }
  
  ]]>
  </perl>
  </window>
  
</object>
