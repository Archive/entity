#!/usr/local/bin/entity
<object>

  <renderer 
	tag="sql"
	lang="perl"
	onrender="rendsql_renderer"
	ondestroy="rendsql_destroy"
	onparent="rendsql_parenter" >

    <attrib
	name="host"
	description="Host where the DB resides."
	value_desc="hostname"
	values="localhost,hostname,ip"
	onset="rendsql_host_attr_set" />

    <attrib
	name="db"
	description="The db to connect to."
	value_desc="string"
	onset="rendsql_db_attr_set" />
	
    <perl>
    sub rendsql_renderer
    {
      my $node = shift;
      print $node,"\n";
    }
	
    </perl>
    <perl>
    sub rendsql_renderer
    {
      my $node = shift;
      print "parenting - $node -\n";
    }

    sub rendsql_parenter
    {
      my $parent = shift;
      my $child = shift;
      print $parent,"\n";
      print $child,"\n";
    }

    sub rendsql_host_attr_set
    {
      my $node = shift;
      my $attr = shift;
      my $value = shift;
      print "$node: $attr => $value\n";
    }

    sub rendsql_host_attr_set
    {
      my $node = shift;
      my $attr = shift;
      my $value = shift;
      print "$node: $attr => $value\n";
    }

    </perl>
  </renderer>

  <window border="3">
   <button label="doit" onclick="doit"/>
  </window>
  <perl>
  sub doit
  {
    my $node = shift;
    my $thunder = enode("sql.db");
    $thunder->attrib("host" => "thunder");
  }
  </perl>

  <sql name="db">
    <button/>
  </sql>

</object>

