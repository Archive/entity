#!/usr/local/bin/entity

<object title="Clock">
  <window ondelete="perl:Entity::exit" title="Clock" >
    <valign>
      <object title="clock" name="clock" dragable="true" expand="false">
	<label name="time" font="10x20" text = "00:00:00"></label>
	<timer interval = "1000" action="update_clock"></timer>
        <perl>
	    sub update_clock
	      {
                my $node = shift;
		my $timelabel = enode("label.time");
		my $found = efind("/", "object.clock");
		$num;
		# aasdfg( - 3;  # this line is for testing the reporting of syntax errors
	        my ($sec, $min, $hour, $mday) = localtime (time);
	        my $str = sprintf ("%02d:%02d:%02d", $hour, $min, $sec);

		$timelabel->attrib("text" => $str);

		print "node = $node\ntimelabel = $timelabel\n";

		enode("label.time")->attrib(text => $str);


		$num++;
		$found->new_child("button.new-$num", 
				  "label" => "new");

		print $timelabel->attrib("text"), "\n";

	      }
	    update_clock ();
	</perl>
      </object>
    </valign>
  </window>
</object>
