#!/usr/local/bin/entity

<object>
 <window border = "15" 
   position = "center"
   ondelete = "Entity::exit"
   title = "Some Cool App"
   name="windowone">

   <valign>
     <spinner name = "myspinner" value="1" min="1" max="10" 
     	places="2" wrap="true" onenter="spintest"/>
   </valign>               

   <perl>
     sub spintest{
       my ( $node ) = @_;
       print "$node\n";
       $text = $node->attrib("value");
       print "$text\n";
     }
   </perl>

 </window>
</object>
