#!/usr/local/bin/entity

<object>
  <window name="main">
  <button label="add" onclick="add_widgets"/>
  <button label="remove" onclick="remove_widgets"/>
  <button label="remove one" onclick="remove_one"/>
  <radio-group>
    <valign name="main">
      <radio name="one" label="one"/>
      <radio name="two" label="two"/>
    </valign>
  </radio-group>

  <perl>
  <![CDATA[
  sub add_widgets
  {
    my $node = shift;
    my $valign = enode("valign.main");
    my $xml;
    $xml .= "<radio/>" x 5;
    
    $valign->append_xml($xml);
    print $node,"\n";
  }
  sub remove_widgets
  {
    my $valign = enode("valign.main"); 
    $valign->delete_children();
  }
  sub remove_one
  {
    my $radio = enode("radio.one"); 
    $radio->delete();
  }
  
  ]]>
  </perl>
  </window>
  
</object>
