#! /usr/local/bin/entity

<object>
  <perl>
  sub tellme
  {
    print "in tellme: ", shift, "\n";
  }
  sub tell_one
  {
    print "in tell_one\n";
  }

  </perl>

  <window name="main">
  <button label="add" onclick="add_widgets"/>
  <button label="remove" onclick="remove_widgets"/>
  <button label="remove one" onclick="remove_one"/>
  <button label="select one" onclick="select_one"/>
  <notebook name="main" onselect="tellme">
    <notepage name="one" label="one" onselect="tell_one"/>
  </notebook>

  <perl><![CDATA[
  sub add_widgets
  {
    my $node = shift;
    my $wnode = enode("notebook.main");
    my $xml;
    #$xml .= "<notepage label=\"hi\"><halign><button height=\"100\"/></halign></notepage>" x 5;
    
    # $wnode->append_xml($xml);
    $wnode->new_child("notepage.mine", "title" => "other");
    print $node,"\n";
  }
  sub remove_widgets
  {
    my $valign = enode("notebook.main"); 
    $valign->delete_children();
  }
  sub remove_one
  {
    my $one = enode("notepage.one"); 
    $one->delete();
  }
  sub select_one
  {
    my $one = enode("notepage.one"); 
    $one->attrib("selected" => "true");
  }

  ]]></perl>
  </window>
  
</object>
