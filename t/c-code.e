#!/usr/local/bin/entity

<object>
 <window border = "15" 
   position = "center"
   ondelete = "Entity::exit"
   title = "Some Cool App"
   name="windowone">


<object dragable = "true" expand = "false">
   <valign>
     <button name = "do100" onclick="c:count_to_100">
       <label text="test 100"/>
     </button>

     <button name = "do50" onclick="c:count_to_50">
       <label text="test 50"/>
     </button>

     <button name = "docount" count="1000" onclick="c:count_to_count">
       <label text="test 1000"/>
     </button>

     <button name = "testargs_b" data="pass me?" onclick="c:test_args">
       <label text="test args"/>
     </button>

     <button name = "dotest" data="pass me?" onclick="c:dotest">
       <label text="Do a Test"/>
     </button>
   </valign>               

   <c-code object="testc">
     <![CDATA[
     void dotest(void* node, GSList* args)
     {
         fprintf(stderr, "in dotest\n");
     }
     ]]>
     <c-function name="dotest"/>
     <c-libs>-L/usr/lib</c-libs> 
   </c-code>

   <c-code object="count">
    <![CDATA[
    void count_to_count(void* node, GSList* args)
    {
      int c;
      int count;

      count = atoi(c_attrib("count"));

      for(c=1; c <= count; c++)
      {
        fprintf(stderr, "count = %i\t", c);
      }
      fprintf(stderr, "\n");

    }

    void count_to_50(void* node, GSList* args)
    {
      int c;
      for(c=1; c <= 50; c++)
      {
        fprintf(stderr, "count = %i\t", c);
      }
      fprintf(stderr, "\n");
    }
    void count_to_100(void* node, GSList* args)
    {
      int c;
      for(c=1; c <= 100; c++)
      {
        fprintf(stderr, "count = %i\t", c);
      }
      fprintf(stderr, "\n");
    }

    void test_args(void* node, GSList* args)
    {
      fprintf(stderr, "How do you tell if the C code actually compiled,\n" 
    		      "you need to trap the return of the gcc exec\n");
      for(; args; args = args->next)
      {
        fprintf(stderr, "-- %s --\n", args->data);
        c_call("dotest", NULL, NULL);
      }
    }
    ]]>
    <c-function name="count_to_50"/>
    <c-function name="count_to_count"/>
    <c-function name="count_to_100"/>
    <c-function name="test_args"/>
    <c-includes>-I/usr/local/include</c-includes>
    <c-libs>-L/usr/local/lib</c-libs>
   </c-code>
  </object>

 </window>

</object>
