#!/usr/local/bin/entity

<object>
  <window ondelete="Entity::exit" title="Clock">
	<valign>
		<object dragable="true" expand="true">
			<label name="time" font="10x20" text = "00:00:00"/>
			<timer interval = "1000" action="tcl:update_clock"/>
			<tcl>
				proc update_clock args {
                    # puts stderr [list update_clock: args=$args]
					set str [clock format [clock seconds] -format %H:%M:%S]
					Entity::set_attr label.time text $str
				}
				update_clock
			</tcl>
		</object>
	</valign>
	<halign>
	<object dragable="true" expand="true">
		<button onclick = "tcl:click_me" border="8">
			<label text = "click me"/>
			<tcl>
				proc click_me args {
                    # puts stderr [list clickme: args=$args]
					puts stderr [list border = [Entity::get_attr $args border]]
				}
			</tcl>
		</button>
	</object>
	</halign>
	<valign>
	<object dragable="true" expand="true">
		<button onclick = "tcl:display_xml" border="8">
			<label text = "display xml"/>
			<tcl>
				proc display_xml args {
                    # puts stderr [list display_xml: args=$args]
					puts stderr [Entity::get_xml //]
				}
			</tcl>
		</button>
	</object>
	</valign>
	<valign>
	<object dragable="true" expand="true">
		<button onclick = "tcl:current_app" border="8">
			<label text = "current application"/>
			<tcl>
				proc current_app args {
                    # puts stderr [list display_xml: args=$args]
					puts stderr [Entity::current_app]
				}
			</tcl>
		</button>
	</object>
	</valign>
	<valign>
	<object dragable="true" expand="true">
		<button onclick = "tcl:ls" border="8">
			<label text = "ls"/>
			<tcl>
				proc ls args {
                    # puts stderr [list display_xml: args=$args]
					puts stderr [Entity::ls //]
				}
			</tcl>
		</button>
	</object>
	</valign>
	<valign>
	<object dragable="true" expand="true">
		<button onclick = "tcl:EXIT" border="8">
			<label text = "delete me"/>
			<tcl>
				proc EXIT args {
                    # puts stderr [list display_xml: args=$args]
					Entity::exit
				}
			</tcl>
		</button>
	</object>
	</valign>
	<valign>
	<object dragable="true" expand="true">
		<button onclick = "tcl:quit" border="8">
			<label text = "quit"/>
			<tcl>
				proc quit args {
                    # puts stderr [list display_xml: args=$args]
					Entity::quit
				}
			</tcl>
		</button>
	</object>
	</valign>
  </window>
</object>
