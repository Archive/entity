#!/usr/local/bin/entity

<object>
 <window border = "15" 
   position = "center"
   ondelete = "Entity::exit"
   title = "Some Cool App"
   name="windowone">

   <valign>
     <radio-group name = "myradiogroup1" onselect="radiotest2">

       <radio name="Item1" ontoggle="radiotest">
         <label text="Item 1"/> </radio>

       <frame text = "Options">
         <halign>

           <radio name="Item2" ontoggle="radiotest">
	     <label text="Item 2"/> </radio>
           <radio name="Item3" ontoggle="radiotest">
	     <label text="Item 3"/> </radio>

	   <button>
	     <label text="button1"/>

	     <radio name="Item8" ontoggle="radiotest">
	       <label text="Item 8"/> </radio>
	   </button>

	   <valign>
	     <button> <label text="button1"/> </button>

             <radio name="Item6" ontoggle="radiotest">
	       <label text="Item 3"/> </radio>

	   </valign>
         </halign>
       </frame>
     </radio-group>

     <radio-group name = "myradiogroup2" onselect="radiotest2">
       <halign>

         <radio name="item4">
	   <label text="item 1"/> </radio>
         <radio name="item5">
	   <label text="item 2"/> </radio>

       </halign>
     </radio-group>

   </valign>               

   <perl>
     sub radiotest{
       my $node = @_;
       #print "$node->path\n";
       # $radio = Entity::is_attr_true($path, "value");
       $radio = $node->attrib_is_true("selected");
       print "($radio)\n";
     }
   </perl>
   <perl>
     sub radiotest2{
       my ($node) = @_;
       print "----$node---\n";
     }
   </perl>

 </window>
</object>
