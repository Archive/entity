#!/usr/local/bin/entity

<object>
  <window position="center" border = "10" name="good" visible="false" 
   title="Woop!" ondelete="close_window">
    <label text="Woop!  Good job!"/>
  </window>
  
  <window ondelete="Entity::exit" title="Fixed Test">
    <fixed width="600" height="420">
      <button name="test" onclick="good_job" x-fixed="5" y-fixed="5">
        <label text="click me if you can!"/>
      </button>
    </fixed>
  </window>
  <timer interval = "100" action="update_fixed"/>
  
  <perl>
    
    sub good_job
      {
        my $node = enode("window.good");
	$node->attrib("visible" => "true");
      }
    
    sub close_window
      {
	my $node = enode(@_);
	$node->attrib("visible" => "false");
      }
      
    sub update_fixed
      {
        $x += 6;
	$y += 3;
	
	my $node = enode("button.test");
	$node->attrib("x-fixed" => "$x");
	$node->attrib("y-fixed" => "$y");

	$x = 2 if ($x > 500);
	$y = 2 if ($y > 400);
      }
   </perl>
</object>
