#!/usr/local/bin/entity

<object>
 <window border = "15"
   position = "center"
   ondelete = "Entity::exit"
   title = "Some Cool App"
   name="windowone">
 <frame text = "test the tree tag">
   <valign  height = "500" width = "300">
    <scrollwindow fill="true" expand="true">
     <tree expand ="true"
           fill="true"
	   name="l1 one"
	   selection-type="multiple"
	   onselect="testtree">
           <label text = "test"/>

       <tree name ="l2 one"><label text="l2 one"/>

	 <tree name="l3 one"><label text="l3 one"/>
	 </tree>

	 <tree name="l3 two" onselect="testtree2"><label text="l3 two"/>
	 </tree>

	 <tree name="l3 three"><label text="l3 three"/>
	 </tree>

	 <tree name="l3 four"><label text="l3 four"/>
	 </tree>
       </tree>

       <tree name="l2 two"><label text="l2 two"/>
       </tree>

       <tree name="l2 three"><label text="l2 three"/>
       </tree>

     </tree>
    </scrollwindow>
   </valign>               
  </frame>

   <perl>
     <![CDATA[
     sub testtree{
       my ( $node ) = @_;
       print "$node->path\n";
       # $text = Entity::get_attr($path, "string");
       $text = $node->attrib("string");
       print "testtree hears -$text-\n";
       #Entity::set_attr($path, "selected", "false");
     }
     ]]>
   </perl>

 </window>
</object>
