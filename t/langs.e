#!/usr/local/bin/entity

<object dragable="true">

<!-- This file contains equivalent code for each language. -->

<perl>
<![CDATA[
sub handler
{
  my($node) = @_;

  my $attr = "bob";
  my $value = 1;

  my $onode = $node->node("tag.name");

  $node->{$attr} =  $value;

  $path = $node->{path};
  @selection = $node->{selection};

  foreach $nnode (@selection)
  {
    print $nnode{name}."\n";
  }

  return 0;
}
]]>
</perl>


<c-code>
<![CDATA[
int handler(ENode* node)
{
  char* path;
  char* attr = "bob";
  char* value = "1";
  int value_int = 1;
  GSList* selection;

  ENode* onode;
  onode = en_node(node, "tag.name");

  en_set(node, attr, value);
  /*OR (either will work.)*/
  en_set_int(node, attr, value_int);


  path = en_get(node, "path");

  selection = en_get(node, "selection"); 
  while(selection)
  {
    ENode* nnode = (ENode*)selection->data

    printf("%s\n", en_get(nnode, "name") );

    selection = selection->next;
  }

  return 0;
}
]]>
</c-code>

<!-- Joze, could you please add the tcl code test? MW-->
<tcl>
<![CDATA[
]]>
</tcl>

<python>
<![CDATA[
def handler(node):
	value = 1
	attr = 'bob'
	
	onode = node[('node', 'tag.name')];
	node[attr] = value
	
	path = node['path']

	selection = node['selection']

	for nnode in selection:
		print nnode['name'], '\n'
		
	return 0
]]>
</python>

</object>

