#!/usr/local/bin/entity

<object>
 <window border = "15" 
   position = "center"
   ondelete = "Entity::exit"
   title = "Some Cool App"
   name="windowone">

   <valign>
     <vpane>
       <halign>
         <button><label text="two"/></button>
         <button label=" text" onclick="set_label"/>
         <checkbox label="label1" ontoggle="set_label"/>
         <checkbox label="label2" ontoggle="set_label"/>
	 <radio-group label="blah">
           <radio label="myrlabel"/>
	 </radio-group>
       </halign>
       <button><label text="three"/></button>
     </vpane>

     <hpane>
       <halign>
         <button><label text="one"/></button>
         <button><label text="two"/></button>
       </halign>
       <button><label text="three"/></button>
     </hpane>

   </valign>               

<perl>
sub set_label
{
  my $node = shift;
  print "$node\n";
  
  my $text = $node->attrib("label");
  $node->attrib("label" => $text.$text);
}
</perl>
 </window>
</object>
