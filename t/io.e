#!/usr/local/bin/entity

<!--
<io
	fd="1"
	oninput="got_part"
	onwrite="progress"
	onerror="got_messed"
	mode="line,chunk"
 />
-->
<object title="io">
  <perl>
  <![CDATA[
  sub get_part
  {
    my $out = enode("io.dest");
    my $node = shift;
    my $data = shift;
    my $size = shift;

    if($size == 0)
    {
      $node->attrib("fd" => -1);
      return;
    }
    # print "'", $data, "'\n";

    $out->write("* '$data' *");
  }
  sub openfiles
  {
    print "opening files\n";
    my $infile = enode("entry.src")->attrib("text");
    my $outfile = enode("entry.dest")->attrib("text");

    open(INFILE, "$infile") or die "unable to open passwd";
    open(OUTFILE, "$outfile");

    my $src = enode("io.src");
    print "-=",$src->attrib("oninput"),"=-\n";

    $src->attrib("fd" => fileno(INFILE) );

    enode("io.dest")->attrib("fd" => fileno(OUTFILE) );
  }
  ]]>
  </perl>

  <window ondelete="Entity::quit">
    <label text="Source"/>
    <entry name="src" text="cat /etc/passwd |" onenter="openfiles"/>
    <label text="Dest"/>
    <entry name="dest" text="| cat" onenter="openfiles"/>

    <button label="copy" onclick="openfiles"/>

  </window>
    
  <io 
    name="src"
    mode="line"
    onnewdata="get_part" 
   />
  <io 
    name="dest"
   />
</object>

