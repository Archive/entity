#! /usr/local/bin/entity

<object title="sdf">
  <window>
    <label text="This is the IO test, this is only a test."/>
    <label text="Had this been a real emergency, we would"/>
    <label text="have fled in terror and you would not"/>
    <label text="have been informed."/>
  </window>
    
  <io 
    name="read"
    onnewdata="read_line"
    onerror="got_messed"
   />
  <io 
    name="write"
    onerror="got_messed"
   />
  <timer interval="1000" action="write_line"/>
  <perl>
  <![CDATA[
    
    open (RFH, "/etc/passwd") or die "Unable to open /etc/passwd";
    my $io = enode ('io.read');
    $fd = fileno (RFH);
    print ("fd is $fd\n");
    $io->attrib ('fd' => $fd);
    print ("read io is $io\n");
    
    open (WFH, ">foofile.txt");
    $io = enode ('io.write');
    $io->attrib ('fd' => fileno (WFH));
    print ("write io is $io\n");

    sub read_line
    {
      my ($node, $chunk, $size) = @_;
      print ("args, node $node, chunk $chunk, size $size\n");
      
      if ($size <= 0) {
        $node->attrib ('fd' => -1);
      }
      print ("read $chunk");
    }
    
    sub write_line
    {
      my $io = enode ('io.write');
      $io->write ("Hello\n");
      if (0) {
        print WFH "Hello";
      }
    }
  ]]>
  </perl>

</object>
