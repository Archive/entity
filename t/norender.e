#!/usr/local/bin/entity

<object>
  <window name="main">
  <button label="add" onclick="add_buttons"/>
  <button label="remove" onclick="remove_buttons"/>
  <valign name="main">
  </valign>
  <norender>
   <button name="$str" label="$label"/>
  </norender>

  <perl>
  <![CDATA[
  sub add_buttons
  {
    my $node = shift;
    my $valign = enode("valign.main");
    my $xml = enode("button.\$str")->get_xml;

    $num++;
    $valign->append_xml($xml, ("str" => '*', "label" => "$num") );
    print $node,"\n";
  }
  sub remove_buttons
  {
    my $valign = enode("valign.main"); 
    $valign->delete_children();
  }
  
  ]]>
  </perl>
  </window>

</object>
