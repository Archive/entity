#!/usr/local/bin/entity

<object>
 <window border = "15" 
   position = "center"
   ondelete = "Entity::exit"
   title = "Some Cool App"
   name="windowone">

   <valign>

     <notebook>

       <notepage label="one">
         <label name="l1" text="text1"/>
         <label name="l2" text="text2"/>
       </notepage>

       <notepage label="two">
         <radio-group>

	   <frame title="These Option">
             <radio><label name="l3" text="text3"/></radio>
             <radio><label name="l4" text="text4"/></radio>
	   </frame>

           <button><label text="try again?"/></button>
	 </radio-group>
       </notepage>

     </notebook>

     <halign>
       <button><label text="ok"/></button>
       <button><label text="cancel"/></button>
     </halign>
   </valign>               

   <perl>
     sub radiotest{
       my $node = @_;
       my $path = $node->path;
       print "$path\n";
       #$radio = Entity::get_attr($path, "value");
       $radio = $node->attrib("value");
       print "$radio is selected\n";
     }
   </perl>

 </window>
</object>
