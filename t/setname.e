#!/usr/local/bin/entity
<object>
  <window name="main">
    <button name="foo" label="rename me" onclick="rename"/>
    <button label="or me" onclick="rename"/>
  </window>
  
  <perl> <![CDATA[
  
  sub rename
  {
    my $node = shift;
    
    print "node $node, name is " . $node->basename() . "\n";
    $node->basename ("foobar2");
    $node->attrib (label => "I'm now a foobar 2");
    print "new node $node, name is " . $node->basename() . "\n";
  }
  
  ]]> </perl>
</object>
