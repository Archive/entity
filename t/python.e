<app default-lang="python">
  <window name="main">
  <button name="add" label="add" onclick="add_buttons"/>
  <button label="remove" onclick="remove_buttons"/>
  <valign name="main">
  </valign>
  <norender>
   <button name="$str" label="$label"/>
  </norender>

<python>
def add_buttons(node):
	loc = enode("valign.main")
	button = enode("button.$str")
	xml = button.get_xml()
	loc.append_xml(xml)
	button = enode("button.add")
	button.attrib( [("label", "pressed"), ("label", "blah")] )
	print button.attrib( ["label"] )
def remove_buttons(node):
	loc = enode("valign.main")
	loc.delete_children()
</python>

  <perl>
  <![CDATA[
  sub add_buttons
  {
    my $node = shift;
    my $valign = enode("valign.main");
    my $xml = enode("button.\$str")->get_xml;

    $num++;
    $valign->append_xml($xml, ("str" => '*', "label" => "$num") );
    print $node,"\n";
  }
  sub remove_buttons
  {
    my $valign = enode("valign.main"); 
    $valign->delete_children();
  }
  
  ]]>
  </perl>
  </window>

</app>
