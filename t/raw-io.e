#!/usr/local/bin/entity

<object title="sdf">
  <perl>
  print "hello\n";
  </perl>

  <perl>
  <![CDATA[
  use IO::Handle;
  sub get_part
  {
    print "in got_part\n";

    my $node = shift;
    my $INFILE = IO::Handle->new_from_fd( $node->attrib("fd"), "r" );

    do
    {
      read $INFILE, $buffer, 2;
      print $buffer;
    }
    while(length($buffer) == 2);
    print "\n----\n";

    if(!$buffer && $ERRNO != EAGAIN) #eof
    {
      $node->attrib("fd" => "-1");
    }
    $data .= $buffer;
  }
  sub do_write
  {
    #print "in do_write\n";
    my $node = shift;
    #local $offset;

    my $OUTFILE = IO::Handle->new_from_fd($node->attrib("fd"), "w");
   
    print OUTFILE $data;
  }
  sub openfiles
  {
    print "opening files\n";
    open(INFILE, "</etc/passwd") or die "unable to open passwd";
    #open(OUTFILE, " |");

    enode("raw-io.src")->attrib("fd" => fileno(INFILE) );
    #enode("raw-io.dest")->attrib("fd" => 1);
  }
  ]]>
  </perl>

  <window>
    <label text="Source"/><entry name="src"/>
    <label text="Dest"/><entry name="dest"/>
    <button label="copy" onclick="openfiles"/>
  </window>
    
  <raw-io 
    name="src"
    onread="get_part" 
    onerror="got_messed"
   />
  <raw-io 
    name="dest"
    onerror="got_messed"
   />
</object>
