#!/usr/local/bin/entity

<object>
  <test-case tag-name="tree" 
             file="gtk-renderers/tree-renderer.c">

    <window title    = "Trees"
            ondelete = "Entity::exit"
	    height="500">

      <scrollwindow height="200" width="450">
        <text width="400" expand="true">
<![CDATA[
Testing:    <tree>
Attributes: onselect       = "function"
            ondeselect     = "function"
            selection-type = "single|browse|multiple|extended"
            selected       = true/false

Cases:      
]]>
        </text>
      </scrollwindow>

      <scrollwindow expand = "true">
      <valign expand = "true">

      <object name      = "Tree Layout"
           aspect    = "Simple layout of trees"
	   dragable = "true">

        <tree>
          <label text="Simple tree layout"/>

	  <tree>
            <label text="level 1 node 1"/>
	    <tree>
              <label text="level 2 node 1"/>
	    </tree>
	    <tree>
              <label text="level 2 node 2"/>
	    </tree>
	  </tree>
	  <tree>
            <label text="level 1 node 2"/>
	    <tree>
              <label text="level 2 node 1"/>
	    </tree>
	  </tree>
	  <tree>
            <label text="level 1 node 3"/>
	  </tree>
        </tree>

      </object>


      <object name      = "Tree Without Sub Widgets"
           aspect    = "Layout a tree without a label widget"
	   dragable = "true">
	<label text="Tree wihout a label"/>

	<tree>
	</tree>

	<label text="Without a label but with a sub tree"/>

	<tree>
	  <tree>
	    <label text="subtree to tree without sub widgets."/>
	  </tree>
	</tree>

      </object>

      <object name      = "Tree Layout One Item"
           aspect    = "Layout only one tree and see if it barfs."
	   dragable = "true">

	<tree>
	  <label text="single tree tag"/>
	</tree>

      </object>

      <object name      = "Multiple labels Item"
           aspect    = "See if multiple items get added to the hbox"
	   dragable = "true">
        <tree>
          <label text="top level tree part1--"/>
	  <label text="==top level tree part2"/>
          <tree>
            <label text="level 1"/>
            <label text=" node 1"/>
            <tree>
              <label text="level 2"/>
              <label text=" node 1"/>
            </tree>
            <tree>
              <label text="level 2"/>
              <label text=" node 2"/>
            </tree>
          </tree>
        </tree>

      </object>

      <object name      = "[De]Select Item"
           aspect    = "Call a callback when an item is [de]selected."
	   dragable = "true">

        <tree onselect   = "tree_onselect_at_top"
              ondeselect = "tree_ondeselect_at_top">
          <label text="Call a callback when an item is [de]selected."/>
          <tree>
            <label text="level 1 node 1"/>
            <tree onselect="tree_onselect_at_item">
              <label text="level 2 node 1"/>
            </tree>
            <tree ondeselect = "tree_ondeselect_at_item">
              <label text="level 2 node 2"/>
            </tree>
          </tree>
          <tree>
            <label text="level 1 node 2"/>
          </tree>
        </tree>

      </object>

      <object name      = "Programmer Deselect"
           aspect    = "Callback function deselects a tree element"
           dragable = "true">

        <perl>
	  sub deselect_current_node
	  {
            my($node) = (@_);
	    print "$node->path\n";
	    #Entity::set_attr($path, "selected", "false");
	    $node->attrib("selected" => "false");
	  }
        </perl>

        <tree onselect="deselect_current_node">
          <label text="Callback function deselects a tree element"/>
          <tree> 
            <label text="level 1 node 1"/>
          </tree>
        </tree>

      </object>

      <object name      = "Selection Types"
           aspect    = "Callback function deselects a tree element"
           dragable = "true">

      <tree selection-type="single">
        <label text="single selection(default)"/>
          <tree>
            <label text="level 1 node 1"/>
            <tree>
              <label text="level 2 node 1"/>
            </tree>
            <tree>
              <label text="level 2 node 2"/>
            </tree>
          </tree>
      </tree>
      <tree selection-type="browse">
        <label text="browse selection"/>
          <tree>
            <label text="level 1 node 1"/>
            <tree>
              <label text="level 2 node 1"/>
            </tree>
            <tree>
              <label text="level 2 node 2"/>
            </tree>
          </tree>
      </tree>
      <tree selection-type="multiple">
        <label text="multiple selection"/>
          <tree>
            <label text="level 1 node 1"/>
            <tree>
              <label text="level 2 node 1"/>
            </tree>
            <tree>
              <label text="level 2 node 2"/>
            </tree>
          </tree>
      </tree>

      </object>

      <object name      = "Selection"
           aspect    = "Test the selection of tree items"
           dragable = "true">

      <tree selection-type="multiple" onselectionchange="echo_select_list">
        <label text="test tree->selection ablity"/>
          <tree>
            <label text="level 1 node 1"/>
            <tree>
              <label text="level 2 node 1"/>
            </tree>
            <tree>
              <label text="level 2 node 2"/>
            </tree>
          </tree>
      </tree>

      </object>


      </valign>
      </scrollwindow>
    </window>
  </test-case>
</object>
