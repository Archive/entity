#!/usr/local/bin/entity

<object>
 <window border = "2" 
   position = "center"
   ondelete = "Entity::exit"
   title = "Progress Test"
   name="windowone">
   
   <object dragable="true">
     <valign border="6" spacing="3">
       <progress name="progrs" value="0" blocks="15" activity="false">
         <timer action = "progtest" interval = "50"/>
       </progress>
       <button onclick="toggle_active"> <label text="Toggle Activity"/> </button>
       <button onclick="dump_xml"> <label text="Dump XML Tree"/> </button>
     </valign>
   
     <perl>
       <![CDATA[

       sub dump_xml
       {
         print ("dumping xml:\n\n");
         $xml_tree = enode("/")->get_xml();
         print $xml_tree;
         print ("..done.\n");
       }
       
       $perdone = 0;
       sub progtest 
       {
	 $perdone = $perdone + .01;
	 $perdone = 0 if ($perdone >= 1.0);
         my $node = enode("progress.progrs");
	 $node->attrib("value" => $perdone);
       }
   
       sub toggle_active
       {
         my $node = enode("progress.progrs");
	 
	 if ($node->attrib("activity") eq "false") {
	   $node->attrib("activity" => "true");
	 } else {
	   $node->attrib("activity" => "false");
	 }
       }
     ]]>
     </perl>
   </object>
 </window>
</object>
