#!/usr/local/bin/entity

<object>
  <window ondelete="entity:exit">
    <label name="test" sensitive="1" text="this is a label"/>
    <button label="toggle sensitivity" onclick="tog_sens"/>
  </window>
  <perl><![CDATA[
  sub tog_sens
  {
    my $label = enode("label.test");
    my $sens = $label->attrib("sensitive");
    $label->attrib("sensitive" => !$sens);
  }
  ]]></perl>
</object>
