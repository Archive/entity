#!/usr/local/bin/entity

<object>
 <window border = "15"
   position = "center"
   ondelete = "Entity::exit"
   title = "Some Cool App"
   name="windowone">
 <frame title = "test the item tag">
   <valign  height = "500" width = "300">
    <scrollwindow fill="true" expand="true">

     <list expand ="true"
           fill="true"
	   name="l1 one"
	   selection-type="multiple"
	   onselect="testlist">

       <label text = "test 1" onselect="testlist"/>
       <label text = "test 2"/>

     </list>
    </scrollwindow>
   </valign>               
  </frame>

   <perl><![CDATA[
   sub testlist
   {
     my ($node) = @_;
     $text = $node->attrib("selected");
     print "testlist hears -$text-\n";
     $node->attrib("selected" => "false");
   }
   ]]></perl>

 </window>
</object>
