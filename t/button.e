#!/usr/local/bin/entity

<object>
  <window name="main" ondelete="Entity::quit">
  <button label="add" onclick="add_buttons"/>
  <button label="remove" onclick="remove_buttons"/>
  <valign name="main">
  </valign>
  <norender>
   <button name="$str" label="other"/>
  </norender>

  <perl>
  <![CDATA[
  sub add_buttons
  {
    my $node = shift;
    my $valign = enode("valign.main");
    my $xml;
    $xml .= '<label text="$str"/>' x 50;
    
    $xml = $xml x 20;
    $valign->append_xml($xml, ("str" => '*') );
    print $node,"\n";

    return "stop handlers";  # don't let anymore handlers see this one.
    return "stop internals"; # don't do any more of anything.
    return "stop";  # callback stops how ever it thinks is best.
    return "anything else"; #continue with the signal.
  }
  sub remove_buttons
  {
    my $valign = enode("valign.main"); 
    $valign->delete_children();
  }
  
    print ("Hello there\n");
  ]]>
  </perl>
  </window>

</object>
