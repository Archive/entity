
<object __perl-namespace="ENet">
    <perl name="socket"><![CDATA[

    use Socket;
    use Fcntl;
    sub socket__start
    {
      print "in socket__start\n";
      my $url = shift;

      my $fh = "SOCKET:$url".ENet_rnd_num();
      $url->attrib("ENet_my_fh" => $fh);

      my $flags = $url->attrib("flags");
      ### We are a PF_UNIX. 
      if ( substr($flags, "NURL_NO_PORT") )
      {
	socket($fh, PF_UNIX, SOCK_STREAM, 0) or die "socket: $!";
      }
      else ### We have to be a FP_INET.
      {
	my $proto = getprotobyname("tcp");

	## We created the socket.
	socket($fh, PF_INET, SOCK_STREAM, $proto) or die "socket: $!";
      }

      ## Set the socket non-blocking.
      fcntl($fh, F_SETFL, O_NONBLOCK);

      ## Let the user use this fd too. 
      $url->attrib("fd" => fileno($fd) );
    } #end start

    sub socket__connect
    {
      my $url = shift;
      my $fh = $url->attrib("ENet_my_fh");
      my $flags = $url->attrib("flags");

      my $port;
      my $iaddr;
      my $paddr;

      ### We are a PF_LOCAL. 
      if ( substr($flags, "NURL_NO_PORT") )
      {
        my $host = $url->attrib("host");
        my $resource = $url->attrib("resource");
        $paddr = sockaddr_un("/".$host."/".$resource);
      }
      else ### We have to be a FP_INET.
      {
        $port = $url->attrib("port");
        $iaddr = inet_aton( $url->attrib("host") );
        $paddr = sockaddr_in($port, $iaddr);
      }
  
      ## add a raw-io tag for the user.
      my $io = $url->new_child("raw-io",
				"fd"       => fileno($fh),
				"oncanread"=>"ENet::socket__raw_io_connected");
      ## try connecting.
      print "url = $url\n";
      print "fh = $fh\n";
      my $did_conn = 1;
      connect($fh, $paddr) or $did_conn = 0;

      ## It connected right away for us. horray pokey!
      if ($did_conn)
      {
	## pretend we had to go through entity.
	socket__raw_io_connected($io);
      }
      elsif ($! != EINPROGRESS)
      {
        print "connect failed miserably: $!\n";
      } 

      ## It may yet happen...

    } #end connect

    sub socket__accept
    {
      print "in socket__accept\n";
      my $url = shift;
      my $fh = $url->attrib("ENet_my_fh");


      my $flags = $url->attrib("flags");
      ### We are a PF_LOCAL.
      if ( substr($flags, "NURL_NO_PORT") )
      {
        my $host = $url->attrib("host");
        my $resource = $url->attrib("resource");
	my $loc = "/".$host."/".$resource;
	#print "loc = $loc\n";

	unlink($loc);  ## Can't have it in the way.
	bind($fh, sockaddr_un($loc) );
	listen($fh, SOMAXCONN);
      }
      else ### We have to be a FP_INET.
      {
	### Bind up and start accpeting.
	setsockopt($fh, SOL_SOCKET, SO_REUSEADDR, pack("l",1) );

	my $port = $url->attrib("port");
        bind($fh, sockaddr_in($port, INADDR_ANY) );
	listen($fh, SOMAXCONN);
      }

      $url->new_child("raw-io", 
			"fd" => fileno($fh),
			"fh" => $fh,
			"oncanread" => "ENet::socket__raw_io_accept",
			);
    }
    
    my %socket = ("start"   => \&socket__start,
    	          "connect" => \&socket__connect,
    	          "accept"  => \&socket__accept,
    	          "end"     => \&socket__end,
    );
    $ENet::url_proto{"socket"} = \%socket;
    ]]></perl>

    <perl name="socket__raw_io_"><![CDATA[
    sub socket__raw_io_connected
    {
      print "in socket__raw_io_connected\n";
      my $io = shift;
      my $url = $io->parent();
      my $fh = $url->attrib("ENet_my_fh");


      my $iotype = $url->attrib("io");
      if ($iotype eq "") { $iotype = "io"; };

      my $onaction = $url->attrib("onaction");
      my $conn = $url->new_child("$iotype",
      				 "fd" => $io->attrib("fd"),
				 );
      $url->call($onaction, $url, $conn);
      print "going to delete $io\n";
      $io->delete();
    }
    sub socket__raw_io_accept
    {
      my $io = shift;
      my $url = $io->parent();

      my $fh = $url->attrib("ENet_my_fh");
      my $clientFH = "CLEINT:$io:".ENet_rnd_num();

      print "fh = $fh\n";
      print "client = $clientFH\n";

      my $paddr = accept($clientFH, $fh) or die "FIXME: accept: $!";

      my $iotype = $url->attrib("io");
      if ($iotype eq "") { $iotype = "io"; };

      ## create the data to hold the new accept.
      my $accept = $url->new_child("$iotype",  
      				   "fd"     => fileno($clientFH),
      				   "fh"     => $clientFH,
      			 	   "action" => "accept",
				   );

      ## Call the onaction function.
      my $onaction = $url->attrib("onaction");
      $url->call($onaction, $url, $accept);
    }
    ]]></perl>
</object>

