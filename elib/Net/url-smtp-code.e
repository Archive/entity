<object __perl-namespace="ENet">
	<perl name="smpt"><![CDATA[

	sub smtp__onnewdata
	{
		my ($io, $data, $size) = @_;
		my $url = $io->parent();

		my $helo = $url->attrib("smtp-heloed");
		if ($helo ne "true")
		{
			$io->write("HELO thunder.cgibuilder.com\n");
			$url->attrib("smtp-heloed" => "true");
			return;
		}

		print "in smtp__onnewdata\n";
		smpt__put_parts($url, $io, $data);
		print "-=+$data+=-\n";
	}
	sub smpt__put_parts
	{
		use strict;
		my ($url, $io, $str) = @_;
	
		my @lines = split("\n", $str);
		print "lastline = $lines[-1]\n";

		## Save off the lastline for later use.
		$io->attrib("lastline" => $lines[-1]) if ( scalar(@lines) );

		my $data;
		my $ddata;
		my @children = $url->children();
		if ($lines[-1] =~ /^[23]\d\d\s+/)
		{
			foreach $data (@children)	## Find first "putting" data node.
			{
				if ( $data->attrib("smtp-internal-action") eq "putting" )
				{
					$ddata = $data;
					last;
				}
			}
			return if ($ddata->attrib("smtp-internal-action") ne "putting" );

			my $status = $ddata->attrib("put-status"); 
			my ($header, $body) = split("\n\n", $ddata->get_data(), 2);

			$header =~ /^(To:\s+.*$)/mi;
			my $to =  $1;
			$header =~ /^(From:\s+.*$)/mi;
			my $from =  $1;

			if ($status eq "need-mail" )
			{
				print "MAIL\n";
				$from =~ /^From:\s+(.*)/i;
				print "$1\n";
				$io->write("MAIL FROM: ".$1."\n");
				$ddata->attrib("put-status" => "need-rcpt"); 
			}
			elsif ($status eq "need-rcpt")
			{
				print "RCPT\n";
				$to =~ /^To:\s+(.*)/i;
				print "$1\n";
				$io->write("RCPT TO: ".$1."\n");
				$ddata->attrib("put-status" => "need-data"); 
			}
			elsif ($status eq "need-data")
			{
				print "DATA\n";
				$io->write("DATA\n");
				$ddata->attrib("put-status" => "need-body"); 
			}
			elsif ($status eq "need-body")
			{
				print "body\n";
				$header =~ /^(Subject:\s+.*$)/mi;
				my $subj =  $1;

				my $bdata = $subj."\n".
				            $to."\n".
				            $from."\n".
				            $body."\n.\n";
				print "bdata = $bdata";
				$io->write($bdata);
				$ddata->attrib("put-status" => "get-conferm"); 
			}
			elsif ($status eq "get-conferm")
			{
				$ddata->attrib("put-status" => "done"); 
				# Done.  Tell user.  They need to free the data node.
			}
		}
	}

	sub smtp__connected
	{
		my $socket = shift;
		my $data = shift;

		## Run up two urls tags.
		my $smtp = $data->find_parent("url")->find_parent("url");

		## Find and populate my io.smtp tag with the data.
		my $io = $smtp->find_child("io.smtp");

		$io->attrib("fd" => $data->attrib("fd") );
		$data->delete();
	}
	sub smtp__start
	{
		print "in smtp__start\n";
		my $url = shift;
		my $host = $url->attrib("host");
		my $port = $url->attrib("port");

		my $io = $url->new_child("io.smtp",
				"onnewdata" => "ENet::smtp__onnewdata",
		);

		## Give us a real port.
		$port = "25" if ($port eq "");

		$url->new_child("url.smtp-socket", 
		                "href"	=> "socket://$host:$port/",
		                "io"	=> "data",	##I'll make my own.
		                "action"	=> "connect",
		                "onaction"	=> "ENet::smtp__connected"
		);

	}

	sub smtp__end
	{
		print "in smtp__end\n";
	}

	sub smtp__put
	{
		print "in smtp__put\n";
		my $url = shift;
		my $io = $url->find_child("io.smtp");
		my @children = $url->children();
		my $data;

		foreach $data (@children)
		{
			if ( lc($data->attrib("action")) eq "put" )
			{
				$data->attrib("smtp-internal-action" => "putting");
				$data->attrib("put-status" => "need-mail");
			}
		}
		my $lastline = $io->attrib("lastline");

		smpt__put_parts($url, $io, $lastline);
	}

	my %smtp = ("start"	=> \&smtp__start,
	            "put"	=> \&smtp__put,
	            "end"	=> \&smpt__end);
    
	$ENet::url_proto{"smtp"} = \%smtp;
	]]></perl>
</object>

