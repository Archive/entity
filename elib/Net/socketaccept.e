<object>
  <url name="myserv" href="socket://tmp/try.sock"
	onaction="doit" 
	action="accept"/>

  <perl><![CDATA[
  # enode("url.myserv")->attrib("action" => "accept");

  sub doit
  {
    my $url = shift;
    $io = shift;

    $io->attrib("onnewdata" => "print_it");
  }
    
  sub print_it
  {
    my $io = shift;
    my $data = shift;

    print "--$data--\n";
    $io->write("Don't do that!\n");
  }

  ]]></perl>
</object>

