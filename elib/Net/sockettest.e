<object>
  <url name="myserv" href="socket://localhost:20000/" onaction="doit" />

  <perl><![CDATA[
  enode("url.myserv")->attrib("action" => "accept");

  sub doit
  {
    $accept_data = shift;
    print "in doit\n";
    $xml = $accept_data->get_xml();
    print "$xml\n";
  }
  ]]></perl>
</object>
