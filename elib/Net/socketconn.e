<object>
  <url name="myserv" 
	href="socket://tmp/try.sock"
	onaction="done_connect" />

  <perl><![CDATA[

  enode("url.myserv")->attrib("action" => "connect");

  sub done_connect
  {
    my $url = shift;
    my $conn = shift;  #<io> node.

    $conn->attrib("onnewdata" => "print_it");
    $conn->write("GET /\n\n");
  }

  sub print_it
  {
    my $io = shift;
    my $data = shift;

    print "--$data--\n";
  }

  ]]></perl>
</object>
