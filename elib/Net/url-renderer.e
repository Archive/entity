#!/usr/local/bin/entity

<object name="url" default-lang="perl" __perl-namespace="ENet">

	<renderer name="entity-networking"
		tag="url"
		lang="perl"
		onrender="rendurl_render"
		ondestroy="rendurl_destroy"
		onparent="rendurl_parent">

		<attrib name="href"
			description="The location of the url."
			value_desc="string"
			values="*"
			onset="rendurl_attr_set" />

		<attrib name="resource"
			description="The resource part of the url."
			value_desc="string"
			values="*"
			onset="rendurl_attr_set" />

		<attrib name="host"
			description="The host part of the url."
			value_desc="string"
			values="*"
			onset="rendurl_attr_set" />

		<attrib name="base"
			description="All but the resource part of the url."
			value_desc="string"
			values="*"
			onset="rendurl_attr_set" />

		<attrib name="user"
			description="The user part of the url."
			value_desc="string"
			values="*"
			onset="rendurl_attr_set" />

		<attrib name="port"
			description="The port part of the url."
			value_desc="string"
			values="*"
			onset="rendurl_attr_set" />

		<attrib name="password"
			description="The password part of the url."
			value_desc="string"
			values="*"
			onset="rendurl_attr_set" />

		<attrib name="type"
			description="The internal type of the url. (No Edit)"
			value_desc="string"
			values="*"/>
			
		<attrib name="io"
			description=
	"The io tag type to create.  Use 'data' if you don't want an io tag. "
			value_desc="string"
			values="io,raw-io,data"/>
			
		<attrib name="flags"
			description="The internal flags of the url. (No Edit)"
			value_desc="string"
			values="*"/>

		<attrib name="action"
			description="The action to do on this url."
			value_desc="string"
			values="*"
			onset="rendurl_action_attr_set" />

		<attrib name="onaction"
			description="The function to call when an action happens."
			value_desc="function"
			values="*"/>

		<!-- socket lib -->
		<require file="Net/url-socket-code.e"/>

		<!-- pop3 lib -->
		<require file="Net/url-pop-code.e"/>

		<!-- smtp lib -->
		<require file="Net/url-smtp-code.e"/>


		<perl name="rendurl_action_attr_set"><![CDATA[
		sub rendurl_action_attr_set
		{
			my ($url, $attr, $value) = @_;
			return if ( !$url->attrib("started") );

			my $proto = $url->attrib("proto");
			print "in rendurl_action_attr_set for $proto, $attr, $value\n";

			my $protoht = $url_proto{lc($proto)};
			my $func = $protoht->{lc($value)};
			if (CODE eq ref($func) )
			{
				$func->($url);  ## Call the function for this url/action.
			}
		}
		]]></perl>
		<perl name="ENet_rnd_num"><![CDATA[
		## make a "random" number.
		sub ENet_rnd_num
		{
			++$rnd_num;
			return "$rnd_num";
		}
		]]></perl>

		<perl name="rendurl_attr_set"><![CDATA[
		require ("elib/Net/patterns");

		sub rendurl_match_url
		{
			my ($node, $value) = @_;
			my $found;
			my @matches;
			my $t;
			my $ttype;
			my $flags;
			my @set;

			print "in rendurl_match_url\n";

			foreach $t (@Regexes)
			{
				my $regex = $t->[1];
				#print "trying $regex on $value\n";
				if ( (@matches) = $value =~m!$regex!i )
				{
						print "    '$t->[0]'  Flags: '$t->[3]'.\n";
					{ local ($") = ", "; print "    @matches\n"; }
					print "\nURL: ";
					$found = 1;
					$ttype = "$t->[0]";
					$flags = "$t->[3]";
					@set = @{$t->[2]};
					last;
				}
			}

			if (!$found)
			{
				return;
			}

			$node->attrib("type" => "$ttype");
			$node->attrib("flags" => "$flags");

			#print "--- $ttype ---\n";
			#print $node->{path}, "\n";
			#print $node->attrib("href"),"--\n";

			###Populate the url tag's attributes now.
			for ($i=0; $i <= scalar(@matches); $i++)
			{
				if ($set[$i+1] == 4)  ###4 == host.
				{
					$node->attrib("host" => $matches[$i]);
				}
				elsif ($set[$i+1] == 1)  ###4 == proto.
				{
					$node->attrib("proto" => $matches[$i]);
				}
				elsif ($set[$i+1] == 6)  ###6 == resource.
				{
					$node->attrib("resource" => $matches[$i]);
				}
				elsif ($set[$i+1] == 2)  ###2 == user.
				{
					$node->attrib("user" => $matches[$i]);
				}
				elsif ($set[$i+1] == 3)  ###2 == pass.
				{
					$node->attrib("password" => $matches[$i]);
				}
				elsif ($set[$i+1] == 5)  ###5 == port.
				{
					$node->attrib("port" => $matches[$i]);
				}
			}##end for.
		}

		sub rendurl_populate_url_tag
		{
			my $node = shift;
			print "in rendurl_populate_url_tag\n";
		}

		sub rendurl_attr_set
		{
			my ($node, $attr, $value) = @_;

			print "$attr => $value\n";
			if ($attr eq "href")
			{
				rendurl_match_url($node, $value);
			}
			else
			{
				## rebuild the url from the parts.
				rendurl_populate_url_tag($node);
			}
		}
		]]></perl>

		<perl name="rendurl_render"><![CDATA[
		sub rendurl_render
		{
			my $node = shift;

			$node->set_all_attribs(\&rendurl_attr_set);

			print "!!!in rendurl_render!!!\n";

			my $proto = $node->attrib("proto");
			if ($proto eq "" )
			{
				print "Need to find a proto for this url...\n";
				return;
			}
			my $action = $node->attrib("action");

			###Call the function for dealing with this url.
			my $protoht = $url_proto{lc($proto)};

			###Start the open and other code for the url.
			$func = $protoht->{"start"};
			if (CODE eq ref($func) )
			{
				$func->($node);
			}
			$node->attrib("started" => "1");

			###Do any current action.
			$func = $protoht->{lc($action)};
			if (CODE eq ref($func) )
			{
				$func->($node);
			}
		}

		]]></perl>

		<perl name="rendurl_parent"><![CDATA[
		sub rendurl_parent
		{
			print "in rendurl_parent\n";
		}
		]]></perl>

		<perl><![CDATA[
		if ($ENet_url_code_loaded)
		{
			print "ENet loaded once already.\n";
			#enode("object.url")->delete();
		}

		$ENet_url_code_loaded = 1;
		]]></perl>

		<perl name="http"><![CDATA[
		sub http__onnewdata
		{
				my ($io, $data, $size) = @_;
				my $url = $io->parent();

				print "in http__onnewdata\n";
				# http__get_parts($url, $io, $data, $size);
				#print "-=+$data+=-\n";

				my $onaction = $url->attrib("onaction");
				$url->call($onaction, $url, $data, $size);
    }   

		sub http__get
		{
			my $url = shift;

			my $io = $url->find_child("io.http");

			my $resource = $url->attrib("resource");
			my $host = $url->attrib("host");
			$io->write("GET /$resource HTTP/1.1\n".
			           "Host: $host\n".
								 "User-Agent: Entity-Url-Http/1.0\n\n");
		}

		sub http__connected
		{
			my $socket = shift;
			my $data = shift;

			## Run up two urls tags.
			my $http = $data->find_parent("url")->find_parent("url");

			## Find and populate my io.smtp tag with the data.
			my $io = $http->find_child("io.http");

			$io->attrib("fd" => $data->attrib("fd") );
			$data->delete();
		}


		sub http__start
		{
			print "in http__start\n";
			my $url = shift;
			my $host = $url->attrib("host");
			my $port = $url->attrib("port");

			my $io = $url->new_child("io.http",
			                         "onnewdata" => "ENet::http__onnewdata",
			);
			
			$port = "80" if ($port eq "");  ## default port then.
	

			$url->new_child("url.http-socket",
			                "href"  => "socket://$host:$port/",
			                "io"    => "data",      ##I'll make my own.
			                "action"        => "connect",
			                "onaction"      => "ENet::http__connected"
			);
		}

		my %http = ("start"	=> \&http__start,
		            "get"		=> \&http__get,
		            "end"		=> \&http__end);

		$url_proto{"http"} = \%http;
		]]></perl>

	</renderer>
</object>

