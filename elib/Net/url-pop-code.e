<object __perl-namespace="ENet">
 <!-- the pop code for the url renderer -->
   <perl name="pop"><![CDATA[
    use Fcntl;
    use IPC::Open2;
    sub pop__myRead
    {
      my ($node, $data, $size) = @_;
      my $ndata;

      print "in pop__myRead -$data-, -$size-\n";

      my $mode = $url->attrib("mode");

      ###Get new mode from data.
      if ($mode eq "nothing")
      {
        my ($ctl, $data) = split("\n", $data);
	print "ctl = $ctl\n";

	if ( $ctl =~ /^Message_lines: (\d+)/ )
	{
	  $mode = "getting";
	  $left = $1;

	  $url->attrib("mode" => "getting");

	  $ndata = $url->new_child("data", "action" => "get");
	  $ndata->attrib("left" => $left);
	}

	elsif ( $ctl =~ /^List_size: (\d+)/ )
	{
	  $mode = "listing";
	  $left = $1;
	  print "in List_size: $left\n";

	  $url->attrib("mode" => "listing");

	  #$ndata = $url->new_child("data", "action" => "list");
	  $ndata = $url->new_child("data.");
	  $ndata->attrib("left" => $left);
	}
	else
	{ return; }

	$url->attrib("ENet_my_current" => $ndata);
        $ndata->attrib("href" 	  => $url->attrib("href"),
		       "action"   => $url->attrib("action"),
			);
      }

      my $n = $url->attrib("ENet_my_current");
      print "n = $n\n";
      $ndata = enode($n);

      print "mode ===$mode for $ndata===\n";

      if ($mode eq "getting")
      {
        my $left = $ndata->attrib("left");

	my @data = split("\n", $data);
	my $got = scalar(@data);
	$left -= $got;
	print "left = $left\n";

	my $onaction = $url->attrib("onaction");
	$url->call($onaction, $url, $ndata, 
		 "@data");

	if ($left == 0) ###Done.
	{
          $ndata->set_data("@data");
	  #print "@data\n";
          $url->attrib("mode" => "nothing");
	}
        $ndata->attrib("left" => $left);
      }
      elsif ($mode eq "listing")
      {
        my $left = $ndata->attrib("left");

	if ( length($data) == 0 )
	  { return; }

	my @data = split("\n", $data);

	print "left = $left\n";
	foreach my $mes (@data)
	{
	  if ($mes =~ /(\d+) (\d+)/)
	  { 
	    $ndata->new_child("data", "num" => $1, "size" => $2);
	    $left--;
	  }
	}
	if ($left == 0) ###Done.
	{
          $url->attrib("mode" => "nothing");
	  my $onaction = $url->attrib("onaction");
	  ###Call the onaction function.
	  if ($onaction ne "")
	  {
	    print $ndata, "\n";
	    #my @children = $ndata->children();
	    print $ndata->parent(), "\n";
	    $url->call($onaction, $url, $ndata, 
	    	scalar(@children) );
	  }

	}
        $ndata->attrib("left" => $left);
      }
    }

    sub pop__start
    {
      $url = shift;
      print "in pop__start\n";

      # open (FHW, "|perl /home/matt/src/gnome/entity/Net/pop-control-app");

      $url->attrib("mode" => "nothing");

      my $rdr = "READ:$url";
      my $wdr = "WRITE:$url";
      $url->attrib("ENet_my_read_fh" => $rdr);
      $url->attrib("ENet_my_write_fh" => $wdr);

      my $pid = open2 ($rdr, $wdr, 
      		"perl /home/matt/src/gnome/entity/Net/pop-control-app");

      # print "the pid = $pid\n";

      ### This keeps it from buffering.
      my $prev = select $wdr;
      $| = 1;
      select $prev;  #choose the old filehandle.
      # fcntl (fileno(FH), F_SETFL, O_ASYNC);

      my $ctl = $url->new_child("io.pop-control", "fd" => fileno($rdr),
      				"onnewdata" => "ENet::pop__myRead"); 

      # $ctl->write("port ".$url->attrib("port")."\n");

      print $wdr "port ".$url->attrib("port")."\n";
      print $wdr "host ".$url->attrib("host")."\n";

      print $wdr "login ".$url->attrib("user"). " ".
      		       	$url->attrib("password"). "\n";
    }
    sub pop__end
    {
      print "in pop__end\n";
      my $url = shift;
      my $wdr = $url->attrib("ENet_my_write_fh");
      print $wdr "end\n";
    }
    sub pop__pop_list
    {
      print "in pop__pop_list\n";
      my $url = shift;
      my $wdr = $url->attrib("ENet_my_write_fh");
      print $wdr "list\n";
    }
    sub pop__get
    {
      print "in pop__get\n";
      my $url = shift;
      my $wdr = $url->attrib("ENet_my_write_fh");

      my $resource = $url->attrib("resource");

      print $wdr "get $resource\n";
    }
    sub pop__delete
    {
      print "in pop__delete\n";
    }

    my %pop = ("start"    => \&pop__start,
    	       "end"      => \&pop__end,
    	       "list"     => \&pop__pop_list,
    	       "get"      => \&pop__get,
    	       "delete"  =>  \&pop__delete,
    );
    $ENet::url_proto{"pop"} = \%pop;

    ]]></perl>
</object>
