<object name="PopIt" 
	onnewmessage="show_mess">

  <url name="popmymail" href="pop://test:pass@thunder.cgibuilder.com/">
  </url>

  <window>
    <button label="Get Mail" onclick="getmail"/>
  </window>

  <perl><![CDATA[
  sub get_a_message
  {
    my $url = shift;  #current node
    my $get = shift;
    my $url = $get->parent();

    my $obj = enode("object.PopIt");
    my $func = $obj->attrib("onnewmessage");

    my $data = shift;  #buffer
    my $size = shift;  #size of buffer

    if ( $size == 0 ) ##finished if $size == 0
    {

      # $obj->lang_call($func, $get);  ##show this new message
      
      print "data ===> $data\n";
      #$get->delete();  ##Got this message completely.
      my $pop_list = enode( $url->attrib("pop-list") );

      start_getting($pop_list);
    }
  }
  
  sub start_getting
  {
    my $pop = enode("url.popmymail");
    my $url = shift;  
    my $pop_list = shift;  ##get the mesage info list

    print "poplist = $pop_list\n";
    my @children = $pop_list->children();
    my $mes_info = shift( @children );

    my $rnum = $mes_info->attrib("num");

    if ($rnum > 0)
    {
      $pop->attrib("resource"  => $rnum,
      		   "action"    => "get",
		   "pop-list"  => $pop_list,    
    		   "onnewdata" => "get_a_message");
    }
    else  #done getting all messages
    {
      $pop_list->delete();
    }

    ##Getting this one right now, so we don't need to look at it again.
    $mes_info->delete();
  }

  sub got_list
  {
    my ($node, $size) = @_;

    start_getting( $node );
  }

  sub getmail
  {
    $pop = enode("url.popmymail");
    #$url = enode("object.PopIt")->attrib("url");

    $pop->attrib("action" => "list",
		 "onnewdata" => "got_list");
  }
  ]]></perl>
  
</object>
