#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "entity.h"
#include "gtk-common.h"
#include "gtk-widget-attr.h"


static void
rendgtk_menuitem_selected (GtkWidget * widget, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;

  function = xml_node_get_attr_str (node, "onselect");
  lang_call_function (node, function, "n", node);
}


static gint
rendgtk_menu_popup_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *menu;

  menu = erend_edata_get (node, "bottom-widget");
  if (!menu)
    return (TRUE);

  if (erend_value_is_true (value))
    {
      gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, 1,
		      gdk_time_get ());
    }
  else
    {
      gtk_menu_popdown (GTK_MENU (menu));
    }

  return (TRUE);
}


static void
rendgtk_optionmenu_parent (XML_Node * parent_node, XML_Node * child_node)
{
  GtkWidget *parent_optionmenu;
  GtkWidget *menu;

  if (!g_str_equal (child_node->element, "menu"))
    {
      g_warning ("Only <menu>'s can be placed inside of a <optionmenu>.");
      return;
    }

  parent_optionmenu = erend_edata_get (parent_node, "top-widget");
  menu = erend_edata_get (child_node, "bottom-widget");
  if (!menu || !parent_optionmenu)
    return;

  gtk_option_menu_set_menu (GTK_OPTION_MENU (parent_optionmenu), menu);
}

static void
rendgtk_optionmenu_render (XML_Node * node)
{
  GtkWidget *optionmenu;

  /* Get attributes and do what ever rendering needed */

  optionmenu = gtk_option_menu_new ();

  rendgtk_show_cond (node, optionmenu);

  erend_edata_set (node, "top-widget", optionmenu);
  erend_edata_set (node, "bottom-widget", optionmenu);

  xml_node_set_all_attr (node);
}


static void
rendgtk_menubar_render (XML_Node * node)
{
  GtkWidget *menubar;

  /* Get attributes and do what ever rendering needed */

  menubar = gtk_menu_bar_new ();

  rendgtk_show_cond (node, menubar);

  erend_edata_set (node, "top-widget", menubar);
  erend_edata_set (node, "bottom-widget", menubar);
  xml_node_set_all_attr (node);
}

static void
rendgtk_menubar_parent (XML_Node * parent_node, XML_Node * child_node)
{
  GtkWidget *parent_menubar;
  GtkWidget *menu;

  if (!g_str_equal (child_node->element, "menu"))
    {
      g_warning ("Only <menu>'s can be placed inside of a <menubar>.");
      return;
    }

  parent_menubar = erend_edata_get (parent_node, "top-widget");
  menu = erend_edata_get (child_node, "top-widget");
  if (!menu || !parent_menubar)
    return;

  gtk_menu_bar_append (GTK_MENU_BAR (parent_menubar), menu);
}

static void
rendgtk_menu_parent (XML_Node * parent_node, XML_Node * child_node)
{
  GtkWidget *parent_menu;
  GtkWidget *menuitem;

  parent_menu = erend_edata_get (parent_node, "bottom-widget");
  menuitem = erend_edata_get (child_node, "top-widget");

  if (!parent_menu || !menuitem)
    return;

  if ((g_str_equal (child_node->element, "menuitem")) ||
      (g_str_equal (child_node->element, "menu")))
    {
      gtk_menu_append (GTK_MENU (parent_menu), menuitem);
    }
  else
    g_warning
      ("Only <menu>'s or <menuitem>'s can be placed inside of a <menu>.");

}

static void
rendgtk_menu_render (XML_Node * node)
{
  GtkWidget *menu;
  GtkWidget *menu_top;
  gchar *text;

  /* Get attributes and do what ever rendering needed */

  menu = gtk_menu_new ();

  text = xml_node_get_attr_str (node, "label");

  /* This is the wrong way to do this! Do this in the parenter. FIXME */
  if (text)
    {
      menu_top = gtk_menu_item_new_with_label (text);
      gtk_menu_item_set_submenu (GTK_MENU_ITEM (menu_top), menu);
      rendgtk_show_cond (node, menu_top);
      erend_edata_set (node, "top-widget", menu_top);
    }
  else
    {
      erend_edata_set (node, "top-widget", menu);
    }

  erend_edata_set (node, "bottom-widget", menu);

  xml_node_set_all_attr (node);
}

static void
rendgtk_menuitem_render (XML_Node * node)
{
  GtkWidget *menuitem;
  GtkWidget *hbox;

  menuitem = gtk_menu_item_new ();

  gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
		      (GtkSignalFunc) rendgtk_menuitem_selected, node);
  
  rendgtk_show_cond (node, menuitem);
  
  hbox = gtk_hbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (menuitem), hbox);
  gtk_widget_show (hbox);
  
  erend_edata_set (node, "top-widget", menuitem);
  erend_edata_set (node, "bottom-widget", hbox);

  xml_node_set_all_attr (node);
}


void
menu_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  /* menubar */
  element = g_malloc0 (sizeof (Element));
  element->render_func = rendgtk_menubar_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_menubar_parent;
  element->tag = "menubar";
  element_register (element);
  rendgtk_widget_attr_register (element, GTK_TYPE_MENU_BAR);


  /* optionmenu */
  element = g_malloc0 (sizeof (Element));
  element->render_func = rendgtk_optionmenu_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_optionmenu_parent;
  element->tag = "optionmenu";
  element_register (element);
  rendgtk_widget_attr_register (element, GTK_TYPE_OPTION_MENU);

  /* menu */
  element = g_malloc0 (sizeof (Element));
  element->render_func = rendgtk_menu_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_menu_parent;
  element->tag = "menu";
  element_register (element);
  rendgtk_widget_attr_register (element, GTK_TYPE_MENU);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "label";
  e_attr->description = "Set the label for the menu.";
  e_attr->value_desc = "string";
  e_attr->possible_values = NULL;
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "popup";
  e_attr->description = "Popup the menu.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "true,false";
  e_attr->set_attr_func = rendgtk_menu_popup_attr_set;
  element_register_attr (element, e_attr);

  /* menu item */
  element = g_malloc0 (sizeof (Element));
  element->render_func = rendgtk_menuitem_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_box_pack;
  element->tag = "menuitem";
  element_register (element);
  
  rendgtk_widget_attr_register (element, GTK_TYPE_MENU_ITEM);
  rendgtk_containerbox_attr_register (element);
}
