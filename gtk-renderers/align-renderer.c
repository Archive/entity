#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "gtk-widget-attr.h"
#include "edebug.h"
#include "erend.h"

/*--element

Element Name: <halign> and <valign>

If you are familiar with GTK programming, these simply create an hbox
or vbox respectively.  This essentially aligns all elements inside it
in a horizontal or vertical manner.  Any children which are to be
placed into an alignment element, may define the "fill" or "expand"
(boolean true/false) attributes to specify whether or not the widget
space should expand if given the opportunity, and if the widget itself
should grow to fill that space.

%widget%

*/


static void
rendgtk_hbox_render (XML_Node * node)
{
  GtkWidget *widget;

  widget = gtk_hbox_new (FALSE, 0);

  erend_edata_set (node, "top-widget", widget);
  erend_edata_set (node, "bottom-widget", widget);

  rendgtk_show_cond (node, widget);

  /* Make it so you can drop other apps into boxes */
  rendgtk_dnd_dragtag_target_create (node, widget);

  xml_node_set_all_attr (node);
}


static void
rendgtk_vbox_render (XML_Node * node)
{
  GtkWidget *widget;

  widget = gtk_vbox_new (FALSE, 0);

  erend_edata_set (node, "top-widget", widget);
  erend_edata_set (node, "bottom-widget", widget);

  rendgtk_show_cond (node, widget);

  /* Make it so you can drop other apps into boxes */
  rendgtk_dnd_dragtag_target_create (node, widget);

  xml_node_set_all_attr (node);
}


static void
rendgtk_align_box_pack (XML_Node * parent_node, XML_Node * child_node)
{
  gint fill = BOX_PACK_FILL_DEFAULT;
  EBuf *fillv;
  gint expand = BOX_PACK_EXPAND_DEFAULT;
  EBuf *expandv;
  gint padding = BOX_PACK_PADDING_DEFAULT;
  EBuf *paddingv;
  GtkWidget *child;
  GtkWidget *parent;

  child = erend_edata_get (child_node, "top-widget");
  parent = erend_edata_get (parent_node, "bottom-widget");

  if (!child || !parent)
    return;
  /* FIXME: Need to use new interface */
  expandv = xml_node_get_attr (child_node, "expand");
  if (expandv)
    expand = erend_value_is_true (expandv);

  fillv = xml_node_get_attr (child_node, "fill");
  if (fillv)
    fill = erend_value_is_true (fillv);

  paddingv = xml_node_get_attr (child_node, "padding");
  if (paddingv)
    padding = erend_get_integer (paddingv);

  gtk_box_pack_start (GTK_BOX (parent), child, expand, fill, padding);
}


void
rendgtk_align_boxpack_child_attr_set (XML_Node * parent_node,
				      XML_Node * child_node,
				      EBuf *attr, EBuf *value)
{
  EBuf *expandv;
  gint expand = BOX_PACK_EXPAND_DEFAULT;
  EBuf *fillv;
  gint fill = BOX_PACK_FILL_DEFAULT;
  EBuf *paddingv;
  guint padding = BOX_PACK_PADDING_DEFAULT;
  GtkWidget *box;
  GtkWidget *child_widget;

  box = erend_edata_get (parent_node, "bottom-widget");
  child_widget = erend_edata_get (child_node, "top-widget");

  if (!box || !child_widget)
    return;

  expandv = xml_node_get_attr (child_node, "expand");
  if (expandv)
    expand = erend_value_is_true (expandv);

  fillv = xml_node_get_attr (child_node, "fill");
  if (fillv)
    fill = erend_value_is_true (fillv);

  paddingv = xml_node_get_attr (child_node, "padding");
  if (paddingv)
    padding = erend_get_integer (paddingv);

  gtk_box_set_child_packing (GTK_BOX (box), child_widget,
			     expand, fill, padding, GTK_PACK_START);
}


void
align_renderer_init (void)
{
  Element *velement;
  Element *helement;
  ElementAttr *e_attr;

  /* vailgn */
  velement = g_malloc0 (sizeof (Element));
  velement->render_func = rendgtk_vbox_render;
  velement->destroy_func = rendgtk_element_destroy;
  velement->parent_func = rendgtk_align_box_pack;
  velement->tag = "valign";
  element_register (velement);

  /* halign */
  helement = g_malloc0 (sizeof (Element));
  helement->render_func = rendgtk_hbox_render;
  helement->destroy_func = rendgtk_element_destroy;
  helement->parent_func = rendgtk_align_box_pack;
  helement->tag = "halign";
  element_register (helement);

  rendgtk_widget_attr_register (helement, GTK_TYPE_BOX);
  rendgtk_widget_attr_register (velement, GTK_TYPE_BOX);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "expand";
  e_attr->description =
    "Toggle whether the widgets area should 'expand' if given space to do so.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "true,false";
  e_attr->set_child_attr_func = rendgtk_align_boxpack_child_attr_set;
  element_register_child_attr (helement, e_attr);
  element_register_child_attr (velement, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "fill";
  e_attr->description =
    "Toggle whether the widget itself should 'fill' any extra space given to it";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "false,true";
  e_attr->set_child_attr_func = rendgtk_align_boxpack_child_attr_set;
  element_register_child_attr (velement, e_attr);
  element_register_child_attr (helement, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "padding";
  e_attr->description = "Amount of padding in pixels to place around widget";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "0,*";
  e_attr->set_child_attr_func = rendgtk_align_boxpack_child_attr_set;
  element_register_child_attr (velement, e_attr);
  element_register_child_attr (helement, e_attr);
}
