#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "gtk-widget-attr.h"
#include "edebug.h"
#include "erend.h"

/*--element

Element Name: <fixed>

This creates a container widget that allows you to specify absolute X/Y
coordinates of the widgets placed in it.  Any elements that are children
of a fixed, may use the "x-fixed" and "y-fixed" attributes specifying
an integer value of x/y coordinates (in pixels).

%widget%

*/



static void
rendgtk_fixed_render (XML_Node * node)
{
  GtkWidget *fixed;

  fixed = gtk_fixed_new ();

  erend_edata_set (node, "top-widget", fixed);
  erend_edata_set (node, "bottom-widget", fixed);

  rendgtk_show_cond (node, fixed);

  /* We may want to enable this in the future, but it should be able to figure
     out x/y coords for drop. */
  /* rendgtk_dnd_dragtag_target_create (node, widget); */

  xml_node_set_all_attr (node);
}


static void
rendgtk_fixed_parent (XML_Node * parent_node, XML_Node * child_node)
{
  EBuf *value;
  gint x = 0;
  gint y = 0;
  GtkWidget *child;
  GtkWidget *parent;

  child = erend_edata_get (child_node, "top-widget");
  parent = erend_edata_get (parent_node, "bottom-widget");

  if (!child || !parent)
    return;

  value = xml_node_get_attr (child_node, "x-fixed");
  if (value)
    x = erend_get_integer (value);

  value = xml_node_get_attr (child_node, "y-fixed");
  if (value)
    y = erend_get_integer (value);

  gtk_fixed_put (GTK_FIXED (parent), child, x, y);
}


void
rendgtk_fixed_child_attr_set (XML_Node * parent_node, XML_Node * child_node,
			      EBuf * attr, EBuf * value)
{
  EBuf *xv;
  EBuf *yv;
  gint x, y;
  GtkWidget *child_widget;
  GtkWidget *fixed;

  fixed = erend_edata_get (parent_node, "bottom-widget");
  child_widget = erend_edata_get (child_node, "top-widget");

  if (!fixed || !child_widget)
    return;

  edebug ("fixed-renderer", "child_attr_set for fixed.  attr/val %s/%s", attr,
	  value);

  xv = xml_node_get_attr (child_node, "x-fixed");
  if (xv)
    x = erend_get_integer (xv);
  else
    x = -1;

  yv = xml_node_get_attr (child_node, "y-fixed");
  if (yv)
    y = erend_get_integer (yv);
  else
    y = -1;

  gtk_fixed_move (GTK_FIXED (fixed), child_widget, x, y);
}



void
fixed_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  /* vailgn */
  element = g_malloc0 (sizeof (Element));
  element->render_func = rendgtk_fixed_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_fixed_parent;
  element->tag = "fixed";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "x-fixed";
  e_attr->description = "Horizontal position of widget in fixed.";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "0,*";
  e_attr->set_child_attr_func = rendgtk_fixed_child_attr_set;
  element_register_child_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "y-fixed";
  e_attr->description = "Vertical position of widget in fixed.";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "0,*";
  e_attr->set_child_attr_func = rendgtk_fixed_child_attr_set;
  element_register_child_attr (element, e_attr);

  rendgtk_widget_attr_register (element, GTK_TYPE_FIXED);
}
