#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "erend.h"
#include "gtk-widget-attr.h"

/*--element

Element Name: <label>

Creates a text label in the appropriate place.

%widget%

*/

static gint
rendgtk_label_set_text_attr (XML_Node * node, EBuf *attr, EBuf *value)
{
  GtkWidget *label;

  label = erend_edata_get (node, "top-widget");
  if (!label)
    return (TRUE);

  gtk_label_set_text (GTK_LABEL (label), value->str);
  return (TRUE);
}

static gint
rendgtk_label_set_justify_attr (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *label;

  label = erend_edata_get (node, "top-widget");
  
  if (!label)
    return (TRUE);

  if (erend_value_equal (value, "left"))
    {
      gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
      return (TRUE);
    }

  if (erend_value_equal (value, "right"))
    {
      gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_RIGHT);
      return (TRUE);
    }

  if (erend_value_equal (value, "fill"))
    {
      gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_FILL);
      return (TRUE);
    }

  /* center is the catchall/default */
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_CENTER);
  return (TRUE);
}


static void
rendgtk_label_render (XML_Node * node)
{
  GtkWidget *label;
  gchar *text = NULL;

  text = xml_node_get_attr_str (node, "text");

  label = gtk_label_new (text);

  erend_edata_set (node, "top-widget", label);
  erend_edata_set (node, "bottom-widget", label);

  rendgtk_show_cond (node, label);

  xml_node_set_all_attr (node);
}


void
label_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  /* labels */
  element = g_malloc0 (sizeof (Element));
  element->render_func = rendgtk_label_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = NULL;
  element->tag = "label";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "text";
  e_attr->description = "Set label text.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = rendgtk_label_set_text_attr;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "justify";
  e_attr->description = "Set label justification.";
  e_attr->value_desc = "choice";
  e_attr->possible_values = "center,right,left,fill";
  e_attr->set_attr_func = rendgtk_label_set_justify_attr;
  element_register_attr (element, e_attr);

  rendgtk_widget_attr_register (element, GTK_TYPE_LABEL);
}
