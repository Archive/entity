#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "gtk-widget-attr.h"
#include "erend.h"
#include "edebug.h"

/*THIS RENDERER IS BROKEN.  WE PLAN ON GETTING A NEW ONE.*/



static void
rendgtk_filesel_onselect_callback (GtkWidget * widget, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;
  GtkWidget *filesel;
  gchar *file;


  function = xml_node_get_attr_str (node, "onselect");

  if (function)
    {
      filesel = erend_edata_get (node, "top-widget");
      if (!filesel)
	return;

      gtk_widget_hide (GTK_WIDGET (filesel));
      xml_node_set_attr_str_quiet (node, "visible", "false");

      /* g_print ("calling function %s\n", function); */
      file = gtk_file_selection_get_filename (GTK_FILE_SELECTION (filesel));
      lang_call_function (node, function, "ns", node, file);
    }
}

static void
rendgtk_filesel_cancel_callback (GtkWidget * widget, gpointer user_data)
{
  XML_Node *node = user_data;
  GtkWidget *filesel;

  filesel = erend_edata_get (node, "top-widget");
  if (!filesel)
    return;

  gtk_widget_hide (GTK_WIDGET (filesel));
  xml_node_set_attr_str_quiet (node, "visible", "false");
}


static gint
rendgtk_filesel_title_attr_set (XML_Node * node, EBuf *attr, EBuf *value)
{
  GtkWidget *filesel;

  filesel = erend_edata_get (node, "top-widget");
  if (!filesel)
    return FALSE;

  gtk_window_set_title (GTK_WINDOW (filesel), value->str);
  return (TRUE);
}

/*
static gint
rendgtk_filesel_selection_attr_set (XML_Node * node, gchar * attr,
				    gchar * value)
{
  GtkWidget *filesel;

  filesel = erend_edata_get (node, "top-widget");
  if (!filesel)
    return FALSE;

  if (!g_strcasecmp (value, "multiple"))
    {
      gtk_clist_set_selection_mode
	(GTK_CLIST (GTK_FILE_SELECTION (filesel)->file_list),
	 GTK_SELECTION_EXTENDED);
    }
  return (TRUE);
}
*/


static void
rendgtk_filesel_render (XML_Node * node)
{
  GtkWidget *filesel;

  filesel = gtk_file_selection_new ("Select");

  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (filesel)->ok_button),
		      "clicked",
		      GTK_SIGNAL_FUNC (rendgtk_filesel_onselect_callback),
		      node);

  gtk_signal_connect (GTK_OBJECT
		      (GTK_FILE_SELECTION (filesel)->cancel_button),
		      "clicked",
		      GTK_SIGNAL_FUNC (rendgtk_filesel_cancel_callback),
		      node);

  erend_edata_set (node, "top-widget", filesel);
  erend_edata_set (node, "bottom-widget", filesel);

  xml_node_set_all_attr (node);
  rendgtk_show_cond (node, filesel);
}


void
filesel_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  /* filesel */
  element = g_new0 (Element, 1);

  element->render_func = rendgtk_filesel_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = NULL;
  element->tag = "filesel";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "title";
  e_attr->description = "Set title and wmclass of window.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = rendgtk_filesel_title_attr_set;
  element_register_attr (element, e_attr);
  
  rendgtk_widget_attr_register (element, GTK_TYPE_FILE_SELECTION);
}
