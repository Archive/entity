#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "perl-embed.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "erend.h"


/*--element

Element Name: <object>

The <object> element does several things.

* It defines a new namespace for any embedded code it contains, thereby
keeping it isolated (relatively) from other <object>'s in the same file,
or in different files.

* Any references used through the Perl API to search for a node by
name will use the parent <object> as a starting point (unless otherwise
specified).

It should be noted that this element does not need to be used as a
widget.  It is only rendered to the screen if it is itself within
another rendered element.  

You should never set the "name" attribute of an <object>.  They must
always be unique, and the only way to ensure this, is to allow the
system to name it.

The current convention is to set the "title" attribute to a
descriptive title.  This helps to identify the application to others.

%widget%

*/

/*--attr

Attribute: "default-lang"

Sets the default language for use in callbacks for tags within its
scope.  eg, setting it to "tcl" allows you to reference tcl functions
in tag attributes without using "tcl::some_func".

Default: "perl"

*/


/* The <object> tag has two forms of being parented.  If it's a object dragable=true
   then it uses box packing, else it shortcurcuits.  This has to be because
   often object isn't used as a widget */
static void
rendgtk_object_parent (XML_Node *parent_node, XML_Node *child_node)
{
  if (parent_node->entity_data)
    rendgtk_box_pack (parent_node, child_node);
  else
    erend_short_curcuit_parent (parent_node, child_node);
}

static void
rendgtk_object_render (XML_Node * node)
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *ehbox;
  GtkWidget *vsep;
  GtkWidget *ebox;
  EBuf *dragable;

  dragable = xml_node_get_attr (node, "dragable");
  
  if (dragable && erend_value_is_true (dragable))
    {
      hbox = gtk_hbox_new (FALSE, 0);

      vbox = gtk_vbox_new (FALSE, 0);
      gtk_widget_show (vbox);

      ebox = gtk_event_box_new ();
      gtk_container_set_border_width (GTK_CONTAINER (ebox), 3);

      ehbox = gtk_hbox_new (TRUE, 1);
      gtk_container_add (GTK_CONTAINER (ebox), ehbox);
      vsep = gtk_vseparator_new ();
      gtk_box_pack_start (GTK_BOX (ehbox), vsep, TRUE, TRUE, 0);
      vsep = gtk_vseparator_new ();
      gtk_box_pack_start (GTK_BOX (ehbox), vsep, TRUE, TRUE, 0);
      gtk_widget_show_all (ebox);

      gtk_box_pack_start (GTK_BOX (hbox), ebox, FALSE, FALSE, 0);
      gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 0);

      erend_edata_set (node, "top-widget", hbox);
      erend_edata_set (node, "bottom-widget", vbox);

      xml_node_set_all_attr (node);

      rendgtk_dnd_dragtag_source_create (node, ebox);
      rendgtk_show_cond (node, hbox);
    }
  else
    {
      node->entity_data = NULL;
    }
}

void
object_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  /* object */
  element = g_new0 (Element, 1);
  element->render_func = rendgtk_object_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_object_parent;
  element->tag = "object";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "dragable";
  e_attr->description = "Toggle dragable status of application object.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "false,true";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  /* rendgtk_widget_attr_register (element, GTK_TYPE_HBOX); */
}
