#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "edebug.h"
#include <stdio.h>
#include "gtk-widget-attr.h"
#include "erend.h"


static gint
rendgtk_scrollwindow_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *sw;
  GtkAdjustment *x_adj;
  GtkAdjustment *y_adj;
  int page;

  sw = erend_edata_get (node, "top-widget");
  if (!sw)
    return FALSE;

  if (erend_value_equal (attr, "x-scroll"))
    {
      edebug ("scrollwindow-renderer", "setting x-scroll");
      x_adj = gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (sw));

      page = x_adj->upper - x_adj->page_size;
      x_adj->value = page * erend_get_integer (value) / 100;
      edebug ("scrollwindow-renderer", "page %i", page);
      gtk_adjustment_value_changed (x_adj);
      return TRUE;
    }
  else if (erend_value_equal (attr, "y-scroll"))
    {
      edebug ("scrollwindow-renderer", "setting y-scroll");
      y_adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (sw));
      page = y_adj->upper - y_adj->page_size;
      y_adj->value = page * erend_get_integer (value) / 100;
      edebug ("scrollwindow-renderer", "page %i", page);
      gtk_adjustment_value_changed (y_adj);
      return TRUE;
    }

  return FALSE;
}

static int
x_adj_changed (GtkAdjustment * adjustment, XML_Node * node)
{
  char val_str[15];
  int value;
  float scw;
  GtkWidget *sw;
  GtkAdjustment *x_adj;
  char *function;

  sw = erend_edata_get (node, "top-widget");
  if (!sw)
    return FALSE;

  x_adj = gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (sw));

  scw = (x_adj->upper - x_adj->page_size);
  if (scw)
    value = (x_adj->value * 100) / scw;
  else
    value = 100;

  sprintf (val_str, "%i", value);

  xml_node_set_attr_str_quiet (node, "x-scroll", val_str);

  function = xml_node_get_attr_str (node, "onx-scrolled");
  lang_call_function (node, function, "n", node);
  return FALSE;
}

static int
y_adj_changed (GtkAdjustment * adjustment, XML_Node * node)
{
  char val_str[15];
  int value;
  float scw;
  GtkWidget *sw;
  GtkAdjustment *y_adj;
  char *function;

  sw = erend_edata_get (node, "top-widget");
  if (!sw)
    return FALSE;

  y_adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (sw));

  scw = (y_adj->upper - y_adj->page_size);
  if (scw)
    value = (y_adj->value * 100) / scw;
  else
    value = 100;

  sprintf (val_str, "%i", value);

  xml_node_set_attr_str_quiet (node, "y-scroll", val_str);

  function = xml_node_get_attr_str (node, "ony-scrolled");
  lang_call_function (node, function, "n", node);
  return FALSE;
}

static void
rendgtk_scrollwindow_render (XML_Node * node)
{
  GtkWidget *sw;
  GtkAdjustment *x_adj;
  GtkAdjustment *y_adj;

  sw = gtk_scrolled_window_new (NULL, NULL);


  x_adj = gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (sw));
  y_adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (sw));

  erend_edata_set (node, "top-widget", sw);
  erend_edata_set (node, "bottom-widget", sw);

  gtk_signal_connect (GTK_OBJECT (y_adj), "value-changed",
		      GTK_SIGNAL_FUNC (y_adj_changed), node);
  gtk_signal_connect (GTK_OBJECT (x_adj), "value-changed",
		      GTK_SIGNAL_FUNC (x_adj_changed), node);

  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW (sw), 
                                 GTK_POLICY_AUTOMATIC,
                                 GTK_POLICY_AUTOMATIC);
  xml_node_set_all_attr (node);
  rendgtk_show_cond (node, sw);
}

static void
rendgtk_scrollwindow_parenter (XML_Node* parent_node, XML_Node* child_node)
{
  GtkWidget* sw;
  GtkWidget* child;


  sw = erend_edata_get(parent_node, "top-widget");
  if (!sw) return;

  child = erend_edata_get(child_node, "top-widget");
  if (!child) return;

  if ( GTK_IS_TEXT(child) )
    {
      gtk_container_add (GTK_CONTAINER (sw), child);
    }
  else
    {
      gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(sw), child);
    }
}

void
scrollwindow_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);
  element->render_func = rendgtk_scrollwindow_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_scrollwindow_parenter;
  element->tag = "scrollwindow";
  element_register (element);
  element->tag = "scrollwin";
  element_register (element);

  rendgtk_widget_attr_register (element, GTK_TYPE_SCROLLED_WINDOW);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "x-scroll";
  e_attr->description =
    "The percentage of the window that is scrolled in the x direction";
  e_attr->value_desc = "percentage";
  e_attr->set_attr_func = rendgtk_scrollwindow_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "y-scroll";
  e_attr->description =
    "The percentage of the window that is scrolled in the y direction";
  e_attr->value_desc = "percentage";
  e_attr->set_attr_func = rendgtk_scrollwindow_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onx-scroll";
  e_attr->description =
    "Function to call when the view is changed in the x direction";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "ony-scroll";
  e_attr->description =
    "Function to call when the view is changed in the y direction";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);
}
