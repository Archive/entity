#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "entity.h"
#include "renderers.h"
#include <stdio.h>
#include "gtk-widget-attr.h"
#include "erend.h"


/*
static gint
rendgtk_pane_attr_set (XML_Node * node, gchar * attr, gchar * value);

static gint
rendgtk_pane_attr_set (XML_Node * node, gchar * attr, gchar * value)
{
  GtkWidget *pane=NULL;
  return TRUE;
}
*/

/*--element

Element Name: <[vh]pane>

created a resizeable paned area.

%widget%

*/

static void
rendgtk_pane_parenter (XML_Node * parent_node, XML_Node * child_node)
{
  GtkWidget *pane;
  GtkWidget *widget;
  int len, loc;
  GSList *child;

  pane = erend_edata_get (parent_node, "top-widget");
  widget = erend_edata_get (child_node, "top-widget");
  if (!pane || !widget)
    return;

  len = g_slist_length (parent_node->children);
  if (len > 2)
    {
      g_warning ("Panes should only have two sub tages.");
      return;
    }

  /*Find the location of child in the xml. */
  child = g_slist_find (parent_node->children, child_node);
  loc = len - g_slist_length (child);

  if (0 == loc)
    {
      gtk_paned_add1 (GTK_PANED (pane), widget);
    }
  else
    {
      gtk_paned_add2 (GTK_PANED (pane), widget);
    }

}

static void
rendgtk_pane_render (XML_Node * node)
{
  GtkWidget *pane;		/*The new pane. */

  if (g_str_equal (node->element, "vpane"))
    pane = gtk_vpaned_new ();
  else if (g_str_equal (node->element, "hpane"))
    pane = gtk_hpaned_new ();
  else
    pane = gtk_vpaned_new ();

  erend_edata_set (node, "top-widget", pane);
  erend_edata_set (node, "button-widget", pane);

  xml_node_set_all_attr (node);

  rendgtk_show_cond (node, pane);
}

void
pane_renderer_init (void)
{
  Element *element;

  element = g_new0 (Element, 1);

  element->render_func = rendgtk_pane_render;
  element->destroy_func = NULL;
  element->parent_func = rendgtk_pane_parenter;
  element->tag = "vpane";
  element_register (element);
  element->tag = "hpane";
  element_register (element);

  rendgtk_widget_attr_register (element, GTK_TYPE_PANED);
}
