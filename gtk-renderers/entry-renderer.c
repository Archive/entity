#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "edebug.h"
#include "gtk-widget-attr.h"
#include "erend.h"


/*--element

Element Name: <entry>

This creates a small, one line high text area allowing you to enter text.

%widget%

*/

/*--attr

Attribute: "onchange"

If this attribute is set, then whenever a change is made (a key is
pressed) in the entry, the perl function specified as it's value will
be called.  The first argument is the full path to the entry that
generated the event.

Default: none (no action)

*/

static void
rendgtk_entry_onchange_callback (GtkWidget * entry, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;
  gchar *text;

  text = gtk_entry_get_text (GTK_ENTRY (entry));

  xml_node_set_attr_str_quiet (node, "text", text);

  function = xml_node_get_attr_str (node, "onchange");

  if (function)
    {
      lang_call_function (node, function, "n", node);;
    }
}

/*--attr

Attribute: "onenter"

When the "Enter" key is pressed, this attribute will be checked.  If
it is set, the perl function specified as it's value will be called.
The argument is the full path to the entry that generated the event.

Default: none (no action)

*/

static void
rendgtk_entry_onenter_callback (GtkWidget * widget, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;

  function = xml_node_get_attr_str (node, "onenter");

  lang_call_function (node, function, "n", node);
}

static gint
rendgtk_entry_text_attr_set (XML_Node * node, EBuf *attr, EBuf *value)
{
  GtkWidget *entry;

  entry = erend_edata_get (node, "top-widget");
  if (!entry)
    return (TRUE);

  gtk_entry_set_text (GTK_ENTRY (entry), value->str);

  return TRUE;
}

static gint
rendgtk_widget_entry_hidden_attr_set (XML_Node * node, EBuf *attr,
				      EBuf *value)
{
  GtkWidget *entry;

  entry = erend_edata_get (node, "top-widget");
  if (!entry)
    return (TRUE);

  gtk_entry_set_visibility (GTK_ENTRY (entry), !erend_value_is_true (value));
  return (TRUE);
}


static void
rendgtk_entry_render (XML_Node * node)
{
  GtkWidget *entry;

  entry = gtk_entry_new ();

  erend_edata_set (node, "top-widget", entry);
  erend_edata_set (node, "bottom-widget", entry);

  xml_node_set_all_attr (node);

  gtk_signal_connect (GTK_OBJECT (entry), "changed",
		      rendgtk_entry_onchange_callback, node);

  gtk_signal_connect (GTK_OBJECT (entry), "activate",
		      rendgtk_entry_onenter_callback, node);

  rendgtk_show_cond (node, entry);
}

void
entry_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  /* clist */
  element = g_malloc0 (sizeof (Element));
  element->render_func = rendgtk_entry_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_box_pack;
  element->tag = "entry";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "hidden";
  e_attr->description = "Set if the widget should echo the text.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "false,true";
  e_attr->set_attr_func = rendgtk_widget_entry_hidden_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "text";
  e_attr->description = "Text displayed in the widget.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = rendgtk_entry_text_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onenter";
  e_attr->description =
    "Sets the function to call when the enter is pressed.";
  e_attr->value_desc = "function";
  /*e_attr->possible_values = ""; */
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  rendgtk_widget_attr_register (element, GTK_TYPE_ENTRY);
}
