#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "entity.h"
#include "gtk-common.h"
#include <gtkdatabox.h>

typedef struct
{
  GtkWidget *databox;
  gint index;
  gint size;
  gfloat x;
  gfloat y;
}
DataPoint;

static int maxundrawn = 1000;
#define TIMEOUT 200

static gint
rendgtk_graph_timeout_callback (gpointer data)
{
  XML_Node *node = data;
  GtkWidget* databox;
  EBuf* value;
  gint redraw;
  
  /* FIXME: Borked */
  value = xml_node_get_attr (node, "frozen");
  if (erend_value_is_true (value))
    return (TRUE);

  redraw = (gint) erend_edata_get (node, "rendgtk-graph-redraw");
  erend_edata_set (node, "rendgtk-graph-redraw", NULL);

  if (redraw)
    {
      databox = erend_edata_get(node, "top-widget");
      if (databox)
        gtk_databox_redraw (GTK_DATABOX (databox));
    }

  return (TRUE);
}


static void
rendgtk_databox_redraw_maybe (XML_Node * graph_point_node,
			      GtkWidget * databox)
{
  XML_Node *node;
  EBuf *value;
  gint redraw;

  if (!databox)
    return;

  node = xml_node_parent (graph_point_node);
  value = xml_node_get_attr (node, "frozen");

  if ( !erend_value_is_true (value) )
    {
      redraw = (int)erend_edata_get(node, "rendgtk-graph-redraw");
      redraw++;
      erend_edata_set(node, "rendgtk-graph-redraw", (void*)redraw);

      if (redraw > maxundrawn)
        gtk_databox_redraw (GTK_DATABOX (databox));
    }
}

static gint
rendgtk_databox_graph_attr_set (XML_Node * node, EBuf *attr, EBuf *value)
{
  GtkWidget *databox;
  DataPoint* point;

  point = erend_edata_get(node, "rendgtk-databox-point");
  if (!point)
    return (FALSE);

  databox = erend_edata_get(node, "top-widget");

  if (erend_value_equal (attr, "frozen"))
    {
      if (erend_value_is_true (value))
	{
	  gtk_databox_redraw (GTK_DATABOX (databox));
	}
      return (TRUE);
    }


  return (TRUE);
}


static void
rendgtk_databox_graph_destroy (XML_Node * node)
{
  int id;

  id = GPOINTER_TO_INT (erend_edata_get (node, "rendgtk-graph-timeout-tag"));
  if (id > 0)
    gtk_timeout_remove (id);

  rendgtk_element_destroy (node);
}

static void
rendgtk_databox_graph_render (XML_Node * node)
{
  GtkWidget *databox;
  int id;

  databox = gtk_databox_new ();
  /*
  gtk_signal_connect (GTK_OBJECT (databox), "destroy",
		      GTK_SIGNAL_FUNC (gtk_databox_data_destroy_all), NULL);
                      */

  erend_edata_set(node, "top-widget", databox);
  erend_edata_set(node, "bottom-widget", databox);

  id = gtk_timeout_add (TIMEOUT, rendgtk_graph_timeout_callback, node);
  erend_edata_set (node, "rendgtk-graph-timeout-tag", GINT_TO_POINTER (id));

  xml_node_set_all_attr (node);

  rendgtk_show_cond (node, databox);
}


static gint
rendgtk_databox_graph_point_attr_set (XML_Node * node, EBuf *attr,
				      EBuf *value)
{
  GtkWidget *databox;
  DataPoint *point;
  GdkColor color;

  point = erend_edata_get(node, "rendgtk-databox-point");
  if (!point)
    return TRUE;

  databox = point->databox;

  if (erend_value_equal (attr, "x"))
    {
      point->x = atof (value->str);
      rendgtk_databox_redraw_maybe (node, databox);
      return (TRUE);
    }

  if (erend_value_equal (attr, "y"))
    {
      point->y = atof (value->str);
      rendgtk_databox_redraw_maybe (node, databox);
      return (TRUE);
    }

  if ((erend_value_equal (attr, "size")) || (erend_value_equal (attr, "width")))
    {
      point->size = atof (value->str);
      rendgtk_databox_redraw_maybe (node, databox);
      return (TRUE);
    }

  if ((erend_value_equal (attr, "color")) || (erend_value_equal (attr, "colour")))
    {
      point->size = atof (value->str);

      if (databox && gdk_color_parse (value->str, &color) )
	{
	  gtk_databox_set_color (GTK_DATABOX (databox), point->index, color);
	}
      /* needed ? */
      /* databox_maybe_redraw (node, databox); */
      return (TRUE);
    }

  return (FALSE);
}

static void
rendgtk_databox_graph_point_render (XML_Node * node)
{
  DataPoint *point;

  point = g_new0 (DataPoint, 1);

  erend_edata_set(node, "rendgtk-databox-point", point);
  erend_edata_set(node, "rendgtk_databox_graph_point_render", "true");

  xml_node_set_all_attr (node);
}

static void
rendgtk_databox_graph_point_parent (XML_Node * parent_node,
				    XML_Node * child_node)
{
  GtkWidget *databox;
  DataPoint *point;
  GdkColor color;
  EBuf *value;
  GtkDataboxDataType graph_type = GTK_DATABOX_BARS;

  point = erend_edata_get(child_node, "rendgtk-databox-point");

  /*g_warning("in rendgtk_databox_graph_point_parent");*/

  /* TODO: Check that the parent is a graph type */
  databox = erend_edata_get(parent_node, "top-widget");

  /* TODO: Check type from parent */

  /* X, Y, size */
  value = xml_node_get_attr (child_node, "x");
  point->x = erend_get_float (value);
  value = xml_node_get_attr (child_node, "y");
  point->y = erend_get_float (value);


  /* Size/width */

  /* default */
  point->size = 5;

  value = xml_node_get_attr (child_node, "size");
  if (!value)
    value = xml_node_get_attr (child_node, "width");

  if (value)
    point->size = erend_get_integer (value);

  if (point->size < 1)
    point->size = 1;

  /* Check color */
  value = xml_node_get_attr (child_node, "color");
  if (!value)
    value = xml_node_get_attr (child_node, "colour");

  if (value)
    {
      if (!gdk_color_parse (value->str, &color))
	{
	  color.red = 0;
	  color.green = 0;
	  color.blue = 0;
	}
    }

  /* Check type */
  value = xml_node_get_attr (child_node, "type");

  if (value)
    {
      if (erend_value_equal (value, "bar"))
	graph_type = GTK_DATABOX_BARS;
      if (erend_value_equal (value, "point"))
	graph_type = GTK_DATABOX_POINTS;
      if (erend_value_equal (value, "line"))
	graph_type = GTK_DATABOX_LINES;
    }

  point->index = gtk_databox_data_add_x_y (GTK_DATABOX (databox), 1,
					   &point->x, &point->y, color,
					   graph_type, point->size);
  gtk_databox_rescale (GTK_DATABOX (databox));

  point->databox = databox;

  edebug ("databox-renderer", "parented point - x %f, y %f, size %d\n",
	  point->x, point->y, point->size);
}

void
databox_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  /* g_warning ("in databox_renderer_init"); */

  element = g_new0(Element, 1);
  element->render_func = rendgtk_databox_graph_render;
  element->destroy_func = rendgtk_databox_graph_destroy; 
  element->parent_func = rendgtk_databox_graph_point_parent;
  element->tag = "graph";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "frozen";
  e_attr->description = "If set, no updates will ocurr on screen.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "false,true"; 
  e_attr->set_attr_func = rendgtk_databox_graph_attr_set;
  element_register_attr (element, e_attr);

  element = g_new0(Element, 1);
  element->render_func = rendgtk_databox_graph_point_render;
  element->parent_func = NULL;
  element->destroy_func = NULL;
  element->tag = "graph-point";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "x";
  e_attr->description = "The x location on the graph.";
  e_attr->value_desc = "integer";
  e_attr->set_attr_func = rendgtk_databox_graph_point_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "y";
  e_attr->description = "The y location on the graph.";
  e_attr->value_desc = "integer";
  e_attr->set_attr_func = rendgtk_databox_graph_point_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "color";
  e_attr->description = "The color of the point.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = rendgtk_databox_graph_point_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "type";
  e_attr->description = "The type of point to display.";
  e_attr->value_desc = "string";
  e_attr->possible_values = "point,bar,line";
  e_attr->set_attr_func = rendgtk_databox_graph_point_attr_set;
  element_register_attr (element, e_attr);

}
