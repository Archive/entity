#include <gtk/gtk.h>
#include <stdio.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "entity.h"
#include "renderers.h"
#include "edebug.h"
#include "erend.h"
#include "gtk-widget-attr.h"


static void
rendgtk_tree_item_onselect_callback (GtkWidget * widget, GtkWidget * item,
				     gpointer user_data)
{
  XML_Node *node = user_data;
  XML_Node *child_node;
  gchar *function = NULL;

  edebug ("tree-renderer", "onselect callback node->name = %s", node->name);

  child_node = gtk_object_get_data (GTK_OBJECT (item), "xml-node");

  if (!child_node)
    return;

  xml_node_set_attr_str_quiet (child_node, "selected", "true");

  /* Find a parent tree that has "onselect" set */
  function = xml_node_find_attr_thru_type (child_node, "tree", "onselect");

  if (function)
    {
      lang_call_function (node, function, "n", child_node);
    }
}

static void
rendgtk_tree_item_expand_callback (GtkWidget * widget, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;

  edebug ("tree-renderer", "expand callback node->name = %s", node->name);

  xml_node_set_attr_str_quiet (node, "expanded", "true");

  /* Find a parent tree that has "onselect" set */
  function = xml_node_find_attr_thru_type (node, "tree", "onexpand");

  if (function)
    {
      lang_call_function (node, function, "n", node);
    }
}

static void
rendgtk_tree_item_collapse_callback (GtkWidget * widget, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;

  edebug ("tree-renderer", "collapse callback node->name = %s", node->name);

  xml_node_set_attr_str_quiet (node, "expanded", "false");

  /* Find a parent tree that has "oncollapse" set */
  function = xml_node_find_attr_thru_type (node, "tree", "oncollapse");

  if (function)
    {
      lang_call_function (node, function, "n", node);
    }
}

static void
rendgtk_tree_item_ondeselect_callback (GtkWidget * widget, GtkWidget * item,
				       gpointer user_data)
{
  XML_Node *node = user_data;
  XML_Node *child;
  gchar *function = NULL;

  edebug ("tree-renderer", "!!!!node->name = %s\n", node->name);

  child = gtk_object_get_data (GTK_OBJECT (item), "xml-node");
  if (!child)
    return;

  xml_node_set_attr_str_quiet (child, "selected", "false");

  /* Recursivily find a tree node with onselect in it. */
  function = xml_node_find_attr_thru_type (child, "tree", "ondeselect");

  if (function)
    {
      lang_call_function (node, function, "n", child);
    }
}

static void
rendgtk_tree_item_onselectchange_callback (GtkWidget * widget,
					   gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;

  function = xml_node_get_attr_str (node, "onselectionchange");

  if (function)
    {
      lang_call_function (node, function, "n", node);
    }
}

static gint
rendgtk_tree_selection_style_attr_set (XML_Node * node, EBuf * attr,
				       EBuf * value)
{
  GtkWidget *tree;
  GtkWidget *tree_item;
  XML_Node *parent;

  parent = xml_node_parent (node);

  if (g_str_equal ("tree", parent->element))
    tree = erend_edata_get (parent, "tree-widget");
  else				/* Are the top tree. */
    tree = erend_edata_get (node, "tree-widget");

  tree_item = erend_edata_get (node, "tree-item-widget");

  if (!tree || !tree_item)
    return FALSE;

  edebug ("tree-renderer", "selection-type set to %s", value);

  if (erend_value_equal (value, "multiple"))
    {
      gtk_tree_set_selection_mode (GTK_TREE (tree), GTK_SELECTION_MULTIPLE);
      return (TRUE);
    }

  if (erend_value_equal (value, "browse"))
    {
      gtk_tree_set_selection_mode (GTK_TREE (tree), GTK_SELECTION_BROWSE);
      return (TRUE);
    }

  if (erend_value_equal (value, "extended"))
    {
      gtk_tree_set_selection_mode (GTK_TREE (tree), GTK_SELECTION_EXTENDED);
      return (TRUE);
    }

  /* Single is default */
  gtk_tree_set_selection_mode (GTK_TREE (tree), GTK_SELECTION_SINGLE);

  return (TRUE);
}


static gint
rendgtk_tree_select_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *tree_item;
  GtkWidget *tree;

  tree_item = erend_edata_get (node, "tree-item-widget");
  tree = erend_edata_get (node, "tree-widget");

  if (!tree_item || !tree)
    return (TRUE);

  if (erend_value_is_true (value))
    gtk_tree_select_child (GTK_TREE (tree), tree_item);
  else
    gtk_tree_unselect_child (GTK_TREE (tree), tree_item);

  return (TRUE);
}

static gint
rendgtk_tree_expanded_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *tree_item;

  tree_item = erend_edata_get (node, "tree-item-widget");

  if (!tree_item)
    return (TRUE);

  if (erend_value_is_true (value))
    gtk_tree_item_expand (GTK_TREE_ITEM (tree_item));
  else
    gtk_tree_item_collapse (GTK_TREE_ITEM (tree_item));

  return (TRUE);
}

void
rendgtk_tree_destroy (XML_Node * node)
{
  GtkWidget *tree_item;

  XML_Node *parent_node;

  edebug ("tree-renderer", "Destroying tree of some sorts..");
  /* Check to see if this is a toplevel tree. */
  parent_node = xml_node_parent (node);
  if (!g_str_equal (parent_node->element, "tree"))
    {
      GtkWidget *widget = erend_edata_get (node, "tree-widget");
      edebug ("tree-renderer", "Destroying top level tree");
      if (widget)
	gtk_widget_destroy (widget);
    }
  else
    {
      /* we are a child of a tree, and we need to remove ourselves
         properly from the tree */
      tree_item = erend_edata_get (node, "tree-item-widget");

      edebug ("tree-renderer", "Destroying tree item");
      /* gtk_tree_item_remove_subtree (GTK_TREE_ITEM (tree_item)); */
      /* I think the above would work if we also got rid of the tree */

      /* Cruelty to dumb trees */
      if (tree_item)
	gtk_widget_hide (tree_item);
    }
}


static void
rendgtk_tree_render (XML_Node * node)
{
  XML_Node *parent;
  GtkWidget *tree;
  GtkWidget *tree_item;
  GtkWidget *hbox;

  /* Check to see if this is a toplevel tree. */
  parent = xml_node_parent (node);
  if (!g_str_equal (parent->element, "tree"))
    {
      tree = gtk_tree_new ();
      erend_edata_set (node, "top-widget", tree);
      erend_edata_set (node, "tree-widget", tree);
      rendgtk_show_cond (node, tree);

      /* Connect signals */
      gtk_signal_connect (GTK_OBJECT (tree), "select-child",
			  rendgtk_tree_item_onselect_callback, node);
      gtk_signal_connect (GTK_OBJECT (tree), "selection-changed",
			  rendgtk_tree_item_onselectchange_callback, node);
      gtk_signal_connect (GTK_OBJECT (tree), "unselect-child",
			  rendgtk_tree_item_ondeselect_callback, node);

      edebug ("tree-renderer", "Creating top level tree");
    }
  else
    {
      /* we are a child tree item, and need to build the appropriate deal 
         to connect to our parent appropriately */
      tree = gtk_tree_new ();
      tree_item = gtk_tree_item_new ();

      /* save the XML_Node for use in the callbacks */
      gtk_object_set_data (GTK_OBJECT (tree_item), "xml-node", node);

      erend_edata_set (node, "top-widget", tree_item);
      erend_edata_set (node, "tree-widget", tree);
      erend_edata_set (node, "tree-item-widget", tree_item);

      /* This is where labels and buttons and thigs get added. */
      hbox = gtk_hbox_new (FALSE, 0);
      gtk_container_add (GTK_CONTAINER (tree_item), hbox);
      erend_edata_set (node, "bottom-widget", hbox);

      edebug ("tree-renderer", "Created new tree item and packed in box");

      /* connect all signals now. */
      gtk_signal_connect (GTK_OBJECT (tree), "select-child",
			  rendgtk_tree_item_onselect_callback, node);
      gtk_signal_connect (GTK_OBJECT (tree), "selection-changed",
			  rendgtk_tree_item_onselectchange_callback, node);
      gtk_signal_connect (GTK_OBJECT (tree), "unselect-child",
			  rendgtk_tree_item_ondeselect_callback, node);
      gtk_signal_connect (GTK_OBJECT (tree_item), "expand",
			  rendgtk_tree_item_expand_callback, node);
      gtk_signal_connect (GTK_OBJECT (tree_item), "collapse",
			  rendgtk_tree_item_collapse_callback, node);

      gtk_widget_show (hbox);
      gtk_widget_show (tree_item);
      gtk_widget_show (tree);
    }

  xml_node_set_all_attr (node);
}

/*
static gint
rendgtk_num_children_of_type (XML_Node * node, char *element)
{
  GSList *list;
  XML_Node *child;
  int ret = 0;

  if (!node)
    return 0;

  for (list = node->children; list; list = list->next)
    {
      child = (XML_Node *) list->data;
      if (g_str_equal (child->element, element))
	ret++;
    }
  return ret;
}
*/

static void
rendgtk_tree_parenter (XML_Node * parent_node, XML_Node * child_node)
{
  GtkWidget *parent_tree;
  GtkWidget *child_tree;
  GtkWidget *child_tree_item;

  /* first thing to do is to see if we're packing a tree or a misc. widget. */
  if (!g_str_equal (child_node->element, "tree"))
    {
      edebug ("tree-renderer", "Using box pack to pack %s", child_node->name);
      rendgtk_box_pack (parent_node, child_node);
      return;
    }

  /* we must be placing a tree inside a tree then. */
  parent_tree = erend_edata_get (parent_node, "tree-widget");

  child_tree = erend_edata_get (child_node, "tree-widget");
  child_tree_item = erend_edata_get (child_node, "tree-item-widget");

  if (!parent_tree || !child_tree || !child_tree_item)
    return;

  gtk_tree_append (GTK_TREE (parent_tree), child_tree_item);
  gtk_tree_item_set_subtree (GTK_TREE_ITEM (child_tree_item), child_tree);
}

void
tree_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);
  element->render_func = rendgtk_tree_render;
  element->destroy_func = rendgtk_tree_destroy;
  element->parent_func = rendgtk_tree_parenter;
  element->tag = "tree";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "selected";
  e_attr->description = "Item is highlighted or not.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "true,false";
  e_attr->set_attr_func = rendgtk_tree_select_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "expanded";
  e_attr->description = "Tree item is expanded or not.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "false,true";
  e_attr->set_attr_func = rendgtk_tree_expanded_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "selection-style";
  e_attr->description = "The mode of the selection.";
  e_attr->value_desc = "string";
  e_attr->possible_values = "single,multiple,browse,extended";
  e_attr->set_attr_func = rendgtk_tree_selection_style_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onselect";
  e_attr->description =
    "Specify function to call when a sub item is 'selected'.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "ondeselect";
  e_attr->description =
    "Specify function to call when a sub item is 'deselected'.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onexpand";
  e_attr->description =
    "Specify function to call when a tree item is expanded.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  rendgtk_widget_attr_register (element, GTK_TYPE_TREE_ITEM);
  rendgtk_containerbox_attr_register (element);
}
