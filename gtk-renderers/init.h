#ifndef _INIT_H_
#define _INIT_H_

void align_renderer_init (void);
void object_renderer_init (void);
void button_renderer_init (void);
void checkbox_renderer_init (void);
void clist_renderer_init (void);
void databox_renderer_init (void);
void dropdown_renderer_init (void);
void databox_renderer_init (void);
void ebox_renderer_init (void);
void entry_renderer_init (void);
void filesel_renderer_init (void);
void fixed_renderer_init (void);
void frame_renderer_init (void);
void image_renderer_init (void);
void io_renderer_init (void);
void label_renderer_init (void);
void list_renderer_init (void);
void menu_renderer_init (void);
void notebook_renderer_init (void);
void pane_renderer_init (void);
void progress_renderer_init (void);
void raw_io_renderer_init (void);
void radio_renderer_init (void);
void scrollwindow_renderer_init (void);
void spinner_renderer_init (void);
void text_renderer_init (void);
void timer_renderer_init (void);
void tree_renderer_init (void);
void window_renderer_init (void);
void wrapalign_renderer_init (void);


#endif /*_INIT_H_*/
