#include <gtk/gtk.h>
#include "elements.h"
#include "xml-node.h"
#include "gtk-common.h"
#include "edebug.h"
#include "erend.h"
#include "lang.h"

static void
button_press_event_callback (GtkWidget * widget,
			     GdkEventButton * event, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;

  function = xml_node_get_attr_str (node, "onbuttonpress");

  edebug ("gtk-common", "buttonpress in %s", node->name);

  /* Arguments will be: node path, button number, x pos, y pos. 
     Other possibles include xtilt, ytilt, and pressure. */
  lang_call_function (node, function, "niii",
		      node, event->button, event->x, event->y);
}

static void
button_release_event_callback (GtkWidget * widget,
			       GdkEventButton * event, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;

  function = xml_node_get_attr_str (node, "onbuttonrelease");

  edebug ("gtk-common", "buttonrelease in %s", node->name);

  /* Arguments will be: node path, button number, x pos, y pos. 
     Other possibles include xtilt, ytilt, and pressure. */
  /* g_print ("executing perl function %s\n", function); */

  lang_call_function (node, function, "niii",
		      node, event->button, event->x, event->y);
}

static void
mousemotion_event_callback (GtkWidget * widget,
			    GdkEventMotion * event, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;

  function = xml_node_get_attr_str (node, "onmousemotion");

  edebug ("gtk-common", "mousemotion event in %s", node->name);

  /* Arguments will be: node path, x pos, y pos. */
  /* note: event->button is not valid for motion events */

  lang_call_function (node, function, "nii", node, event->x, event->y);
}

static gint
keypress_event_callback (GtkWidget * widget,
			 GdkEventKey * event, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;
  EBuf* ret;

  function = xml_node_get_attr_str (node, "onkeypress");

  edebug ("gtk-widget-attr", "keypress event in %s", node->name->str);

  /* Arguments will be: node path, key number, key string */
  ret = lang_call_function (node, function, "nsi",
          node, g_strdup(event->string), event->keyval);


  if(ret && ebuf_equal_strcase(ret, "stop") )
    {
      edebug ("gtk-widget-attr", "ret = %s", ret->str);
      gtk_signal_emit_stop_by_name(GTK_OBJECT(widget), "key_press_event");
      return TRUE;
    }
  return FALSE;
}

static void
focused_event_callback (GtkWidget * widget, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;

  if (!GTK_WIDGET_HAS_FOCUS (widget))
    return;

  function = xml_node_get_attr_str (node, "onfocus");

  lang_call_function (node, function, "n", node);


}


/*****************************************************************
 *    container widget section 					 *
 *****************************************************************/

gint
rendgtk_widget_container_border_set (XML_Node * node, EBuf * attr,
				     EBuf * value)
{
  GtkWidget *widget = erend_edata_get (node, "top-widget");

  if (widget)
    gtk_container_set_border_width (GTK_CONTAINER (widget),
				    erend_get_integer (value));

  return (TRUE);
}

void
rendgtk_type_container_attr_register (Element * element)
{
  ElementAttr *e_attr;

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "border";
  e_attr->description = "Set size of border.";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "0,*";
  e_attr->set_attr_func = rendgtk_widget_container_border_set;
  element_register_attr (element, e_attr);
}

/*****************************************************************
 *    toggle widget section 					 *
 *****************************************************************/

gint
rendgtk_widget_toggle_button_attr_set (XML_Node * node, EBuf * attr,
				       EBuf * value)
{
  GtkWidget *toggle;

  toggle = erend_edata_get (node, "top-widget");

  edebug ("gtk-widget-attr", "toggle button value being set to %s\n", value);

  if (toggle)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toggle),
				  erend_value_is_true (value));

  return (TRUE);
}

void
rendgtk_type_toggle_button_attr_register (Element * element)
{
  ElementAttr *e_attr;
  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "value";
  e_attr->description = "Set if the button is toggled to true.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "false,true";
  e_attr->set_attr_func = rendgtk_widget_toggle_button_attr_set;
  element_register_attr (element, e_attr);
}

/*****************************************************************
 *    editable widgets section   				 *
 *****************************************************************/

static int
rendgtk_widget_editable_edit_attr_set (XML_Node * node, 
				       EBuf * attr, 
				       EBuf * value)
{
  GtkWidget *editable;

  editable = erend_edata_get (node, "top-widget");

  if (editable)
    gtk_editable_set_editable (GTK_EDITABLE (editable),
			       erend_value_is_true (value));
  return (TRUE);
}

void
rendgtk_type_editable_attr_register (Element * element)
{
  ElementAttr *e_attr;

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "editable";
  e_attr->description = "Set if the user can type into the widget.";
  e_attr->value_desc = "boolean";
  /*e_attr->possible_values = ""; */
  e_attr->set_attr_func = rendgtk_widget_editable_edit_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onchange";
  e_attr->description = "Sets the function to call when text is changed.";
  e_attr->value_desc = "function";
  /*e_attr->possible_values = ""; */
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);
}

/*****************************************************************
 *    box widget section    					 *
 *****************************************************************/

gint
rendgtk_widget_box_homogenous_set (XML_Node * node, 
				   EBuf * attr,
				   EBuf * value)
{
  GtkWidget *widget = erend_edata_get (node, "top-widget");

  if (widget)
    gtk_box_set_homogeneous (GTK_BOX (widget), erend_value_is_true (value));

  return (TRUE);
}

gint
rendgtk_widget_box_spacing_set (XML_Node * node, 
				EBuf * attr, 
				EBuf * value)
{
  GtkWidget *widget = erend_edata_get (node, "top-widget");

  if (widget)
    gtk_box_set_spacing (GTK_BOX (widget), erend_get_integer (value));

  return (TRUE);
}

void
rendgtk_type_box_attr_register (Element * element)
{
  ElementAttr *e_attr;

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "homogenous";
  e_attr->description = "Set all contained widgets to be the same size.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "false,true";
  e_attr->set_attr_func = rendgtk_widget_box_homogenous_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "spacing";
  e_attr->description = "Set spacing between child nodes.";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "0,*";
  e_attr->set_attr_func = rendgtk_widget_box_homogenous_set;
  element_register_attr (element, e_attr);
}

/*****************************************************************
 *     widget section    					 *
 *****************************************************************/

/* I think these are unborken. MW */
static EBuf*
rendgtk_widget_widget_focused_attr_get (XML_Node * node, EBuf *attr)
{
  GtkWidget* widget;
  EBuf* focus;
  
  widget = erend_edata_get (node, "top-widget");
  if (!widget)
    return (NULL);

  if (GTK_WIDGET_HAS_FOCUS (widget))
    focus = ebuf_new_with_cstring("true");
  else
    focus = ebuf_new_with_cstring("false");

  xml_node_set_attr(node, "focus", focus);
  return (focus);
}

static gint
rendgtk_widget_widget_focused_attr_set (XML_Node * node, EBuf *attr, EBuf *value)
{
  GtkWidget* widget;
  
  /* unset this out of the xml. */
  xml_node_set_attr_str_quiet (node, "focused", NULL);

  widget = erend_edata_get (node, "top-widget");
  if (!widget)
    return (TRUE);

  edebug ("gtk-widget-attr", "going to draw the focus");

  gtk_widget_grab_focus (widget);

  return (TRUE);
}

/* FIXME: I know i am special-casing the heck out of this but I don't
   see any way around it.  MW.  We may want to look at a better
   way to do this... maybe a element->on_set_in_child_attr_func ... */
static gint
rendgtk_widget_widget_selected_attr_set (XML_Node * node, 
					 EBuf * attr, 
					 EBuf * value)
{

  if (!node->parent)
    return (TRUE);

  if (strcmp (node->parent->element, "list") == 0)
    {
      XML_Node* parent;
      GtkWidget* list;
      GtkWidget* item;

      parent = node->parent;

      list = erend_edata_get (parent, "top-widget");
      item = erend_edata_get (node, "rendgtk-list-item");

      if (!list || !item)
        return (TRUE);

      if (erend_value_is_true (value))
        gtk_list_select_child (GTK_LIST(list), item);
      else
        gtk_list_unselect_child (GTK_LIST(list), item);
    }
  /* Any other special cases... */
  
  return (TRUE);
}

gint
rendgtk_widget_widget_usize_set (XML_Node * node, 
				 EBuf * attr, 
				 EBuf * value)
{
  GtkWidget *widget;
  gfloat xival;
  gfloat yival;
  EBuf *xval;
  EBuf *yval;

  widget = erend_edata_get (node, "top-widget");
  if (!widget)
    return (TRUE);

  yval = xml_node_get_attr (node, "height");
  if (yval)
    yival = erend_get_integer (yval);
  else
    yival = -1;

  xval = xml_node_get_attr (node, "width");
  if (xval)
    xival = erend_get_integer (xval);
  else
    xival = -1;

  gtk_widget_set_usize (GTK_WIDGET (widget), xival, yival);
  return (TRUE);
}

gint
rendgtk_widget_widget_sensitive_set (XML_Node * node, EBuf *attr,
				     EBuf * value)
{
  GtkWidget *widget = erend_edata_get (node, "top-widget");

  if (widget)
    gtk_widget_set_sensitive (GTK_WIDGET (widget),
			      erend_value_is_true (value));
  
  return (TRUE);
}

gint
rendgtk_widget_widget_visible_set (XML_Node * node, EBuf *attr,
				   EBuf *value)
{
  GtkWidget *widget = erend_edata_get (node, "top-widget");

  if (widget)
    {
      if (erend_value_is_true (value))
        gtk_widget_show (widget);
      else
	gtk_widget_hide (widget);
    }

  return (TRUE);
}

gint
rendgtk_widget_widget_dragdata_set (XML_Node * node, EBuf *attr,
				    EBuf *value)
{
  GtkWidget *widget = erend_edata_get (node, "top-widget");

  if (widget)
    rendgtk_dnd_dragable_set (node, widget);

  return (TRUE);
}

gint
rendgtk_widget_widget_ondrop_set (XML_Node *node, EBuf *attr,
				  EBuf *value)
{
  GtkWidget *widget;
  
  widget = erend_edata_get (node, "top-widget");
  
  if (widget)
    rendgtk_dnd_target_create (node, widget);
  
  return (TRUE);
}

gint
rendgtk_widget_widget_font_set (XML_Node * node, EBuf *attr, EBuf *value)
{
  GtkWidget *widget;
  GtkStyle *style;
  GdkFont *font;

  widget = erend_edata_get (node, "top-widget");
  if (!widget)
    return (TRUE);

  font = gdk_font_load (value->str);

  if (font)
    {
      gtk_widget_ensure_style (widget);
      style = gtk_style_copy (widget->style);
      style->font = font;
      gtk_widget_set_style (widget, style);
    }
  return (TRUE);
}

gint
rendgtk_widget_widget_event_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *widget;

  widget = erend_edata_get (node, "top-widget");
  if (!widget)
    return (TRUE);

  if (erend_value_equal (attr, "onbuttonpress"))
    {
      if (!strlen (value->str))
	{
	  edebug ("gtk-common", "disconnecting button_press_event"
		  " signal handler from %s", node->name);

	  gtk_signal_disconnect_by_func (GTK_OBJECT (widget),
					 button_press_event_callback, node);
	}
      else
	{
	  edebug ("gtk-common", "connecting button_press_event "
		  "signal handler to %s", node->name);
	  gtk_signal_connect (GTK_OBJECT (widget), "button_press_event",
			      button_press_event_callback, node);
	}
      return (TRUE);
    }

  else if (erend_value_equal (attr, "onbuttonrelease"))
    {
      if (!strlen (value->str))
	{
	  edebug ("gtk-common", "disconnecting button_release_event"
		  " signal handler from %s", node->name);
	  gtk_signal_disconnect_by_func (GTK_OBJECT (widget),
					 button_release_event_callback, node);
	}
      else
	{
	  edebug ("gtk-common", "connecting button_release_event"
		  " signal handler to %s", node->name);
	  gtk_signal_connect (GTK_OBJECT (widget), "button_release_event",
			      button_release_event_callback, node);
	}
      return (TRUE);
    }

  else if (erend_value_equal (attr, "onmousemotion"))
    {
      if (!strlen (value->str))
	{
	  edebug ("gtk-common", "disconnecting mousemotion"
		  " signal handler from %s", node->name);
	  gtk_signal_disconnect_by_func (GTK_OBJECT (widget),
					 mousemotion_event_callback, node);
	}
      else
	{
	  edebug ("gtk-common", "connecting mousemotion"
		  " signal handler to %s", node->name);
	  gtk_signal_connect (GTK_OBJECT (widget), "motion_notify_event",
			      mousemotion_event_callback, node);
	}
      return (TRUE);
    }

  else if (erend_value_equal (attr, "onkeypress"))
    {
      if (!strlen (value->str))
	{
	  edebug ("gtk-common", "disconnecting keypress"
		  " signal handler from %s", node->name);
	  gtk_signal_disconnect_by_func (GTK_OBJECT (widget),
					 keypress_event_callback, node);
	}
      else
	{
	  edebug ("gtk-common", "connecting keypress"
		  " signal handler to %s", node->name);
	  gtk_signal_connect (GTK_OBJECT (widget), "key-press-event",
			      keypress_event_callback, node);
	}
      return (TRUE);
    }
  else if (erend_value_equal (attr, "onfocus"))
    {
      if (!strlen (value->str))
        {
          edebug ("gtk-common", "disconnecting focus"
                  " signal handler from %s", node->name);
          gtk_signal_disconnect_by_func (GTK_OBJECT (widget),
                                         focused_event_callback, node);
        }
      else
        {
          edebug ("gtk-common", "connecting focus"
                  " signal handler to %s", node->name);
          gtk_signal_connect (GTK_OBJECT (widget), "draw-focus",
                              focused_event_callback, node);
        }
      return (TRUE);
    }

  
  /* should not be reached */
  return (FALSE);
}

void
rendgtk_type_widget_attr_register (Element * element)
{
  ElementAttr *e_attr;

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "width";
  e_attr->description = "Set total width of widget in pixels."
    " -1 specifies natural width.";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "-1,*";
  e_attr->set_attr_func = rendgtk_widget_widget_usize_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "height";
  e_attr->description = "Set total height of widget in pixels."
    " -1 specifies natural height.";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "-1,*";
  e_attr->set_attr_func = rendgtk_widget_widget_usize_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "sensitive";
  e_attr->description = "Toggle widget sensitivity."
    "  Will propogate through widget tree.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "true,false";
  e_attr->set_attr_func = rendgtk_widget_widget_sensitive_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "visible";
  e_attr->description =
    "Toggle if the widget is to be visible on the screen.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "true,false";
  e_attr->set_attr_func = rendgtk_widget_widget_visible_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "dragdata-text";
  e_attr->description = "Specify the text data to be given as drag data.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = rendgtk_widget_widget_dragdata_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "dragdata-url";
  e_attr->description = "Specify the URL to be given as drag data.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = rendgtk_widget_widget_dragdata_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "ondrop";
  e_attr->description = "Makes widget accept drag events, and"
    " calls specified function on a drop.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = rendgtk_widget_widget_ondrop_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "ondrag";
  e_attr->description = "Makes widget a drag source, and calls"
    " specified function when drag starts";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "font";
  e_attr->description = "Specify font used in widget.";
  e_attr->value_desc = "font";
  e_attr->set_attr_func = rendgtk_widget_widget_font_set;
  element_register_attr (element, e_attr);

  /* Various signals to connect to arbitrary widgets
   * (only with windows though really..  may want to 
   * check that in the future */
  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onbuttonpress";
  e_attr->description = "Specify function called on a button press event.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = rendgtk_widget_widget_event_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onbuttonrelease";
  e_attr->description = "Specify function called on a button release event.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = rendgtk_widget_widget_event_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onmousemotion";
  e_attr->description = "Specify function called when the mouse"
    " is moved on this widget";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = rendgtk_widget_widget_event_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onkeypress";
  e_attr->description = "Specify function called on a keypress event.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = rendgtk_widget_widget_event_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "selected";
  e_attr->description = "boolean";
  e_attr->value_desc = "false,true";
  e_attr->set_attr_func = rendgtk_widget_widget_selected_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "focused";
  e_attr->description = "boolean";
  e_attr->value_desc = "false,true";
  e_attr->set_attr_func = rendgtk_widget_widget_focused_attr_set;
  e_attr->get_attr_func = rendgtk_widget_widget_focused_attr_get;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onfocus";
  e_attr->description = "Specify function called on a focus in event.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = rendgtk_widget_widget_event_set;
  element_register_attr (element, e_attr);

}



/*****************************************************************
 *     misc widget section    *
 *****************************************************************/

gint
rendgtk_widget_misc_align_set (XML_Node * node, EBuf *attr, EBuf *value)
{
  GtkWidget *widget;
  gfloat xfval;
  gfloat yfval;
  EBuf *xval;
  EBuf *yval;

  widget = erend_edata_get (node, "top-widget");
  if (!widget)
    return (TRUE);
  
  /* FIXME: These need to use xml_node_get_attr_str_str proper. */
  yval = xml_node_get_attr (node, "yalign");
  if (yval)
    yfval = erend_get_percentage (yval);
  else
    yfval = 0.5;

  xval = xml_node_get_attr (node, "xalign");
  if (xval)
    xfval = erend_get_percentage (xval);
  else
    xfval = 0.5;

  gtk_misc_set_alignment (GTK_MISC (widget), xfval, yfval);
  return (TRUE);
}

gint
rendgtk_widget_misc_pad_set (XML_Node * node, EBuf *attr, EBuf *value)
{
  GtkWidget *widget;
  gint xival;
  gint yival;
  EBuf *xval;
  EBuf *yval;

  widget = erend_edata_get (node, "top-widget");
  if (!widget)
    return (TRUE);

  /* FIXME: These need to use xml_node_get_attr_str_str too */
  yval = xml_node_get_attr (node, "yalign");
  if (yval)
    yival = erend_get_integer (yval);
  else
    yival = 1;

  xval = xml_node_get_attr (node, "xalign");
  if (xval)
    xival = erend_get_integer (xval);
  else
    xival = 1;

  gtk_misc_set_padding (GTK_MISC (widget), xival, yival);
  return (TRUE);
}

void
rendgtk_type_misc_attr_register (Element * element)
{
  ElementAttr *e_attr;

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "yalign";
  e_attr->description = "Virtical alignment.";
  e_attr->value_desc = "percentage";
  e_attr->set_attr_func = rendgtk_widget_misc_align_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "xalign";
  e_attr->description = "Horizontal alignment.";
  e_attr->value_desc = "percentage";
  e_attr->set_attr_func = rendgtk_widget_misc_align_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "xpad";
  e_attr->description = "Horizontal padding in pixels.";
  e_attr->value_desc = "integer";
  e_attr->set_attr_func = rendgtk_widget_misc_pad_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "ypad";
  e_attr->description = "Vertical padding in pixels.";
  e_attr->value_desc = "integer";
  e_attr->set_attr_func = rendgtk_widget_misc_pad_set;
}


/*****************************************************************
 *     general widget attribute registration			 *
 *****************************************************************/


void
rendgtk_widget_attr_register (Element * element, GtkType widget_type)
{
  GtkObjectClass *klass;
  GtkType type;

  /* ensure the class is initialized */
  klass = gtk_type_class (widget_type);

  type = widget_type;

  /* ITERATE through types until either you find the attr your looking
   * for your you end up at GTK_OBJECT! */
  while ((type != GTK_TYPE_NONE) && (type != GTK_TYPE_OBJECT))
    {

      if (type == GTK_TYPE_WIDGET)
	{
	  rendgtk_type_widget_attr_register (element);
	}

      else if (type == GTK_TYPE_CONTAINER)
	{
	  rendgtk_type_container_attr_register (element);
	}

      else if (type == GTK_TYPE_BOX)
	{
	  rendgtk_type_box_attr_register (element);
	}

      else if (type == GTK_TYPE_MISC)
	{
	  rendgtk_type_misc_attr_register (element);
	}

      else if (type == GTK_TYPE_TOGGLE_BUTTON)
	{
	  rendgtk_type_toggle_button_attr_register (element);
	}

      else if (type == GTK_TYPE_EDITABLE)
	{
	  rendgtk_type_editable_attr_register (element);
	}

      type = gtk_type_parent (type);
    }
}
