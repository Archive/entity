#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "edebug.h"
#include "erend.h"

/*  The way an raw-io tag can look in the xml.
<raw-io
    name="thisio"
    fd="1"
    oncanread="get_part"
    oncanwrite="can_write"
    onerror="got_messed"
 />
*/

static void
rendgtk_raw_io_do (XML_Node * node, int source, char *function)
{
  lang_call_function (node, function, "n", node);
}

static int
rendgtk_raw_io_ready_callback (XML_Node * node, gint source,
			   GdkInputCondition cond)
{
  gchar *oncanread;
  gchar *oncanwrite;
  gchar *onerror;

  edebug ("raw-io-renderer", "in rendgtk_raw_io_ready_callback, cond = %i", cond);

  if (cond & GDK_INPUT_READ)
    {
      oncanread = xml_node_get_attr_str (node, "oncanread");
      rendgtk_raw_io_do (node, source, oncanread);
    }
  if (cond & GDK_INPUT_WRITE)
    {
      oncanwrite = xml_node_get_attr_str (node, "oncanwrite");
      rendgtk_raw_io_do (node, source, oncanwrite);
    }
  if (cond & GDK_INPUT_EXCEPTION)
    {
      onerror = xml_node_get_attr_str (node, "onerror");
      rendgtk_raw_io_do (node, source, onerror);
    }

  return FALSE;
}

static GdkInputCondition
rendgtk_raw_io_get_condition (XML_Node * node)
{
  GdkInputCondition cond = GDK_INPUT_READ;
  char *oncanread;
  char *oncanwrite;
  char *onerror;

  oncanread = xml_node_get_attr_str (node, "oncanread");
  oncanwrite = xml_node_get_attr_str (node, "oncanwrite");
  onerror = xml_node_get_attr_str (node, "onerror");

  if (oncanread && strlen (oncanread))
    {
      cond |= GDK_INPUT_READ;
    }

  if (oncanwrite && strlen (oncanwrite))
    {
      cond |= GDK_INPUT_WRITE;
    }

  if (oncanwrite && strlen (oncanwrite))
    {
      cond |= GDK_INPUT_EXCEPTION;
    }

  edebug ("raw-io-renderer", "cond = %i, read = %i", cond, GDK_INPUT_READ);
  return cond;
}

static int
rendgtk_raw_io_fd_attr_set (XML_Node * node, EBuf *attr, EBuf *value)
{
  GdkInputCondition cond;
  int source;
  int tag;

  char *old_tag;
  char int_buf[20];

  old_tag = erend_edata_get (node, "rendgtk-raw-io-tag");
  /*
  edebug ("raw-io-renderer", "old tag = -%s-, -%i-", old_tag, atoi (old_tag));
  edebug ("raw-io-renderer", "name = %s", node->name);
  */

  source = erend_get_integer (value);
  if (source < 0)
    {
      /*need to delete the old tag and callback connection. */
      edebug ("raw-io-renderer", "going to delete old tag");
      if (old_tag)
	{
	  gdk_input_remove (atoi (old_tag));
	}
      return TRUE;		/*Source of <0 means disconnect completely. */
    }

  if (old_tag && atoi (old_tag))
    {
      /*need to delete the old tag and callback connection. */
      gdk_input_remove (atoi (old_tag));
    }

  cond = rendgtk_raw_io_get_condition (node);

  /*No blocking io is alowed... */
  fcntl (source, F_SETFL, O_NONBLOCK);

  edebug ("raw-io-renderer", "source = %i, cond = %i", source, cond);

  tag = gdk_input_add_full (source, cond,
			    (void*)rendgtk_raw_io_ready_callback, node, NULL);

  sprintf (int_buf, "%i", tag);
  edebug ("raw-io-renderer", "tag = %s", int_buf);

  erend_edata_set (node, "rendgtk-raw-io-tag", g_strdup (int_buf));

  return TRUE;
}

static void
rendgtk_raw_io_render (XML_Node * node)
{
  xml_node_set_all_attr (node);
  erend_edata_set (node, "rendgtk-raw-io-tag", "-1");
}

static void
rendgtk_raw_io_destroy (XML_Node * node)
{
  char *tagstr;
  tagstr = erend_edata_get (node, "rendgtk-raw-io-tag");

  if (tagstr)
    gdk_input_remove (atoi (tagstr));
}

static void
rendgtk_raw_io_parent (XML_Node * parent_node, XML_Node * child_node)
{
  /* Short curcuit the node so that you can stick the timer node anywhere */
  parent_node->entity_data = child_node->entity_data;
}


void
raw_io_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);
  element->render_func = rendgtk_raw_io_render;
  element->destroy_func = rendgtk_raw_io_destroy;
  element->parent_func = rendgtk_raw_io_parent;
  element->tag = "raw-io";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "fd";
  e_attr->description = "Sepcify the file descripter to watch";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "-1,*";
  e_attr->set_attr_func = rendgtk_raw_io_fd_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "oncanread";
  e_attr->description = "Function to call when the fd is ready for reading.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "oncanwrite";
  e_attr->description = "Function to call when the fd is ready for writing.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onerror";
  e_attr->description =
    "Function to call when an exception occurs on the fd.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

}
