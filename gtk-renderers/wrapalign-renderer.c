#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "gtk-widget-attr.h"
#include "edebug.h"
#include "erend.h"
#include <gtkwrapbox.h>
#include <gtkvwrapbox.h>
#include <gtkhwrapbox.h>


static void
rendgtk_hwrapbox_render (XML_Node * node)
{
  GtkWidget *widget;

  widget = gtk_type_new (gtk_hwrap_box_get_type ());

  erend_edata_set (node, "top-widget", widget);
  erend_edata_set (node, "bottom-widget", widget);

  rendgtk_show_cond (node, widget);

  /* Make it so you can drop other apps into boxes */
  rendgtk_dnd_dragtag_target_create (node, widget);

  xml_node_set_all_attr (node);
}

static void
rendgtk_vwrapbox_render (XML_Node * node)
{
  GtkWidget *widget;

  widget = gtk_type_new (gtk_vwrap_box_get_type ());

  erend_edata_set (node, "top-widget", widget);
  erend_edata_set (node, "bottom-widget", widget);

  rendgtk_show_cond (node, widget);

  /* Make it so you can drop other apps into boxes */
  rendgtk_dnd_dragtag_target_create (node, widget);

  xml_node_set_all_attr (node);
}


static void
rendgtk_wrapalign_box_pack (XML_Node * parent_node, XML_Node * child_node)
{
  gint vfill = BOX_PACK_FILL_DEFAULT;
  gint hfill = BOX_PACK_FILL_DEFAULT;
  EBuf *fillv;
  EBuf *fillh;
  gint vexpand = BOX_PACK_EXPAND_DEFAULT;
  gint hexpand = BOX_PACK_EXPAND_DEFAULT;
  EBuf *expandv;
  EBuf *expandh;
  GtkWidget *child;
  GtkWidget *parent;

  child = erend_edata_get (child_node, "top-widget");
  parent = erend_edata_get (parent_node, "bottom-widget");

  if (!child || !parent)
    return;

  expandh = xml_node_get_attr(child_node, "hexpand");
  if(expandh)
    hexpand = erend_value_is_true (expandh);

  expandv = xml_node_get_attr(child_node, "vexpand");
  if(expandv)
    vexpand = erend_value_is_true (expandv);

  fillh = xml_node_get_attr(child_node, "hfill");
  if(fillh)
    hfill = erend_value_is_true (fillh);

  fillv = xml_node_get_attr(child_node, "vfill");
  if(fillh)
    vfill = erend_value_is_true (fillv);

  gtk_wrap_box_pack (GTK_WRAP_BOX (parent), child, 
          hexpand, hfill, vexpand, vfill);
}

void
rendgtk_wrapalign_boxpack_child_attr_set (XML_Node * parent_node,
					  XML_Node * child_node,
                                          EBuf * attr, EBuf * value)
{
  gint vfill = BOX_PACK_FILL_DEFAULT;
  gint hfill = BOX_PACK_FILL_DEFAULT;
  EBuf *fillv;
  EBuf *fillh;
  gint vexpand = BOX_PACK_EXPAND_DEFAULT;
  gint hexpand = BOX_PACK_EXPAND_DEFAULT;
  EBuf *expandv;
  EBuf *expandh;
  GtkWidget *box;
  GtkWidget *child_widget;

  box = erend_edata_get (parent_node, "bottom-widget");
  child_widget = erend_edata_get (child_node, "top-widget");

  if (!box || !child_widget)
    return;

  expandh = xml_node_get_attr(child_node, "hexpand");
  if (expandh)
    hexpand = erend_value_is_true (expandh);

  expandv = xml_node_get_attr(child_node, "vexpand");
  if (expandv)
    vexpand = erend_value_is_true (expandv);

  fillh = xml_node_get_attr(child_node, "hfill");
  if (fillh)
    hfill = erend_value_is_true (fillh);

  fillv = xml_node_get_attr(child_node, "vfill");
  if (fillv)
    vfill = erend_value_is_true (fillv);

  gtk_wrap_box_set_child_packing (GTK_WRAP_BOX (box),
				  child_widget,
				  hexpand, hfill,
				  vexpand, vfill);
}

gint
rendgtk_wrapalign_hspacing_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *widget = erend_edata_get (node, "top-widget");

  if (widget)
    gtk_wrap_box_set_hspacing (GTK_WRAP_BOX (widget), erend_get_integer (value));

  return (TRUE);
}

gint
rendgtk_wrapalign_vspacing_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *widget = erend_edata_get (node, "top-widget");

  if (widget)
    gtk_wrap_box_set_vspacing (GTK_WRAP_BOX (widget), erend_get_integer (value));

  return (TRUE);
}

void
wrapalign_renderer_init (void)
{
  Element *velement;
  Element *helement;
  ElementAttr *e_attr;

  /* vwrapailgn */
  velement = g_malloc0 (sizeof (Element));
  velement->render_func = rendgtk_vwrapbox_render;
  velement->destroy_func = rendgtk_element_destroy;
  velement->parent_func = rendgtk_wrapalign_box_pack;
  velement->tag = "vwrapalign";
  element_register (velement);

  /* hwrapalign */
  helement = g_malloc0 (sizeof (Element));
  helement->render_func = rendgtk_hwrapbox_render;
  helement->destroy_func = rendgtk_element_destroy;
  helement->parent_func = rendgtk_wrapalign_box_pack;
  helement->tag = "hwrapalign";
  element_register (helement);

  rendgtk_widget_attr_register (helement, GTK_TYPE_WRAP_BOX);
  rendgtk_widget_attr_register (velement, GTK_TYPE_WRAP_BOX);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "vexpand";
  e_attr->description =
    "Toggle whether the widgets area should 'expand' vertically if given space to do so.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "true,false";
  e_attr->set_child_attr_func = rendgtk_wrapalign_boxpack_child_attr_set;
  element_register_child_attr (helement, e_attr);
  element_register_child_attr (velement, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "hexpand";
  e_attr->description =
    "Toggle whether the widgets area should 'expand' horizontally if given space to do so.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "true,false";
  e_attr->set_child_attr_func = rendgtk_wrapalign_boxpack_child_attr_set;
  element_register_child_attr (helement, e_attr);
  element_register_child_attr (velement, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "hfill";
  e_attr->description =
    "Toggle whether the widget itself should 'fill' horizontally any extra space given to it";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "false,true";
  e_attr->set_child_attr_func = rendgtk_wrapalign_boxpack_child_attr_set;
  element_register_child_attr (velement, e_attr);
  element_register_child_attr (helement, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "vfill";
  e_attr->description =
    "Toggle whether the widget itself should 'fill' vertically any extra space given to it";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "false,true";
  e_attr->set_child_attr_func = rendgtk_wrapalign_boxpack_child_attr_set;
  element_register_child_attr (velement, e_attr);
  element_register_child_attr (helement, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "hspacing";
  e_attr->description = "Set horizontal spacing between child nodes.";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "0,*";
  e_attr->set_attr_func = rendgtk_wrapalign_hspacing_set;
  element_register_attr (velement, e_attr);
  element_register_attr (helement, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "vspacing";
  e_attr->description = "Set horizontal spacing between child nodes.";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "0,*";
  e_attr->set_attr_func = rendgtk_wrapalign_vspacing_set;
  element_register_attr (velement, e_attr);
  element_register_attr (helement, e_attr);
}
