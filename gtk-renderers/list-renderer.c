#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "edebug.h"
#include "gtk-widget-attr.h"
#include "erend.h"


static void
rendgtk_list_item_onselect_callback (GtkWidget * widget, gpointer user_data);
static void
rendgtk_list_item_ondeselect_callback (GtkWidget * widget,
				       gpointer user_data);


static gint
rendgtk_list_selection_style_attr_set (XML_Node * node, 
				       EBuf * attr, 
				       EBuf * value)
{
  GtkWidget *list;

  list = erend_edata_get (node, "top-widget");
  
  if (!list)
    return FALSE;
  
  /*GTK_SELECTION_SINGLE,
     GTK_SELECTION_BROWSE,
     GTK_SELECTION_MULTIPLE,
     GTK_SELECTION_EXTENDED */

  edebug ("list-renderer", "selection type set to %s", value);

  if (erend_value_equal (value, "multiple"))
    {
      gtk_list_set_selection_mode (GTK_LIST (list), GTK_SELECTION_MULTIPLE);
      return (TRUE);
    }

  if (erend_value_equal (value, "browse"))
    {
      gtk_list_set_selection_mode (GTK_LIST (list), GTK_SELECTION_BROWSE);
      return (TRUE);
    }

  if (erend_value_equal (value, "extended"))
    {
      gtk_list_set_selection_mode (GTK_LIST (list), GTK_SELECTION_EXTENDED);
      return (TRUE);
    }

  gtk_list_set_selection_mode (GTK_LIST (list), GTK_SELECTION_SINGLE);

  return (TRUE);
}

static void
rendgtk_list_render (XML_Node * node)
{
  GtkWidget *list;

  list = gtk_list_new ();

  erend_edata_set (node, "top-widget", list);
  erend_edata_set (node, "bottom-widget", list);

  xml_node_set_all_attr (node);
  rendgtk_show_cond (node, list);
}



static void
rendgtk_list_parent (XML_Node * parent_node, XML_Node * child_node)
{
  GtkWidget *list;
  GtkWidget *listitem;
  GtkWidget *hbox;
  GtkWidget *widget;
  GList *items = NULL;

  list = erend_edata_get (parent_node, "top-widget");
  if (!list)
    return;

  widget = erend_edata_get (child_node, "top-widget");
  if (!widget)
    return;
  
  listitem = gtk_list_item_new ();
  hbox = gtk_hbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (listitem), hbox);
  gtk_signal_connect (GTK_OBJECT (listitem), "select",
			  rendgtk_list_item_onselect_callback, child_node);
  gtk_signal_connect (GTK_OBJECT (listitem), "deselect",
		      rendgtk_list_item_ondeselect_callback, child_node);
  gtk_widget_show (listitem);
  gtk_widget_show (hbox);
  
  erend_edata_set (child_node, "extra-destroy-widget", listitem);
  erend_edata_set (child_node, "rendgtk-list-item", listitem);

  gtk_box_pack_start (GTK_BOX (hbox), widget, BOX_PACK_EXPAND_DEFAULT, 
			  BOX_PACK_FILL_DEFAULT, BOX_PACK_PADDING_DEFAULT);
  
  items = g_list_append (items, listitem);
  gtk_list_append_items (GTK_LIST (list), items);
}

static void
rendgtk_list_item_onselect_callback (GtkWidget * widget, gpointer user_data)
{
  XML_Node *node = user_data;
  XML_Node *parent_node;
  gchar *function = NULL;

  edebug ("list-renderer", 
          "got select signal for list item - name %s", node->name);

  xml_node_set_attr_str_quiet (node, "selected", "true");
  
  function = xml_node_get_attr_str (node, "onselect");

  /* If this doesn't implement the onselect function, check if parent does */
  if (!function)
    {
      edebug ("list-renderer", 
              "%s does not have onselect set, checking parent", node->name);
      
      parent_node = xml_node_parent (node);
      if (parent_node)
	function = xml_node_get_attr_str (parent_node, "onselect");
    }
  
  if (function)
    lang_call_function (node, function, "n", node);
}


static void
rendgtk_list_item_ondeselect_callback (GtkWidget * widget, gpointer user_data)
{
  XML_Node *node = user_data;
  XML_Node *parent_node;
  gchar *function = NULL;

  edebug ("list-renderer", 
          "got select signal for list item - name %s", node->name);

  xml_node_set_attr_str_quiet (node, "selected", "false");
  
  function = xml_node_get_attr_str (node, "ondeselect");

  /* If this doesn't implement the onselect function, check if parent does */
  if (!function)
    {
      edebug ("list-renderer", 
              "%s does not have ondeselect set, checking parent", node->name);
      
      parent_node = xml_node_parent (node);
      if (parent_node)
	function = xml_node_get_attr_str (parent_node, "ondeselect");
    }
  
  if (function)
    lang_call_function (node, function, "n", node);
}

void
list_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  /*List */
  element = g_new0 (Element, 1);
  element->render_func = rendgtk_list_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_list_parent;
  element->tag = "list";
  element_register (element);
  rendgtk_widget_attr_register (element, GTK_TYPE_LIST);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "selection-style";
  e_attr->description = "The mode of the selection.";
  e_attr->value_desc = "string";
  e_attr->possible_values = "single,multiple,browse,extended";
  e_attr->set_attr_func = rendgtk_list_selection_style_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onselect";
  e_attr->description =
    "Specify function to call when a sub item is 'selected'.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "ondeselect";
  e_attr->description =
    "Specify function to call when a sub item is 'deselected'.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);
}

