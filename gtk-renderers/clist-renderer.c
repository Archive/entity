#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "erend.h"
#include "edebug.h"
#include "gtk-widget-attr.h"

/*
   See t/clist.e
 */

/*
Because of the nature of of gtk(which is out primary toolkit.) we can't
grow the size of clists, so we will create the clist with cols+3 columns
and only show the ones that need to be showed.  Or, if you set in the 
clist the max cols we will create the clist with that many instead.

Clists are pretty lightweight but, we don't want to bog down the system
with 256 columns or some ridiculus size when we only need 4 or 5
usually.
*/

static gchar* 
rendgtk_clist_clrow_fill_1_txt(XML_Node* child, char **txt, int i);
static gchar*
rendgtk_clist_clrow_fill_txt(
        XML_Node* node, XML_Node* child, char** txt, int size);

/* only show if visible attribute is not "false" */

static void
rendgtk_clist_click_column (GtkCList * clist, gint n, gpointer user_data)
{
  edebug ("clist-renderer", "setting sort column to %d", n);
  gtk_clist_set_sort_column (GTK_CLIST (clist), n);
  gtk_clist_sort (GTK_CLIST (clist));
  /* gtk_clist_columns_autosize (GTK_CLIST (clist)); */
}

static gint
rendgtk_clist_select_row (GtkCList *clist, gint row, gint column,
        GdkEventButton *event, XML_Node* node)
{
  XML_Node* clr=NULL;

  gchar* onselect;

  clr = gtk_clist_get_row_data(GTK_CLIST(clist), row);
  if(!clr)
    return TRUE;

  onselect = xml_node_get_attr_str(clr, "onselect");
  if(!onselect)
    onselect = xml_node_get_attr_str(node, "onselect");
  lang_call_function(clr, onselect, "n", clr);

  return FALSE;
}

static gint
rendgtk_clist_frozen_attr_set
        (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget* clist;

  clist = erend_edata_get(node, "bottom-widget");
  if(!clist)
    return TRUE;

  if (erend_value_is_true(value) )
    gtk_clist_freeze (GTK_CLIST (clist));
  else
    gtk_clist_thaw (GTK_CLIST (clist));
 
  return TRUE;
}
static gint
rendgtk_clist_clheader_width_attr_set
        (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget* clist;
  gint col;
  gint width;
  EBuf* visible;

  edebug("clist-renderer", "width=%s", value);
  if(!node->parent && !node->parent->parent)
    return TRUE;

  clist = erend_edata_get(node->parent->parent, "bottom-widget");
  if(!clist)
    return (TRUE);

  col = (int)erend_edata_get(node, "rendgtk-clist-cl-header-col");
  width = erend_get_integer(value);

  gtk_clist_set_column_width(GTK_CLIST(clist), col, width);
  if(width)
    {
      visible = xml_node_get_attr(node, "visible");
      if(!visible || erend_value_is_true(visible) )
        {
          /* Make sure we set it visible if it should be showing. */
          gtk_clist_set_column_visibility (GTK_CLIST(clist), col, TRUE);
        }
    }
  else /* If 0 we'll just set it invisible.  Nice trick for the user. */
    {
      gtk_clist_set_column_visibility (GTK_CLIST(clist), col, FALSE);
    }
  return TRUE;
}

static gint
rendgtk_clist_clheader_visible_attr_set 
        (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget* clist;
  gint col;

  if(!node->parent && !node->parent->parent)
    return TRUE;

  clist = erend_edata_get(node->parent->parent, "bottom-widget");
  if(!clist)
    return (TRUE);

  col = (int)erend_edata_get(node, "rendgtk-clist-cl-header-col");
  
  /* Show this col. */
  gtk_clist_set_column_visibility (GTK_CLIST(clist), col, 
          erend_value_is_true(value) );

  return TRUE;
}


static gint
rendgtk_clist_sort_col_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget* clist;
  int col;

  col = erend_get_integer(value);
  clist = erend_edata_get(node, "top-widget");
  gtk_clist_set_sort_column (GTK_CLIST (clist), col);
  gtk_clist_sort (GTK_CLIST (clist));
  return (TRUE);
}

/* Not exactly sure right here, maybe thigs will be more clean in time.
   Probably will have to look through the children's chidren to find all
   the cl-headers that are visible and show them from column 0 up to
   the newly set set cols, and turn the rest off. */
static gint
rendgtk_clist_cols_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  XML_Node* clr;
  XML_Node* clh;
  GtkWidget* clist;
  gint cur;
  gint cols;
  gint i;

  clist = erend_edata_get(node, "bottom-widget");
  if(!clist)
    return TRUE;

  cur = erend_get_integer ( xml_node_get_attr(node, "cols") ); 
  cols = erend_get_integer(value);
  
  /* Well lets make out token effort to act right, but
     under the circumstances i can't do much of anything. */
  clr = (XML_Node*)node->children->data;
  for(i=cur; i<cols; i++)
    {
      clh = g_slist_nth_data(clr->children, i);
      if(clh)
        {
          erend_edata_set(clh, "rendgtk-clist-cl-header-col", (EBuf*)i);
          xml_node_set_all_attr (clh);
        }
    }
  for(i=cols; i>cur; i++)
    {
      gtk_clist_set_column_visibility (GTK_CLIST(clist), i, FALSE);
    }


  return TRUE;
}


static void
rendgtk_clist_render (XML_Node * node)
{
  GtkWidget *clist;
  GtkWidget *sw;
  int i;
  int viscols;
  int maxcols;
  int createdcols;

  viscols = erend_get_integer (xml_node_get_attr(node, "cols") );
  maxcols = erend_get_integer (xml_node_get_attr(node, "maxcols") );

  /* We do this because we can't grow these clists in gtk. 
     This relies on the first cl-row to have the cl-headers.... MW. */
  if (0 == viscols) /* Looks like they forgot to specify cols... */
    {
      GSList* tmp;
      XML_Node* first_child;  /* Hopefully a cl-row. */
      XML_Node* gchild;       /* Hopefully a cl-header. */
      edebug("clist-renderer", "having to find the maxcols dynamicly.");

      /* Time to look through the tree for the cols info. */
      if (node->children && node->children->data)
        {
          first_child = (XML_Node*)node->children->data;
          if (g_str_equal(first_child->element, "cl-row") )
            {
              for(tmp = first_child->children; tmp; tmp = tmp->next)
                {
                  gchild = (XML_Node*)tmp->data;
                  if (g_str_equal(gchild->element, "cl-header") )
                      viscols++;  /* Something worthwhile finaly. */
                }
            }
          else
            {
              /* Suck! I guess we don't know, lets hope the progammer does. */
            }
        }
    }

  /* If maxcols can't be right then, use viscols +3 */
  if (viscols > maxcols)
    createdcols = viscols + 3;
  else
    createdcols = maxcols;

  /* Will need to know this in ther parenting process. */
  erend_edata_set(node, "rendgtk-clist-createdcols", (EBuf*)createdcols);

  clist = gtk_clist_new (createdcols);



  gtk_signal_connect (GTK_OBJECT (clist), "click_column",
		      GTK_SIGNAL_FUNC (rendgtk_clist_click_column), NULL);
  gtk_signal_connect (GTK_OBJECT (clist), "select-row",
		      GTK_SIGNAL_FUNC (rendgtk_clist_select_row), node);

  sw = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_container_add (GTK_CONTAINER (sw), clist);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW (sw),
          GTK_POLICY_AUTOMATIC,
          GTK_POLICY_AUTOMATIC);
  gtk_widget_show (sw);

  erend_edata_set(node, "top-widget", sw);
  erend_edata_set(node, "bottom-widget", clist);

  gtk_clist_column_titles_show(GTK_CLIST(clist) );
  for(i=viscols; i<createdcols; i++)
    {
      gtk_clist_set_column_visibility (GTK_CLIST(clist), i, FALSE);
    }

  xml_node_set_all_attr (node);

  rendgtk_show_cond (node, clist);
}

/* Update the clist based on the text attrib. */
static gint
rendgtk_clist_clrow_onstring_child_update
        (XML_Node* node, EBuf* attr, EBuf* value)
{
  /* Find the row. */
  /* Find the column. */
  /* Set the text. */
  return TRUE;
}

static gchar*
rendgtk_clist_clrow_fill_txt(
        XML_Node* node, XML_Node* child, char** txt, int size)
{
  gchar* row_type;
  int i=0;

  i = g_slist_index(node->children, child);

  row_type = rendgtk_clist_clrow_fill_1_txt(child, txt, i);

  /* Make sure we set this array of char*s so we don't get a segv. */
  while(i<size)
    {
      if(!txt[i])
        txt[i] = g_strdup("");
      i++;
    }

  return row_type;
}

static gchar* 
rendgtk_clist_clrow_fill_1_txt(XML_Node* child, char **txt, int i)
{
  gchar* row_type = ""; /* Just in case we don't set it. */
  gchar* str;

  /* Need to free up any previously set string. */
  if(txt[i])
    g_free(txt[i]);

  edebug("clist-renderer", "child is %s.", child->name);

  if(g_str_equal(child->element, "string") )
    {
      str = xml_node_get_attr_str(child, "text");
      if(str)
        txt[i] = g_strdup(str);
      else
        txt[i] = g_strdup("");
      /*
         xml_node_set_onattr_update(child, 
         "text", rendgtk_clist_clrow_onstring_child_update);
       */
      row_type = "clrow-regular"; /* Means it regular row. */
    }
  else if (g_str_equal(child->element, "cl-header") )
    {
      str = xml_node_get_attr_str(child, "title");
      if(str)
        txt[i] = g_strdup(str);
      else
        txt[i] = g_strdup("");
      row_type = "clrow-header"; /* Means it a header row. */
    }
  else /* go into complex mode deal with images and strings and stuff. */
    {
    }

  return row_type;
}

static void
rendgtk_clist_clrow_parent (XML_Node * parent_node, XML_Node * child_node)
{
  XML_Node* parent;
  gint txt_size;
  gchar** txt;
  gchar* type;

  parent = parent_node->parent;
  if(!parent && g_str_equal(parent->element, "clist") )
    return;

  /* Silently ignore any alien chidren. */
  if (!g_str_equal (parent_node->element, "cl-row"))
    return;

  txt_size = (int)erend_edata_get(parent, "rendgtk-clist-createdcols");
  if (!txt_size)
    {
      txt_size = 3;  /* What the heck happend? */
    }

  txt = erend_edata_get(parent_node, "rendgtk-clrow-txt");
  if (!txt)
    {
      txt = g_new0(char*, txt_size);
      erend_edata_set(parent_node, "rendgtk-clrow-txt", txt);
    }

  /* Fill in this node. */
  type = rendgtk_clist_clrow_fill_txt(parent_node, child_node, txt, txt_size); 

  erend_edata_set(parent_node, "rendgtk-clrow-type", type);
}

static void
rendgtk_clist_parent (XML_Node * parent_node, XML_Node * child_node)
{
  GtkWidget *clist;
  XML_Node* clh;
  gchar** txt;
  gint txt_size;
  gchar* type;
  gint row_num;
  gint i;

  edebug("clist-renderer", "in rendgtk_clist_parent.");
  clist = erend_edata_get(parent_node, "bottom-widget");
  if (!clist)
    return;

  txt = erend_edata_get(child_node, "rendgtk-clrow-txt");
  type = erend_edata_get(child_node, "rendgtk-clrow-type");
  
  txt_size = (int)erend_edata_get(parent_node, "rendgtk-clist-createdcols");

  if(g_str_equal(type, "clrow-regular") )
    {
      row_num = gtk_clist_append (GTK_CLIST (clist), txt);

      edebug("clist-renderer", "appending txt, txt[0] = %s\n", txt[0]);

      /* Now we map one to one from cl-rows to GtkCList rows. :) */
      gtk_clist_set_row_data(GTK_CLIST(clist), row_num, child_node);
    }
  else
    {
      clh = child_node->children->data;
      for(i=0; i<txt_size; i++)
        {
          gtk_clist_set_column_title(GTK_CLIST(clist), i, txt[i]);
          clh = g_slist_nth_data(child_node->children, i);
          if(clh)
            {
              erend_edata_set(clh, "rendgtk-clist-cl-header-col", (void*)i);
              xml_node_set_all_attr (clh);
            }
        }
    }

}

static void
rendgtk_clist_clrow_visible_attr_set 
        (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *clist;
  gint row_num;

  if(!node->parent)
    return;

  clist = erend_edata_get(node->parent, "rendgtk-clist");
  if(!clist)
    return;

  row_num = gtk_clist_find_row_from_data (GTK_CLIST(clist), node);

  /* Don't know how to deal with this yet... MW
  gtk_clist_remove (GTK_CLIST (clist), row_num);
  */
}


static void
rendgtk_clist_clrow_destroy (XML_Node * node)
{
  gint row_num;
  GtkWidget* clist;

  if(!node->parent)
    return;

  clist = erend_edata_get(node->parent, "bottom-widget");
  if(!clist)
    return;

  row_num = gtk_clist_find_row_from_data (GTK_CLIST(clist), node);

  gtk_clist_remove (GTK_CLIST (clist), row_num);
}


static void
rendgtk_clist_clrow_render (XML_Node* node)
{
  erend_edata_set(node, "rendgtk-clist-clrow-do", "do");
  xml_node_set_all_attr (node);
}

static void
rendgtk_clist_clheader_render (XML_Node* node)
{
  erend_edata_set(node, "rendgtk-clist-clheader-do", "do");
  /* xml_node_set_all_attr (node); */
}

/* Find the column that that this header is for and
   remove it by hiding it. I really can't do any more
   without wroking way to hard and probably ending up
   with something that is buggy and doesn't do what we 
   want anyway. MW */
static void
rendgtk_clist_clheader_destroy (XML_Node* node)
{
  GtkWidget* clist;
  XML_Node *clh;
  GSList* siblings;
  GSList* tmp;
  gint col;

  edebug("clist-renderer", "in rendgtk_clist_clheader_destroy"); 

  if(!node->parent && !node->parent->parent)
    return;

  clist = erend_edata_get(node->parent->parent, "bottom-widget");
  if(!clist)
    return;

  col = (int)erend_edata_get(node, "rendgtk-clist-cl-header-col");

  /* Hide this col. */
  gtk_clist_set_column_visibility (GTK_CLIST(clist), col, FALSE);
  edebug("clist-renderer", "col = %i", col);

  siblings = node->parent->children;
  for(tmp = siblings; tmp; tmp = tmp-> next)
    {
      clh = (XML_Node*)tmp->data;
      edebug("clist-renderer", "clh = %s", xml_node_get_path(clh) );
    }
}


void
clist_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  /* clist */
  element = g_new0 (Element, 1);
  element->render_func = rendgtk_clist_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_clist_parent;
  element->tag = "clist";
  element_register (element);
  rendgtk_widget_attr_register (element, GTK_TYPE_CLIST);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "frozen";
  e_attr->description = "When set true the clist is frozen so dynamic "
                        "updates don't show until it is set false.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "true,false";
  e_attr->set_attr_func = rendgtk_clist_frozen_attr_set;
  element_register_attr (element, e_attr);

  element = g_new0 (Element, 1);
  element->render_func = rendgtk_clist_clrow_render;
  element->destroy_func = rendgtk_clist_clrow_destroy;
  element->parent_func = rendgtk_clist_clrow_parent;
  element->tag = "cl-row";
  element_register (element);

  element = g_new0 (Element, 1);
  element->render_func = rendgtk_clist_clheader_render;
  element->destroy_func = rendgtk_clist_clheader_destroy;
  element->tag = "cl-header";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "visible";
  e_attr->description = "Sets visiblity for the entier column.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "true,false";
  e_attr->set_attr_func = rendgtk_clist_clheader_visible_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "width";
  e_attr->description = "Sets width for the entire column.";
  e_attr->value_desc = "integer";
  e_attr->set_attr_func = rendgtk_clist_clheader_width_attr_set;
  element_register_attr (element, e_attr);
}

