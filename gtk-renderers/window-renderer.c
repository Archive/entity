#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "gtk-widget-attr.h"
#include "edebug.h"
#include "erend.h"


static gint
rendgtk_window_ondelete_callback (GtkWidget * widget,
				  GdkEvent * event, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;

  function = xml_node_get_attr_str (node, "ondelete");

  lang_call_function (node, function, "n", node);

  return (TRUE);
}

static void
rendgtk_window_resize_callback (GtkWidget * widget,
				GdkEventConfigure * event, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar value[100];
  
  /* g_print ("x = %d, y = %d, width = %d, height = %d\n", event->x, event->y, 
     event->width, event->height); */

  /* to remember window width and height.  May also want to check an
   * attribute in case they don't want this to be saved */

  g_snprintf (value, 100, "%d", event->width);
  xml_node_set_attr_str_quiet (node, "width", value);

  g_snprintf (value, 100, "%d", event->height);
  xml_node_set_attr_str_quiet (node, "height", value);
}


static gint
rendgtk_window_set_title_attr (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *window;

  window = erend_edata_get (node, "top-widget");
  if (!window)
    return FALSE;

  gtk_window_set_title (GTK_WINDOW (window), value->str);

  /* 2nd arg is wmclass_name, 3rd arg is wmclass_class */
  /* May want to move this into a seperate attr */
  if (!GTK_WIDGET_REALIZED (window))
    gtk_window_set_wmclass (GTK_WINDOW (window), value->str, value->str);
  
  return (TRUE);
}

static gint
rendgtk_window_set_xyposition_attr (XML_Node * node, EBuf * attr,
				    EBuf * value)
{
  GtkWidget *window;
  gint xipos;
  gint yipos;
  EBuf *val;
  
  window = erend_edata_get (node, "top-widget");
  if (!window)
    return (TRUE);
  
  val = xml_node_get_attr (node, "y-position");
  if (val)
    yipos = erend_get_integer (val);
  else
    yipos = -1;

  val = xml_node_get_attr (node, "x-position");
  if (val)
    xipos = erend_get_integer (val);
  else
    xipos = -1;
  
  gtk_window_reposition (GTK_WINDOW (window), xipos, yipos);
  
  return (TRUE);
}

static gint
rendgtk_window_set_position_attr (XML_Node * node, EBuf * attr,
				  EBuf * value)
{
  GtkWidget *window;
  gint intval;

  window = erend_edata_get (node, "top-widget");
  if (!window)
    return FALSE;

  intval = GTK_WIN_POS_NONE;

  if (erend_value_equal (value, "center"))
    intval = GTK_WIN_POS_CENTER;

  if (erend_value_equal (value, "mouse"))
    intval = GTK_WIN_POS_MOUSE;

  gtk_window_set_position (GTK_WINDOW (window), intval);

  return (TRUE);
}


static void
rendgtk_window_pack (XML_Node * parent_node, XML_Node * child_node)
{
  GtkWidget *window;
  
  /* Only show window once something gets packed into it, should make
     it pop up better */
  window = erend_edata_get (parent_node, "top-widget");
  
  rendgtk_box_pack (parent_node, child_node);
  rendgtk_show_cond (parent_node, window); 
}

static void
rendgtk_window_render (XML_Node * node)
{
  GtkWidget *window;
  GtkWidget *vbox;

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), vbox);
  
  erend_edata_set (node, "top-widget", window);
  erend_edata_set (node, "bottom-widget", vbox);

  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (rendgtk_window_ondelete_callback),
		      (gpointer) node);

  gtk_signal_connect (GTK_OBJECT (window), "configure_event",
		      GTK_SIGNAL_FUNC (rendgtk_window_resize_callback),
		      (gpointer) node);

  gtk_window_set_policy (GTK_WINDOW (window), TRUE, TRUE, FALSE);

  xml_node_set_all_attr (node);

  gtk_widget_show (vbox);
}


void
window_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);
  element->render_func = rendgtk_window_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_window_pack;
  element->tag = "window";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "ondelete";
  e_attr->description = "Specify function to call when window is 'closed'.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "title";
  e_attr->description = "Set title and wmclass of window.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = rendgtk_window_set_title_attr;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "position";
  e_attr->description = "Set position of window.";
  e_attr->value_desc = "choice";
  e_attr->possible_values = "none,center,mouse";
  e_attr->set_attr_func = rendgtk_window_set_position_attr;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "x-position";
  e_attr->description = "Set absolute x position of window.";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "-1,*";
  e_attr->set_attr_func = rendgtk_window_set_xyposition_attr;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "y-position";
  e_attr->description = "Set absolute y position of window.";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "-1,*";
  e_attr->set_attr_func = rendgtk_window_set_xyposition_attr;
  element_register_attr (element, e_attr);
  
  rendgtk_widget_attr_register (element, GTK_TYPE_WINDOW);
  rendgtk_containerbox_attr_register (element);
}

