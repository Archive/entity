#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "entity.h"
#include "renderers.h"
#include <stdio.h>
#include "toggle-renderer.h"
#include "gtk-widget-attr.h"
#include "edebug.h"
#include "erend.h"

static gint
rendgtk_radio_group_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  return TRUE;
}

static void
rendgtk_radio_group_render (XML_Node * node)
{
  edebug ("radio-renderer", "in rendgtk_radio_group_render");
  xml_node_set_all_attr (node);
}

static void
rendgtk_radio_group_destroy (XML_Node * node)
{
  GSList* freelist;
  freelist = erend_edata_get (node, "rendgtk-radio-group-freelist");

  for ( ; freelist; freelist = freelist->next)
    {
      gtk_widget_destroy(GTK_WIDGET(freelist->data) );
    }
}

static void
rendgtk_radio_destroy (XML_Node * node)
{
  XML_Node* parent;
  GtkWidget* radio;
  GSList* freelist;
  int ttag, stag;

  radio = erend_edata_get (node, "top-widget");
  if (!radio) return;

  parent = xml_node_find_parent (node, "radio-group");
  if (!parent)
    {
      g_warning ("radio %s not in a <radio-group> tag.", node->name->str);
      return;
    }

  /* Add the radio to the radio-group's freelist. */
  freelist = erend_edata_get (node, "rendgtk-radio-group-freelist");
  freelist = g_slist_append(freelist, radio);
  erend_edata_set (parent, "rendgtk-radio-group-freelist", freelist);

  ttag = (int)erend_edata_get (node, "rendgtk-radio-ttag");
  stag = (int)erend_edata_get (node, "rendgtk-radio-stag");

  /*Free the tags from the hash table.*/
  erend_edata_set (node, "rendgtk-radio-ttag", NULL);
  erend_edata_set (node, "rendgtk-radio-stag", NULL);

  /*Disconnect the signals to this radio.*/
  gtk_signal_disconnect(GTK_OBJECT(radio), ttag);
  gtk_signal_disconnect(GTK_OBJECT(radio), stag);

  /*Now make sure this one isn't toggled.*/
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(radio), FALSE);
  gtk_widget_hide(radio);

  /* Wish we could call one of theses... MW*/
  /*rendgtk_element_destroy(node);*/
  /*gtk_widget_destroy(radio);*/
}

static void
rendgtk_radio_render (XML_Node * node)
{
  GtkWidget *radio;
  GtkWidget *vbox;
  GSList *group = NULL;
  XML_Node *parent;
  int ttag, stag;  /* toggle, select tags.*/

  edebug ("radio-renderer", "rendgtk_radio_render");
  radio = gtk_radio_button_new (NULL);
  vbox = gtk_vbox_new (TRUE, 0);

  gtk_container_add (GTK_CONTAINER (radio), vbox);

  ttag = gtk_signal_connect (GTK_OBJECT (radio), "toggled",
		      GTK_SIGNAL_FUNC (rendgtk_toggle_ontoggle_callback),
		      node);
  stag = gtk_signal_connect (GTK_OBJECT (radio), "toggled",
		      GTK_SIGNAL_FUNC (rendgtk_toggle_onselect_callback),
		      node);

  gtk_widget_show (vbox);

  erend_edata_set (node, "top-widget", radio);
  erend_edata_set (node, "bottom-widget", vbox);

  erend_edata_set (node, "rendgtk-radio-ttag", (void*)ttag);
  erend_edata_set (node, "rendgtk-radio-stag", (void*)stag);

  parent = xml_node_find_parent (node, "radio-group");
  if (!parent)
    {
      g_warning ("radio %s not in a <radio-group> tag.", node->name->str);
      return;
    }
  group = erend_edata_get (parent, "rendgtk-radio-group");
  edebug ("radio-renderer", "group = %i", group);
  if (!group)
    {
      group = gtk_radio_button_group (GTK_RADIO_BUTTON (radio));
      erend_edata_set (parent, "rendgtk-radio-group", group);
    }
  else
    {
      gtk_radio_button_set_group (GTK_RADIO_BUTTON (radio), group);
      group = gtk_radio_button_group (GTK_RADIO_BUTTON (radio));
      erend_edata_set (parent, "rendgtk-radio-group", group);
    }

  xml_node_set_all_attr (node);
  rendgtk_show_cond (node, radio);
}

static void
radio_group_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);

  element->render_func = rendgtk_radio_group_render;
  element->destroy_func = rendgtk_radio_group_destroy;
  /*element->parent_func = rendgtk_radio_group_parenter; */
  element->tag = "radio-group";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onselect";
  e_attr->description = "Sets up a handler for all radio onselect events.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = rendgtk_radio_group_attr_set;
  element_register_attr (element, e_attr);
}

void
radio_renderer_init (void)
{
  Element *element;

  radio_group_renderer_init ();

  element = g_new0 (Element, 1);

  element->render_func = rendgtk_radio_render;
  element->destroy_func = rendgtk_radio_destroy;
  element->parent_func = rendgtk_box_pack;
  element->tag = "radio";
  element_register (element);

  rendgtk_widget_attr_register (element, GTK_TYPE_RADIO_BUTTON);
  rendgtk_toggle_attr_register (element);
  rendgtk_containerbox_attr_register (element);
}
