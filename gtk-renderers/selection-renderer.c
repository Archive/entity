#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "xml-node.h"
#include "gtk-common.h"
#include "erend.h"
#include "edebug.h"



typedef enum {
  SEL_TYPE_NONE,
  APPLE_PICT,
  ATOM,
  ATOM_PAIR,
  BITMAP,
  C_STRING,
  COLORMAP,
  COMPOUND_TEXT,
  DRAWABLE,
  INTEGER,
  PIXEL,
  PIXMAP,
  SPAN,
  STRING,
  TEXT,
  WINDOW,
  LAST_SEL_TYPE
} SelType;

GdkAtom seltypes[LAST_SEL_TYPE];

typedef struct _Target {
  gchar *target_name;
  SelType type;
  GdkAtom target;
  gint format;
} Target;

/* The following is a list of all the selection targets defined
   in the ICCCM */

static Target targets[] = {
  { "ADOBE_PORTABLE_DOCUMENT_FORMAT",	    STRING, 	   0, 8 },
  { "APPLE_PICT", 			    APPLE_PICT,    0, 8 },
  { "BACKGROUND",			    PIXEL,         0, 32 },
  { "BITMAP", 				    BITMAP,        0, 32 },
  { "CHARACTER_POSITION",                   SPAN, 	   0, 32 },
  { "CLASS", 				    TEXT, 	   0, 8 },
  { "CLIENT_WINDOW", 			    WINDOW, 	   0, 32 },
  { "COLORMAP", 			    COLORMAP,      0, 32 },
  { "COLUMN_NUMBER", 			    SPAN, 	   0, 32 },
  { "COMPOUND_TEXT", 			    COMPOUND_TEXT, 0, 8 },
  /*  { "DELETE", "NULL", 0, ? }, */
  { "DRAWABLE", 			    DRAWABLE,      0, 32 },
  { "ENCAPSULATED_POSTSCRIPT", 		    STRING, 	   0, 8 },
  { "ENCAPSULATED_POSTSCRIPT_INTERCHANGE",  STRING, 	   0, 8 },
  { "FILE_NAME", 			    TEXT, 	   0, 8 },
  { "FOREGROUND", 			    PIXEL, 	   0, 32 },
  { "HOST_NAME", 			    TEXT, 	   0, 8 },
  /*  { "INSERT_PROPERTY", "NULL", 0, ? NULL }, */
  /*  { "INSERT_SELECTION", "NULL", 0, ? NULL }, */
  { "LENGTH", 				    INTEGER, 	   0, 32 },
  { "LINE_NUMBER", 			    SPAN, 	   0, 32 },
  { "LIST_LENGTH", 			    INTEGER,       0, 32 },
  { "MODULE", 				    TEXT, 	   0, 8 },
  /*  { "MULTIPLE", "ATOM_PAIR", 0, 32 }, */
  { "NAME", 				    TEXT, 	   0, 8 },
  { "ODIF", 				    TEXT,          0, 8 },
  { "OWNER_OS", 			    TEXT, 	   0, 8 },
  { "PIXMAP", 				    PIXMAP,        0, 32 },
  { "POSTSCRIPT", 			    STRING,        0, 8 },
  { "PROCEDURE", 			    TEXT,          0, 8 },
  { "PROCESS",				    INTEGER,       0, 32 },
  { "STRING", 				    STRING,        0, 8 },
  { "TARGETS", 				    ATOM, 	   0, 32 },
  { "TASK", 				    INTEGER,       0, 32 },
  { "TEXT", 				    TEXT,          0, 8  },
  { "TIMESTAMP", 			    INTEGER,       0, 32 },
  { "USER", 				    TEXT, 	   0, 8 },
};

static int num_targets = sizeof(targets)/sizeof(Target);


static void
init_atoms (void)
{
  int i;

  seltypes[SEL_TYPE_NONE] = GDK_NONE;
  seltypes[APPLE_PICT] = gdk_atom_intern ("APPLE_PICT",FALSE);
  seltypes[ATOM]       = gdk_atom_intern ("ATOM",FALSE);
  seltypes[ATOM_PAIR]  = gdk_atom_intern ("ATOM_PAIR",FALSE);
  seltypes[BITMAP]     = gdk_atom_intern ("BITMAP",FALSE);
  seltypes[C_STRING]   = gdk_atom_intern ("C_STRING",FALSE);
  seltypes[COLORMAP]   = gdk_atom_intern ("COLORMAP",FALSE);
  seltypes[COMPOUND_TEXT] = gdk_atom_intern ("COMPOUND_TEXT",FALSE);
  seltypes[DRAWABLE]   = gdk_atom_intern ("DRAWABLE",FALSE);
  seltypes[INTEGER]    = gdk_atom_intern ("INTEGER",FALSE);
  seltypes[PIXEL]      = gdk_atom_intern ("PIXEL",FALSE);
  seltypes[PIXMAP]     = gdk_atom_intern ("PIXMAP",FALSE);
  seltypes[SPAN]       = gdk_atom_intern ("SPAN",FALSE);
  seltypes[STRING]     = gdk_atom_intern ("STRING",FALSE);
  seltypes[TEXT]       = gdk_atom_intern ("TEXT",FALSE);
  seltypes[WINDOW]     = gdk_atom_intern ("WINDOW",FALSE);

  for (i=0; i<num_targets; i++)
    targets[i].target = gdk_atom_intern (targets[i].target_name, FALSE);
}

static void
rendgtk_button_onclick_callback (GtkWidget * widget, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;

  function = xml_node_get_attr_str_str (node, "onclick");

  lang_call_function (node, function, "n", node);
}

static gint
rendgtk_selection_set (XML_Node *node, gchar *attr, gchar *value)
{
  
  return (TRUE);
}


static void
rendgtk_selection_new (void)
{
  XML_Node *node;
  
  node = xml_node_new ("selection");
  
  node->parent = xml_node_root ();
  node->flags |= XML_NODE_RENDERED;
  node->flags |= XML_NODE_PARENTED;
}

void
selection_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);
  element->render_func = NULL;
  element->destroy_func = NULL;
  element->parent_func = NULL;
  element->tag = "selection";
  element->no_render_children = TRUE;
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "value";
  e_attr->description = "The current X selection.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = rendgtk_selection_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onclick";
  e_attr->description = "The selection mime type.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = rendgtk_selection_type_set;
  element_register_attr (element, e_attr);

  /* we now immediately make a single instance in the root */
  rendgtk_selection_new ()
}


