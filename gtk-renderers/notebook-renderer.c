#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "entity.h"
#include "renderers.h"
#include <stdio.h>
#include "gtk-widget-attr.h"
#include "erend.h"
#include "edebug.h"


/*
static gint
rendgtk_page_attr_set (XML_Node * node, gchar * attr, gchar * value)
{
  return TRUE;
}
*/


static void
rendgtk_page_render (XML_Node * node)
{
  GtkWidget *vbox;

  vbox = gtk_vbox_new (FALSE, 0);

  erend_edata_set (node, "top-widget", vbox);
  erend_edata_set (node, "bottom-widget", vbox);

  xml_node_set_all_attr (node);
  gtk_widget_show (vbox);
}

static void
rendgtk_page_destroy (XML_Node * node)
{
  GtkWidget* page;
  GtkWidget* notebook;
  int page_num;

  if (!node->parent)
    return;

  page = erend_edata_get (node, "top-widget");
  if (!page)
    return;

  notebook = erend_edata_get (node->parent, "top-widget");
  if (!notebook)
    return;

  page_num = gtk_notebook_page_num (GTK_NOTEBOOK(notebook), page);

  gtk_notebook_remove_page (GTK_NOTEBOOK(notebook), page_num);

  rendgtk_element_destroy (node);
}

static gint
rendgtk_notepage_title_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget* page;
  GtkWidget* notebook;

  page = erend_edata_get (node, "top-widget");
  if (!page)
    return TRUE;

  notebook = erend_edata_get (node->parent, "top-widget");
  if (!notebook)
    return TRUE;

  gtk_notebook_set_tab_label_text (GTK_NOTEBOOK(notebook), page, value->str);

  return TRUE;
}

/* FIXME: I dunno what the heck this is supposed to do, but it's a big 'nop' atm. */
static gint
rendgtk_notepage_selected_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget* page;
  GtkWidget* notebook;
  gint page_num;

  page = erend_edata_get (node, "top-widget");
  if (!page)
    return TRUE;

  notebook = erend_edata_get (node->parent, "top-widget");
  if (!notebook)
    return TRUE;

  page_num = gtk_notebook_page_num (GTK_NOTEBOOK(notebook), page);
  gtk_notebook_set_page (GTK_NOTEBOOK(notebook), page_num);

  return TRUE;
}

static int
rendgtk_notebook_switch_page_callback (
        GtkNotebook *notebook, GtkNotebookPage *page,
        gint page_num, XML_Node* node)
{
  XML_Node* child;
  GSList* list;
  XML_Node* node_page = NULL;
  gchar* onselect=NULL;
  int i=0;
  
  edebug("notebook-renderer", "page_num = %i", page_num);

  for(list = node->children; list; list = list->next)
  {
    child = (XML_Node*) list->data;

    if (page_num != i)
      xml_node_set_attr_str_quiet(child, "selected", "false");
    else
      {
        xml_node_set_attr_str_quiet(child, "selected", "true");
	node_page = child;
        onselect = xml_node_get_attr_str(node_page, "onselect");
      }

    i++;
  }

  if(!onselect)
    onselect = xml_node_get_attr_str(node, "onselect");
  if (onselect && node_page)
    lang_call_function(node, onselect, "ni", node_page, page_num);
  
  return FALSE;
}

static void
rendgtk_notebook_render (XML_Node * node)
{
  GtkWidget *notebook;

  notebook = gtk_notebook_new ();

  erend_edata_set (node, "top-widget", notebook);
  erend_edata_set (node, "bottom-widget", notebook);

  xml_node_set_all_attr (node);

  /* Connect this AFTER we set all the attribs because for some reason
   * gtk wants to emit this signal on create. GACK! */
  gtk_signal_connect(GTK_OBJECT(notebook), "switch-page", 
		     GTK_SIGNAL_FUNC (rendgtk_notebook_switch_page_callback),
		     node);

  rendgtk_show_cond (node, notebook);
}

static void
rendgtk_notebook_parent (XML_Node * parent_node, XML_Node * child_node)
{
  GtkWidget *notebook;
  GtkWidget *vbox;
  GtkWidget *label_w;
  char *label;

  label = xml_node_get_attr_str (child_node, "label");
  if (!label)
    label = "";

  edebug("notebook-renderer", "label = %s", label);
  label_w = gtk_label_new (label);

  notebook = erend_edata_get (parent_node, "top-widget");
  vbox = erend_edata_get (child_node, "top-widget");

  gtk_notebook_append_page (GTK_NOTEBOOK (notebook), vbox, label_w);
}


void
notebook_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);
  element->render_func = rendgtk_notebook_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_notebook_parent;
  element->tag = "notebook";
  element_register (element);
  rendgtk_widget_attr_register (element, GTK_TYPE_NOTEBOOK);

  element = g_new0 (Element, 1);
  element->render_func = rendgtk_page_render;
  element->destroy_func = rendgtk_page_destroy;
  element->parent_func = rendgtk_box_pack;
  element->tag = "notepage";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "title";
  e_attr->description = "Sets the title for the notepage.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = rendgtk_notepage_title_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "selected";
  e_attr->description = "Set if this page is the current notepage.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = rendgtk_notepage_selected_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onselect";
  e_attr->description = "Function to call when a notepage is selected.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL; 
  element_register_attr (element, e_attr);

}
