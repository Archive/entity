#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "perl-embed.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "gtk-widget-attr.h"
#include "erend.h"



static void
rendgtk_ebox_render (XML_Node * node)
{
  GtkWidget *ebox;
  GtkWidget *vbox;

  /* We use the eventbox so we can attach a few signals if needed */
  ebox = gtk_event_box_new ();

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (vbox), ebox);
  gtk_widget_show (vbox);

  erend_edata_set (node, "top-widget", ebox);
  erend_edata_set (node, "bottom-widget", vbox);

  xml_node_set_all_attr (node);
  rendgtk_show_cond (node, ebox);
}

void
ebox_renderer_init (void)
{
  Element *element;

  /* ebox */
  element = g_new0 (Element, 1);
  element->render_func = rendgtk_ebox_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_box_pack;
  element->tag = "ebox";
  element_register (element);

  rendgtk_widget_attr_register (element, GTK_TYPE_EVENT_BOX);
}
