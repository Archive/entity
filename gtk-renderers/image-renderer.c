#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "config.h"
#include "edebug.h"
#include "gtk-widget-attr.h"
#include "erend.h"



/* We only build image support if we have IMLIB. */

#ifdef USE_IMLIB

#include <gdk_imlib.h>

/* These should probly be jacked up a bit more.. */
#define MAX_IMAGE_WIDTH 1024
#define MAX_IMAGE_HEIGHT 768


static GtkWidget *
rendgtk_load_image (gchar * filename, gint width, gint height)
{
  GdkImlibImage *img;
  GtkWidget *pw;
  GdkBitmap *mask = NULL;
  GdkPixmap *pixmap = NULL;

  img = gdk_imlib_load_image (filename);

  if (!img)
    return (NULL);

  if (width < 0)
    width = img->rgb_width;

  if (height < 0)
    height = img->rgb_height;


  gdk_imlib_render (img, width, height);

  pixmap = gdk_imlib_move_image (img);
  mask = gdk_imlib_move_mask (img);

  pw = gtk_pixmap_new (pixmap, mask);

  gdk_imlib_kill_image (img);
  gdk_imlib_free_pixmap (pixmap);
  /* need to free mask ? */

  gtk_widget_show (pw);
  return (pw);
}


static gint
rendgtk_image_src_attr_set (XML_Node * node, EBuf *attr, EBuf *value)
{
  GtkWidget *ebox;
  GtkWidget *image;
  gint width;
  gint height;
  EBuf *val;

  ebox = erend_edata_get (node, "top-widget");
  if (!ebox)
    return FALSE;

  /* to make life easier, this is the only place that the image is actually
     loaded */

  edebug ("image-renderer", "attempting to load image '%s'", value);

  /* FIXME: This is gonna break from xml_node_get_attr_str_str change */
  val = xml_node_get_attr (node, "width");
  if (val)
    width = erend_get_integer (val);
  else
    width = -1;

  val = xml_node_get_attr (node, "height");
  if (val)
    height = erend_get_integer (val);
  else
    height = -1;

  image = rendgtk_load_image (value->str, width, height);

  if (image)
    {
      GtkWidget *old_image;
      old_image = erend_edata_get (node, "rendgtk-image-widget");
      if (old_image)
	gtk_widget_destroy (GTK_WIDGET (old_image));
      
      gtk_container_add (GTK_CONTAINER (ebox), image);
      gtk_widget_show (image);
      erend_edata_set (node, "rendgtk-image-widget", image);
      edebug ("image-renderer", "image '%s' loaded", value);
    }
  else
    {
      edebug ("image-renderer", "failed to load '%s'", value);
    }
  return (TRUE);
}

static gint
rendgtk_image_width_attr_set (XML_Node * node, EBuf *attr, EBuf *value)
{
  GtkWidget *ebox;

  ebox = erend_edata_get (node, "top-widget");

  return (TRUE);
}

static gint
rendgtk_image_height_attr_set (XML_Node * node, EBuf *attr, EBuf *value)
{
  GtkWidget *ebox;

  ebox = erend_edata_get (node, "top-widget");
  if (!ebox)
    return (TRUE);

  return (TRUE);
}




void
rendgtk_image_render (XML_Node * node)
{
  GtkWidget *ebox;

  /* We use the eventbox so we can attach a few signals if needed */
  ebox = gtk_event_box_new ();
  
  /* experiment */
  gtk_container_set_resize_mode (GTK_CONTAINER (ebox), GTK_RESIZE_PARENT);

  erend_edata_set (node, "top-widget", ebox);
  erend_edata_set (node, "bottom-widget", ebox);

  xml_node_set_all_attr (node);
  rendgtk_show_cond (node, ebox);
}

void
image_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  gdk_imlib_init ();

  edebug ("image-renderer",
	  "Built with IMLIB support - imlib initialized.\n");

  /* image */
  element = g_new0 (Element, 1);
  element->render_func = rendgtk_image_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = NULL;
  element->tag = "image";
  element_register (element);
  rendgtk_widget_attr_register (element, GTK_TYPE_EVENT_BOX);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "src";
  e_attr->description = "Set the location of the image file on disk.";
  e_attr->value_desc = "filename";
  e_attr->set_attr_func = rendgtk_image_src_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "width";
  e_attr->description = "Set the width of the image.";
  e_attr->value_desc = "integer";
  e_attr->set_attr_func = rendgtk_image_width_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "height";
  e_attr->description = "Set the height of the image.";
  e_attr->value_desc = "integer";
  e_attr->set_attr_func = rendgtk_image_height_attr_set;
  element_register_attr (element, e_attr);
}

#else /* USE_IMLIB */

void
image_renderer_init (void)
{
  return;
}

#endif /* USE_IMLIB */
