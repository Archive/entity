#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "entity.h"
#include "erend.h"
#include "edebug.h"
#include "renderers.h"
#include <stdio.h>
#include "toggle-renderer.h"

static gint
rendgtk_toggle_selected_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget* toggle;

  edebug ("checkbox-renderer", "in rendgtk_toggle_selected_attr_set");
  toggle = erend_edata_get(node, "top-widget");
  if (!toggle) return FALSE;

  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(toggle),
                                        erend_value_is_true(value) );
  return TRUE;
}


void
rendgtk_toggle_ontoggle_callback (GtkWidget * toggle, XML_Node * node)
{
  gchar *function = NULL;
  int isset;

  function = xml_node_get_attr_str (node, "ontoggle");
  if (!function && g_str_equal (node->element, "radio"))
    {
      XML_Node *parent;
      parent = xml_node_find_parent (node, "radio-group");
      function = xml_node_get_attr_str (parent, "ontoggle");
    }

  /*Retrieve the state of the widget and set it to the state of the node. */
  isset = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));
  if (isset)
    {
      xml_node_set_attr_str_quiet (node, "selected", "true");
    }
  else
    {
      xml_node_set_attr_str_quiet (node, "selected", "false");
    }

  edebug("toggle-renderer", "selected = %s\n", 
                xml_node_get_attr_str(node, "selected") );

  if (function)
    {
      lang_call_function (node, function, "n", node);
    }
}

void
rendgtk_toggle_onselect_callback (GtkWidget * toggle, XML_Node * node)
{
  gchar *function = NULL;
  int isset;

  function = xml_node_get_attr_str (node, "onselect");

  if (!function && g_str_equal (node->element, "radio"))
    {
      XML_Node *parent;
      parent = xml_node_find_parent (node, "radio-group");
      function = xml_node_get_attr_str (parent, "onselect");
    }

  /*Retrieve the state of the widget and set it to the state of the node. */
  isset = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle));
  if (isset)
    {
      xml_node_set_attr_str (node, "selected", "true");
    }
  else
    {
      xml_node_set_attr_str (node, "selected", "false");
      return; /*Don't tell them about the toggle OFF signal. */
    }

  if (function)
    {
      lang_call_function (node, function, "n", node);
    }
}


void rendgtk_toggle_attr_register (Element* element)
{
  ElementAttr *e_attr;

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "selected";
  e_attr->description = "Toggles the button on or off.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "false,true";
  e_attr->set_attr_func = rendgtk_toggle_selected_attr_set;
  element_register_attr (element, e_attr);

}
