#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "entity.h"
#include "edebug.h"
#include "renderers.h"
#include <stdio.h>
#include "gtk-widget-attr.h"
#include "erend.h"

static void
rendgtk_dropdown_selchild_callback (GtkWidget * list, GtkWidget * unused,
				    gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *text;
  GtkWidget *dropdown;

  dropdown = erend_edata_get (node, "top-widget");
  if (!dropdown)
    return;

  text = gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (dropdown)->entry));
  xml_node_set_attr_str (node, "text", text);
}

static void
rendgtk_dropdown_onenter_callback (GtkWidget * widget, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;
  gchar *text;
  GtkWidget *dropdown;

  edebug ("dropdown-renderer", "in rendgtk_dropdown_onenter_callback\n");

  dropdown = erend_edata_get (node, "top-widget");
  if (!dropdown)
    return;

  gtk_signal_emit_stop_by_name (GTK_OBJECT
				(GTK_COMBO (dropdown)->entry), "activate");

  function = xml_node_get_attr_str (node, "onenter");

  text = gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (dropdown)->entry));
  xml_node_set_attr_str (node, "text", text);

  lang_call_function (node, function, "n", node);
}

static gint
rendgtk_dropdown_text_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget* dropdown;

  edebug("dropdown-renderer", "in rendgtk_dropdown_text_attr_set. %s", 
                value->str);

  dropdown = erend_edata_get(node, "top-widget");
  if (!dropdown)
    return TRUE;

  edebug("dropdown-renderer", "Setting.");

  /* Has to be a cstring... */
  gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(dropdown)->entry), value->str);
  return TRUE;
}

/*--element

Element Name: <dropdown>

This creates a gtk combo box

%widget%

*/

/*--attr

Default: none (no action)

*/
void
rendgtk_dropdown_parent (XML_Node * parent_node, XML_Node * child_node)
{
  GtkWidget *dropdown;
  GList *strings = NULL;
  char *text;
  char* ddtext;

  edebug ("dropdown-renderer", "in rendgtk_dropdown_parent\n");

  dropdown = erend_edata_get (parent_node, "top-widget");
  if (!dropdown)
    return;

  if (!g_str_equal (child_node->element, "string"))
    return;

  text = xml_node_get_attr_str (child_node, "text");

  if (!text)
    return;

  /* save the current value. */
  ddtext = xml_node_get_attr_str (parent_node, "text");
  edebug("dropdown-renderer", "was %s\n", ddtext);

  strings = gtk_object_get_data (GTK_OBJECT (dropdown), "strings");

  strings = g_list_append (strings, text);
  gtk_combo_set_popdown_strings (GTK_COMBO (dropdown), strings);

  gtk_object_set_data (GTK_OBJECT (dropdown), "strings", strings);

  /* Reset the text. */
  if (ddtext)
    {
      xml_node_set_attr_str(parent_node, "text", ddtext);
      edebug("dropdown-renderer", "was %s\n", ddtext);
    }
}



static void
rendgtk_dropdown_render (XML_Node * node)
{
  GtkWidget *dropdown;		/*The new dropdown. */

  dropdown = gtk_combo_new ();

  erend_edata_set (node, "top-widget", dropdown);
  erend_edata_set (node, "bootom-widget", dropdown);

  edebug ("dropdown-renderer", "in rendgtk_dropdown_parent\n");

  /*
     gtk_signal_connect_after (GTK_OBJECT(dropdown), "changed",
     rendgtk_dropdown_onchange_callback, node);
   */

  gtk_signal_connect (GTK_OBJECT (GTK_COMBO (dropdown)->entry),
		      "activate", rendgtk_dropdown_onenter_callback, node);

  gtk_signal_connect_after (GTK_OBJECT (GTK_COMBO (dropdown)->list),
			    "select-child",
			    rendgtk_dropdown_selchild_callback, node);

  xml_node_set_all_attr (node);
  rendgtk_show_cond (node, dropdown);
}

void
dropdown_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);
  element->render_func = rendgtk_dropdown_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_dropdown_parent;
  element->tag = "dropdown";
  element_register (element);

  /*No GTK_TYPE_COMBO! */
  rendgtk_widget_attr_register (element, GTK_TYPE_BOX);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "text";
  e_attr->description = "The text currently in the entry widget.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = rendgtk_dropdown_text_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onenter";
  e_attr->description = "Specify function called on a onenter event.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);
}

