#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "entity.h"
#include "renderers.h"
#include <stdio.h>
#include "edebug.h"
#include "erend.h"
#include "gtk-widget-attr.h"


static gint
rendgtk_spinner_attr_set (XML_Node * node, EBuf * attr, EBuf * value);

static void
rendgtk_spinner_onenter_callback (GtkWidget * widget, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;
  gchar *text;
  GtkWidget *spinner;

  spinner = erend_edata_get (node, "top-widget");
  if (!spinner)
    return;

  function = xml_node_get_attr_str (node, "onenter");

  text = gtk_entry_get_text (GTK_ENTRY (GTK_SPIN_BUTTON (spinner)));
  xml_node_set_attr_str_quiet (node, "value", text);

  lang_call_function (node, function, "n", node);
}

static void
rendgtk_spinner_onchange_callback (GtkWidget * widget, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;
  gchar *text;
  GtkWidget *spinner;

  spinner = erend_edata_get (node, "top-widget");
  if (!spinner)
    return;

  text = gtk_entry_get_text (GTK_ENTRY (GTK_SPIN_BUTTON (spinner)));
  xml_node_set_attr_str_quiet (node, "value", text);

  function = xml_node_get_attr_str (node, "onchange");

  lang_call_function (node, function, "n", node);
}


static gint
rendgtk_spinner_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *spinner;		/*The spinner. */
  GtkAdjustment *spinner_adj;	/*How to act. */
  float opt;


  spinner = erend_edata_get (node, "top-widget");
  if (!spinner)
    return FALSE;

  spinner_adj = gtk_spin_button_get_adjustment (GTK_SPIN_BUTTON (spinner));

  opt = erend_get_float(value);

  if (erend_value_equal (attr, "max"))
    {
      spinner_adj->upper = opt;
    }
  else if (erend_value_equal (attr, "min"))
    {
      spinner_adj->lower = opt;
    }
  else if (erend_value_equal (attr, "value"))
    {
      spinner_adj->value = opt;
      gtk_spin_button_set_value (GTK_SPIN_BUTTON (spinner), opt);
    }
  else if (erend_value_equal (attr, "step"))
    {
      spinner_adj->step_increment = opt;
    }
  else if (g_str_equal (attr, "wrap"))
    {
      gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (spinner),
				erend_value_is_true (value));
      return TRUE;
    }
  else if (erend_value_equal (attr, "places"))
    {
      gtk_spin_button_set_digits (GTK_SPIN_BUTTON (spinner), 
                                                erend_get_integer (value));
    }
  else if (erend_value_equal (attr, "onchange"))
    {
      return TRUE;
    }
  else if (erend_value_equal (attr, "onenter"))
    {
      return TRUE;
    }
  else
    {
      return FALSE;
    }

  gtk_spin_button_set_adjustment (GTK_SPIN_BUTTON (spinner), spinner_adj);
  return TRUE;
}

/*--element

Element Name: <spinner>

This creates a spin button for int and floating point input.

%widget%

*/

/*--attr

Attribute: "onchange"
If this attribute is set, then whenever a change is made 
to the spinner, the function specified as it's value will
be called.  The first argument is the full path to the entry that
generated the event.

Attribute: "max"
The largest value that the spinner can be.

Attribute: "min"
The smallest value the spinner can be.

Attribute: "step"
How much to add or subtract when a up or down button is pressed.
How much to add or subtract when a up or down key is pressed.

Attribute: "places"
The number of significan't places past the decimal point.

Attribute: "wrap"
If true then the spinner will wrap around at both min and max.

Default: none n(no action)

*/

static void
rendgtk_spinner_render (XML_Node * node)
{
  GtkWidget *spinner;		/*The new spinner. */
  GtkAdjustment *spinner_adj;	/*How to act. */

  gfloat stepf = 1.0, minf = 0.0;
  EBuf *step, *min;

  step = xml_node_get_attr (node, "step");
  if (step)
    stepf = erend_get_float (step);

  min = xml_node_get_attr (node, "min");
  if (min)
    minf = erend_get_float (min);

  edebug ("spinner-renderer", "step = %f, min = %f\n", stepf, minf);

  spinner_adj = (GtkAdjustment *)
    gtk_adjustment_new (minf, 0.0, 0.0, stepf, 0.0, 0.0);
  spinner = gtk_spin_button_new (spinner_adj, stepf, 0);

  erend_edata_set (node, "top-widget", spinner);
  erend_edata_set (node, "bottom-widget", spinner);

  xml_node_set_all_attr (node);

  gtk_signal_connect_after (GTK_OBJECT (spinner), "changed",
			    rendgtk_spinner_onchange_callback, node);
  gtk_signal_connect_after (GTK_OBJECT (spinner), "activate",
			    rendgtk_spinner_onenter_callback, node);

  rendgtk_show_cond (node, spinner);
}

void
spinner_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);

  element->render_func = rendgtk_spinner_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_box_pack;
  element->tag = "spinner";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onenter";
  e_attr->description =
    "Sets the function to call when the enter is pressed.";
  e_attr->value_desc = "function";
  /*e_attr->possible_values = ""; */
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onchange";
  e_attr->description =
    "Sets the function to call when the contents is changed.";
  e_attr->value_desc = "function";
  /*e_attr->possible_values = ""; */
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "min";
  e_attr->description = "Sets the minimum value the spinner can be.";
  e_attr->value_desc = "float";
  /*e_attr->possible_values = ""; */
  e_attr->set_attr_func = rendgtk_spinner_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "max";
  e_attr->description = "Sets the maximum value the spinner can be.";
  e_attr->value_desc = "float";
  /*e_attr->possible_values = ""; */
  e_attr->set_attr_func = rendgtk_spinner_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "step";
  e_attr->description = "Sets the increment size of the spinner.";
  e_attr->value_desc = "float";
  /*e_attr->possible_values = ""; */
  e_attr->set_attr_func = rendgtk_spinner_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "places";
  e_attr->description =
    "Sets the number of decimal places to show in the spinner.";
  e_attr->value_desc = "integer";
  /*e_attr->possible_values = ""; */
  e_attr->set_attr_func = rendgtk_spinner_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "wrap";
  e_attr->description =
    "If set the spinner should wrap around at min-1 and max+1";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "false,true";
  e_attr->set_attr_func = rendgtk_spinner_attr_set;
  element_register_attr (element, e_attr);

  rendgtk_widget_attr_register (element, GTK_TYPE_SPIN_BUTTON);
}
