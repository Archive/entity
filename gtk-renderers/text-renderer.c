#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "perl-embed.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "gtk-widget-attr.h"
#include "erend.h"
#include "edebug.h"


static GdkFont *font = NULL;

static int
rendgtk_text_onchange_callback (GtkWidget * text, XML_Node * node)
{
   /*FIXME*/ return TRUE;
}

static void
rendgtk_text_set_data (XML_Node * node, XML_Char * data)
{
  GtkWidget *text;
  gint len;
  gint point;

  text = erend_edata_get (node, "top-widget");
  if (!text)
    return;

  gtk_text_freeze (GTK_TEXT (text));
  len = gtk_text_get_length (GTK_TEXT (text));
  point = gtk_text_get_point (GTK_TEXT (text));

  edebug("text-renderer", "len = %i, point = %i", len, point);

  gtk_text_set_point (GTK_TEXT (text), 0);
  gtk_text_forward_delete (GTK_TEXT (text), len);
  
  /* we just set it straight to the text widget, and never update 
     the node data, as its done as a virtual get straight from the
     text widget anyway. */
  if (data)
    gtk_text_insert (GTK_TEXT (text), font, NULL, NULL, data, strlen (data));
  else if (node->data && node->data->str)
    gtk_text_insert (GTK_TEXT (text), font, NULL, NULL, data, strlen (data));

  /* try to reset the point in the text widget, 
     but if we can just set as the end.*/
  len = gtk_text_get_length (GTK_TEXT (text));
  if (len < point)
    point = len;

  gtk_text_set_point (GTK_TEXT (text), point);

  gtk_text_thaw (GTK_TEXT (text));
}

static GString *
rendgtk_text_get_data (XML_Node * node)
{
  static GString *data_str = NULL;
  GtkWidget *text;
  gchar *chars;
  gint len;

  if (!data_str)
    data_str = g_string_sized_new (8192);

  g_string_truncate (data_str, 0);
  
  edebug("text-renderer", "in get_data");
  
  text = erend_edata_get (node, "top-widget");
  if (!text)
    return NULL;

  len = gtk_text_get_length (GTK_TEXT (text));
  chars = gtk_editable_get_chars (GTK_EDITABLE (text), 0, len);

  g_string_append (data_str, chars);

  return (data_str);
}


static void
rendgtk_text_render (XML_Node * node)
{
  GtkWidget *text;

  text = gtk_text_new (NULL, NULL);

  erend_edata_set (node, "top-widget", text);
  erend_edata_set (node, "bottom-widget", text);

  gtk_signal_connect (GTK_OBJECT (text), "changed",
		      GTK_SIGNAL_FUNC (rendgtk_text_onchange_callback), node);
  if (!font)
    font = gdk_font_load ("-adobe-courier-medium-r-normal--*-120-*-*-*-*-*-*");

  /* TODO add font tags etc. */

  /* Note that this must be done here because the parser will append data
     directly to the node data */
  if (node->data)
    gtk_text_insert (GTK_TEXT (text), font, NULL, NULL,
		     node->data->str, node->data->len);
  else
    gtk_text_insert (GTK_TEXT (text), font, NULL, NULL,
		     "", 0);
    
  edebug("text-renderer", "rendered text!");

  xml_node_set_all_attr (node);
  rendgtk_show_cond (node, text);
}

static void
rendgtk_text_parent (XML_Node * parent_node, XML_Node * child_node)
{
  return;
}

void
text_renderer_init (void)
{
  Element *element;

  /* text */
  element = g_new0 (Element, 1);
  element->render_func = rendgtk_text_render;
  element->destroy_func = NULL;
  element->parent_func = rendgtk_text_parent;
  element->set_data_func = rendgtk_text_set_data;
  element->get_data_func = rendgtk_text_get_data;
  element->tag = "text";
  element_register (element);

  rendgtk_widget_attr_register (element, GTK_TYPE_TEXT);
}



