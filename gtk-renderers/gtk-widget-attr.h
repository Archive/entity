#ifndef __GTK_WIDGET_ATTR_H__
#define __GTK_WIDGET_ATTR__H__

#include <gtk/gtk.h>
#include "elements.h"
#include "xml-node.h"

/* Smartly registers and facilitates setting of widget attributes */
void rendgtk_widget_attr_register (Element * element, GtkType widget_type);


#endif /* __GTK_WIDGET_ATTR_H__ */
