#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "edebug.h"
#include "erend.h"


#ifndef BUFSIZ
#define BUFSIZ 1024
#endif

/*
 * The way an io tag can look in the xml. 
 * <io fd="1"
 *     onnewdata="got_part"
 *     onwrite="progress"        
 *     onerror="got_messed"
 *     mode="line,chunk"
 *  />
 */

typedef struct _WriteBuf
{
  char *buffer;
  char *obuf;
  int size;
}
WriteBuf;

/* This ends up being the max size of a line */
#define QUEUE_BUF_SIZE 65536
GSList *buffer_list;


/* Releases all memory and watch tags involved with doing
   reading */
static void
rendgtk_io_release_read_watch (XML_Node *node)
{
  gint rtag;
  EBuf *buffer;
  
  edebug ("io-renderer", "releasing input stream information");
  
  rtag = GPOINTER_TO_INT (erend_edata_get (node, "rendgtk-io-rtag"));
  erend_edata_set (node, "rendgtk-io-rtag", NULL);
  if (rtag)
    gdk_input_remove (rtag);
  
  edebug ("io-renderer", "releasing input stream tag %d", rtag);
  
  buffer = erend_edata_get (node, "rendgtk-io-read-buffer");
  if (buffer)
    ebuf_free (buffer);
  
  erend_edata_set (node, "rendgtk-io-read-buffer", NULL);
}  

/* Releases watch tags involved with doing writes */

static void
rendgtk_io_release_write_watch (XML_Node *node)
{
  gint wtag;
  
  edebug ("io-renderer", "releasing write stream information");
  
  wtag = GPOINTER_TO_INT (erend_edata_get (node, "rendgtk-io-wtag"));
  erend_edata_set (node, "rendgtk-io-wtag", NULL);
  if (wtag)
    gdk_input_remove (wtag);
  
  edebug ("io-renderer", "releasing input stream tag %d", wtag);
  
  erend_edata_set (node, "rendgtk-io-read-buffer", NULL);
}  

/* chunk mode is real easy.. read chunk, pass it on */
static int
rendgtk_io_read_chunk_mode_callback (XML_Node * node,
				     gint source, GdkInputCondition cond)
{
  gchar *function;
  gint nread;
  gint fd;
  gchar *buffer[BUFSIZ + 1];

  function = xml_node_get_attr_str (node, "onnewdata");

  /* Get fd */
  fd = GPOINTER_TO_INT (erend_edata_get (node, "rendgtk-io-fd"));
  if (fd < 0)
    return (FALSE);

  edebug ("io-renderer", "in chunk_mode read callback, gonna read from fd %d",
	  fd);
  
  nread = read (fd, buffer, BUFSIZ);

  edebug ("io-renderer", "binary, nread = %i", nread);

  /* If we read -1, assume there's an error and quit watching for reads */
  if (nread <= 0)
    rendgtk_io_release_read_watch (node);
  
  /* we pass nread straight so they can use it to see status too */
  if (function)
    lang_call_function (node, function, "nbi", node, buffer, nread, nread);
  
  return (TRUE);
}

  
/* When new data arrives in line mode, we call this with the 
   new data.. this will sort out the data and call a function
   if there's a new line to pass on */
static void
rendgtk_io_renderer_newdata (XML_Node *node, gchar *inbuf, 
			     gint has_newline)
{
  EBuf *buffer;
  gint nread;
  gchar *function;
  
  nread = strlen (inbuf);
  
  function = xml_node_get_attr_str (node, "onnewdata");
  buffer = erend_edata_get (node, "rendgtk-io-read-buffer");
  
  edebug ("io-renderer", "newdata function is %s", function);
  
  /* if we find an eol.. */
  if (has_newline)
    {
      edebug ("io-renderer", "EOL found checking..");
      
      /* check to see if there's already something in the buffer from before */
      if (!buffer || buffer->len == 0) 
	{
	  edebug ("io-renderer","No previous buffer, sending directly.");
	  
	  /* If not, we can pass this straight to the application */
	  if (function)
	    lang_call_function (node, function, "nbi", node, inbuf, nread, nread);
	  return;
	}
      else
	{
            edebug ("io-renderer", 
                    "EOL found, appending '%s' to previous buffer '%s' and sending.",
                    inbuf, buffer->str);
            /*else we have to get the previous buffer first, and then pass it on*/
            ebuf_append_cstring (buffer, inbuf);

            if (function)
                lang_call_function (node, function, "nbi", 
                        node, buffer->str, buffer->len, buffer->len);

            /* truncate buffer */
            ebuf_truncate (buffer, 0);
            return;
        }
    }
  else
    {
      /* If we make it here, it means there was no end of line found, 
         and we have to store it for the next pass */
      
      /* init buffer if we don't already have one */
      if (!buffer)
	{
	  buffer = ebuf_new ();
	  erend_edata_set (node, "rendgtk-io-read-buffer", buffer);
	}
      
      edebug ("io-renderer", "no EOL, appending '%s' to buffer.", inbuf);
      
      /* append data to buffer */
      ebuf_append_cstring (buffer, inbuf);
    }
}

/* Line mode is a bit more tricky, must write to a queue until we've
   determined that we have a full line to pass to the application */
static gint
rendgtk_io_read_line_mode_callback (XML_Node * node,
				    gint source, GdkInputCondition cond)
{
  gchar buffer[BUFSIZ + 1];
  gchar *p;
  gchar s;
  gint nread;
  gint fd;
  gint i;
  gint found = FALSE;
  
  edebug ("io-renderer",
	  "in rendgtk_io_read_callback, cond = %i", cond);
  
  fd = GPOINTER_TO_INT (erend_edata_get (node, "rendgtk-io-fd"));
  
  if (fd < 0)
    return (FALSE);
  
  nread = read (fd, buffer, BUFSIZ);
  
  /* insure \0 */
  buffer[nread] = '\0';

  p = buffer;
  /* Walk through string and break it into chunks on newlines */
  for (i = 0; i <= nread; i++)
    {
      if (buffer[i] == '\n')
	{
	  /* Save old value right after \n */
	  s = buffer [i + 1];
	  buffer[i + 1] = '\0';
	  rendgtk_io_renderer_newdata (node, p, TRUE);
	  /* restore value after \n */
	  buffer[i + 1] = s;
	  /* update p */
	  p = &buffer[i + 1];
	  found = TRUE;
	}
    }
  
  rendgtk_io_renderer_newdata (node, p, FALSE);
  
  if (nread <= 0)
    rendgtk_io_release_read_watch (node);    

  return (TRUE);
}


static GSList *
free_write_buffer (GSList *list, WriteBuf * buffer)
{
  GSList *tmp;
  
  if (list && list->data == buffer)
    {
      tmp = list;
      list = list->next;
      tmp->next = NULL;
      g_slist_free (tmp);
    } 

  g_free (buffer->obuf);
  g_free (buffer);
  return (list);
}


static int
rendgtk_io_sendq_callback (XML_Node * node,
			   gint source, GdkInputCondition cond)
{
  char *function;
  WriteBuf *buffer;
  GSList *write_list;
  gint ret;
  gint fd;

  edebug ("io-renderer", "rendgtk_io_sendq_callback");

  /* Get the fd */
  fd = GPOINTER_TO_INT (erend_edata_get (node, "rendgtk-io-fd"));

  write_list = erend_edata_get (node, "rendgtk-io-write-list");
  edebug ("io-renderer", "write_list = %i", write_list);


  if (!write_list || !write_list->data)
    {
      /* Nothing to write. */
      rendgtk_io_release_write_watch (node);
      return (FALSE);
    }

  buffer = (WriteBuf *) write_list->data;
  ret = write (fd, buffer->buffer, buffer->size);
  edebug ("io-renderer", "write ret = %i", ret);
  
  if (ret <= 0)
    {			
      /* Error on descriptor, effectivly and eof, clean up time */
      g_warning ("Error writing to file: %s", g_strerror (errno));
      rendgtk_io_release_write_watch (node);
      
      return (FALSE);
    }
  else if (ret < buffer->size)
    {
      buffer->buffer += ret;
      buffer->size -= ret;
    }
  else if (ret == buffer->size)
    {
      write_list = free_write_buffer (write_list, buffer);
      erend_edata_set (node, "rendgtk-io-write-list", write_list);
      
      if (!write_list)
	{	
	  /* We have written everything. */
	  edebug ("io-renderer", "done writing");
	  rendgtk_io_release_write_watch (node);
	}
    }
  
  function = xml_node_get_attr_str (node, "onwrite");
  lang_call_function (node, function, "ni", node, ret);

  return (TRUE);
}

static int
rendgtk_io_set_attr_sendq (XML_Node * node, gchar * data, gint send_size)
{
  WriteBuf *buffer;
  gint wtag;
  gint fd;
  GSList *write_list = NULL;
  GSList *write_list_tail = NULL;

  edebug ("io-renderer", "sendq buffer set, buffer size = %i", send_size);

  if (send_size <= 0)		/* nothing to send. */
    return (FALSE);

  buffer = g_new0 (WriteBuf, 1);
  buffer->size = send_size;

  buffer->obuf = g_new (char, send_size);
  memcpy (buffer->obuf, data, send_size);
  buffer->buffer = buffer->obuf;

  write_list = erend_edata_get (node, "rendgtk-io-write-list");
  write_list_tail = erend_edata_get (node, "rendgtk-io-write-list-tail");
  write_list = g_slist_append_tail (write_list, buffer, &write_list_tail);
  erend_edata_set (node, "rendgtk-io-write-list", write_list);
  erend_edata_set (node, "rendgtk-io-write-list-tail", write_list_tail);

  /* Get fd */
  fd = GPOINTER_TO_INT (erend_edata_get (node, "rendgtk-io-fd"));

  wtag = GPOINTER_TO_INT (erend_edata_get (node, "rendgtk-io-wtag"));

  if (fd && !wtag)
    {
      wtag = gdk_input_add_full (fd, GDK_INPUT_WRITE,
				 (void *) rendgtk_io_sendq_callback,
				 node, NULL);
      edebug ("io-renderer", "added new write tag, wtag = %i", wtag);
      erend_edata_set (node, "rendgtk-io-wtag", GINT_TO_POINTER (wtag));
    }

  return (TRUE);
}

static int
rendgtk_io_error_callback (XML_Node * node,
			   gint source, GdkInputCondition cond)
{
  gchar *function;
  
  edebug ("io-renderer", "rendgtk_io_error_callback");
  
  function = xml_node_get_attr_str (node, "onerror");
  
  lang_call_function (node, function, "n", node);
  
  return (TRUE);
}

static void
rendgtk_io_release_watch (XML_Node *node)
{
  gint old_etag;

  /* Delete old io watchers if they exist ... */
  
  /* This looks after all the read watcher stuff */
  rendgtk_io_release_read_watch (node);
  /* and the error tag... */
  rendgtk_io_release_write_watch (node);
  old_etag = GPOINTER_TO_INT (erend_edata_get (node, "rendgtk-io-etag"));

  /* need to delete the old tag and callback connection. */
  edebug ("io-renderer", "going to delete old tags");

  if (old_etag)
      gdk_input_remove (old_etag);
  
  erend_edata_set (node, "rendgtk-io-etag", NULL);
}

/* Called with the fd is set, so we have to setup new handlers for everything. */

static int
rendgtk_io_fd_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  gint source;
  gint rtag;
  gint etag;
  EBuf *line_mode;
  
  edebug ("io-renderer", "fd attribute set");
  
  /* Release old fd watchers */
  rendgtk_io_release_watch (node);

  /* clear list */
  erend_edata_set (node, "rendgtk-io-line", NULL);
  erend_edata_set (node, "rendgtk-io-write-list", NULL);

  /* if source is < 0 then they are telling us to stop watching, or they
     have a bad fd or something.. */
  source = erend_get_integer (value);
  if (source < 0)
    return (TRUE);

  edebug ("io-renderer", "fd set to %d", source);
  
  /* No blocking io... this could cause odd interactions, we'll have to see */
  fcntl (source, F_SETFL, O_NONBLOCK);

  edebug ("io-renderer", "source = %i", source);
  
  line_mode = xml_node_get_attr (node, "mode");
  
  if (line_mode && erend_value_equal (line_mode, "line")) {
    rtag = gdk_input_add_full (source, GDK_INPUT_READ,
			       (void *) rendgtk_io_read_line_mode_callback, node,
			       NULL);
  } else {
    rtag = gdk_input_add_full (source, GDK_INPUT_READ,
			       (void *) rendgtk_io_read_chunk_mode_callback, node,
			       NULL);
  }
  
  erend_edata_set (node, "rendgtk-io-rtag", GINT_TO_POINTER (rtag));
  edebug ("io-renderer", "rtag = %i", rtag);

  etag = gdk_input_add_full (source, GDK_INPUT_EXCEPTION,
			     (void *) rendgtk_io_error_callback, node,
			     NULL);
  erend_edata_set (node, "rendgtk-io-etag", GINT_TO_POINTER (etag));
  edebug ("io-renderer", "etag = %i", etag);
  
  erend_edata_set (node, "rendgtk-io-fd", GINT_TO_POINTER (source));

  return (TRUE);
}


static void
rendgtk_io_render (XML_Node * node)
{
  edebug ("io-renderer", "rendering");
  xml_node_set_all_attr (node);
}

static void
rendgtk_io_destroy (XML_Node * node)
{
  /* Release old fd watchers */
  rendgtk_io_release_watch (node);
}

void
io_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);
  element->render_func = rendgtk_io_render;
  element->destroy_func = rendgtk_io_destroy;
  element->tag = "io";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "fd";
  e_attr->description = "Specify the file descripter to watch.";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "-1,*";
  e_attr->set_attr_func = rendgtk_io_fd_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "sendq";
  e_attr->description = "Specify the data to send on the fd.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = (void *) rendgtk_io_set_attr_sendq;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "mode";
  e_attr->description = "Specify whether to send lines or binary chunks.";
  e_attr->value_desc = "string";
  e_attr->possible_values = "chunk,line";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onnewdata";
  e_attr->description = "Function to call with data read from the fd.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onwrite";
  e_attr->description =
    "Function to call with the number of bytes written to fd.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onerror";
  e_attr->description =
    "Function to call when an exception occurs on the fd.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);
}

