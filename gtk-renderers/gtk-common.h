#ifndef __GTK_COMMON_H__
#define __GTK_COMMON_H__

#include <gtk/gtk.h>
#include "elements.h"

#define BOX_PACK_FILL_DEFAULT TRUE
#define BOX_PACK_EXPAND_DEFAULT FALSE
#define BOX_PACK_PADDING_DEFAULT 1


void rendgtk_init (void);


/* only show if visible attribute is not "false" */
void rendgtk_show_cond (XML_Node * node, GtkWidget * widget);

/* most common packing technique */
void rendgtk_box_pack (XML_Node * parent_node, XML_Node * child_node);

/* Default gtk element destructor */
void rendgtk_element_destroy (XML_Node * node);

void rendgtk_containerbox_attr_register (Element * element);

/* This one is for <drag> tags */
void rendgtk_dnd_dragtag_source_create (XML_Node * node, GtkWidget * widget);

/* This one is to make it a viable destination for a dragged <drag> node */
void rendgtk_dnd_dragtag_target_create (XML_Node * node, GtkWidget * widget);

/* Set a widget as dragable */
void rendgtk_dnd_dragable_set (XML_Node * node, GtkWidget * widget);

/* Set as a viable target for dnd ops */
void rendgtk_dnd_target_create (XML_Node * node, GtkWidget * widget);

void rendgtk_adj_print (GtkAdjustment * adj);


#endif /* __GTK_COMMON_H__ */
