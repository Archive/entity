#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include "entity.h"
#include "gtk-common.h"
#include "gtk-widget-attr.h"


static gint
rendgtk_progress_value_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *progress;
  gfloat opt;
  EBuf *activity;

  progress = erend_edata_get (node, "top-widget");
  if (!progress)
    return (TRUE);

  opt = erend_get_float (value);

  if (opt <= 1)
    gtk_progress_bar_update (GTK_PROGRESS_BAR (progress), opt);

  activity = xml_node_get_attr (node, "activity");
  /* If we are in activity mode just do stuff. Doesn't work.  FIXME */
  if (activity && erend_value_is_true (activity))
    {
      gtk_progress_bar_update (GTK_PROGRESS_BAR (progress), .1);
    }

  return (TRUE);
}

static gint
rendgtk_progress_steps_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *progress;
  progress = erend_edata_get (node, "top-widget");
  if (!progress)
    return (TRUE);

  gtk_progress_bar_set_bar_style (GTK_PROGRESS_BAR (progress),
				  GTK_PROGRESS_DISCRETE);
  gtk_progress_bar_set_activity_step (GTK_PROGRESS_BAR (progress),
				      erend_get_integer (value));

  return (TRUE);
}

static gint
rendgtk_progress_blocks_attr_set (XML_Node * node, EBuf * attr,
				  EBuf * value)
{
  GtkWidget *progress;
  progress = erend_edata_get (node, "top-widget");
  if (!progress)
    return (TRUE);


  gtk_progress_bar_set_bar_style (GTK_PROGRESS_BAR (progress),
				  GTK_PROGRESS_DISCRETE);
  gtk_progress_bar_set_discrete_blocks (GTK_PROGRESS_BAR (progress),
					erend_get_integer (value));
  /*Wonder what this does...I'll set it for now and hope it does 
   * what people want.  MW*/
  gtk_progress_bar_set_activity_blocks (GTK_PROGRESS_BAR (progress),
					erend_get_integer (value));
  return (TRUE);
}

static gint
rendgtk_progress_activity_attr_set (XML_Node * node, EBuf * attr,
				    EBuf * value)
{
  GtkWidget *progress;

  progress = erend_edata_get (node, "top-widget");
  if (!progress)
    return (TRUE);

  gtk_progress_set_activity_mode (GTK_PROGRESS (progress),
				  erend_value_is_true (value));
  return (TRUE);
}

static void
rendgtk_progress_render (XML_Node * node)
{
  GtkWidget *progress;
  GtkProgressBarOrientation orient;

  progress = gtk_progress_bar_new ();

  orient = GTK_PROGRESS_LEFT_TO_RIGHT;
  gtk_progress_bar_set_orientation (GTK_PROGRESS_BAR (progress), orient);
  gtk_progress_bar_set_bar_style (GTK_PROGRESS_BAR (progress),
				  GTK_PROGRESS_CONTINUOUS);

  erend_edata_set (node, "top-widget", progress);
  erend_edata_set (node, "bottom-widget", progress);

  xml_node_set_all_attr (node);
  rendgtk_show_cond (node, progress);
}

void
progress_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);

  element->render_func = rendgtk_progress_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_box_pack;
  element->tag = "progress";
  element_register (element);
  rendgtk_widget_attr_register (element, GTK_TYPE_PROGRESS_BAR);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "value";
  e_attr->description =
    "What percentage of the way done the meter should read.";
  e_attr->value_desc = "percentage";
  e_attr->set_attr_func = rendgtk_progress_value_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "steps";
  e_attr->description = "I really don't know what this does. [MW]";
  e_attr->value_desc = "integer";
  e_attr->set_attr_func = rendgtk_progress_steps_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "blocks";
  e_attr->description = "The number of blocks to cut the meter into.";
  e_attr->value_desc = "integer";
  e_attr->set_attr_func = rendgtk_progress_blocks_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "activity";
  e_attr->description = "If set the meter bounces back and forth.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "true,false";
  e_attr->set_attr_func = rendgtk_progress_activity_attr_set;
  element_register_attr (element, e_attr);


}
