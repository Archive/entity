#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "erend.h"
#include "edebug.h"
#include "gtk-widget-attr.h"

/*--element

Element Name: <button>

Creates a new button at the appropriate place in the tree.  Note that
it's up to you to fill the button with another element (usually a
label, but could be just about anything).

*/


static void
rendgtk_button_onclick_callback (GtkWidget * widget, gpointer user_data)
{
  XML_Node *node = user_data;
  gchar *function = NULL;

  function = xml_node_get_attr_str (node, "onclick");

  lang_call_function (node, function, "n", node);
}

static gint
rendgtk_button_onclick_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *button;

  if (!node->entity_data)
    return (FALSE);

  button = erend_edata_get (node, "top-widget");

  return (TRUE);
}


static void
rendgtk_button_render (XML_Node * node)
{
  GtkWidget *button;
  GtkWidget *vbox;

  button = gtk_button_new ();

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (button), vbox);

  erend_edata_set (node, "top-widget", button);
  erend_edata_set (node, "bottom-widget", vbox);

  edebug ("button-renderer",
	  "connecting onclick signal handler to %s", node->name);

  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      rendgtk_button_onclick_callback, node);

  xml_node_set_all_attr (node);

  rendgtk_show_cond (node, button);
  gtk_widget_show (vbox);
}


void
button_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);
  element->render_func = rendgtk_button_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_box_pack;
  element->tag = "button";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onclick";
  e_attr->description = "Sets up a handler for click events.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = rendgtk_button_onclick_attr_set;
  element_register_attr (element, e_attr);

  rendgtk_widget_attr_register (element, GTK_TYPE_BUTTON);
  rendgtk_containerbox_attr_register (element);
}
