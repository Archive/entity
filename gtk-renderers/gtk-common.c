#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include <string.h>

#include "elements.h"
#include "xml-node.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "lang.h"
#include "init.h"
#include "edebug.h"
#include "erend.h"
#include "xml-parser.h"
#include "entity.h"

enum
{
  TARGET_STRING,
  TARGET_URL,
  TARGET_XML_SRC
};

/* For now, I'm only going to do URI/string drags.. */

static GtkTargetEntry target_table[] = {
  {"STRING", 0, TARGET_STRING},
  {"text/plain", 0, TARGET_STRING},
  {"text/uri-list", 0, TARGET_URL},
  {"text/url-list", 0, TARGET_URL},
  {"text/url", 0, TARGET_URL},
  {"text/xml-source", 0, TARGET_XML_SRC}
};

static guint n_targets = sizeof (target_table) / sizeof (target_table[0]);

static GtkTargetEntry xml_node_target_table[] = {
  {"text/xml-source", 0, TARGET_XML_SRC}
};

static guint n_xml_node_targets =
  sizeof (xml_node_target_table) / sizeof (xml_node_target_table[0]);


/* Called when something is dragged onto something else. */
static void
builtins_drag_source_get_data (GtkWidget * widget,
			       GdkDragContext * context,
			       GtkSelectionData * selection_data,
			       guint info, guint time, gpointer data)
{
  gchar *value = NULL;
  gchar *function;
  XML_Node *node = data;

  /* If the node that's being dragged is a <object> element, we just send the
     data to be the XML of the object */
  if (g_str_equal (node->element, "object"))
    {
      gchar *str;

      str = xml_tree_get_xml (node);

      gtk_selection_data_set (selection_data,
			      selection_data->target, 8, str, strlen (str));
      return;
    }

  function = xml_node_get_attr_str (node, "ondrag");
  lang_call_function (node, function, "n", node);

  if (info == TARGET_STRING)
    {
      value = xml_node_get_attr_str (node, "dragdata-text");
      edebug ("gtk-common", "drag dest wanted string");
    }

  if (info == TARGET_URL)
    {
      edebug ("gtk-common", "drag dest wanted url");
      value = xml_node_get_attr_str (node, "dragdata-url");
    }

  if (value)
    gtk_selection_data_set (selection_data,
			    selection_data->target, 8, value, strlen (value));
}

/* Called when something receives a drop */
static void
builtins_drag_target_data_received (GtkWidget * widget,
				    GdkDragContext * context,
				    gint x,
				    gint y,
				    GtkSelectionData * data,
				    guint info, guint time)
{
  XML_Node *node;
  gchar *function;
  gchar *drag_data;

  node = gtk_object_get_data (GTK_OBJECT (widget), "xml-node");
  drag_data = (gchar *) data->data;

  
  if (info == TARGET_XML_SRC)
    {
      edebug ("gtk-common", "XML Source dropped - '%s'", drag_data);
      
      if (context->suggested_action == GDK_ACTION_MOVE)
	{
	  /* TODO: Should delete source tree */
	  xml_parse_string (node, drag_data);
	  xml_tree_render (node);
	}
      else
	{
	  xml_parse_string (node, drag_data);
	  xml_tree_render (node);
	}

      return;
    }

  if ((data->length >= 0) && (data->format == 8))
    {
      edebug ("gtk-common", "Received \"%s\" for drag data", drag_data);
      gtk_drag_finish (context, TRUE, FALSE, time);

      if (node)
	{
	  edebug ("gtk-common", "would notify node '%s'", node->name);

	  function = xml_node_get_attr_str (node, "ondrop");
	  edebug ("gtk-common",
		  "Checking suggested drag type - %d",
		  context->suggested_action);

	  if (context->suggested_action == GDK_ACTION_MOVE)
	    {
	      lang_call_function (node, function, "nss",
				  node, drag_data, g_strdup ("move"));
	    }
	  else
	    {
	      lang_call_function (node, function, "nss",
				  node, drag_data, g_strdup ("copy"));
	    }
	}
      return;
    }
  else
    {
      gtk_drag_finish (context, FALSE, FALSE, time);
    }
}


/* This one is for <object> tags */
void
rendgtk_dnd_dragtag_source_create (XML_Node * node, GtkWidget * widget)
{
  gtk_drag_source_set (widget, GDK_BUTTON1_MASK | GDK_BUTTON3_MASK,
		       xml_node_target_table, n_xml_node_targets,
		       GDK_ACTION_COPY | GDK_ACTION_MOVE);

  /* Insure previous handler is disconnected */
  /* gtk_signal_disconnect_by_data (GTK_OBJECT (widget), node); */
  
  gtk_signal_connect (GTK_OBJECT (widget), "drag_data_get",
		      GTK_SIGNAL_FUNC (builtins_drag_source_get_data), node);
}


/* This one is to make it a viable destination for a dragged <drag> node */
void
rendgtk_dnd_dragtag_target_create (XML_Node * node, GtkWidget * widget)
{
  /* TODO: this should not be GTK_DEST_DEFAULT_ALL */
  gtk_drag_dest_set (widget,
		     GTK_DEST_DEFAULT_ALL,
		     target_table, n_targets,
		     GDK_ACTION_COPY | GDK_ACTION_MOVE);

  gtk_signal_connect (GTK_OBJECT (widget), "drag_data_received",
		      GTK_SIGNAL_FUNC (builtins_drag_target_data_received),
		      NULL);

  gtk_object_set_data (GTK_OBJECT (widget), "xml-node", node);
}


/* Set a widget as dragable */

void
rendgtk_dnd_dragable_set (XML_Node * node, GtkWidget * widget)
{

  gtk_drag_source_set (widget, GDK_BUTTON1_MASK | GDK_BUTTON3_MASK,
		       target_table, n_targets,
		       GDK_ACTION_COPY | GDK_ACTION_MOVE);

  /*
     gtk_drag_source_set_icon (widget,
     gtk_widget_get_colormap (window),
     drag_icon, drag_mask);

     gdk_pixmap_unref (drag_icon);
     gdk_pixmap_unref (drag_mask);
   */

  gtk_signal_connect (GTK_OBJECT (widget), "drag_data_get",
		      GTK_SIGNAL_FUNC (builtins_drag_source_get_data), node);

  /* gtk_signal_connect (GTK_OBJECT (widget), "drag_data_delete",
     GTK_SIGNAL_FUNC (source_drag_data_delete), NULL); */
}


/* Set as a viable target for dnd ops */
void
rendgtk_dnd_target_create (XML_Node * node, GtkWidget * widget)
{
  gtk_drag_dest_set (widget,
		     GTK_DEST_DEFAULT_ALL,
		     target_table, n_targets,
		     GDK_ACTION_COPY | GDK_ACTION_MOVE);

  gtk_signal_connect (GTK_OBJECT (widget), "drag_data_received",
		      GTK_SIGNAL_FUNC (builtins_drag_target_data_received),
		      NULL);

  gtk_object_set_data (GTK_OBJECT (widget), "xml-node", node);
}


static gint
rendgtk_widget_containerbox_label_set (XML_Node * node,
				       EBuf * attr, EBuf * value)
{
  GtkWidget *box;
  GtkWidget *label;

  edebug ("gtk-widget-attr", "node->element = %s", node->element);
  box = erend_edata_get (node, "bottom-widget");
  label = erend_edata_get (node, "bottom-widget-label");

  if (!box)			/* DON'T TEST LABEL! It doesn't have to be created yet. */
    return FALSE;

  /* Unshow the label if its set to "". */
  if (strlen (value->str) == 0 && label)
    {
      gtk_widget_hide (GTK_WIDGET (label));
      return (TRUE);
    }

  if (!label)
    {
      label = gtk_label_new (value->str);

      erend_edata_set (node, "bottom-widget-label", label);
      gtk_box_pack_start (GTK_BOX (box), label, 0, 0, 0);
      gtk_widget_show (label);
    }
  else
    {
      gtk_label_set_text (GTK_LABEL (label), value->str);
      gtk_widget_show (label);
    }

  return (TRUE);
}

void
widget_containerbox_child_attr_set (XML_Node * parent_node,
				    XML_Node * child_node, EBuf * attr,
				    EBuf * value)
{
  EBuf *expandv;
  gint expand = BOX_PACK_EXPAND_DEFAULT;
  EBuf *fillv;
  gint fill = BOX_PACK_FILL_DEFAULT;
  EBuf *paddingv;
  guint padding = BOX_PACK_PADDING_DEFAULT;
  GtkWidget *box;
  GtkWidget *child_widget;

  box = erend_edata_get (parent_node, "bottom-widget");
  child_widget = erend_edata_get (child_node, "top-widget");

  if (!box || !child_widget)
    return;

  expandv = xml_node_get_attr (child_node, "expand");
  if (expandv)
    expand = erend_value_is_true (expandv);

  fillv = xml_node_get_attr (child_node, "fill");
  if (fillv)
    fill = erend_value_is_true (fillv);

  paddingv = xml_node_get_attr (child_node, "padding");
  if (paddingv)
    padding = erend_get_integer (paddingv);

  gtk_box_set_child_packing (GTK_BOX (box), child_widget,
			     expand, fill, padding, GTK_PACK_START);
}

void
rendgtk_containerbox_attr_register (Element * element)
{
  ElementAttr *e_attr;

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "label";
  e_attr->description = "Add a label the the widgets's bottom level box.";
  e_attr->value_desc = "string";
  /*e_attr->possible_values = ""; */
  e_attr->set_attr_func = rendgtk_widget_containerbox_label_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "expand";
  e_attr->description =
    "Toggle whether the widgets area should 'expand' if given space to do so.";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "true,false";
  e_attr->set_child_attr_func = widget_containerbox_child_attr_set;
  element_register_child_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "fill";
  e_attr->description =
    "Toggle whether the widget itself should 'fill' any extra space given to it";
  e_attr->value_desc = "boolean";
  e_attr->possible_values = "false,true";
  e_attr->set_child_attr_func = widget_containerbox_child_attr_set;
  element_register_child_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "padding";
  e_attr->description = "Amount of padding in pixels to place around widget";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "0,*";
  e_attr->set_child_attr_func = widget_containerbox_child_attr_set;
  element_register_child_attr (element, e_attr);
}

/* only show if visible attribute is not "false" */
void
rendgtk_show_cond (XML_Node * node, GtkWidget * widget)
{
  EBuf *value;

  value = xml_node_get_attr (node, "visible");

  if ((!value) || erend_value_is_true (value))
    gtk_widget_show (widget);
}

/* Default destroy for gtk widgets */
void
rendgtk_element_destroy (XML_Node * node)
{
  GtkWidget *top = erend_edata_get (node, "top-widget");
  GtkWidget *extra= erend_edata_get (node, "extra-destroy-widget");

  if (top)
    gtk_widget_destroy (top);

  if (extra)
    gtk_widget_destroy (extra);

  erend_edata_set(node, "top-widget", NULL);
  erend_edata_set(node, "bottom-widget", NULL);
  erend_edata_set(node, "extra-destroy-widget", NULL);
}

/* This box_pack is used mostly for inside other widgets */
void
rendgtk_box_pack (XML_Node * parent_node, XML_Node * child_node)
{
  gint fill = BOX_PACK_FILL_DEFAULT;
  EBuf *fillv;
  gint expand = BOX_PACK_EXPAND_DEFAULT;
  EBuf *expandv;
  gint padding = BOX_PACK_PADDING_DEFAULT;
  EBuf *paddingv;
  GtkWidget *child;
  GtkWidget *parent;

  child = erend_edata_get (child_node, "top-widget");
  parent = erend_edata_get (parent_node, "bottom-widget");

  if (!child || !parent)
    return;
  
  if (GTK_IS_WINDOW (child))
    return;

  expandv = xml_node_get_attr (child_node, "expand");
  if (expandv)
    expand = erend_value_is_true (expandv);

  fillv = xml_node_get_attr (child_node, "fill");
  if (fillv)
    fill = erend_value_is_true (fillv);

  paddingv = xml_node_get_attr (child_node, "padding");
  if (paddingv)
    padding = erend_get_integer (paddingv);
  
  edebug ("gtk-common", "Packing child %s into parent %s", child_node->name,
	  parent_node->name);
  
  gtk_box_pack_start (GTK_BOX (parent), child, expand, fill, padding);
}


void
rendgtk_init (void)
{
  align_renderer_init ();
  object_renderer_init ();
  button_renderer_init ();
  checkbox_renderer_init ();
  clist_renderer_init ();
  databox_renderer_init (); 
  dropdown_renderer_init ();
  ebox_renderer_init ();
  entry_renderer_init ();
  filesel_renderer_init ();
  fixed_renderer_init ();
  frame_renderer_init ();
  image_renderer_init ();
  io_renderer_init ();
  label_renderer_init ();
  list_renderer_init ();
  menu_renderer_init ();
  notebook_renderer_init ();
  pane_renderer_init ();
  progress_renderer_init ();
  raw_io_renderer_init ();
  radio_renderer_init ();
  scrollwindow_renderer_init ();
  spinner_renderer_init ();
  text_renderer_init ();
  timer_renderer_init ();
  tree_renderer_init ();
  window_renderer_init ();
  wrapalign_renderer_init ();
}

