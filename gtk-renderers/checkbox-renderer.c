#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "lang.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "entity.h"
#include "erend.h"
#include "renderers.h"
#include <stdio.h>
#include "toggle-renderer.h"
#include "gtk-widget-attr.h"

static void
rendgtk_checkbox_render (XML_Node * node)
{
  GtkWidget *checkbox;
  GtkWidget *vbox;

  checkbox = gtk_check_button_new ();
  vbox = gtk_vbox_new (TRUE, 0);
  gtk_container_add (GTK_CONTAINER (checkbox), vbox);

  erend_edata_set (node, "top-widget", checkbox);
  erend_edata_set (node, "bottom-widget", vbox);

  xml_node_set_all_attr (node);

  gtk_signal_connect (GTK_OBJECT (checkbox), "toggled",
		      GTK_SIGNAL_FUNC (rendgtk_toggle_ontoggle_callback),
		      node);
  gtk_signal_connect (GTK_OBJECT (checkbox), "toggled",
		      GTK_SIGNAL_FUNC (rendgtk_toggle_onselect_callback),
		      node);

  rendgtk_show_cond (node, checkbox);
  gtk_widget_show (vbox);
}

void
checkbox_renderer_init (void)
{
  Element *element;

  element = g_new0 (Element, 1);

  element->render_func = rendgtk_checkbox_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_box_pack;
  element->tag = "checkbox";
  element_register (element);

  rendgtk_widget_attr_register (element, GTK_TYPE_CHECK_BUTTON);

  rendgtk_toggle_attr_register (element);

  rendgtk_containerbox_attr_register (element);
}
