#include <gtk/gtk.h>
#include <xmlparse.h>
#include <stdlib.h>
#include "elements.h"
#include "perl-embed.h"
#include "xml-tree.h"
#include "gtk-common.h"
#include "gtk-widget-attr.h"
#include "erend.h"


static gint
rendgtk_frame_title_attr_set (XML_Node * node, EBuf * attr, EBuf * value)
{
  GtkWidget *frame;

  frame = erend_edata_get (node, "top-widget");
  if (!frame)
    return FALSE;

  gtk_frame_set_label (GTK_FRAME (frame), value->str);
  return (TRUE);
}

static void
rendgtk_frame_render (XML_Node * node)
{
  GtkWidget *frame;
  GtkWidget *vbox;

  frame = gtk_frame_new ("");

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  erend_edata_set (node, "top-widget", frame);
  erend_edata_set (node, "bottom-widget", vbox);

  gtk_widget_show (vbox);
  rendgtk_show_cond (node, frame);
  xml_node_set_all_attr (node);
}

void
frame_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  /* frame */
  element = g_new0 (Element, 1);
  element->render_func = rendgtk_frame_render;
  element->destroy_func = rendgtk_element_destroy;
  element->parent_func = rendgtk_box_pack;
  element->tag = "frame";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "title";
  e_attr->description = "Set the text at the top of the frame.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = rendgtk_frame_title_attr_set;
  element_register_attr (element, e_attr);

  rendgtk_widget_attr_register (element, GTK_TYPE_FRAME);
}
