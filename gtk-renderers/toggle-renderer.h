#ifndef __TOGGLE_RENDERER_H__
#define __TOGGLE_RENDERER_H__
static gint
rendgtk_toggle_selected_attr_set (XML_Node * node, EBuf * attr, EBuf * value);
void rendgtk_toggle_ontoggle_callback (GtkWidget * toggle, XML_Node * node);
void rendgtk_toggle_onselect_callback (GtkWidget * toggle, XML_Node * node);
void rendgtk_toggle_attr_register (Element* element);
#endif /*__TOGGLE_RENDERER_H__ */
