#include <gtk/gtk.h>
#include <stdlib.h>
#include "entity.h"
#include "gtk-widget-attr.h"


static gint
rendgtk_timer_callback (gpointer data)
{
  XML_Node *node = data;
  gchar *function;

  function = xml_node_get_attr_str (node, "action");

  lang_call_function (node, function, "n", node);

  return (TRUE);
}

static void
rendgtk_timer_clear_tags (XML_Node *node)
{
  gint tag;
  
  tag = GPOINTER_TO_INT (erend_edata_get (node, "rendgtk-timer-timeout-tag"));

  if (tag > 0)
    gtk_timeout_remove (tag);
  
  erend_edata_set (node, "rendgtk-timer-timeout-tag", NULL);

  tag = GPOINTER_TO_INT (erend_edata_get (node, "rendgtk-timer-idle-tag"));

  if (tag > 0)
    gtk_idle_remove (tag);
  
  erend_edata_set (node, "rendgtk-timer-idle-tag", NULL);
}


static gint
rendgtk_timer_interval_attr_set (XML_Node * node, EBuf *attr, EBuf *value)
{
  gint intval;
  gint id;

  edebug ("timer-renderer", "setting up timeout_add for %s", node->name);
  intval = erend_get_integer (value);

  /* remove old timer */
  rendgtk_timer_clear_tags (node);
  
  if (intval < 0)
    return (TRUE);
  
  /* If less then 50, we setup an idle instead.. keeps gtk from going nutso */
  if (intval < 50)
    {
      id = gtk_idle_add (rendgtk_timer_callback, node);
      erend_edata_set (node, "rendgtk-timer-idle-tag", GINT_TO_POINTER (id));
      return (TRUE);
    }

  id = gtk_timeout_add (intval, rendgtk_timer_callback, node);
  erend_edata_set (node, "rendgtk-timer-timeout-tag", GINT_TO_POINTER (id));

  return (TRUE);
}

static void
rendgtk_timer_destroy (XML_Node * node)
{
  rendgtk_timer_clear_tags (node);
}

void
rendgtk_timer_render (XML_Node * node)
{
  xml_node_set_all_attr (node);
}


void
timer_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);
  element->render_func = rendgtk_timer_render;
  element->destroy_func = rendgtk_timer_destroy;
  element->tag = "timer";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "interval";
  e_attr->description =
    "Sepcify the time interval before calling the action function.";
  e_attr->value_desc = "integer";
  e_attr->possible_values = "-1,*";
  e_attr->set_attr_func = rendgtk_timer_interval_attr_set;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "action";
  e_attr->description =
    "Set to the function that will be called when a timeout occurs.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);
}
