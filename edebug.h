#ifndef __EDEBUG_H__
#define __EDEBUG_H__


void edebug_enable (gchar * domains);

void edebug (gchar * domain, ...);

#endif /* __EDEBUG_H__ */
