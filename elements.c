#include <glib.h>
#include <stdio.h>
#include <string.h>
#include "elements.h"
#include "entity.h"
#include "erend.h"
#include "edebug.h"
#include "eutils.h"
#include "ebuffer.h"

static GHashTable *element_ht = NULL;

Element *
element_lookup_element (XML_Node * node)
{
  return g_hash_table_lookup (element_ht, node->element);
}

void
element_insert_element (gchar * tag, Element * element)
{
  g_hash_table_insert (element_ht, tag, element);
}

void
element_register (Element * element)
{
  if (element_ht == NULL)
    element_ht = g_hash_table_new (g_str_hash, g_str_equal);

  element_insert_element (element->tag, element);
}

void
element_register_attr (Element * element, ElementAttr * element_attr)
{

  if (!element || !element_attr || !element_attr->attribute)
    return;

  if (element->attributes == NULL)
    element->attributes = g_hash_table_new (g_str_hash, g_str_equal);

  g_hash_table_insert (element->attributes,
		       (gpointer) element_attr->attribute, element_attr);
  edebug ("elements", "'%s' attribute '%s', accepts type '%s', '%s'",
	  element->tag, element_attr->attribute, element_attr->value_desc,
	  element_attr->possible_values);
}

void
element_register_child_attr (Element * element, ElementAttr * element_attr)
{

  if (!element || !element_attr || !element_attr->attribute)
    return;

  if (element->child_attributes == NULL)
    element->child_attributes = g_hash_table_new (g_str_hash, g_str_equal);

  g_hash_table_insert (element->child_attributes,
		       (gpointer) element_attr->attribute, element_attr);

  edebug ("elements",
	  "'%s' registered attribute on CHILD's behalf '%s', accepts type '%s', '%s'",
	  element->tag, element_attr->attribute, element_attr->value_desc,
	  element_attr->possible_values);
}

void
element_render (XML_Node * node)
{
  Element *element;
  XML_Node *parent;

  element = element_lookup_element (node);

  parent = xml_node_parent (node);

  /* This is true if its the root node.. or a screwed node I guess */
  if (!parent)
    return;

  /* If parent not rendered neither should node. */
  if (!(parent->flags & XML_NODE_RENDERED))
    return;

  /* If parent is a NO_RENDER_CHILD node, we shouldn't be renderering */
  if (parent->flags & XML_NODE_NO_RENDER_CHILDREN)
    return;

  /* Don't render if we are already rendered.. */
  if (node->flags & XML_NODE_RENDERED)
    return;

  if (element && element->render_func)
    {
      /*g_print ("rendering for tag %s\n", node->element); */
      element->render_func (node);
    }

  node->flags |= XML_NODE_RENDERED;
}


void
element_destroy (XML_Node * node)
{
  Element *element;

  element = element_lookup_element (node);

  if (element && element->destroy_func)
    {
      edebug ("elements", "destroying node '%s'", node->name);
      element->destroy_func (node);
      /* reset flags. */
      node->flags = 0;
    }
}


void
element_parent (XML_Node * parent, XML_Node * child)
{
  Element *element;

  /* Parenting is done based on the parents type */
  element = element_lookup_element (parent);

  /* Check to be sure its not already parented */
  if (child->flags & XML_NODE_PARENTED)
    return;

  /* Node should be rendered first */
  if (!(child->flags & XML_NODE_RENDERED))
    return;

  if (element)
    {
      if (element->parent_func)
	{
	  edebug ("elements", "Parenting child '%s' to parent '%s'",
		  child->name, parent->name);

	  /* One last check - verify that the child has entity data.  This may have
	     to be changed in the future */
	  if (child->entity_data)
	    element->parent_func (parent, child);
	  child->flags |= XML_NODE_PARENTED;
  	  return;
	}
    }
  /* If any of the above failed.. */
  erend_short_curcuit_parent (parent, child);
  /* Note that this *must* be done after the short cuircuit 
     is called or it won't work */
  child->flags |= XML_NODE_PARENTED;
}

ElementAttr *
element_attr_info (XML_Node * node, gchar *attr)
{
  XML_Node *parent;
  Element *element;

  element = element_lookup_element (node);

  if (element && element->attributes)
    {
      ElementAttr *element_attr;

      element_attr = g_hash_table_lookup (element->attributes, attr);
      if (element_attr)
	return (element_attr);
    }

  /* if the above didn't work, try parent */
  parent = xml_node_parent (node);

  /* find its element type */
  element = element_lookup_element (parent);

  /* see if it implements child attributes */
  if (element && element->child_attributes)
    {
      ElementAttr *element_attr;
      element_attr = g_hash_table_lookup (element->child_attributes, attr);
      if (element_attr)
	return (element_attr);
    }

  /* give up */
  return (NULL);
}

GSList *
element_supported_atts (XML_Node * node)
{
  XML_Node *parent;
  Element *element;
  GSList *attr_list = NULL;

  element = element_lookup_element (node);

  if (element && element->attributes)
    attr_list = eutils_key_list (element->attributes, NULL);

  /* check parent */
  parent = xml_node_parent (node);

  if (parent)
    {
      /* find its element type */
      element = element_lookup_element (parent);

      /* see if it implements child attributes */
      if (element && element->child_attributes)
	attr_list = eutils_key_list (element->child_attributes, attr_list);

      return (attr_list);
    }
  return (attr_list);
}

void
element_set_attr (XML_Node * node, EBuf *attr, EBuf *value)
{
  XML_Node *parent;
  Element *element;
  gint ret = FALSE;

  element = element_lookup_element (node);

  if (element && element->attributes)
    {
      ElementAttr *element_attr;

      element_attr = g_hash_table_lookup (element->attributes, attr->str);

      if (element_attr && element_attr->set_attr_func)
	{
	  ret = element_attr->set_attr_func (node, attr, value);
	}
      else
	{
	  element_attr = g_hash_table_lookup (element->attributes, "*");
	  if (element_attr && element_attr->set_attr_func)
	    ret = element_attr->set_attr_func (node, attr, value);
	}

      /* if setting of this attribute fails, we see if the parent implements
       * a method for us to check. */
      if (ret == FALSE)
	{
	  edebug ("elements", "checking parent for attribute %s in node %s",
		  attr, node->name);

	  /* Get parent node */
	  parent = xml_node_parent (node);
	  /* find its element type */
	  element = element_lookup_element (parent);

	  /* see if it implements child attributes */
	  if (element && element->child_attributes)
	    {
	      ElementAttr *element_attr;
	      element_attr =
		g_hash_table_lookup (element->child_attributes, attr->str);

	      /* if it does, call it */
	      if (element_attr && element_attr->set_child_attr_func)
		{
		  edebug ("elements",
			  "Calling parent function for attribute %s, node %s",
			  attr, parent->name);
		  element_attr->set_child_attr_func (parent, node, attr,
						     value);
		}
	    }
	}
    }
}

/* The attribute returned from this is not free'd automatically! */
EBuf *
element_get_attr (XML_Node * node, EBuf *attr)
{
  Element *element;

  /* get element type for this node */
  element = element_lookup_element (node);

  if (element && element->attributes)
    {
      ElementAttr *element_attr;

      element_attr = g_hash_table_lookup (element->attributes, attr->str);

      if (element_attr && element_attr->get_attr_func)
	{
	  /* the element attribute that's being set has a get method, call it for
	     answer, it's up to this method to set the attribute in the attribute
	     hash if it wants it saved */
	  return (element_attr->get_attr_func (node, attr));
	}
    }

  /* there's no get method, just return attribute straight */
  return (g_hash_table_lookup (node->atts, attr));
}


/* If they have set_data method implemented, use that, else set it in struct */
void
element_set_data (XML_Node * node, XML_Char * data)
{
  Element *element;

  /* Parenting is done based on the parents type */
  element = element_lookup_element (node);

  if (element && element->set_data_func)
    {
      edebug ("elements", "Data set in node '%s'", node->name);
      element->set_data_func (node, data);
    }
  else
    {

      if (node->data == NULL)
	{
	  node->data = g_string_new (data);
	}
      else
	{
	  node->data = g_string_assign (node->data, data);
	}
    }
}

/* If they have a get_data method implemented get the data through that,
   else use the data variable in the struct */

GString *
element_get_data (XML_Node * node)
{
  Element *element;

  /* Parenting is done based on the parents type */
  element = element_lookup_element (node);

  if (element && element->get_data_func)
    {
      edebug ("elements", "Data get for node '%s'", node->name);
      return (element->get_data_func (node));
    }
  else
    {
      return (node->data);
    }
}





