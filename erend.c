#include <glib.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "xml-tree.h"
#include "erend.h"
#include "elements.h"
#include "eutils.h"
#include "ebuffer.h"

/* Returns true if the string value has truth. */
gint
erend_value_is_true (EBuf *value)
{
  gint istrue = FALSE;
  EBuf *val;

  if (!value)
    return (FALSE);
  
  val = ebuf_new_with_ebuf (value);
  ebuf_down (val);

  if (ebuf_equal_str (val, "yes") ||
      ebuf_equal_str (val, "true") ||
      ebuf_equal_str (val, "on") || atoi (value->str))
    {
      istrue = TRUE;
    }

  ebuf_free (val);

  return (istrue);
}


gfloat
erend_get_percentage (EBuf *value)
{
  gfloat val;
  val = atoi (value->str) / 100.0;

  edebug("erend", "value = %s, percent = %f", value->str, val);

  return (val);
}

gfloat
erend_get_float (EBuf *value)
{
  gfloat val;

  if (!value)
    return 0.0;
  
  sscanf (value->str, "%f", &val);

  return (val);
}

gint
erend_get_integer (EBuf *value)
{
  gint val;

  if (!value)
    return 0;
  val = atoi (value->str);

  return (val);
}

gint
erend_value_equal (EBuf *value, gchar *testval)
{
  return (ebuf_equal_strcase (value, testval));
}


void
erend_edata_set (XML_Node * node, gchar * key, gpointer value)
{
  if (!node)
    return;

  if (!node->entity_data)
    node->entity_data = g_hash_table_new (g_str_hash, g_str_equal);

  if (key && value)
    g_hash_table_insert (node->entity_data, key, value);
  else if (key && !value)
    g_hash_table_remove (node->entity_data, key);
}

gpointer
erend_edata_get (XML_Node * node, gchar * key)
{
  gpointer value;

  if (!node || !node->entity_data)
    return (NULL);

  value = g_hash_table_lookup (node->entity_data, key);
  return (value);
}

void
erend_short_curcuit_parent (XML_Node * parent_node, XML_Node * child_node)
{
  XML_Node *parent_parent = parent_node;
  
  if (!child_node->entity_data)
    return;
  
  /* Search for a parent with entity_data to parent against. */
  do 
    {
      parent_parent = xml_node_parent (parent_parent);
      if (!parent_parent)
	return;
    } while (parent_parent->entity_data == NULL);
  
  element_parent (parent_parent, child_node);
}

