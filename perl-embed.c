#include <assert.h>
#include <EXTERN.h>
#include <perl.h>
#include <XSUB.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <gtk/gtk.h>
#include "elements.h"
#include "xml-node.h"
#include "xml-tree.h"
#include "xml-parser.h"
#include "lang.h"
#include "entity.h"
#include "renderers.h"
#include "erend.h"
#include "edebug.h"

#define PERL_ENTITY_INIT "E-init.pl"

/* This variable is used to keep track of which node has called a
   perl function so that we know from where the various C functions
   called from perl, know where to find the object root (<object> tag).
   
   This only really works because the whole thing is a single thread.
   In the future, we may need to do some other magic to make this work.
*/

static XML_Node *current_perl_calling_node = NULL;

static PerlInterpreter *my_perl;


void perl_init (void);
void perl_end (void);

static PerlInterpreter *my_perl;

XS (XS_Entity_node_set_attrib);
XS (XS_Entity_node_get_attrib);
XS (XS_Entity_node_get_set_attribs);
XS (XS_Entity_node_supported_attribs);
XS (XS_Entity_node_attrib_info);
XS (XS_Entity_node_attrib_is_true);
XS (XS_Entity_node_set_data);
XS (XS_Entity_node_get_data);
XS (XS_Entity_node_find_child);
XS (XS_Entity_node_find_parent);
XS (XS_Entity_node_find_fancy);
XS (XS_Entity_node_parent);
XS (XS_Entity_node_append_xml);
XS (XS_Entity_node_get_xml);
XS (XS_Entity_node_delete_tree);
XS (XS_Entity_node_delete_tree_in);
XS (XS_Entity_node_children);
XS (XS_Entity_node_ioprint);
XS (XS_Entity_node_new_child);
XS (XS_Entity_node_full_path);
XS (XS_Entity_node_call);
XS (XS_Entity_set_reference_node);
XS (XS_Entity_get_reference_node);
XS (XS_Entity_exit);
XS (XS_Entity_quit);


void
perl_reload (void)
{
  perl_end ();
  perl_init ();
}


/* These are used to bootstrap dynamic module loading in perl */
void boot_DynaLoader _((CV * cv));

void
xs_init ()
{
  char *file = __FILE__;
  /* DynaLoader is a special case */
  newXS ("DynaLoader::boot_DynaLoader", boot_DynaLoader, file);
}


static void
create_new_interp (void)
{
  char *perl_args[] = { "perl", NULL, NULL, NULL, "-w" };
  gint ret;

  perl_args[1] =
    g_strconcat ("-I", g_get_home_dir (), "/entity/modules", NULL);
  perl_args[2] = g_strconcat ("-I", DATADIR, NULL);
  perl_args[3] = g_strconcat (DATADIR, "/", PERL_ENTITY_INIT, NULL);
  edebug ("perl", "Calling: %s %s %s %s", perl_args[0], perl_args[1],
	  perl_args[2], perl_args[3]);

  my_perl = perl_alloc ();
  perl_construct (my_perl);
  ret = perl_parse (my_perl, xs_init, 5, perl_args, NULL);

  if (ret)
    {
      g_error ("Error initializing perl, perhaps couldn't load '%s'",
	       PERL_ENTITY_INIT);
    }

  perl_run (my_perl);

  /*
    perl_eval_pv(load_file, TRUE);
    perl_inbound_handlers = g_slist_alloc();
    perl_command_handlers = g_slist_alloc();
   */

  /* load up custom XML/Entity perl functions */
  newXS ("Entity::node_set_attrib", XS_Entity_node_set_attrib, "Entity");
  newXS ("Entity::node_get_attrib", XS_Entity_node_get_attrib, "Entity");
  newXS ("Entity::node_get_set_attribs", XS_Entity_node_get_set_attribs, "Entity");
  newXS ("Entity::node_supported_attribs", XS_Entity_node_supported_attribs, "Entity");
  newXS ("Entity::node_attrib_info", XS_Entity_node_attrib_info, "Entity");
  newXS ("Entity::node_attrib_is_true", XS_Entity_node_attrib_is_true, "Entity");
  newXS ("Entity::node_set_data", XS_Entity_node_set_data, "Entity");
  newXS ("Entity::node_get_data", XS_Entity_node_get_data, "Entity");
  newXS ("Entity::node_find_child", XS_Entity_node_find_child, "Entity");
  newXS ("Entity::node_find_parent", XS_Entity_node_find_parent, "Entity");
  newXS ("Entity::node_find_fancy", XS_Entity_node_find_fancy, "Entity");
  newXS ("Entity::node_parent", XS_Entity_node_parent, "Entity");
  newXS ("Entity::node_children", XS_Entity_node_children, "Entity");
  newXS ("Entity::node_ioprint", XS_Entity_node_ioprint, "Entity");
  newXS ("Entity::node_delete_tree", XS_Entity_node_delete_tree, "Entity");
  newXS ("Entity::node_delete_tree_in", XS_Entity_node_delete_tree_in, "Entity");
  newXS ("Entity::node_append_xml", XS_Entity_node_append_xml, "Entity");
  newXS ("Entity::node_get_xml", XS_Entity_node_get_xml, "Entity");
  newXS ("Entity::node_new_child", XS_Entity_node_new_child, "Entity");
  newXS ("Entity::node_full_path", XS_Entity_node_full_path, "Entity");
  newXS ("Entity::node_call", XS_Entity_node_call, "Entity");
  newXS ("Entity::set_reference_node", XS_Entity_set_reference_node, "Entity");
  newXS ("Entity::get_reference_node", XS_Entity_get_reference_node, "Entity");
  newXS ("Entity::exit", XS_Entity_exit, "Entity");
  newXS ("Entity::quit", XS_Entity_quit, "Entity");
}

/* Here we are setting/getting the appopriate namespace
 * (using the 'package' command in perl to set it) */

static gchar *
get_perl_namespace (XML_Node * calling_node)
{
  return (lang_namespace_get ("perl", calling_node));
}

static SV *
perl_get_enode (gchar * path)
{
  SV *sv;
  gint count;
  dSP;
  ENTER;
  SAVETMPS;
  PUSHMARK (sp);

  XPUSHs (sv_2mortal (newSVpv (path, strlen (path))));

  PUTBACK;
  count = perl_call_pv ("Enode::enode", G_SCALAR | G_EVAL);
  SPAGAIN;

  if (SvTRUE (GvSV (errgv)))
    {
      g_warning ("Error evaluationg perl function '%s': %s\n",
		 "Enode::enode", SvPV (GvSV (errgv), na));
      return FALSE;
    }

  edebug ("perl-embed", "returned %d from Enode::enode", count);

  sv = POPs;

  /* increment refcount so it survives the FREETMPS below, after that it's
     down to 1 which I beleive is safe for pushing onto stack and having 
     it freed properly.  MW Not quite, but we have to _dec once the func is
     called. */
  SvREFCNT_inc (sv);

  edebug ("perl-embed", "SvROK is %d, refcount %d", SvROK (sv),
	  SvREFCNT (sv));


  FREETMPS;
  LEAVE;

  return (sv);
}

/* Freed MW */
EBuf*
execute_perl_function (XML_Node * calling_node, gchar * function,
		       GSList * args)
{
  int stack_size;
  static EBuf* retbuf=NULL;
  static GString *perl_cmd = NULL;
  GSList *tmp;
  GSList *nodelist = NULL;
  LangArg *arg;
  gchar *namespace;
  gchar *path;
  SV *enode_sv = NULL;
  dSP;

  current_perl_calling_node = calling_node;
  namespace = get_perl_namespace (calling_node);

  if (!perl_cmd)
    perl_cmd = g_string_sized_new (1024);

  g_string_truncate (perl_cmd, 0);

  /* Only set the namespace if they're calling into 'main' */
  if (!strstr (function, "::"))
    {
      g_string_append (perl_cmd, namespace);
      g_string_append (perl_cmd, "::");
    }
  g_string_append (perl_cmd, function);

  ENTER;
  SAVETMPS;
  PUSHMARK (sp);

  tmp = args;

  while (tmp)
    {
      arg = (LangArg *) tmp->data;

      if (arg->type == LANG_NODE)
	{
	  path = xml_node_get_path (arg->data);	/*Freed MW */
	  enode_sv = perl_get_enode (path);
	  g_free (path);

	  edebug ("perl-embed", "SvROK is now %d, refcount %d",
		  SvROK (enode_sv), SvREFCNT (enode_sv));
	  if (enode_sv)
	    {
	      XPUSHs (enode_sv);
	      nodelist = g_slist_append (nodelist, enode_sv);
	    }
	}
      else if (arg->type == LANG_STRING ||
	       arg->type == LANG_INT || arg->type == LANG_BINSTRING)
	{
	  if (arg->size > 0)
            {
              edebug("perl-embed", "arg = %s, size= %i", arg->data, arg->size);
              XPUSHs (sv_2mortal (newSVpv (arg->data, arg->size)));
            }
	  else
	    XPUSHs (sv_2mortal (newSVpv ("", 1)));
	}

      lang_arg_free (arg);
      tmp = tmp->next;
    }

  PUTBACK;
  stack_size = perl_call_pv (perl_cmd->str, G_EVAL);
  SPAGAIN;

  /* OK, time to get the return value. */
  edebug("perl-embed-test", "stack_size = %i", stack_size );
  if(stack_size) /* Don't deal with lists, just get the first. */
    {
      char* tmpstr;
      tmpstr = POPp;
      edebug("perl-embed-test", "POPp = %s", tmpstr);
      if(!retbuf) retbuf = ebuf_new_sized(25);
      ebuf_set_to_cstring(retbuf, tmpstr);
    }

  /* Free our perl enodes. */
  for (tmp = nodelist; tmp; tmp = tmp->next)
    {
      enode_sv = tmp->data;
      SvREFCNT_dec (enode_sv);
    }
  g_slist_free (nodelist);

  if (SvTRUE (GvSV (errgv)))
    {
      g_warning ("Error evaluationg perl function '%s': %s\n",
		 function, SvPV (GvSV (errgv), na));
      return (NULL);
    }

  PUTBACK ; /* This is really needed or perl dies. Spent around 1hr on this. */
  FREETMPS;
  LEAVE;

  return (retbuf);
}

static void
my_perl_eval_pv (gchar * p)
{
  dSP;
  SV *sv = newSVpv (p, 0);

  PUSHMARK (SP);
  perl_eval_sv (sv, G_SCALAR | G_KEEPERR);
  SvREFCNT_dec (sv);

  SPAGAIN;
  PUTBACK;
}

void
execute_perl_code (XML_Node * calling_node, gchar * code)
{
  static GString *perl_cmd = NULL;
  gchar *namespace;

  if (perl_cmd == NULL)
    perl_cmd = g_string_sized_new (1024);
  g_string_truncate (perl_cmd, 0);

  current_perl_calling_node = calling_node;

  namespace = get_perl_namespace (calling_node);

  g_string_append (perl_cmd, "package ");
  g_string_append (perl_cmd, namespace);
  g_string_append (perl_cmd, "; Enode::import ('enode', 'efind'); ");
  g_string_append (perl_cmd, code);

  edebug ("perl-embed", "executing perl '%s'", perl_cmd->str);
  my_perl_eval_pv (perl_cmd->str);

  if (SvTRUE (GvSV (errgv)))
    {
      g_warning ("Error evaluating %s: %s\n",
		 calling_node->name->str, SvPV (GvSV (errgv), na));
    }
}

/* Various XML handlers */
static void
perl_node_render (XML_Node * node)
{
  if (node && node->data)
    execute_perl_code (node, node->data->str);
}

static void
perl_node_destroy (XML_Node * node)
{
  /* TODO: Should do all that spiffy namespace cleaning stuff */
  return;
}

/* initialize perl */
void
perl_init (void)
{
  Element *element;

  /* initialize perl interpreter */
  create_new_interp ();

  /* for extra cleaning.. we hope */
  /* PL_perl_destruct_level = 1; */

  /* Register perl as a tag type */
  element = g_malloc0 (sizeof (Element));
  element->render_func = perl_node_render;
  element->destroy_func = perl_node_destroy;
  element->tag = "perl";

  element_register (element);

  /* register perl language type */
  lang_register ("perl", execute_perl_function);
}


void
perl_end (void)
{
  perl_destruct (my_perl);
  perl_free (my_perl);
}


/*-- 

Entity::node_find_fancy (target_node);

Find a specific node.  Uses fancy_find lookup.

target_node: fancy path to the desired node.

*/

XS (XS_Entity_node_find_fancy)
{
  int len;
  gchar *path;
  XML_Node *node;

  dXSARGS;
  
  if (items != 1)
    XSRETURN_EMPTY;

  path = SvPV (ST (0), len);

  node = xml_node_fancy_find (current_perl_calling_node, path);

  if (node)
    {
      XST_mPV (0, xml_node_get_path (node));
      XSRETURN (1);
    }
  else
    {
      XSRETURN_EMPTY;
    }
}


/*-- 

Entity::node_set_attrib (path, attribute, value)

Set or change an attribute of an XML node.

path: path to the desired node.
attribute: Name of the attribute you wish to set.  eg "text".
value: Value that you wish to set that attribute to.

*/

XS (XS_Entity_node_set_attrib)
{
  int len;
  gchar *path;
  gchar *attr;
  gchar *value;
  EBuf *evalue;
  XML_Node *node;

  dXSARGS;
  items = 0;

  path = SvPV (ST (0), len);
  attr = SvPV (ST (1), len);
  value = SvPV (ST (2), len);
  evalue = ebuf_new_with_data (value, len);
  node = xml_node_lookup (path);

  if (node)
    xml_node_set_attr (node, attr, evalue);
  else
    g_warning ("Unable to find path '%s'\n", path);

  XSRETURN_EMPTY;
}

/*--

Entity::node_get_attrib (path, attribute) 

Get the value of the specified attribute of the node given by path.

path: Path to desired node.
attribute: Attribute string.

*/



XS (XS_Entity_node_get_attrib)
{
  int len;
  gchar *path;
  gchar *attr;
  EBuf *value;
  XML_Node *node;

  dXSARGS;
  items = 0;

  path = g_strdup (SvPV (ST (0), len));
  attr = g_strdup (SvPV (ST (1), len));

  if (path && attr)
    node = xml_node_lookup (path);
  else
    {
      g_warning ("Entity::node_get_attrib: requires 2 arguemts, 'path' and 'attr'");
      XSRETURN_EMPTY;
    }

  if (!node)
    {
      g_warning ("Entity::node_get_attrib: node '%s' not found", path);
      XSRETURN_EMPTY;
    }

  value = xml_node_get_attr (node, attr);
  
  edebug ("perl-embed", "returning for node %s, value %s, attr %s\n", 
	  node->name, value ? value->str : "NULL", attr);

  if (value)
    {
      XST_mPV (0, g_strdup (value->str));
      XSRETURN (1);
    }
  else
    XSRETURN_UNDEF;
}

/*--

Entity::node_get_set_attribs (path)

Get a list of all the set attributes set for an XML node.

path: Path to desired node.

*/

XS (XS_Entity_node_get_set_attribs)
{
  int i = 0;
  gchar *path;
  GSList *list;
  GSList *tmp;
  gint len;
  XML_Node *node;
  dXSARGS;
  
  if (items != 1)
    XSRETURN_EMPTY;
  
  path = g_strdup (SvPV (ST (0), len));

  node = xml_node_lookup (path);

  if (!node)
    XSRETURN_EMPTY;

  edebug ("perl-embed", "Entity::get_set_atts (%s) called.", path);

  list = xml_node_attr_list (node);
  tmp = list;

  while (tmp)
    {
      EBuf *attr = tmp->data;
      edebug ("perl-embed", "adding %s to list..", attr->str);
      XST_mPV (i, g_strdup (attr->str));
      i++;
      tmp = tmp->next;
    }

  g_slist_free (list);

  XSRETURN (i);
}

/*--

Entity::node_supported_attribs (path)

Get a list of all possible attributes for this node.

path: Path to desired node.

*/

XS (XS_Entity_node_supported_attribs)
{
  int i = 0;
  gchar *path;
  GSList *list;
  GSList *tmp;
  gint len;
  XML_Node *node;
  dXSARGS;

  if (items != 1)
    XSRETURN_EMPTY;
  
  path = g_strdup (SvPV (ST (0), len));

  node = xml_node_lookup (path);

  if (!node)
    XSRETURN_EMPTY;

  edebug ("perl-embed", "Entity::get_set_atts (%s) called.", path);

  list = element_supported_atts (node);
  tmp = list;

  while (tmp)
    {
      gchar *attr = tmp->data;
      edebug ("perl-embed", "adding %s to list..", attr);
      /* IAN: FIXME: Should be using SV's */
      XST_mPV (i, g_strdup (attr));
      i++;
      tmp = tmp->next;
    }

  g_slist_free (list);

  XSRETURN (i);
}

/*--

Entity::node_attrib_info (path, attr)

Return a list containing info about the attribute 'attr'.

The list follows the format:
($description, $value_desc, $possible_values)

path: Path to desired node.
attr: Name of attribute.

*/

XS (XS_Entity_node_attrib_info)
{
  gchar *path;
  gchar *attr;
  ElementAttr *e_attr;
  gint len;
  XML_Node *node;
  dXSARGS;
  
  if (items != 2)
    XSRETURN_EMPTY;

  path = g_strdup (SvPV (ST (0), len));
  attr = g_strdup (SvPV (ST (1), len));

  node = xml_node_lookup (path);

  if (!node)
    XSRETURN_EMPTY;

  edebug ("perl-embed", "Entity::node_attrib_info (%s) called.", path);

  e_attr = element_attr_info (node, attr);

  if (!e_attr)
    XSRETURN_EMPTY;

  if (e_attr->description)
    XST_mPV (0, g_strdup (e_attr->description));
  else
    XST_mUNDEF (0);

  if (e_attr->value_desc)
    XST_mPV (1, g_strdup (e_attr->value_desc));
  else
    XST_mUNDEF (1);

  if (e_attr->possible_values)
    XST_mPV (2, g_strdup (e_attr->possible_values));
  else
    XST_mUNDEF (2);


  XSRETURN (3);
}

XS (XS_Entity_node_attrib_is_true)
{
  int len;
  gchar *path;
  gchar *attr;
  EBuf *value;
  XML_Node *node;
  dXSARGS;
  
  if (items != 2)
    XSRETURN_EMPTY;

  path = g_strdup (SvPV (ST (0), len));
  attr = g_strdup (SvPV (ST (1), len));

  if (path && attr)
    node = xml_node_lookup (path);
  else
    {
      g_warning
	("Entity::is_attr_true: requires 2 arguemts, 'path' and 'attr'");
      XSRETURN_EMPTY;
    }

  if (!node)
    {
      g_warning ("Entity::is_attr_get: node '%s' not found", path);
      XSRETURN_EMPTY;
    }

  value = xml_node_get_attr (node, attr);

  if (erend_value_is_true (value))
    {
      XST_mPV (0, g_strdup ("1"));
      XSRETURN (1);
    }
  else
    XSRETURN_EMPTY;
}



/*--

Entity::node_set_data (path, string) 

Set the data of a given XML node.
The data is the text between a given tag.  eg:

<text>
This is the data.
</text>

path: Path to the desired XML Node.
string: String that you wish to set the data to.

*/

XS (XS_Entity_node_set_data)
{
  int len;
  gchar *path;
  gchar *data;
  XML_Node *node;
  dXSARGS;

  if (items != 2)
    XSRETURN_EMPTY;
  
  path = g_strdup (SvPV (ST (0), len));
  data = g_strdup (SvPV (ST (1), len));

  node = xml_node_lookup (path);

  if (node)
    {
      xml_node_set_data (node, data);
    }
  else
    g_warning ("Entity::node_set_data: Unable to find path '%s'\n", path);

  XSRETURN_EMPTY;
}

/*--

Entity::node_get_data (path) 

Get the data of a given XML node.
The data is the text between a given tag.  eg:

<text>
This is the data.
</text>

path: Path to the desired XML Node.

Returns a string containing the data contained in that node.

*/
XS (XS_Entity_node_get_data)
{
  int len;
  gchar *path;
  XML_Node *node;
  GString *str;
  dXSARGS;
  
  if (items != 1)
    XSRETURN_EMPTY;
  
  path = g_strdup (SvPV (ST (0), len));

  if (path)
    node = xml_node_lookup (path);
  else
    {
      g_warning ("Entity::get_data: requires 'path' as argument");
      XSRETURN_EMPTY;
    }

  if (!node)
    {
      g_warning ("Entity::get_data: node '%s' not found", path);
      XSRETURN_EMPTY;
    }

  /* TODO: Fix memory leak */
  str = xml_node_get_data (node);

  /* Can't deref bad pointer. MW */
  if(!str)
    XSRETURN_EMPTY;

  XST_mPV (0, g_strdup (str->str));
  XSRETURN (1);
}


XS (XS_Entity_node_new_child)
{
  int len;
  gchar *name = NULL;
  GSList* atts = NULL;
  gchar *tag;
  gchar *path;
  XML_Node *parent;
  XML_Node *node;
  gchar *node_path;
  dXSARGS;
  
  if (items < 2)
    XSRETURN_EMPTY;
  
  path = g_strdup (SvPV (ST (0), len));
  tag = g_strdup (SvPV (ST (1), len));
  if (items >= 3)
    name = g_strdup (SvPV (ST (2), len));

  if (!path || !tag)
    {
      g_warning ("Entity::new_child: requires 3 arguemts,"
		 " 'start_path, tag, name'");
      XSRETURN_EMPTY;
    }
  /* First we find the node that the first path refers to */
  parent = xml_node_lookup (path);

  if (!parent)
    {
      g_warning ("Entity::new_child: parent node '%s' not found", path);
      XSRETURN_EMPTY;
    }
  /*g_free(path); Add me? MW */

  if(0 == (items-3)%2) /* See if we have an even stack after the first three. */
    {
      char* attrib;
      char* value;
      int attrlen, vallen;
      int i;
      EBuf* val;
      for(i = 3; i < items; i=i+2) /* Get two at a time. */
        {
          attrib = SvPV (ST(i), attrlen);
          value = SvPV (ST(i+1), vallen);
          val = ebuf_new_with_data(value, vallen);
          atts = g_slist_append(atts, g_strdup(attrib) );
          atts = g_slist_append(atts, value);
        }
    }

  node = xml_node_new_child (parent, tag, name, atts);
  node_path = xml_node_get_path (node);

  if (node_path)
    {
      XST_mPV (0, node_path);
      XSRETURN (1);
    }
  else
    XSRETURN_EMPTY;
}




/* This is used to find nodes starting from a
   specified path. */

/*--

Entity::node_find_child (path, name)

Allows you to search for a node starting at the specified path.
Useful for finding a node in a parent object etc.

path: Path to the top node to start the search from.
name: The full name of the node you are after.  eg "button.foo".

*/

XS (XS_Entity_node_find_child)
{
  int len;
  gchar *name;
  XML_Node *node;
  gchar *path;
  dXSARGS;
  
  if (items != 2)
    XSRETURN_EMPTY;
  
  path = g_strdup (SvPV (ST (0), len));
  name = g_strdup (SvPV (ST (1), len));

  if (!path || !name)
    {
      g_warning ("Entity::node_find_child: requires 2 arguemts, 'start_path, name'");
      XSRETURN_EMPTY;
    }

  /* First we find the node that the first path refers to */
  node = xml_node_lookup (path);
  
  if (!node)
    {
      g_warning ("Entity::node_find_child: node '%s' not found", name);
      XSRETURN_EMPTY;
    }

  edebug ("perl-embed", 
          "in Entity::node_find_child - found start search path at %s",
	  xml_node_get_path (node));

  /* now we find the path relative to that node */
  node = xml_node_find_by_name (node, name);

  if (!node) 
    {
      g_warning ("Entity::node_find_child: node '%s' not found", name);
      XSRETURN_EMPTY;
    }
   

  path = xml_node_get_path (node);

  if (path)
    {
      /*XST_mPV (0, g_strdup (path)); */
      XST_mPV (0, path);
      XSRETURN (1);
    }
  else
    XSRETURN_EMPTY;
}

/*--

Entity::node_find_parent (path, type) 

Find a given node by looking up starting at start_path for a specified
type.  Useful for finding the parent object node, or any other parent
node in the tree.

Example:

Entity::node_find_parent ($path, "object");

will return the path to its parent object.

path: The path to begin the search at (is not included in the search).
type: The node type to find.

*/

XS (XS_Entity_node_find_parent)
{
  int len;
  gchar *name;
  XML_Node *node;
  XML_Node *parent_node;
  gchar *path;
  dXSARGS;
  
  if (items != 2)
    XSRETURN_EMPTY;
  
  path = g_strdup (SvPV (ST (0), len));
  name = g_strdup (SvPV (ST (1), len));

  if (!path || !name)
    {
      g_warning
	("Entity::node_find_parent: requires 2 arguemts, 'start_path, name'");
      XSRETURN_EMPTY;
    }

  node = xml_node_lookup (path);

  if (!node)
    {
      g_warning ("Entity::node_find_parent: node '%s' not found", path);
      XSRETURN_EMPTY;
    }

  parent_node = xml_node_find_parent (node, name);

  if (parent_node)
    {
      XST_mPV (0, xml_node_get_path (parent_node));
      XSRETURN (1);
    }
  else
    XSRETURN_EMPTY;
}

/*--

Entity::node_parent (path) 

Return parent node

Example:

Entity::node_parent ("path");

will return the path to its parent.

path: The path to the node to return the parent of

*/

XS (XS_Entity_node_parent)
{
  int len;
  XML_Node *node;
  XML_Node *parent_node;
  gchar *path;
  dXSARGS;
  
  if (items != 1)
    {
      g_warning
	("Entity::node_parent: requires 1 arguemt, 'path'");
      XSRETURN_EMPTY;
    }  
  
  path = g_strdup (SvPV (ST (0), len));

  node = xml_node_lookup (path);

  if (!node)
    {
      g_warning ("Entity::parent: node '%s' not found", path);
      XSRETURN_EMPTY;
    }

  parent_node = xml_node_parent (node);

  if (parent_node)
    {
      XST_mPV (0, xml_node_get_path (parent_node));
      XSRETURN (1);
    }
  else
    XSRETURN_EMPTY;
}


/*--

Entity::node_append_xml (parent_path, xml) 

Append a chunk of XML to a node.  Note that the XML is rendered so that
it's contained by the parent_path.

parent_path: The path to the parent node that will contain the specified xml.
xml: A string containing proper xml to be appended.

*/

XS (XS_Entity_node_append_xml)
{
  int len;
  gchar *path;
  gchar *xml;
  XML_Node *node;
  dXSARGS;
  
  if (items != 2)
    XSRETURN_EMPTY;
  
  path = g_strdup (SvPV (ST (0), len));
  xml = g_strdup (SvPV (ST (1), len));

  /* TODO: Sanity checks. */
  node = xml_node_lookup (path);

  if (!node)
    {
      g_warning ("Entity::node_append_xml: node '%s' not found", path);
      XSRETURN_EMPTY;
    }

  xml_parse_string (node, xml);

  xml_tree_render (node);

  g_free (path);
  g_free (xml);

  XSRETURN_EMPTY;
}


/*--

Entity::node_get_xml (top_path) 

Returns an string containing the XML starting at the specified path.
This is useful for a number of things, including saving the state of your
application etc.

top_path: The path to the top XML node to begin generating xml text at.

Returns a string of XML.

*/

XS (XS_Entity_node_get_xml)
{
  int len;
  gchar *path;
  gchar *xml;
  XML_Node *node;
  dXSARGS;
  
  if (items != 1)
    {
      g_warning ("Entity::node_get_xml: requires 1 arguemts, 'path'");
      XSRETURN_EMPTY;
    }      

  path = g_strdup (SvPV (ST (0), len));

  node = xml_node_lookup (path);

  xml = xml_tree_get_xml (node);

  XST_mPV (0, g_strdup (xml));
  g_free (path);

  XSRETURN (1);
}


/*--
 
Entity::node_delete_tree (path) 

Deletes a tree of XML starting at, and including the specified node.
basically like 'rm -rf'

path: The node to begin deleting at. 

*/

XS (XS_Entity_node_delete_tree)
{
  int len;
  gchar *path;
  XML_Node *node;
  dXSARGS;
  
  if (items != 1)
    XSRETURN_EMPTY;
  
  path = g_strdup (SvPV (ST (0), len));

  /* TODO: Sanity checks. */
  node = xml_node_lookup (path);

  if (node)
    xml_tree_delete (node, TRUE);

  g_free (path);

  XSRETURN_EMPTY;
}

/*--

Entity::node_delete_tree_in (path) 

Delete the XML inside the given path, but does not delete the specified node.

path: The node to begin deleting inside of.

*/

XS (XS_Entity_node_delete_tree_in)
{
  int len;
  gchar *path;
  XML_Node *node;
  dXSARGS;
  
  if (items != 1)
    XSRETURN_EMPTY;

  path = g_strdup (SvPV (ST (0), len));

  node = xml_node_lookup (path);

  if (node)
    xml_tree_delete_in (node, TRUE);

  g_free (path);

  XSRETURN_EMPTY;
}


XS (XS_Entity_node_ioprint)
{
  int len;
  int size;
  gchar *path;
  gchar *data;
  XML_Node *node;
  Element *element;
  ElementAttr *e_attr;
  void (*sendq) (XML_Node * node, gchar * data, gint send_size);

  dXSARGS;
  
  if (items != 2)
    XSRETURN_EMPTY;
  
  path = g_strdup (SvPV (ST (0), len));
  node = xml_node_lookup (path);

  element = element_lookup_element (node);
  edebug ("perl-embed", "ioprint element = %i", element);
  if (!element)
    goto done;
  
  e_attr = g_hash_table_lookup (element->attributes, "sendq");
  edebug ("perl-embed", "ioprint e_attr = %i", e_attr);
  if (!e_attr)
    goto done;
  
  sendq = (void *) e_attr->set_attr_func;
  if (!sendq)
    goto done;
  edebug ("perl-embed", "ioprint sendq = %i", sendq);
  
  data = SvPV (ST (1), size);
  sendq (node, data, size);
  
 done:
  g_free (path);
  XSRETURN_EMPTY;
}



/*--

Entity::node_children (path)

Returns an array of the paths to the nodes inside the specified node.

path: Node to list children of.

*/

XS (XS_Entity_node_children)
{
  int i = 0;
  gchar *path;
  GSList *list;
  GSList *tmp;
  int len;
  XML_Node *node;
  dXSARGS;
  
  if (items != 1)
    XSRETURN_EMPTY;

  path = g_strdup (SvPV (ST (0), len));
  
  node = xml_node_lookup (path);
  
  if (!node)
    XSRETURN_EMPTY;
  
  list = xml_node_ls (node);
  tmp = list;

  while (tmp)
    {
      gchar *name = tmp->data;
      XST_mPV (i, g_strdup (name));
      i++;
      tmp = tmp->next;
    }

  g_slist_free (list);

  XSRETURN (i);
}


/*--

Entity::node_full_path (path)

Returns the full path from a node name.

path: Fancy path name.

*/


XS (XS_Entity_node_full_path)
{
  int len;
  XML_Node *node;
  gchar *path;

  dXSARGS;
  
  if (items != 1)
    XSRETURN_EMPTY;

  path = g_strdup (SvPV (ST (0), len));

  if (!path)
    {
      g_warning ("Entity::get_path: requires 1 arguemt, 'path'");
      XSRETURN_EMPTY;
    }

  /* First we find the node that the path refers to */
  node = xml_node_lookup (path);

  if (!node)
    {
      g_warning ("Entity::get_path: node '%s' not found", path);
      g_free (path);
      XSRETURN_EMPTY;
    }

  g_free (path);
  path = xml_node_get_path (node);

  if (path)
    {
      /*XST_mPV (0, g_strdup (path)); */
      XST_mPV (0, path);
      XSRETURN (1);
    }

  g_free (path);
  XSRETURN_EMPTY;
}


/*--

Entity::node_call (path, function)

Call a perl function.

path: The fully qualified path name.
function: function to call

*/


XS (XS_Entity_node_call)
{
  int len;
  XML_Node *node;
  gchar *path;
  gchar *function;
  gchar *namespace;
  gchar *full_func;
  
  dXSARGS;
  
  edebug ("perl-embed", "there are %d arguments", items);
  
  if (items < 2)
    {
      g_warning ("Entity::call: requires at least 2 arguemt, 'path, function'");
      XSRETURN_EMPTY;
    }
  
  path = g_strdup (SvPV (ST (0), len));
  function = g_strdup (SvPV (ST (1), len));
  
  /* Pop these 2 args off the stack */
  (void)POPs;
  (void)POPs;
  
  /* First we find the node that the path refers to */
  node = xml_node_lookup (path);
  
  if (!node)
    {
      g_warning ("Entity::get_path: node '%s' not found", path);
      g_free (path);
      XSRETURN_EMPTY;
    }

  g_free (path);
  
  edebug ("perl-embed", "calling function %s from node %s", 
	  function, node->name);
  
  namespace = get_perl_namespace (node);
  full_func = g_strconcat (namespace, "::", function, NULL);
  
  /* We just totally cheat here and use the perl stack to pass arguments
     in and out.  We probably should make this use lang_call, but that's
     a lot more work. */
  
  perl_call_pv (full_func, G_EVAL);
  
  g_free (full_func);
  g_free (function);
  
  /* FIXME: Should return value */
  return;
}

/*--
 
Entity::set_reference_node (path)

Set the node from which future perl functions should be
concidered to come from.  Used to enter another namespace.

path: Path name.
 
*/


XS (XS_Entity_set_reference_node)
{
  int len;
  XML_Node *node;
  gchar *path;
  
  dXSARGS;
  
  if (items != 1)
    XSRETURN_EMPTY;
  
  path = g_strdup (SvPV (ST (0), len));
  
  if (!path)
    {
      g_warning ("Entity::set_reference_node: requires 1 arguemt, 'path'");
      XSRETURN_EMPTY;
    }
  
  /* First we find the node that the path refers to */
  node = xml_node_lookup (path);
  
  if (node)
    current_perl_calling_node = node;
  
  XSRETURN_EMPTY;
}


/*--
 
Entity::get_reference_node ()

Returns the path to the current reference node (for namespace purposes).
 
*/


XS (XS_Entity_get_reference_node)
{
  gchar *path;
  dXSARGS;
  
  items = 0;

  /* First we find the node that the path refers to */
  path = xml_node_get_path (current_perl_calling_node);
  
  if (path)
    {
      XST_mPV (0, path);
      XSRETURN (1);
    }
  
  XSRETURN_EMPTY;
}


/*--

Entity::exit () 

Exits from the current object.  This destroys all nodes inside
the current object.

*/

XS (XS_Entity_exit)
{
  XML_Node *objnode;

  objnode = xml_node_find_parent (current_perl_calling_node, "object");

  if (objnode)
    {
      xml_tree_delete (objnode, TRUE);
    }
}

/*-- 

Entity::quit () 

Quits this instance of Entity.  Should only be used if you know
you are supposed to have the Entity core exit.

*/

XS (XS_Entity_quit)
{
  gtk_main_quit ();
}


