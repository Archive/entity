#ifndef __XML_TREE_H__
#define __XML_TREE_H__

#include "xml-node.h"

void xml_tree_render (XML_Node * top_node);

void xml_tree_delete_in (XML_Node * topnode, gint free_nodes);

void xml_tree_delete (XML_Node * node, gint free_nodes);

void xml_tree_move (XML_Node * src_node, XML_Node * dest_node);

void xml_tree_copy (XML_Node * src_node, XML_Node * dest_node_parent);

gchar *xml_tree_get_xml (XML_Node * topnode);

#endif /* __XML_RENDER_H__ */
