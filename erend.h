#ifndef __EREND_H__
#define __EREND_H__

#include <glib.h>

gint
erend_value_is_true (EBuf * value);

gfloat
erend_get_percentage (EBuf * value);

gfloat
erend_get_float (EBuf *value);

gint 
erend_get_integer (EBuf * value);

gint
erend_value_equal (EBuf *value, gchar *testval);

/* For nodes that don't implement any form of parenting */
void
erend_short_curcuit_parent (XML_Node * parent_node, XML_Node * child_node);

/* used to store arbitrary information for a node.  This can be used to hold 
   eg. GTK widgets and other things that you need to keep track of.  For gtk widget
   renderers, we are currently using "top-widget" and "bottom-widget" to point to 
   the top and bottom widgets of the tree.  Anything specific to the tag shoudl be
   prefixed by the tag.  eg "app-ebox-widget".
*/

void 
erend_edata_set (XML_Node * node, gchar * key, gpointer value);

gpointer
erend_edata_get (XML_Node * node, gchar * key);


#endif /* __EREND_H__ */
