#include <glib.h>
#include <gqueue.h>
#include "entity.h"

XML_Node *xml_root = NULL;

/* Number of nodes to allocate at a time */
#define XML_NODE_ALLOC_NUM 200
static EMemChunk *xml_node_chunk_admin = NULL;


void
xml_node_init (void)
{
  XML_Node *node;
  
  xml_node_chunk_admin = eutils_memchunk_admin_new (sizeof (XML_Node),
						    XML_NODE_ALLOC_NUM);
  
  node = eutils_memchunk_alloc (xml_node_chunk_admin);
  node->name = ebuf_new_with_cstring ("/");
  node->element = "root";
  node->flags |= XML_NODE_RENDERED;
  xml_root = node;
}

XML_Node *
xml_node_root (void)
{
  return (xml_root);
}

XML_Node *
xml_node_parent (XML_Node * node)
{
  XML_Node *parent = NULL;

  if (node->parent)
    parent = node->parent;
  return (parent);
}

gchar *
xml_node_get_path (XML_Node * node)
{
  XML_Node *tmp_node;
  EBuf *str;
  gchar *ret;
  
  if (!node)
    return (NULL);
  
  str = ebuf_new ();

  ebuf_append_ebuf (str, node->name);
  ebuf_prepend_cstring (str, "/");

  tmp_node = xml_node_parent (node);

  while (tmp_node)
    {
      if (tmp_node->name->str[0] == '/')
	break;
      ebuf_prepend_cstring (str, tmp_node->name->str);
      ebuf_prepend_char (str, '/');
      tmp_node = xml_node_parent (tmp_node);
    }

  /* always double / so it's absolute */
  ebuf_prepend_char (str, '/');
  ret = g_strdup (str->str);
  
  ebuf_free (str);
  return (ret);
}

static gint
xml_free_atts (XML_Node *node, EBuf * attr, EBuf * value)
{
  ebuf_free (value);
  ebuf_free (attr);
  return (TRUE);
}

void
xml_node_destroy (XML_Node *node)
{
  element_destroy (node);
}

void
xml_node_stats (void)
{
  g_print ("For XML_Node structs:\n");
  eutils_memchunk_stats (xml_node_chunk_admin);
}

XML_Node *
xml_node_new (gchar * tag)
{
  XML_Node *new_node;
  Element *element;

  edebug ("xml-node", "in xml_node_new, tag = %s", tag);

  if (!tag)
    return (NULL);

  new_node = eutils_memchunk_alloc (xml_node_chunk_admin);

  new_node->element = g_strdup (tag);

  /* Check to see if we aren't suppose to renderer children of this
     node */
  element = element_lookup_element (new_node);

  if (element && element->no_render_children)
    {
      edebug ("xml-node", "Creating new node %s with NO_RENDER_CHILDREN set.",
	      tag);
      new_node->flags |= XML_NODE_NO_RENDER_CHILDREN;
    }

  new_node->atts = g_hash_table_new (ebuf_hash, ebuf_equal_ebufcase);

  /* Set to assigned name */
  new_node->name = ebuf_new ();
  xml_node_set_name (new_node, NULL);
  
  return (new_node);
}

void
xml_node_free (XML_Node * node)
{
  g_free (node->element);
  ebuf_free (node->name);

  if (node->data)
    g_string_free (node->data, FALSE);

  /* go through all the attr's and free them */
  xml_node_foreach_attr (node, (void *) xml_free_atts);

  if (node->atts)
    g_hash_table_destroy (node->atts);
  if (node->entity_data)
    g_hash_table_destroy (node->entity_data);

  g_slist_free (node->children);

  xml_node_remove_from_parent (node);
  
  eutils_memchunk_free (xml_node_chunk_admin, node);
}

void
xml_node_set_parent (XML_Node *node, XML_Node *parent)
{
  if (node && parent) 
    {
      /* Special case, if it's an object, we want to create an <instance>
	 tag as an intermediary, but only if it's not already an instance
         tag above it */
      if (g_str_equal (node->element, "object") &&
	  !g_str_equal (parent->element, "instance"))
	{
	  XML_Node *instance;

	  instance = xml_node_new ("instance");
	  xml_node_set_parent (instance, parent);
	  xml_node_set_parent (node, instance);
	}
      else
	{
	  node->parent = parent;
	  parent->children = g_slist_append_tail (parent->children, node, 
						  &parent->children_tail);
	}
    }
}

void
xml_node_remove_from_parent (XML_Node *node)
{
  XML_Node *parent_node;
  
  if (node)
    {
      parent_node = xml_node_parent (node);
      parent_node->children = g_slist_remove_tail (parent_node->children, node, 
						   &parent_node->children_tail);
    }
}

gchar *
xml_node_set_name (XML_Node * node, gchar * name)
{
  static gint unique_element_id = 0;
  gchar name_buf[128];
  
  if (!node)
    {
      g_warning ("xml_node_set_name: NULL node!");
      return (NULL);
    }
  
  ebuf_truncate (node->name, 0);
  ebuf_append_cstring (node->name, node->element);
  ebuf_append_cstring (node->name, ".");
  
  if (name == NULL || strlen (name) == 0)
    {
      g_snprintf (name_buf, 128, "_%d", unique_element_id++);
      ebuf_append_cstring (node->name, name_buf);
    }
  else
    ebuf_append_cstring (node->name, name);
  
  return (node->name->str);
}

XML_Node *
xml_node_new_child (XML_Node * parent, gchar * tag, gchar * name, GSList* atts)
{
  XML_Node *node;
  char* attr;
  EBuf* value;
  GSList* tmp;

  if (!parent)
    return (NULL);
  if (!tag)
    return (NULL);

  node = xml_node_new (tag);

  /* Force a even number of elements. MW */
  for(tmp=atts; tmp && tmp->next; tmp = tmp->next->next)
    {
      attr = (char*)tmp->data;
      value = (EBuf*)tmp->next->data;
      xml_node_set_attr_quiet(node, attr, value);
    }

  xml_node_set_parent (node, parent);
  xml_node_set_attr_str_quiet(node, "name", name);
  /*xml_node_set_name (node, name);*/

  element_render (node);
  element_parent (parent, node);


  return (node);
}

gint
xml_node_data_is_cdata (XML_Node * node)
{
  if (node)
    return (node->data_is_cdata);
  else
    return (FALSE);
}

void
xml_node_append_cdata (XML_Node * node, XML_Char * data)
{
  if (!node)
    return;
  xml_node_append_data (node, data);

  node->data_is_cdata = TRUE;
}

void
xml_node_set_data (XML_Node * node, XML_Char * data)
{

  /* We're silently ignoring requests to set data for bad nodes */
  if (!node)
    return;

  element_set_data (node, data);
}

GString *
xml_node_get_data (XML_Node * node)
{

  /* We're silently ignoring requests to set data for bad nodes */
  if (!node)
    return (NULL);

  return (element_get_data (node));
}

/* FIXME: This is slightly broken.. it's actually built around the parser,
 */

void
xml_node_append_data (XML_Node * node, XML_Char * data)
{

  /* We're silently ignoring requests to set data for bad nodes */
  if (!node)
    return;

  if (node->data == NULL)
    {
      node->data = g_string_new (data);
    }
  else
    {
      /* we're going to try inserting \0's whenever there's a break caused
         by another tag */
      /* FIXME: this is incorrect.. this \0 should be added in a different
         fasion, as this function could be used by interp etc. */
      node->data = g_string_append_c (node->data, '\0');
      node->data = g_string_append (node->data, data);
    }
}


/* get a single attribute value */
EBuf *
xml_node_get_attr (XML_Node * node, gchar * attr)
{
  EBuf *value;
  EBuf *attrib;
  
  attrib = ebuf_new_with_cstring (attr);
  /* ebuf_down (attrib); */
  value = element_get_attr (node, attrib);
  ebuf_free (attrib);
  
  edebug ("xml-node", "Returning value attr on node '%s', attr '%s', value '%s'", 
	  node->name->str, attr, value ? value->str : "NULL");

  return (value);
}

gchar *
xml_node_get_attr_str (XML_Node * node, gchar * attr)
{
  EBuf *value;
  value = xml_node_get_attr (node, attr);

  if (value)
    return (value->str);
  else
    return (NULL);
}

void
xml_node_clear_attr (XML_Node *node, EBuf *attr)
{
  gint found;
  EBuf *old_key;
  EBuf *old_val;
  
  found = g_hash_table_lookup_extended (node->atts, attr,
					(gpointer *) &old_key, 
					(gpointer *) &old_val);
  if (found)
    {
      g_hash_table_remove (node->atts, attr);
      xml_free_atts (node, old_key, old_val);
    }
}

void
xml_node_set_attr_real (XML_Node * node, gchar *attr, EBuf *value,
			gint notify_renderer)
{
  EBuf *attrib;
  EBuf *val;
  gint free_attrib = TRUE;
  
  /* Force all attrs to lower case */
  attrib = ebuf_new_with_cstring (attr);
  ebuf_down (attrib);

  val = g_hash_table_lookup (node->atts, attrib);
  
  edebug ("xml-node", "Setting attribute for node '%s', attr '%s', value '%s'",
	  node->name->str, attr, value ? value->str : "NULL");
  
  if (value)
    {
      if (val)
	{
	  ebuf_set_to_ebuf (val, value);
	}
      else
	{
	  val = ebuf_new_with_ebuf (value);
	  g_hash_table_insert (node->atts, attrib, val);
	  free_attrib = FALSE;
	}
    }
  else
    {
      xml_node_clear_attr (node, attrib);
    }

  /* Update name if applicable */
  if (g_str_equal (attrib->str, "name"))
    xml_node_set_name (node, value ? value->str : NULL);
  
  /* notify the rendering code of the attribute change */
  if (notify_renderer)
    element_set_attr (node, attrib, value);
  
  if (free_attrib)
    ebuf_free (attrib);
}


void
xml_node_set_attr_str (XML_Node * node, gchar *attr, gchar *value)
{
  EBuf *val = NULL;
  
  if (value)
    val = ebuf_new_with_cstring (value);
  
  xml_node_set_attr_real (node, attr, val, TRUE);
  
  if (value)
    ebuf_free (val);
}

/* sets an attribute without alerting the renderer - useful for setting
   things inside the renderer itself, and in a few other situations */
void
xml_node_set_attr_str_quiet (XML_Node * node, gchar *attr, gchar *value)
{
  EBuf *val = NULL;
  
  if (value)
    val = ebuf_new_with_cstring (value);
  
  xml_node_set_attr_real (node, attr, val, FALSE);
  
  if (value)
    ebuf_free (val);
}

void
xml_node_set_attr (XML_Node * node, gchar *attr, EBuf *value)
{
  xml_node_set_attr_real (node, attr, value, TRUE);
}

/* sets an attribute without alerting the renderer - useful for setting
   things inside the renderer itself, and in a few other situations */
void
xml_node_set_attr_quiet (XML_Node * node, gchar *attr, EBuf *value)
{
  xml_node_set_attr_real (node, attr, value, FALSE);
}




