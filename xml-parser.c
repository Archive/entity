#include <glib.h>
#include <stdio.h>
#include <string.h>
#include <xmlparse.h>
#include <errno.h>
#include "xml-node.h"
#include "xml-tree.h"
#include "perl-embed.h"
#include "edebug.h"
#include "entity.h"
#include "glib.h"

static GString *data_string = NULL;
static gint data_string_is_cdata = FALSE;


static XML_Node *working_node = NULL;
static XML_Node *top_of_new_tree = NULL;

static void
start_element (void *user_data, char *name, char **atts)
{

  if (data_string->len > 0)
    {
      if (data_string_is_cdata)
	xml_node_append_cdata (working_node, data_string->str);
      else
	xml_node_append_data (working_node, data_string->str);
      
      data_string_is_cdata = FALSE;
      g_string_truncate (data_string, 0);
    }

  /* insert the node into the tree */
  working_node = xml_node_append (working_node, name, atts);
  
  /* FIXME: Is it freeing its own memory or something ?
     g_strfreev (atts); */
}

static void
end_element (void *user_data, const char *name)
{
  if (data_string->len > 0)
    {
      /* FIXME: This appears to be checked in both start and ending
	 of element, is this necessary ? */
      if (data_string_is_cdata)
	xml_node_append_cdata (working_node, data_string->str);
      else
	xml_node_append_data (working_node, data_string->str);
      data_string_is_cdata = FALSE;
    }

  g_string_truncate (data_string, 0);
  
  working_node = xml_node_parent (working_node);
  
  /* One special case, since the node_parent will create a new <instance>
     for app tags, we have to walk over that here or we get screwed up */
  if (g_str_equal (working_node->element, "instance"))
    {
      top_of_new_tree = working_node;
      working_node = xml_node_parent (working_node);
    }
}

static void
character_data_handler (void *user_data, const XML_Char * s, int len)
{
  gint i;

  /* have to do it this way because there can be \0's in the string */
  for (i = 0; i < len; i++)
    {
      g_string_append_c (data_string, s[i]);
    }
}

static void
cdata_start_section_handler (void *user_data)
{
  data_string_is_cdata = TRUE;
}

static void
cdata_end_section_handler (void *user_data)
{
  return;
}


static void
processing_instruction_handler (void *user_data,
				const XML_Char * target,
				const XML_Char * data)
{
  /* printf ("target is %s, data is %s\n", target, data); */
}

#if 0
static void
default_handler (void *user_data, const XML_Char * s, int len)
{
  /* g_print ("default handler called\n"); */
}

#endif /*0 */


/* For adding the __filename attr to <object> tags */
static gchar *current_filename;

gchar *
xml_parser_get_current_filename (void)
{
  return (current_filename);
}


/* Returns the top node from the parsing */
XML_Node *
xml_parse_file (XML_Node *top_node, gchar *filename)
{
  char buf[BUFSIZ];
  XML_Parser parser;
  int done;
  FILE *fp;

  fp = fopen (filename, "r");

  if (!fp)
    {
      g_warning ("Unable to open file %s: %s", filename, g_strerror (errno));
      return (NULL);
    }

  current_filename = filename;
  if (top_node == NULL)
    working_node = xml_node_root ();
  else
    working_node = top_node;
  
  top_of_new_tree = NULL;
  
  /* do a little peak to see if we're running from command line interp. */
  fgets (buf, BUFSIZ, fp);

  /* if the first char is a '#', just leave it, else rewind fp to parse
     entire file */
  if (buf[0] != '#')
    {
      rewind (fp);
    }

  parser = XML_ParserCreate (NULL);
  data_string = g_string_sized_new (65535);
  /* XML_SetUserData (parser, path); */

  XML_SetElementHandler (parser, (void *) start_element, end_element);
  XML_SetCharacterDataHandler (parser, character_data_handler);
  XML_SetProcessingInstructionHandler (parser,
				       processing_instruction_handler);
  XML_SetCdataSectionHandler (parser, cdata_start_section_handler,
			      cdata_end_section_handler);

  do
    {
      size_t len = fread (buf, 1, sizeof (buf), fp);
      done = len < sizeof (buf);
      if (!XML_Parse (parser, buf, len, done))
	{
	  g_warning ("%s at line %d\n",
		     XML_ErrorString (XML_GetErrorCode (parser)),
		     XML_GetCurrentLineNumber (parser));
	  return (NULL);
	}
    }
  while (!done);
  
  XML_ParserFree (parser);

  current_filename = NULL;

  return (top_of_new_tree);
}

XML_Node *
xml_parse_string (XML_Node *top_node, gchar * xml)
{
  XML_Parser parser;
  
  working_node = top_node;
  top_of_new_tree = NULL;
  
  g_string_truncate (data_string, 0);

  parser = XML_ParserCreate (NULL);
  XML_Parse (parser, "<fake>", strlen ("<fake>"), 0);

  /* XML_SetUserData (parser, path); */

  XML_SetElementHandler (parser, (void *) start_element, end_element);
  XML_SetCharacterDataHandler (parser, character_data_handler);
  XML_SetProcessingInstructionHandler (parser,
				       processing_instruction_handler);

  if (!XML_Parse (parser, xml, strlen (xml), 0))
    {
      g_warning ("%s at line %d, near '%s'\n",
		 XML_ErrorString (XML_GetErrorCode (parser)),
		 XML_GetCurrentLineNumber (parser),
                 xml);
      return (NULL);
    }

  XML_SetElementHandler (parser, NULL, NULL);
  XML_SetCharacterDataHandler (parser, NULL);
  XML_SetProcessingInstructionHandler (parser, NULL);
  XML_Parse (parser, "</fake>", strlen ("</fake>"), 1);

  XML_ParserFree (parser);

  return (0);
}














