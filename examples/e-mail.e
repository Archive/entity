#!/usr/local/bin/entity

<object>
  <window position="center" title="Mailer" ondelete="perl:Entity::exit">
    <frame border="3" title="Mailer" expand="true">
	  <halign>
	    <label text="To:" width="60"/>
	    <entry name="to" expand="true"/>
	  </halign>
	  <halign>
	    <label text="Subject:" width="60"/>
	    <entry name="subject" expand="true"/>
	  </halign>
	  <scrollwin expand="true">
	    <text name="body" editable="true"> </text>
	  </scrollwin>
	  <button label="Mail it!" onclick="mailit"/>
    </frame>
  </window>
  <perl><![CDATA[
    sub mailit {
		$to= enode("entry.to")->attrib("text");
		$subject= enode("entry.subject")->attrib("text");
		$body= enode("text.body")->get_data();
		open(OUT,"| /usr/bin/mail $to");
		print OUT "Subject: $subject\n$body\n";
		close(OUT);
		
		Entity::exit();
	}
  ]]></perl>
</object>
