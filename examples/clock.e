#!/usr/local/bin/entity

<object>
  <window ondelete="perl:Entity::exit" title="Clock">
    <valign>
      <object name="clock" dragable="true" expand="false">
	<label name="time" font="10x20" text = "00:00:00"/>
	<timer interval = "50" action="update_clock"/>
        <perl>
	    sub update_clock
	      {
	        my ($sec, $min, $hour, $mday) = localtime (time);
		
	        my $str = sprintf ("%02d:%02d:%02d", $hour, $min, $sec);
		
		my $label = enode ("label.time");
		$label->attrib(text => $str);
	      }
	    update_clock ();
	</perl>
      </object>
    </valign>
  </window>
</object>
