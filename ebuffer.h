#ifndef __EBUFFER_H__
#define __EBUFFER_H__

#include <glib.h>


typedef enum
{
  EBUF_CHUNK_NONE   = 0,
  EBUF_CHUNK_SMALL  = 1 << 0,
  EBUF_CHUNK_MEDIUM = 1 << 1,
  EBUF_CHUNK_LARGE  = 1 << 2
} EBufChunkSize;


typedef struct _EBuf    EBuf;

struct _EBuf
{
  gchar *str;
  gint len;
  gint alloc;
  EBufChunkSize type;
};

EBuf *
ebuf_new (void);

EBuf *
ebuf_new_sized (guint size);

EBuf *
ebuf_new_with_cstring (gchar *str);

EBuf *
ebuf_new_with_int (gint val);

EBuf *
ebuf_new_with_data (gchar *str, gint len);

EBuf *
ebuf_new_with_ebuf (EBuf *buf);

void
ebuf_free (EBuf *buf);

void
ebuf_set_to_cstring (EBuf *buf, gchar *str);

void
ebuf_set_to_ebuf (EBuf *buf, EBuf *val);

void
ebuf_append_cstring (EBuf *buf, gchar *str);

void
ebuf_append_ebuf (EBuf *buf, EBuf *newbuf);

void
ebuf_append_data (EBuf *buf, gchar *str, gint len);

void
ebuf_prepend_cstring (EBuf *buf, gchar *str);

void
ebuf_prepend_char (EBuf *buf, gchar c);

void
ebuf_insert_cstring (EBuf *buf,
		     gint pos,
		     gchar *val);

void
ebuf_insert_data (EBuf *buf,
		  gint pos,
		  gchar *val,
		  gint len);

void
ebuf_truncate (EBuf *buf, gint len);

void
ebuf_sprintf (EBuf *buf,
	      gchar *fmt,
	      ...);

gint
ebuf_equal_ebuf (EBuf *buf1, EBuf *buf2);

gint
ebuf_equal_ebufcase (EBuf *buf1, EBuf *buf2);

gint
ebuf_equal_str (EBuf *buf1, gchar *str);

gint
ebuf_equal_strcase (EBuf *buf1, gchar *str);

void
ebuf_down (EBuf *buf);

void
ebuf_up (EBuf *buf);

guint
ebuf_hash (EBuf *buf);

void
ebuf_stats (void);


#endif /* __EBUFFER_H__ */

