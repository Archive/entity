#ifndef __EUTILS_H__
#define __EUTILS_H__

#include <glib.h>

GSList *
eutils_key_list (GHashTable *table, GSList *list);

gint
eutils_file_exists (gchar *filename);

gchar *
eutils_file_search (gchar *filename);


/* Memory chunk allocator interface */

#define EMEMCHUNK_PROFILE 1


typedef struct _EMemChunk    EMemChunk;

struct _EMemChunk
{
  GSList *free_list;
  guint size;
  guint alloc_num;
  guint num_free_chunks;

#ifdef EMEMCHUNK_PROFILE  
  gint number_of_freed_chunks;
  gint number_of_used_chunks;
  gint number_of_allocated_chunks;
#endif

};

EMemChunk *
eutils_memchunk_admin_new (guint chunk_size, guint alloc_num);

void *
eutils_memchunk_alloc (EMemChunk *chunk);

void
eutils_memchunk_free (EMemChunk *chunk, void *buf);

void
eutils_memchunk_stats (EMemChunk *chunk);

#endif	/* __EUTILS_H__ */




