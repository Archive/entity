#ifndef __XML_NODE_UTILS_H__
#define __XML_NODE_UTILS_H__

#include "xmlparse.h"
#include "xml-node.h"
#include <glib.h>

/* Return a static address to "element.name" */
gchar* element_name (gchar *element, gchar *name);

/* Get a listing of a path (eg ls in dir */
GSList *xml_node_ls (XML_Node * node);

XML_Node *
xml_node_append (XML_Node* parent_node, gchar * element, gchar ** atts);

XML_Node *xml_node_append_copy (XML_Node * src, XML_Node * dest_parent);

gchar *xml_node_set_attributes (gchar * path, char **atts);

XML_Node *xml_node_lookup (gchar * path);

void xml_node_set_all_attr (XML_Node * node);

/* Callback for the xml_node_foreach_attr function */
typedef void
  (*XMLNodeForeachAttrFunc) (XML_Node * node, gchar * attr, gchar * value);

void
xml_node_foreach_attr (XML_Node * node, XMLNodeForeachAttrFunc foreach_func);

/* return all the set attributes of a node */
GSList *xml_node_attr_list (XML_Node * node);


typedef gint (*XMLTreeForeachNodeFunc) (XML_Node * node, gpointer user_data);

void
xml_tree_foreach_node (XML_Node *topnode, XMLTreeForeachNodeFunc foreach_func,
		       gpointer user_data);

XML_Node *xml_node_find_by_name (XML_Node *start_node, gchar * name);

XML_Node *xml_node_find_parent (XML_Node * start_node,
				gchar * name);

gchar *xml_node_find_attr_thru_type (XML_Node * parent, gchar * element,
				     gchar * attr);

/* Do a fancy find for a node.

   - a path starting with '//' does a search from the real root tree.
   - a path starting with '/' does a search from the <app> root.
   - a path starting with any other char does *find* from <app> root.

*/
XML_Node *xml_node_fancy_find (XML_Node * calling_node, gchar * path);

#endif /* __XML_TREE_H__ */
