/* vim:set ts=8 sw=2: */
#include <gtk/gtk.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <signal.h>
#include <sys/un.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "config.h"

#ifdef HAVE_GETOPT_H
# include <getopt.h>
#endif

#include "xml-tree.h"
#include "perl-embed.h"
#include "xml-node.h"
#include "xml-parser.h"
#include "renderers.h"
#include "entity.h"
#include "edebug.h"
#include "econfig.h"
#include "ebuffer.h"

#define ENTITY_LINK_PATH "/entity/.link"


static gint
link_exists (gchar * path)
{
  struct stat s;

  return (stat (path, &s) == 0);
}

static gchar *
full_path (gchar * filename)
{
  gchar *full_path;
  gchar buf[PATH_MAX];

  if (filename[0] == '/')
    {
      full_path = g_strdup (filename);
    }
  else
    {
      getcwd (buf, PATH_MAX);
      full_path = g_strconcat (buf, "/", filename, NULL);
    }

  return (full_path);
}


static gint
check_master (gchar * load_file)
{
  gchar *path;
  gint fd;
  struct sockaddr_un addr;
  gint len;
  gint ret;

  path = g_strconcat (g_get_home_dir (), ENTITY_LINK_PATH, NULL);

  if (link_exists (path))
    {
      edebug ("main", "link found, sending path %s", load_file);

      fd = socket (AF_UNIX, SOCK_STREAM, 0);
      if (fd < 0)
	g_error
	  ("Error opening link to Master Entity: creating AF_UNIX socket: '%s': %s",
	   path, g_strerror (errno));

      memset (&addr, 0, sizeof (addr));
      addr.sun_family = AF_UNIX;
      strcpy (addr.sun_path, path);
      len = sizeof (addr);

      ret = connect (fd, (struct sockaddr *) &addr, len);
      if (ret < 0)
	{
	  edebug ("main",
		  "Master Entity doesn't want to talk to us: %s - Running as Master",
		  g_strerror (errno));
	  /* in this case, we delete the old link, and rerun as the new master */
	  unlink (path);
	  close (fd);
	  fd = check_master (load_file);
	  return (fd);
	}

      write (fd, load_file, strlen (load_file));
      close (fd);
      edebug ("main", "wrote %s to link", load_file);
      exit (0);

    }
  else
    {
      edebug ("main", "no link, creating");

      mkdir (g_dirname (path), 00750);

      /* create link */
      fd = socket (AF_UNIX, SOCK_STREAM, 0);

      if (fd < 0)
	g_error
	  ("Error while trying to take over the universe: creating AF_UNIX socket: '%s': %s",
	   path, g_strerror (errno));

      /* establish a unix domain socket */
      memset (&addr, 0, sizeof (addr));
      addr.sun_family = AF_UNIX;
      strcpy (addr.sun_path, path);

      len = sizeof (addr);

      ret = bind (fd, (struct sockaddr *) &addr, len);
      if (ret < 0)
	g_error ("World takeover plan failed at stage 2: %s",
		 g_strerror (errno));

      ret = listen (fd, 5);
      edebug ("main", "opened link, returning");
      return (fd);
    }
}



static void
link_data_ready (gpointer data, gint fd, GdkInputCondition condition)
{
  gchar buf[PATH_MAX];
  gint fd2;
  struct sockaddr_un addr;
  gint len;
  XML_Node *node;

  len = sizeof (addr);
  fd2 = accept (fd, (struct sockaddr *) &addr, &len);

  memset (buf, 0, PATH_MAX);
  edebug ("main", "going to read from buf - condition %d", condition);
  recv (fd2, buf, PATH_MAX, 0);

  node = xml_parse_file (NULL, buf);
  if (!node)
    {
      g_warning ("Error parsing file '%s'", buf);
      return;
    }
  
  edebug ("main", "rendering tree from %s", node->name);
  xml_tree_render (node);
}

void
print_usage (void)
{
  g_print ("Usage: %s  [OPTION]... [FILE]\n"
#ifdef HAVE_LONG_OPTIONS
	   "Mandatory arguments to long options are mandatory for short options too.\n"
#endif
	   "  -V, --version            print version number.\n"
	   "  -g, --debug              turn on debug messages. Accepts domain as argument, or 'all'\n"
	   "  -s, --standalone         standalone mode, don't offload program on running entity.\n"
	   "  -h, --help               print this and die.\n"
	   "%s %s, compiled: %s\n"
	   "Report bugs to <entity@netidea.com>\n",
	   PACKAGE, PACKAGE, VERSION, __DATE__);
}

/* Stupid human tricks .... MW */
/* typedef int dipshit; */
/* IAN: no no no! ;-) */

typedef int ian;

ian main (int argc, gchar * argv[])
{
  XML_Node *node;
  gint fd=0;
  gchar *wd;
  gchar *filename;
  gint c;


#ifdef HAVE_GETOPT_H
  static struct option long_options[] = {

    {"debug", required_argument, 0, 'g'},
    {"help", no_argument, 0, 'h'},
    {"standalone", no_argument, 0, 's'},
    {"version", no_argument, 0, 'V'},
    {0, 0, 0, 0}
  };
#endif /* HAVE_GETOPT_H */

  /* init config stuff first thing */
  edebug ("main", "initializing entity config");
  econfig_init ();

  while (TRUE)
    {

      const char *optstr = "sg:hV";
#ifdef HAVE_GETOPT_H
      int option_index = 0;

      c = getopt_long (argc, argv, optstr, long_options, &option_index);
#else
      c = getopt (argc, argv, optstr);
#endif

      if (c == -1)
	break;

      switch (c)
	{

	case 'g':
	  edebug_enable (optarg);
	  break;
	case 'h':
	case '?':
	  print_usage ();
	  exit (0);
	  /* NOTREACHED */
	  break;
	case 's':
	  econfig_set_attr ("standalone", "true");
	  break;
	case 'V':
	  g_print ("%s %s\n", PACKAGE, VERSION);
	  exit (0);
	  /* NOTREACHED */
	  break;

	default:
	  g_print ("?? getopt returned character code 0%o ??\n", c);

	}			/* switch (c) */


    }				/* while ( .. */

  if (optind >= argc)
    {
      print_usage ();
      exit (0);
    }
  else
    {
      filename = full_path (argv[optind]);
      /* remaining args are ignored currently. */
    }

  if (!econfig_is_set ("standalone"))
    fd = check_master (filename);

  edebug ("main", "gtk init");
  gtk_init (&argc, &argv);

  if (!econfig_is_set ("standalone"))
    {
      edebug ("main", "Installing watcher on link");
      /* setup listener and callback on fifo */
      gdk_input_add (fd, GDK_INPUT_READ, link_data_ready, NULL);
    }

  edebug ("main", "xml tree init");
  xml_node_init ();

  edebug ("main", "renderers init");
  renderers_init ();

  edebug ("main", "parsing file...");
  node = xml_parse_file (NULL, filename);

  /* Set working directory */
  wd = g_strconcat (g_get_home_dir (), "/entity", NULL);
  chdir (wd);
  g_free (wd);

  if (node)
    {
      edebug ("main", "rendering tree from %s", node->name);
      edebug ("main", "Rendering node %s\n", node->name);
      xml_tree_render (node);
    }
  else
    {
      exit (1);
    }

  gtk_main ();

  edebug ("main", "Exiting...");
  ebuf_stats ();
  xml_node_stats ();
  return 0;
}


