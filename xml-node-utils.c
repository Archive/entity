#include <glib.h>
#include <gqueue.h>
#include <string.h>
#include "xml-tree.h"
#include "elements.h"
#include "xml-parser.h"
#include "entity.h"
#include "edebug.h"
#include "xml-node.h"
#include "eutils.h"
#include "ebuffer.h"

extern XML_Node *xml_root;


/* Lookup a node given a path.
   TODO: Implement lookup cache */

XML_Node *
xml_node_lookup (gchar * path)
{
  /* start at the root node and work our way through */
  XML_Node *curnode;
  gchar **dirname;
  GSList *tmp;
  gint i;
  gint plen;

  /* do a few simple sanity checks */
  if (!path)
    return (NULL);

  /* relative lookups may be possible in the future.. */
  if (path[0] != '/')
    return (NULL);

  /* check for root of tree */
  if (path[1] == '\0')
    return (xml_root);

  plen = strlen (path);

  /* break up the dir names */
  dirname = g_strsplit (path, "/", 65535);

  curnode = xml_root;

  /* starting at 1 assuming '/' */
  for (i = 1; dirname[i]; i++)
    {
      gint found = FALSE;

      if (dirname[i][0] == '\0')
	continue;

      tmp = curnode->children;

      while (tmp)
	{
	  XML_Node *test_node = tmp->data;
	  /* g_print ("NODE lookup: comparing supplied '%s' and tree branch '%s'\n", 
	     dirname[i], test_node->name); */

	  if (ebuf_equal_str (test_node->name, dirname[i]))
	    {
	      curnode = test_node;
	      found = TRUE;
	      break;
	    }

	  tmp = tmp->next;
	}

      if (!found)
	{
	  g_strfreev (dirname);
	  return (NULL);
	}
    }

  g_strfreev (dirname);
  return (curnode);
}


/* for this find function */
static XML_Node *found_node = NULL;

static gint
foreach_node_find (XML_Node * node, gpointer data)
{
  gchar *name = data;
  
  /* g_print ("testing tree name '%s' against '%s'\n", node->name, name); */

  if (ebuf_equal_str (node->name, name))
    {
      found_node = node;
      return (TRUE);
    }

  else
    return (FALSE);
}


XML_Node *
xml_node_find_by_name (XML_Node * node, gchar * name)
{
  found_node = NULL;
  
  /* Special case, check to be sure the current node isn't the one
     we're after */
  if (ebuf_equal_str (node->name, name))
    {
      return (node);
    }
  
  /* search tree */
  xml_tree_foreach_node (node, foreach_node_find, (gpointer) name);

  return (found_node);
}

/* Finds an attribute only traversing up thru elements of type element. */
gchar *
xml_node_find_attr_thru_type (XML_Node * parent, gchar * element,
			      gchar * attr)
{
  gchar *value = NULL;

  if (!parent)
    return (NULL);

  while (!value)
    {
      edebug ("xml-node", "finding attr (%s) for parent (%s)\n",
	      attr, parent->name->str);

      if (g_str_equal (parent->element, element))
	{
	  value = xml_node_get_attr_str (parent, attr);
	  if (value)
	    break;		/* Found what we were looking for. */
	}
      else
	break;			/* Parent is currently the top level. */

      parent = xml_node_parent (parent);
      if (!parent)
	break;			/* Didn't find it. */
    }

  return (value);
}

XML_Node *
xml_node_find_parent (XML_Node * start_node, gchar *name)
{
  XML_Node *node;

  node = xml_node_parent (start_node);
  
  /* See if we're looking for a full name, or just a type */
  if (strstr (name, "."))
    {
      /* search on full name */
      while (node)
	{
	  if (ebuf_equal_str (node->name, name))
	    return (node);
	  node = xml_node_parent (node);
	}
    }
  else
    {
      /* search for type */
      while (node)
	{
	  if (g_str_equal (node->element, name))
	    return (node);
	  node = xml_node_parent (node);
	}
    }

  return (NULL);
}


/* insert a new node - returns new full pathname on success, or NULL on 
   failure.  This is only really used by the parser. */
XML_Node *
xml_node_append (XML_Node* parent_node, gchar * element, gchar ** atts)
{
  XML_Node *new_node;
  gint i;

  if (!parent_node)
    return (NULL);

  new_node = xml_node_new (element);
  xml_node_set_parent (new_node, parent_node);
  
  /* copy attributes into list  */
  for (i = 0; atts[i] != NULL; i += 2)
    {
      xml_node_set_attr_str_quiet (new_node, atts[i], atts[i + 1]);
    }

  /* If this is an <object> tag, try to find the current filename 
     for setting the __filename attribute. */
  if (g_str_equal (element, "object"))
    {
      gchar *filename;

      filename = xml_parser_get_current_filename ();
      if (filename)
	xml_node_set_attr_str_quiet (new_node, g_strdup ("__filename"),
				     filename);
    }
  
  return (new_node);
}


/* Get a listing of a path (eg ls in dir */
GSList *
xml_node_ls (XML_Node * node)
{
  GSList *list;
  GSList *ls = NULL;
  GSList *tail = NULL;
  
  if (!node)
    return (NULL);

  list = node->children;

  while (list)
    {
      XML_Node *node;

      node = list->data;
      ls = g_slist_append_tail (ls, xml_node_get_path (node), &tail);
      list = list->next;
    }

  return (ls);
}


static void
xml_node_foreach_atts_copy (gpointer key, gpointer value, gpointer user_data)
{
  XML_Node *node = user_data;

  xml_node_set_attr_quiet (node, key, value);
}

/* Append a copy of a single node to an existing tree -
   returns the newly created node */

XML_Node *
xml_node_append_copy (XML_Node * src, XML_Node * dest_parent)
{
  XML_Node *new_node;

  if (!src || !dest_parent)
    return (NULL);

  edebug ("xml-node", "supposed to copy '%s' to '%s'\n",
	  xml_node_get_path (src), xml_node_get_path (dest_parent));

  /* first thing to do is copy all the interesting parts to the new node */
  new_node = xml_node_new (src->element);
  xml_node_set_name (new_node, NULL);
  
  new_node->atts = g_hash_table_new (g_str_hash, g_str_equal);
  g_hash_table_foreach (src->atts, xml_node_foreach_atts_copy, new_node);

  /* FIXME: This won't properly deal with \0's in the string */
  if (src->data)
    new_node->data = g_string_new (src->data->str);

  new_node->data_is_cdata = src->data_is_cdata;

  /* handle node references */
  xml_node_set_parent (new_node, dest_parent);

  return (new_node);
}


/* just calls the element_set_attr for each currently set attribute */
void
xml_node_set_all_attr (XML_Node * node)
{
  GSList *atts;
  GSList *tmp;

  if (!node)
    return;

  atts = xml_node_attr_list (node);

  tmp = atts;

  while (tmp)
    {
      EBuf *attr;
      EBuf *value;
      
      attr = tmp->data;
      value = xml_node_get_attr (node, attr->str);

      element_set_attr (node, attr, value);
      tmp = tmp->next;
    }
  g_slist_free (atts);
}


typedef struct _ForeachAttrDispatch ForeachAttrDispatch;

struct _ForeachAttrDispatch
{
  XMLNodeForeachAttrFunc foreach_func;
  XML_Node *node;
};


static void
xml_node_foreach_attr_dispatch (gpointer key,
				gpointer value, gpointer user_data)
{
  ForeachAttrDispatch *disp = user_data;

  disp->foreach_func (disp->node, (gchar *) key, (gchar *) value);
}

void
xml_node_foreach_attr (XML_Node * node, XMLNodeForeachAttrFunc foreach_func)
{
  static ForeachAttrDispatch *disp = NULL;

  if (!disp)
    disp = g_malloc (sizeof (ForeachAttrDispatch));

  disp->foreach_func = foreach_func;
  disp->node = node;

  g_hash_table_foreach (node->atts, xml_node_foreach_attr_dispatch, disp);
}

/* list of currently set attributes */

GSList *
xml_node_attr_list (XML_Node * node)
{
  GSList *list = NULL;

  if (!node || !node->atts)
    return (NULL);

  list = eutils_key_list (node->atts, NULL);
  return (list);
}


void
xml_tree_foreach_node (XML_Node * topnode,
		       XMLTreeForeachNodeFunc foreach_func,
		       gpointer user_data)
{
  XML_Node *curnode;
  XML_Node *parentnode;
  GSList *child;
  GQueue *q;
  GQueue *cq;
  gint count = 0;
  gint done = FALSE;

  if (!topnode)
    return;

  q = g_queue_create ();
  cq = g_queue_create ();

  g_queue_push_tail (q, NULL);

  /* now that we have the node, we have to keep walking through until we
     render everything. */
  curnode = topnode;
  child = curnode->children;
  g_queue_push_tail (cq, child);


  while (TRUE)
    {
      while (child)
	{
	  /* save the current parent */
	  g_queue_push_tail (q, curnode);

	  /* save the current child */
	  g_queue_push_tail (cq, child);

	  /* delve into next child */
	  curnode = child->data;

	  done = foreach_func (curnode, user_data);
	  if (done)
	    goto out;

	  count++;

	  /* find first child and descend */
	  child = curnode->children;
	}

      /* when we end up here, it means we've descended all the way down a fork.
         Now we climb back out and set it to the next child if it's found */
      parentnode = g_queue_pop_tail (q);

      /* if we've returned back beyond the top, we've completed the run. */
      if (parentnode == NULL)
	{
	  goto out;
	}

      /* find the next child in the parents list */
      child = g_queue_pop_tail (cq);

      if (child)
	child = child->next;

      curnode = parentnode;

    }

out:
  g_queue_free (q);
  g_queue_free (cq);
  /* g_print ("foreached on %d nodes\n", count); */
}


/* Do a fancy find for a node.

   - a path starting with '//' does a search from the real root tree.
   - a path starting with '/' does a search from the <object> root.
   - a path starting with any other char does *find* from <object> root.

*/

XML_Node *
xml_node_fancy_find (XML_Node * calling_node, gchar * path)
{
  gint plen;
  gchar *top_path;
  XML_Node *objnode;
  XML_Node *node;
  gchar *find_path;

  if (!path)
    return (NULL);

  /* first step is to find what mode we're looking up in */
  plen = strlen (path);

  if ((plen >= 2) && (path[0] == '/') && (path[1] == '/'))
    {
      /* we're doing full lookup - just use the usual lookup. */
      /* g_print ("did lookup - using absolute path for '%s'\n", path); */
      return (xml_node_lookup (path));
    }

  /* The remaining lookups find relative to 'object' if applicable */
  
  if (g_str_equal (calling_node->element, "object"))
    objnode = calling_node;
  else
    objnode = xml_node_find_parent (calling_node, "object");

  if (objnode)
    top_path = xml_node_get_path (objnode);
  else
    top_path = g_strdup ("/");

  if ((path[0] == '/') && (plen >= 1))
    {
      /* we're doing lookup relative to <object> tag. */
      find_path = g_strconcat (top_path, "/", path, NULL);
      node = xml_node_lookup (find_path);
      g_free (find_path);
      g_free (top_path);
      /* g_print ("did lookup - did lookup from parent <object> path for '%s' - found '%s'\n", 
         path, node->name); */

      return (node);
    }

  /* assume we're doing find */
  /* g_print ("did lookup - used find parent <object> (%s) path for '%s'\n", top_path, path); */
  node = xml_node_find_by_name (objnode, path);
  g_free (top_path);

  return (node);
}
