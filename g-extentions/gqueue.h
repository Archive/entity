#include <glib.h>

typedef struct _GQueue		GQueue;
typedef struct _GTrashStack     GTrashStack;

struct _GTrashStack
{
  GTrashStack *next;
};

/* Trash Stacks
 * elements need to be >= sizeof (gpointer)
 */
void	g_trash_stack_push	(GTrashStack **stack_p,
				 gpointer      data_p);

gpointer	g_trash_stack_pop	(GTrashStack **stack_p);


gpointer	g_trash_stack_peek	(GTrashStack **stack_p);


guint	g_trash_stack_height	(GTrashStack **stack_p);


struct _GQueue
{
  GList *head;
  GList *tail;
  guint  length;
};

/* Queues
 */
GQueue*  g_queue_create         (void);
void     g_queue_free           (GQueue  *queue);
void     g_queue_push_head      (GQueue  *queue,
				 gpointer data);
void     g_queue_push_tail      (GQueue  *queue,
				 gpointer data);
gpointer g_queue_pop_head       (GQueue  *queue);
gpointer g_queue_pop_tail       (GQueue  *queue);
gboolean g_queue_is_empty       (GQueue  *queue);
gpointer g_queue_peek_head      (GQueue  *queue);
gpointer g_queue_peek_tail      (GQueue  *queue);
void     g_queue_push_head_link (GQueue  *queue,
				 GList   *link);
void     g_queue_push_tail_link (GQueue  *queue,
				 GList   *link);
GList*   g_queue_pop_head_link  (GQueue  *queue);
GList*   g_queue_pop_tail_link  (GQueue  *queue);

