/* INTACT GLIB LICENSE. */
/* GLIB - Library of useful routines for C programming
 * Copyright (C) 1995-1997  Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GLib Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GLib Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GLib at ftp://ftp.gtk.org/pub/gtk/. 
 */

/*
 * Modified by Ian Main for use in Entity. 1999.
 */


#include <glib.h>

GSList*
g_slist_remove_tail (GSList   *list,
		     gpointer  data,
		     GSList  **tail)
{
  GSList *tmp;
  GSList *prev;
  GSList *maybe_tail = NULL;
  
  prev = NULL;
  tmp = list;

  while (tmp)
    {
      if (tmp->data == data)
	{
	  if (prev)
	    {
	      if (!tmp->next)
		maybe_tail = prev;
	      prev->next = tmp->next;
	    }
	  if (tmp == list)
	    list = list->next;

	  tmp->next = NULL;
	  g_slist_free (tmp);

	  break;
	}

      prev = tmp;
      tmp = tmp->next;
    }
  
  if (maybe_tail)
    *tail = maybe_tail;

  if (list == NULL)
    *tail = NULL;
  
  return (list);
}

GSList *
g_slist_append_tail (GSList   *list,
		     gpointer  data,
		     GSList  **tail)
{
  GSList *rtail = *tail;
  
  if (list == NULL)
    {
      list = g_slist_append (list, data);
      *tail = list;
    }
  else
    {
      rtail = g_slist_append (rtail, data);
      *tail = rtail->next;
    }
  return (list);
}

