#ifndef __MY_GSLIST_H__
#define __MY_GSLIST_H__

GSList*
g_slist_remove_tail (GSList   *list,
		     gpointer  data,
		     GSList  **tail);

GSList *
g_slist_append_tail (GSList   *list,
		     gpointer  data,
		     GSList  **tail);

#endif /* __MY_GSLIST_H__ */

