#ifndef __ELEMENTS_H__
#define __ELEMENTS_H__

#include <xmlparse.h>
#include <glib.h>
#include "xml-tree.h"
#include "ebuffer.h"

/* Element handler functions. */

/* Node rendering function */
typedef void (*RenderFunc) (XML_Node * node);

/* destruction notice */
typedef void (*DestroyFunc) (XML_Node * node);

/* parenting function */
typedef void (*ParentFunc) (XML_Node * parent, XML_Node * child);

/* attribute is being set */
typedef gint (*SetAttrFunc) (XML_Node * node, EBuf *attr, EBuf *value);

/* attribute get - if you want to be able to return it on the fly */
typedef EBuf *(*GetAttrFunc) (XML_Node * node, EBuf *attr);

/* Called if a set_attr call did not find a valid attr in child,
 * it is propogated to the parent */
typedef void
  (*SetChildAttrFunc) (XML_Node * parent, XML_Node * child, EBuf *attr,
		       EBuf *value);

typedef void (*SetDataFunc) (XML_Node * node, XML_Char * data);

typedef void (*AppendDataFunc) (XML_Node * node, XML_Char * data);

typedef GString *(*GetDataFunc) (XML_Node * node);


typedef struct
{
  gchar *tag;

  /* function to render a node */
  RenderFunc render_func;

  /* called on destruction */
  DestroyFunc destroy_func;

  /* to parent a node */
  ParentFunc parent_func;

  /* Data set function */
  SetDataFunc set_data_func;

  /* data append function - TODO need insert as well */
  AppendDataFunc append_data_func;

  /* data_get hook */
  GetDataFunc get_data_func;

  /* Hash table of attributes */
  GHashTable *attributes;

  /* Attributes this widget will set on behalf of its children */
  GHashTable *child_attributes;
  
  /* Block child rendererings */
  gint no_render_children;

  /* Documentation */
  gchar *description;
} Element;


typedef struct
{
  /* name of attribute, or "*" for catchall */
  gchar *attribute;

  /* called on xml_node_set_attr () */
  SetAttrFunc set_attr_func;

  /* If registering with element_register_child_attr (), this will be the 
     function called */
  SetChildAttrFunc set_child_attr_func;

  /* called on xml_node_get_attr (), can optionally set the value in the 
     xml node struct via xml_node_set_attr_quiet (). */
  GetAttrFunc get_attr_func;

  /* Breif documentation on what the attribute does */
  gchar *description;

  /* Type of value stored in attribute, should be one of: 
     - list
     - integer
     - precentage
     - string
     - boolean
     - choice
     - font
     - etc.. (add it here if you use it.
   */
  gchar *value_desc;

  /* if a list, a comma seperated list of possiblities.
     if integer a range of possibilities (eg "1,5", "-1,10", "0,*" denotes 0 to infinity).
     if boolean, specify "true,false" with default first.

     Always list default first.
   */
  gchar *possible_values;

} ElementAttr;

/* register a new element type. */
void element_register (Element * element);

/* register an element attribute.  Can be done at any time. */
void element_register_attr (Element * element, ElementAttr * element_attr);

/* register an element attribute for a child of this element type */
void
element_register_child_attr (Element * element, ElementAttr * element_attr);

/* return attribute information from a node */
ElementAttr *element_attr_info (XML_Node * node, gchar * attr);

/* return all attributes available from a node */
GSList *element_supported_atts (XML_Node * node);



/* used internally */
void element_render (XML_Node * node);

void element_parent (XML_Node * parent, XML_Node * child);

void element_set_data (XML_Node * node, XML_Char * data);

void element_append_data (XML_Node * node, XML_Char * data);

GString *element_get_data (XML_Node * node);

void element_destroy (XML_Node * node);

void element_set_attr (XML_Node * node, EBuf *attr, EBuf *value);

EBuf *element_get_attr (XML_Node * node, EBuf *attr);

Element *element_lookup_element (XML_Node * node);

#endif /* __ELEMENTS_H__ */
