#include <glib.h>
#include <stdlib.h>
#include <string.h>

#include "elements.h"
#include "xml-node.h"
#include "xml-tree.h"

#include "string.h"
#include "norender.h"
#include "userrend.h"
#include "includerend.h"
#include "requirerend.h"

void
rendgeneric_init (void)
{
  include_renderer_init ();
  require_renderer_init ();
  string_renderer_init ();
  userrend_renderer_init ();
  norenderer_init ();
}

