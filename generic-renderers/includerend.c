#include <string.h>
#include <stdlib.h>

#include "elements.h"
#include "entity.h"
#include "config.h"
#include "xml-tree.h"
#include "xml-parser.h"
#include "renderers.h"

#include "erend.h"
#include "lang.h"
#include "edebug.h"
#include "eutils.h"

static void
rend_include_render (XML_Node * node)
{
  gchar *filename;
  XML_Node *retnode = NULL;
  
  filename = xml_node_get_attr_str (node, "file");
  
  if (filename)
    {
      edebug("includerend", "file = %s", filename);
      filename = eutils_file_search (filename);
      
      if (filename)
	{
	  retnode = xml_parse_file (node, filename);
	  
	  if (retnode)
	    xml_tree_render (retnode);
	  else
	    g_warning ("Error loading file for inclusion: %s", filename);
	  
	  g_free (filename);
	}
    }
}

void
include_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);
  element->render_func = rend_include_render;
  element->tag = "include";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "file";
  e_attr->description = "Path to XML file to include in place.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);
}





