#include <string.h>
#include <stdlib.h>

#include "elements.h"
#include "entity.h"
#include "config.h"
#include "xml-tree.h"
#include "renderers.h"

#include "erend.h"
#include "lang.h"
#include "edebug.h"


void
norenderer_init (void)
{
  Element *element;

  element = g_new0 (Element, 1);

  element->no_render_children = TRUE;

  element->tag = "norender";
  element_register (element);
}
