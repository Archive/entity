#include <string.h>
#include <stdlib.h>

#include "elements.h"
#include "entity.h"
#include "config.h"
#include "xml-tree.h"
#include "renderers.h"

#include "erend.h"
#include "lang.h"
#include "edebug.h"

static void
rendgtk_string_render (XML_Node * node)
{
  /*Call this to make sure that we are parented. */
  erend_edata_set (node, "erend-string-data", "true");
  xml_node_set_all_attr (node);
}

static void
rendgtk_string_destroy (XML_Node * node)
{
  /* Do nothing, entity will free everything for us. */
}

void
string_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);
  element->render_func = rendgtk_string_render;
  element->destroy_func = rendgtk_string_destroy;
  element->tag = "string";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "text";
  e_attr->description = "The string.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

}
