#include <string.h>
#include <stdlib.h>

#include "elements.h"
#include "entity.h"
#include "config.h"
#include "xml-tree.h"
#include "xml-parser.h"
#include "renderers.h"

#include "erend.h"
#include "lang.h"
#include "edebug.h"
#include "eutils.h"

static GHashTable *require_ht = NULL;

static gint
rend_require_file_already_loaded (gchar *filename)
{
  return (GPOINTER_TO_INT (g_hash_table_lookup (require_ht, filename)));
}

static void
rend_require_loaded_file (gchar *filename)
{
  g_hash_table_insert (require_ht, filename, GINT_TO_POINTER (TRUE));
}

static void
rend_require_render (XML_Node * node)
{
  gchar *filename;
  XML_Node *retnode = NULL;

  /* Initialize hash table if necessary */
  if (require_ht == NULL)
    require_ht = g_hash_table_new (g_str_hash, g_str_equal);
  
  filename = xml_node_get_attr (node, "file");
  
  if (filename)
    {
      filename = eutils_file_search (filename);
      
      if (filename)
	{
	  /* Check to see if it's already been loaded */
	  if (rend_require_file_already_loaded (filename))
	    {
	      edebug ("requirerend", "Not loading %s, already loaded.", filename);
	      g_free (filename);
	      return;
	    }
	  
	  retnode = xml_parse_file (node, filename);
	  
	  if (retnode)
	    {
	      xml_tree_render (retnode);
	      rend_require_loaded_file (filename);
	    }
	  else
	    g_warning ("Error loading file for inclusion: %s", filename);
	  
	  g_free (filename);
	}
      else
	g_warning ("Error loading file for inclusion: Unable to locate '%s'", filename);
    }
}

void
require_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  element = g_new0 (Element, 1);
  element->render_func = rend_require_render;
  element->tag = "require";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "file";
  e_attr->description = "Path to XML file to require for use.  Specified file is only ever loaded once.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);
}





