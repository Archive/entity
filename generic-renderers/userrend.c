#include <glib.h>
#include <string.h>
#include <stdlib.h>

#include "config.h"
#include "xml-tree.h"
#include "renderers.h"
#include "gtk-renderers/gtk-common.h"

#include "erend.h"
#include "lang.h"
#include "edebug.h"


GHashTable *userrend_tag_nodes_ht;

static void
userrend_passthru_parenter (XML_Node * parent_node, XML_Node * child_node)
{
  XML_Node *renderer_node;
  char *onparent;

  renderer_node = g_hash_table_lookup (userrend_tag_nodes_ht,
				       parent_node->element);
  if (!renderer_node)
    return;

  onparent = xml_node_get_attr (renderer_node, "onparent");
  if (!onparent)
    return;

  lang_call_function (renderer_node, onparent, "nn", parent_node, child_node);
}

static void
userrend_passthru_renderer (XML_Node * node)
{
  XML_Node *renderer_node;
  char *onrender;

  renderer_node = g_hash_table_lookup (userrend_tag_nodes_ht, node->element);
  if (!renderer_node)
    return;

  onrender = xml_node_get_attr (renderer_node, "onrender");
  if (!onrender)
    return;

  lang_call_function (renderer_node, onrender, "n", node);
  /*xml_node_set_all_attr (node);*/
}

static void
userrend_passthru_destroy (XML_Node * node)
{
  XML_Node *renderer_node;
  char *ondestroy;

  renderer_node = g_hash_table_lookup (userrend_tag_nodes_ht, node->element);
  if (!renderer_node)
    return;

  ondestroy = xml_node_get_attr (renderer_node, "ondestroy");
  if (!ondestroy)
    return;

  lang_call_function (renderer_node, ondestroy, "n", node);
}

static gint
userrend_passthru_attr_set (XML_Node * node, gchar * attr, gchar * value)
{
  XML_Node *renderer_node;
  XML_Node *attrib_node;
  GHashTable *attrib_ht;

  char *onset;

  edebug ("renderers", "userrend_passthru_attr_set");

  renderer_node = g_hash_table_lookup (userrend_tag_nodes_ht, node->element);
  if (!renderer_node)
    return FALSE;
  attrib_ht = erend_edata_get (renderer_node, "userrend-renderer-attrib-ht");

  attrib_node = g_hash_table_lookup (attrib_ht, attr);
  if (!attrib_node)
    return FALSE;

  onset = xml_node_get_attr (attrib_node, "onset");
  if (!onset)
    return FALSE;

  /* Call the onset function. */
  lang_call_function (renderer_node, onset, "nss", node, attr, value);
  return TRUE;
}

static void
userrend_renderer_parenter (XML_Node * parent_node, XML_Node * child_node)
{
  Element *element;
  ElementAttr *e_attr;

  char *tag;

  char *name;
  char *description;
  char *value_desc;
  char *values;
  XML_Node *renderer_node;
  GHashTable *attrib_ht;


  edebug ("renderers", "userrend_renderer_parenter");

  tag = xml_node_get_attr (parent_node, "tag");
  if (!tag)
    return;

  renderer_node = g_hash_table_lookup (userrend_tag_nodes_ht, tag);
  if (!renderer_node)
    return;
  attrib_ht = erend_edata_get (renderer_node, "userrend-renderer-attrib-ht");
  if (!attrib_ht)
    return;

  element = erend_edata_get (parent_node, "userrend-renderer-element");
  if (!element)
    return;

  if (!g_str_equal (child_node->element, "attrib"))
    return;

  name = xml_node_get_attr (child_node, "name");
  if (!name)
    return;

  description = xml_node_get_attr (child_node, "description");
  if (!description)
    g_warning
      ("Element <%s tag=%s>'s attrib <%s> doesn't have a description.",
       parent_node->name, tag, child_node->name);

  value_desc = xml_node_get_attr (child_node, "value_desc");
  values = xml_node_get_attr (child_node, "values");

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = name;
  e_attr->description = description;
  e_attr->value_desc = value_desc;
  e_attr->possible_values = values;
  e_attr->set_attr_func = userrend_passthru_attr_set;
  element_register_attr (element, e_attr);

  g_hash_table_insert (attrib_ht, name, child_node);

}

static void			/* Silly name but thats what this function is. */
userrend_renderer_renderer (XML_Node * node)
{
  Element *element;
  GHashTable *attrib_ht;

  char *tag;

  tag = xml_node_get_attr (node, "tag");
  if (!tag)
    return;

  erend_edata_set (node, "userrend_renderer_renderer-do", "true");

  element = g_new0 (Element, 1);
  element->render_func = userrend_passthru_renderer;
  element->destroy_func = userrend_passthru_destroy;
  element->parent_func = userrend_passthru_parenter;
  element->tag = tag;
  element_register (element);

  erend_edata_set (node, "userrend-renderer-element", element);

  attrib_ht = g_hash_table_new (g_str_hash, g_str_equal);
  erend_edata_set (node, "userrend-renderer-attrib-ht", attrib_ht);

  g_hash_table_insert (userrend_tag_nodes_ht, tag, node);

  xml_node_set_all_attr (node);
}

static void
userrend_attrib_renderer (XML_Node * node)
{
  erend_edata_set (node, "userrend-attrib-renderer-do", "true");

  xml_node_set_all_attr (node);
}


void
userrend_renderer_init (void)
{
  Element *element;
  ElementAttr *e_attr;

  userrend_tag_nodes_ht = g_hash_table_new (g_str_hash, g_str_equal);

  element = g_new0 (Element, 1);
  element->render_func = userrend_renderer_renderer;
  /*element->destroy_func = userrend_renderer_destroy; */
  element->parent_func = userrend_renderer_parenter;
  element->tag = "renderer";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "tag";
  e_attr->description = "The name of the tag for the new renderer.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "lang";
  e_attr->description = "The default language of the renderer.";
  e_attr->value_desc = "string";
  e_attr->possible_values = "language";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onrender";
  e_attr->description = "The function or method used to render the tag.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "ondestroy";
  e_attr->description = "The function or method used to remove the tag.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onparent";
  e_attr->description = "The function or method used to parent child tags.";
  e_attr->value_desc = "function";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);


  /* Attrib tag element. */
  element = g_new0 (Element, 1);
  element->render_func = userrend_attrib_renderer;
  /*element->destroy_func = userrend_attrib_destroy; */
  /*element->parent_func = userrend_attrib_parenter; */
  element->tag = "attrib";
  element_register (element);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "name";
  e_attr->description = "The attribute's name.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "description";
  e_attr->description = "The description of the attribute.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "value_desc";
  e_attr->description = "The type of value accepted.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "values";
  e_attr->description = "The possible values for this attribute.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

  e_attr = g_new0 (ElementAttr, 1);
  e_attr->attribute = "onset";
  e_attr->description =
    "The function or method to call when this attribute is set.";
  e_attr->value_desc = "string";
  e_attr->set_attr_func = NULL;
  element_register_attr (element, e_attr);

}
