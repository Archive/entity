#ifndef __LANG_H__
#define __LANG_H__

/* All arguments passed to lang_call_function in the arg_list need
   to be of this type. */

#define LANG_INT          (1<<0)
#define LANG_STRING       (1<<1)
#define LANG_NODE         (1<<2)
#define LANG_BINSTRING    (1<<3)
#define LANG_FLOAT        (1<<4)
#define LANG_DOUBLE       (1<<5)

typedef struct _LangArg
{
  void *data;			/* string, binary string, or XML_Node* */
  gint type;			/* A LANG_* */
  gint size;			/* Length of data. */
  gint intdata;			/* Integer, ->data will be set with the string aswell. */
}
LangArg;

GSList *lang_push_int (GSList * arg_list, gint arg);

GSList *lang_push_str (GSList * arg_list, gchar * arg);

GSList *lang_push_data (GSList * arg_list, gchar * arg, gint size);

GSList *lang_push_float (GSList * arg_list, gfloat arg);

GSList *lang_push_double (GSList * arg_list, gdouble arg);

GSList *lang_push_node (GSList * arg_list, XML_Node * node);

void lang_arg_free (LangArg * arg);

EBuf*
lang_call_function_with_list (XML_Node * calling_node,
			      gchar * function, GSList * arg_list);

/*
  This function uses vargs to call the embedded function.
  The format strings can have the following items:
  s     A \0 terminated string.
        Example:  "s", "str"
  b     A binary string.
        Example:  "b", binarydata, size
  e     An EBuf*.
        Example:  "e", ebuff
  n     An XML_Node*
        Example:  "n", node
  i     An integer.
        Example:  "i", 7
  f     A float.
        Example:  "f", 5.6
  d     A double.
        Example:  "d", .000000000000000000000000000000000000000000001

  For example, lets say that we want to pass the node and three ints:
  lang_call_function (node, function, "niii", node, 1, 2, 3);
*/
EBuf*
lang_call_function (XML_Node * calling_node, gchar * function,
		    gchar * fmt, ... /*args, args, ... */ );

typedef EBuf*
  (*LangDispatchFunc) (XML_Node * calling_node, gchar * function,
		       GSList * arg_list);

void lang_register (gchar * language_name, LangDispatchFunc func);

gchar *lang_namespace_get (gchar * language, XML_Node * calling_node);

#endif /* __LANG_H__ */
