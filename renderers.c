#include <glib.h>
#include <string.h>
#include <stdlib.h>

#include "config.h"
#include "xml-tree.h"
#include "renderers.h"
#include "gtk-renderers/gtk-common.h"
#include "generic-renderers/generic.h"
#include "c-embed/c-embed.h"
#include "perl-embed.h"
#include "python-embed/python-embed.h"
#include "tcl-embed/tcl-embed.h"

#include "erend.h"
#include "lang.h"
#include "edebug.h"

void entity_lang_init(void);

/* For now this just initalizes the known builtin renderers.
 * In the future, though, this will be the home of the
 * dynamic renderer loading */

void
renderers_init (void)
{
  perl_init ();

#ifdef USE_TCL
  tcl_init ();
#endif /* USE_TCL */

  c_init ();
#ifdef USE_PYTHON
  python_init ();
#endif /* USE_PYTHON */

  entity_lang_init();

  rendgtk_init ();

  rendgeneric_init ();

}
