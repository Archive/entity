#!/usr/local/bin/entity

<object>
	<window border = "5" 
 		title = "Entity" position = "center"
 		ondelete = "Entity::exit">
 
		<valign>
			<text name= "out" width = "100" height = "100"
			 editable = "true"/>
		</valign>
		
		<valign>
			<button name = "do" onclick="test">
				<label text="test"/>
			</button>
		</valign>		
	</window>
	
	<perl>
		Entity::require("datagrabber.pl");
	</perl>
		
</object>
