#!/usr/local/bin/entity

<object title = "Some Cool App">
  
  <menu name = "context"  label="foo">
    <menuitem onselect="menu_selected">
      <label text="Hello there"/>
      <button label="foobar" onclick="menu_selected"/>
    </menuitem>
    <menuitem onselect = "menu_selected" label = "choice 2"/>
      <menu label="Close2">
        <menuitem onselect = "menu_selected" label = "Foo1"/>
	<menuitem onselect = "menu_selected" label = "Foo2"/>
	<menu label = "foobar">
	  <menuitem onselect = "menu_selected" label = "Foo's go bye bye!"/>
	</menu>
	<menuitem onselect = "menu_selected" label = "Foo3"/>
      </menu>
    <menuitem onselect = "menu_selected" label = "choice 3"/>
  </menu>
  
  <window title="Some Cool object" x="60" y="60">
    
    <menubar>
      <menu label="File">
        <menuitem onselect="menu_selected">
	  <label text="Hello there"/>
	  <button label="foobar" onclick="menu_selected"/>
	</menuitem>
        <menuitem onselect = "menu_selected" label = "Dump Tree"/>
	<menu label="Close2">
	    <menuitem onselect = "menu_selected" label = "Foo1"/>
	    <menuitem onselect = "menu_selected" label = "Foo2"/>
	    <menu label = "foobar">
	      <menuitem onselect = "menu_selected" label = "Fooey on youey"/>
	      <menuitem onselect = "menu_selected" label = "Foo's go bye bye!"/>
	    </menu>
	    <menuitem onselect = "menu_selected" label = "Foo3"/>
	</menu>
	<menuitem onselect = "menu_selected" label = "New"/>
	<menuitem onselect = "menu_selected" label = "Quit"/>
      </menu>
      <menu label="Edit">
        <menuitem onselect = "menu_selected" label = "Select"/>
	<menuitem onselect = "menu_selected" label = "Cut"/>
	<menuitem onselect = "menu_selected" label = "Copy"/>
      </menu>
    </menubar>
    
    <halign homogeneous="false">
    <perl>
      sub menu_selected
        {
	  my ($path) = @_;
	  
	  $text = Entity::get_attr ($path, "label");
	  
	  print ("PERLY talkin' here - menu item '$text' selected\n");
	  if ($text eq "Quit") {
	    Entity::exit();
	  }
	  if ($text eq "Dump Tree") {
	    $xml_tree = Entity::get_xml("//");
	    print ("XML DUMP:\n$xml_tree\n");
	  }
	}
      
      sub attr_list
        {
	  my ($path) = @_;
	  my @list = Entity::get_set_atts ($path);
	  
	  print ("attributes set are: @list\n");
	  
	  @list = Entity::get_all_atts ($path);
	  
	  print ("possible attributes are: @list\n");
	  
	  my ($description, $value_desc, $possible_values);
	  
	  foreach $attr (@list) {
	    ($description, $value_desc, $possible_values) =
	          Entity::get_attr_info ($path, $attr);
	    print ("Attribute: $attr\n");
	    print ("  description: $description\n");
	    print ("  value_desc:  $value_desc\n");
	    print ("  possible:    $possible_values\n");
	  }
	}
      
      sub popup_context
        {
	  my ($name, $button, $x, $y) = @_;
	  print ("Hello - $name, $button, $x, $y\n");
	  if ($button == 3) {
	    Entity::set_attr ("menu.context", "popup", "true");
	  }
	}
    </perl>
      
      <image src="entity_logo.gif" height ="100"
       onbuttonpress="popup_context" border="10" />
     
       <valign name = "dump" border = "10">
        <button dragdata-text = "http://www.netidea.com/text" 
	 width="100" onclick="attr_list">
	  <label border="10" xalign = "45%" yalign = "70%" text = "Attribute list" />
	</button>
        
	  <object dragable="true">
	    <halign border="5">
	    <button dragdata-url = "http://www.netidea.com" 
	     dragdata-text = "http://www.netidea.com" 
	     onclick="Entity::exit">
	       <label text = "Netidea home page" />
	    </button>
	    </halign>
	  </object>
	
      </valign>
    
    </halign>
  
    <!-- Boxes with various attributes -->
    
    <perl>
      sub dropped
        {
	   my ($path, $drop_data, $action) = @_;
	   print ("PERL saying this - data dropped on $path, action $action, data $drop_data!\n");
	}
    </perl>
    
    <halign xalign = "50%" height = "60" spacing = "5">
      <button ondrop="dropped" onclick="Entity::exit" width = "110">
	<label xalign = "30%" yalign = "70%" text = "Hello" />
      </button>
      <button onclick="Entity::exit" expand="false" width = "90">
	<label xalign = "75%" text = "Hello" />
      </button>
      <button onclick="Entity::exit" expand="false" width = "70">
	<label xalign = "100" text = "Hello" />
      </button>
      <button onclick="Entity::exit" expand = "false">
	<label xalign = "75" yalign = "80%" text = "Hello" />
      </button>
    </halign>

    <!-- one last crazy test.. see if we can make a column
         of buttons going down the center of the window -->
    
    <halign expand="true" fill = "false">
      <valign width = "80">
        <button height = "50" onclick="Entity::exit">
	  <label text = "Hello" />
        </button>
        <button height = "40" onclick="Entity::exit">
  	  <label text = "Hello" />
        </button>
        <button height = "30" onclick="Entity::exit">
	  <label  text = "Hello" />
        </button>
      </valign>
    </halign>
  </window>
</object>
