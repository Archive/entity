#!/usr/local/bin/entity

<object>
  <window ondelete="Entity::exit" title="Timer" border="5">
    <valign>
      <object dragable="true">
        <button onclick="Entity::quit">
	  <label text="Quit Entity"/>
	</button>
	<frame border="5">
	  <label text = "This is a test to find"/>
	  <label text = "memory leaks in the perl"/>
	  <label name = "foo" text = "calling functions"/>
	</frame>
	<timer interval = "300" action="set_attr"/>
        <perl>
	  <![CDATA[
	    $toggle = 0;
	    $count = 0;
	    
	    sub set_attr
	      {
		$count++;
		enode ("label.foo")->attrib ("text", "Counting $count");
	      }
	   ]]>
	</perl>
      </object>
    </valign>
  </window>
</object>
	
