#!/usr/local/bin/entity

<object>
  <window border = "5" title="Hello World" width="300">
    <halign expand="true">
    <valign expand="true" fill="false">
      <button fill="false" label="hello" onclick="test" width="50" height="50"/>
      <button expand="false" label="again" onclick="test"/>
      <button label="hmm" onclick="test" width="50" height="50"/>
      <fixed width="100" height="200">
        <button label="one more" x-fixed="50" y-fixed="90" onclick="test"
	 width="50" height="50"/>
      </fixed>
    </valign>
    </halign>
  </window>
  
  <perl>
    sub test
    {
      my ($node) = @_;
      
      my $label = $node->attrib("label");
      print ("Hello - $label\n");
      $node->attrib(label => "foobared", width => "200");
    }
  </perl>
</object>
