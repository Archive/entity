#!/usr/local/bin/entity

<object title = "Some Cool App">
  
  <filesel name="main" title="Open File.." visible="false"
  onselect="filesel_selection"/>
  
  <window title="Some Cool object" width="200" height="200"
   ondelete="Entity::quit">
    <menubar expand="false">
      <menu label="File">
        <menuitem onselect = "menu_selected" label = "Save XML"/>
	<menuitem onselect = "Entity::quit" label = "Quit"/>
      </menu>
    </menubar>
    <valign expand="true" fill="true"/>
    
    <perl>
      <![CDATA[
      sub menu_selected
        {
	  my ($node) = @_;
	  print ("path is $node\n");
	  my $label = $node->attrib("label");
	  
	  print ("PERLY talkin' here - menu item '$label' selected\n");
	  
	  if ($label eq "Save XML") {
	    enode("filesel.main")->attrib( "visible" => "true");
	  }
	}

      sub filesel_selection
        {
	  my ($path, $file) = @_;
	  
	  my $xml_tree = enode("/")->get_xml();
	  
	  print ("Saving xml tree in $file\n");
	  open (OUT, ">$file");
	  print OUT $xml_tree;
	  close (OUT);
	}
      ]]>
    </perl>
  </window>
</object>
