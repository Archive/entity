#ifndef __ECONFIG_H__
#define __ECONFIG_H__

void econfig_set_attr (gchar * attr, gchar * value);

gint econfig_is_set (gchar * attr);

gchar *econfig_get_attr (gchar * attr);

void econfig_init (void);


#endif /* __ECONFIG_H__ */
