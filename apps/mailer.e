#!/usr/local/bin/entity -s

<object class="eMailer">
  <!-- Main window with incomming mail -->
  <window width="600" 
  	  height="450"
	  name="main"
	  visible="false"
	  ondelete="entity:exit">

    <menubar>
      <menu label="File">
	<menuitem name="quit" label="Quit" onselect="Entity::exit"/>
      </menu>
      <menu label="Mail">
	<menuitem name="get" label="Get Mail"/>
      </menu>
      <menu label="Settings">
	<menuitem name="setup" label="Setup..." onselect="show_setting"/>
	<menuitem name="accounts" label="Accounts..." onselect="show_setting"/>
	<menuitem name="aliases" label="Aliases..." onselect="show_setting"/>
	<menuitem name="filters" label="Filters..." onselect="show_setting"/>
      </menu>
      <menu name="help" label="Help">
	<menuitem name="needhelp" label="We Need Help"/>
      </menu>
      <perl><![CDATA[ 
      sub show_setting
      {
        my $node = shift;
        my $win = enode("window.".$node->attrib("name") );
        $win->attrib("visible" => "true");
      }
      ]]>
      </perl>
    </menubar>

    <halign border="4">
      <button name="compose" label="Compose Mail" onclick="show_new_compose"/>
      <button name="reply" label="Reply"/>
      <button name="get" label="Get Mail"/>
      <label text="     "/>
      <button name="trash" label="Trash Mail"/>
    </halign>

    <hpane expand="true">
      <scrollwindow expand="true" width="170">
        <tree name="mailboxes" expand="true" label="Mail Boxes">
	  <tree name="inbox" label="Inbox"/>
	  <tree name="outbox" label="Outbox"/>
	  <tree name="draft" label="Drafts"/>
	  <tree name="sent" label="Sent"/>
	  <tree name="trash" label="Trash"/>

          <tree name="accounts" label="Accounts">
	    <!-- <tree name="$USER" label="$USER"/> -->
	  </tree>

	</tree>
      </scrollwindow>
        <clist name="mail" expand="true">
	  <cl-row>
	    <cl-header title="^"/>
            <cl-header title="Status"/>
            <cl-header title="Date"/>
            <cl-header width="65" title="Sender"/>
            <cl-header title="Size"/>
            <cl-header title="Subject"/>
	  </cl-row>
	  <cl-row>
	    <string text="1"/>
	    <string text="read"/>
	    <string text="Dec 8"/>
	    <string text="Matt Wimer"/>
	    <string text="1.5k"/>
	    <string text="Hiya Stud"/>
	  </cl-row>
        </clist>
    </hpane>
    <entry name="status" text="Current Account = none" editable="false"/>
  </window>

  <!-- Compose window -->
  <norender>
  <window title="$title" 
    	  name="$name" 
	  width="400"
	  height="350"
	  visible="$visible" 
	  ondelete="close_compose">
    <halign border = "4" expand="false">
      <button name="send" label="Send Mail" onclick="send_mail"/>
      <label text="  "/>
      <button name="postpone" label="Postpone Mail" onclick="postpone_mail"/>
      <label text="    "/>
      <button name="attach" label="Attach"/>
      <button name="advanced" label="Advanced" visible="false"/>
    </halign>
    <halign>
      <label text="To:" width ="100"/>
      <entry name="to" expand="true"/>
    </halign>
    <halign>
      <label text="Cc:" width ="100"/>
      <entry name="cc" expand="true"/>
    </halign>
    <halign>
      <label text="From:" width ="100"/>
      <entry name="from" expand="true"/>
    </halign>
    <scrollwindow expand="true">
      <text name="compose" 
            expand="true"
	    editable="true"
	>$signature</text>
    </scrollwindow>
  </window>
  </norender>
  <perl><![CDATA[
  $current_account;

  sub get_account_info_data
  {
    my $node = shift;
    return (
    	   $node->attrib("real-name"),
    	   $node->attrib("address"),
    	   $node->attrib("reply-to"),
    	   $node->attrib("org"),
    	   $node->attrib("account-type"),
    	   $node->attrib("sig"),
    	   $node->basename(),
	   );
  }
  sub show_new_compose
  {
    my $win = enode("window.\$name");
    my $xml = $win->get_xml();

    my ($name, 
	$address, 
	$reply_to, 
	$org, 
	$type, 
	$sig,
	$xml_name) = get_account_info_data($current_account);

    if ($sig ne "" ) 
    {
      $sig = `cat $sig`;  ##Get sig file.
    }

    $win->object()->append_xml($xml, 
	    "title" => "Compose Message",
	    "signature" => $sig,
	    "name" => "compose-".++$num,
	    "visible" => "false");

    my $from = efind("window.compose-$num", "entry.from");

    print "$current_account\n";
    print "name = $name\n";
    $from->attrib("text" => $name);
    enode("window.compose-$num")->attrib("visible" => "true");
  }
  sub send_mail
  {
  }
  sub postpone_mail
  {
    my $node = shift;
    my $win = $node->find_parent_type("window");

    ###
    ## Postpone message...move message into Postponed.
    ###

    $win->delete();
  }
  sub close_compose
  {
    my $node = shift;

    $node->delete();
  }
  ]]>
  </perl>

  <!-- Read Window -->
  <window title="$Title" 
	  name="Name" 
	  width="400"
	  height="350"
   	  visible="false">
    <halign border = "4" expand="false">
      <button name="close" label="  Close  "/>
      <button name="attach" label="View Attachments"/>
      <button name="advanced" label="Advanced"/>
      <label text="  "/>
      <button name="trash" label="Trash"/>
    </halign>
    <halign>
      <label text="To:" width ="100"/>
      <entry name="to" expand="true" editable="false"/>
    </halign>
    <halign>
      <label text="Cc:" width ="100"/>
      <entry name="cc" expand="true" editable="false"/>
    </halign>
    <halign>
      <label text="From:" width ="100"/>
      <entry name="from" expand="true" editable="false"/>
    </halign>
    <scrollwindow expand="true">
      <text name="compose" expand="true" editable="false">
      </text>
    </scrollwindow>
  </window>

  <!-- Filters dialog-->
  <window title="Filters" 
	  name="filters" 
	  width="500"
	  height="300"
	  visible="false"
	  ondelete="close_filters">
    <halign border="4" expand="false">
      <button label="   New   "/>
      <button label="  Save  "/>
      <button label="  Close  " onclick="close_filters"/>
      <label  text ="        "/>
      <button label=" Remove "/>
    </halign> 

    <halign expand="true">
      <hpane expand="true">
        <scrollwindow expand="true" width="110">
          <list name="filters" expand="true">
            <label text="AOL"/>
            <label text="Hotmail"/>
          </list>
        </scrollwindow>
        <scrollwindow expand="true">
          <valign>
	    <halign>
	      <label text="Name" width="70"/>
	      <entry name="name" width="110" expand="true"/>
            </halign> 

	    <halign name="rx1">
	      <optionmenu>
	        <menu>
	          <menuitem name="subject" label="Subject:"/>
	          <menuitem name="from" label="From:"/>
	          <menuitem name="to" label="To:"/>
	          <menuitem name="cc" label="CC:"/>
                </menu>
	      </optionmenu>
	      <label text="Regex:"/>
	      <entry name="regex" width="100" expand="true"/>
	      <optionmenu>
	        <menu>
	          <menuitem name="action" label="Action"/>
	          <menuitem name="delete" label="Delete"/>
                </menu>
	      </optionmenu>
            </halign> 

	    <halign name="rx2">
	      <optionmenu>
	        <menu>
	          <menuitem name="subject" label="Subject:"/>
	          <menuitem name="from" label="From:"/>
	          <menuitem name="to" label="To:"/>
	          <menuitem name="cc" label="CC:"/>
                </menu>
	      </optionmenu>
	      <label text="Regex:"/>
	      <entry name="regex" width="100" expand="true"/>
	      <optionmenu>
	        <menu>
	          <menuitem name="action" label="Action"/>
	          <menuitem name="delete" label="Delete"/>
                </menu>
	      </optionmenu>
            </halign> 

	    <halign name="rx3">
	      <optionmenu>
	        <menu>
	          <menuitem name="subject" label="Subject:"/>
	          <menuitem name="from" label="From:"/>
	          <menuitem name="to" label="To:"/>
	          <menuitem name="cc" label="CC:"/>
                </menu>
	      </optionmenu>
	      <label text="Regex:"/>
	      <entry name="regex" width="100" expand="true"/>
	      <optionmenu>
	        <menu>
	          <menuitem name="action" label="Action"/>
	          <menuitem name="delete" label="Delete"/>
                </menu>
	      </optionmenu>
            </halign> 

	    <halign name="rx4">
	      <optionmenu>
	        <menu>
	          <menuitem name="subject" label="Subject:"/>
	          <menuitem name="from" label="From:"/>
	          <menuitem name="to" label="To:"/>
	          <menuitem name="cc" label="CC:"/>
                </menu>
	      </optionmenu>
	      <label text="Regex:"/>
	      <entry name="regex" width="100" expand="true"/>
	      <optionmenu>
	        <menu>
	          <menuitem name="action" label="Action"/>
	          <menuitem name="delete" label="Delete"/>
                </menu>
	      </optionmenu>
            </halign> 

          </valign>
        </scrollwindow>
      </hpane>
    </halign>
    <perl><![CDATA[
    sub close_filters
    {
      my $win = enode("window.filters");
      $win->attrib("visible" => "false");
    }
    ]]>
    </perl>

  </window>


  <!--Accounts dialog-->
  <window name="accounts" 
	  title="Acounts"
	  width="500"
	  height="300"
	  visible="false"
	  ondelete="close_accounts">
    <halign border="4" expand="false">
      <button label="   New   " onclick="pop_newaccount"/>
      <button label="  Close  " onclick="close_accounts"/>
      <label text ="      "/>
      <button label=" Remove "/>
    </halign> 
    <hpane expand="true">
      <scrollwindow expand="true" width="160">
        <list name="accounts" expand="true" onselect="display_account">
	  <!-- <label text="$USER"/> -->
        </list>
      </scrollwindow>
      <scrollwindow expand="true">
        <valign border="4">
          <halign>
	    <label text="Address:" width="90"/>
	    <entry name="address" editable="false"/>
	  </halign>
          <halign>
	    <label text="Name:" width="90"/>
	    <entry name="name"/>
	  </halign>
          <halign>
	    <label text="Reply-To:" width="90"/>
	    <entry name="reply-to"/>
	  </halign>
          <halign>
	    <label text="Org:" width="90"/>
	    <entry name="org"/>
	  </halign>
          <halign>
	    <label text=".signature" width="90"/>
	    <entry name="sig"/>
	    <button label="Browse"/>
	  </halign>
          <halign expand="false" homogenous="true">
	    <radio-group>
	      <radio name="imap" label="IMAP"/>
	      <radio name="pop" label="POP3"/>
	      <radio name="local" label="Local"/>
	    </radio-group>
	  </halign>
	  <halign border="3" spacing="4" homogenous="true">
            <button border="4" label="Save"  onclick="save_account"/>
            <button border="4" label="Reset"  onclick="reset_account"/>
	  </halign>
        </valign>
      </scrollwindow>
    </hpane>
    <perl><![CDATA[
    $current_account_displayed;
    sub close_accounts
    {
      my $win = enode("window.accounts");
      $win->attrib("visible" => "false");
    }
    sub pop_newaccount
    {
      enode("window.newaccount")->attrib("visible" => "true");
    }

    sub account_get_from_window
    {
      my $win = enode(shift);

      my ($address, $name, $reply_to, $org, $sig) =
        $win->find_children_attribs
          ("entry.address"  => "text", "entry.name" => "text",
	   "entry.reply-to" => "text", "entry.org"  => "text",
	   "entry.sig"      => "text" );

      my $acctype;
      my $type;
      foreach $type ("local", "pop", "imap" )
      {
         if ($win->find_child("radio.$type")->attrib("selected") eq "true")
	 {
	   $acctype = $type;
	   last;
	 }
      }

      return ($name, 
	      $address, 
	      $reply_to, 
	      $org, 
	      $acctype, 
	      $sig,
	      $xml_name);
    }

    sub save_account
    {
      my $accounts = enode("data.accounts");
      my $account = $current_account_displayed;

      my ($name, 
	  $address, 
	  $reply_to, 
	  $org, 
	  $type, 
	  $sig,
	  $xml_name) 
	  = account_get_from_window("window.accounts");

      $account->attrib(
      		"real-name" => $name,
		"address" => $address,
		"reply-to" => $reply_to,
		"org" => $org,
		"sig" => $sig,
		"account-type" => $type,
		);
    }
    sub display_account
    {
      my $node = shift;
      my $xml_name = $node->attrib("xml-name");
      my $accounts = enode("data.accounts");
      my $accwin = enode("window.accounts");

      my $account = $accounts->find_child("data.$xml_name");

      my ($name, 
	  $address, 
	  $reply_to, 
	  $org, 
	  $type, 
	  $sig,
	  $xml_name) 

	  = get_account_info_data($account);

      $current_account_displayed = $account;

      $accwin->find_child("entry.address")->attrib("text" => $address);
      $accwin->find_child("entry.name")->attrib("text" => $name);
      $accwin->find_child("entry.reply-to")->attrib("text" => $reply_to);
      $accwin->find_child("entry.org")->attrib("text" => $org);
      $accwin->find_child("entry.sig")->attrib("text" => $sig);
      $accwin->find_child("radio.$type")->attrib("selected" => "true");
    }
    ]]>
    </perl>

  </window>

  <!-- New Account Window -->
  <window name="newaccount" 
  	  title="New Account"
	  visible="false">
        <valign border="4">
          <halign>
	    <label text="Address:" width="90"/>
	    <entry name="address"/>
	  </halign>
          <halign>
	    <label text="Name:" width="90"/>
	    <entry name="name"/>
	  </halign>
          <halign>
	    <label text="Reply-To:" width="90"/>
	    <entry name="reply-to"/>
	  </halign>
          <halign>
	    <label text="Org:" width="90"/>
	    <entry name="org"/>
	  </halign>
          <halign>
	    <label text=".signature" width="90"/>
	    <entry name="sig"/>
	    <button label="Browse"/>
	  </halign>
          <halign expand="false" homogenous="true">
	    <radio-group>
	      <radio name="imap" label="IMAP"/>
	      <radio name="pop" label="POP3"/>
	      <radio name="local" label="Local"/>
	    </radio-group>
	  </halign>
	  <halign border="3" spacing="4" homogenous="true">
            <button border="4" label="Save"  onclick="save_newaccount"/>
            <button border="4" label="Close"  onclick="close_newaccount"/>
	  </halign>
        </valign>
    <perl><![CDATA[
    sub save_newaccount
    {
      my $accounts = enode("data.accounts");
      my ($data, $address) = account_get_from_window("window.newaccount");

      my $account = $accounts->new_child("data.$address");

      #print "$data";
      $account->set_data($data);
      print $account->get_data(),"\n";
      display_accounts();
    }
    sub close_newaccount
    {
      enode("window.newaccount")->attrib("visible" => "false");
    }
    ]]></perl>
  </window>

  <!--Aliases Dialog-->
  <window name="aliases" 
	  title="Aliases" 
          width="450"
	  height="300"
	  visible="false"
	  ondelete="close_aliases"
   >
    <halign border="4" expand="false">
      <button label="   New   "/>
      <button label="  Save  "/>
      <button label="  Close  " onclick="close_aliases" />
      <label text ="      "/>
      <button label=" Remove "/>
    </halign> 

    <halign expand="true">
    <hpane expand="true">
      <scrollwindow expand="true" width="110">
        <list expand="true">
          <label text="alias one"/>
          <label text="alias two"/>
          <label text="alias three"/>
        </list>
      </scrollwindow>
      <scrollwindow expand="true">
        <valign border="4">
          <halign>
            <label text="Alias:" width="90"/>
            <entry name="alias"/>
          </halign>
          <halign>
            <label text="Name:" width="90"/>
            <entry name="name"/>
          </halign>
          <halign>
            <label text="Address:" width="90"/>
            <entry name="address"/>
          </halign>
        </valign>
      </scrollwindow>
    </hpane>
    </halign>
    <perl><![CDATA[
    sub close_aliases
    {
      my $win = enode("window.aliases");
      $win->attrib("visible" => "false");
    }
    ]]>
    </perl>
  </window>

  <!--Accounts data, needs to be a seperate file...-->
  <data name="accounts">
  </data>

  <include name="rc" file="mailer.rc"/>

  <!-- Account Template -->
  <norender name="tmpl">
      <data 
	  name="$acc_name"
          type="account" 
          real-name="$name"
      	  address="$address"
      	  reply-to="$reply_to"
      	  org="$org"
      	  account-type="$acc_type"
      	  sig="$sig"
      />
      <data name="$name"
          type="filter-rule"
          part="$part"
          regex="$regex"
          to="$mailbox"
          command="$command"
       />
  </norender>

  <perl name="startup code"><![CDATA[
  sub display_current_account
  {
    enode("entry.status")->attrib("label" => "Current Account = ".
    				  $current_account->basename() );
  }

  sub display_accounts
  {
    my $rc = enode("include.rc");
    my $accounts = enode("data.accounts");
    my $list = enode("list.accounts");
    my $tree = enode("tree.accounts");
    $list->delete_children();
    $tree->delete_children();

    ## Load rc.
    my $obj = $rc->find_child("object.rc");
    if($obj)
    {
      my $rc_xml = $obj->get_xml();
      print "rc_xml = $rc_xml\n";
      $accounts->append_xml($rc_xml);
    }

    $account_num = 1;
    if ( !scalar($accounts->children()) ) 
    {
      my $xml = (enode("norender.tmpl")->children())[0]->get_xml();

      #print "Xml = $xml\n";

      $accounts->append_xml($xml, 
      		"name" => $ENV{USER},
		"address" => $ENV{USER},
		"reply_to" => $ENV{USER},
		"org" => "None",
		"sig" => "~/.sig",
		"acc_type" => "local",
		"acc_name" => "$account_num",
		);
    }

    foreach my $account ($accounts->children())
    {
      if ($firsttime == undef)
      {
        $firsttime = 1;
	$current_account = $account;
      }

      my ($real_name, 
	  $address, 
	  $reply_to, 
	  $org, 
	  $type, 
	  $sig,
	  $xml_name) 
	  = get_account_info_data($account);

      if(! $type =~ /account/)
      {
	next;  ## probably a filter.
      }

      $tree->new_child("tree.", "label" => $address, "xml-name" => $xml_name);
      $list->new_child("label.", "text" => $address, "xml-name" => $xml_name);
    }
  }
  display_accounts();

  display_current_account();

  ##Display now that we are done.
  enode("window.main")->attrib("visible" => "true");

  ]]></perl>
</object>
