#!/usr/local/bin/entity
<object>
  <window title="#entity" 
  	width="400" height="400"
	ondelete="entity:exit">
    <scrollwindow name="main" expand="true">
      <text name="main">Welcome to #entity
</text>
    </scrollwindow>
  </window>

  <require file="Net/url-renderer.e"/>

  <url name="matts"
  	href="socket://thunder.cgibuilder.com:6789/"
	onaction="myconnected"/>

  <perl><![CDATA[
  
  enode("url.matts")->attrib("action" => "connect");
  sub myconnected
  {
    my ($url, $io) = @_;
    $io->attrib("onnewdata" => "write_text");
  }
  sub write_text
  {
    my ($io, $data, $size) = @_;
    if ($size == 0)
    {
      $io->attrib("fd" => "-1");
      $data ="DISCONECTED\n";
    }

    enode("text.main")->append_data($data);
    enode("scrollwindow.main")->attrib("y-scroll"=>"100");
  }
  ]]></perl>

</object>

