<!-- Document WareHousing Repository (DocuWHR) -->
<!-- %1: DCB% -->
<object>

<window name="README" ondelete="hide_window" visible="false"
        width="690" height="674">
  <scrollwindow expand="true"> 

<!-- README -->
    <text>
Welcome to the Document WareHousing Repository (DocuWHR) v1.0alpha3.

INTRO 
  We thank you for your interest in this software, and in the Entity XML system.  Your
support gives meaning to our wretched existence, and we are gratified to know that there are
other people outside the dimly lit cubicles in which we toil.  But, enough about my life,
let's get down to the DocuWHR system.


RELEASE NOTES 
  As this is alpha-release software, I shall not apologize for a lack of features or for any
undocumented surprises you may find.  That's the hazard in using alpha software.  Still,
thanks to the work of everyone behind Entity, its quite stable as-is.  Still, a quick
disclaimer is in order:  This software is provided as-is and is not sold or released for any
purpose whatsoever and most especially not for the purpose of determining the root password
on a system, or creating snide comments about a co-worker from a database of rude sayings.  
Nor is this software released with any intention of it being used in a mission-critical
environment, and if it is, well...Please don't hit Control-J, think of the Children!


WHAT IT DOES 
  The point of all this is to provide a documentation system that is easy-to-use, as well as
highly functional for annotating an existing code base.  In theory, one could write raw code
with DocuWHR, but that's not its original purpose.  The idea is to provide a mechanism
whereby long comments, things relevant only to the manuals, or other such notes are kept out
of the code base and in a separate file system.


TYING IT ALL TOGETHER 
  The next part of DocuWHR is the document generation system whereby all of the .DWH files
and annotated source code will be merged into a lovely web page (thereby leveraging the
power of XML and HTML together).  This will be gotten around to shortly, and as this was an
afternoon's work to create DocuWHR, that part shouldn't take much longer.


CUSTOMIZING DOCUWHR 
  Certain variables have been hard-coded for my (David Camden-Britton) convenience.  These
will be identified with the tag:  ### CUSTOMIZE ### So that they can be easily searched for
and changed to an appropriate value for your usage.  Eventually, we'll have either a GUI way
to change these settings, or some happy little RC file for stuff.

     
OBTAINING DOCUWHR 
  The most current version of this program can be retreived from the Cylant Technology web
page (http://www.cylant.com) or by emailing David Camden-Britton (davidcb@acm.org) with
"DOCUWHR" in the subject line.


THE AUTHORS 
  Matt Wimer created the base code for this app as a demonstration of Entity's power.  
Original idea by David Camden-Britton, who currently maintains the code.  This was created
through the auspices of Cylant Technology, LLC (http://www.cylant.com) and much thanks go to
them for allowing us to release this code to the public.  Both Matt and David are employees
of Cylant Technology.


LICENSING AGREEMENT 
  This code is released under the BSD license with the caveat that it is alpha software and
is probably only vaguely functional.  If it maims your system, or calls your loved ones
funny names, we disavow any knowledge of its actions and take no responsibility for any
damages resulting from its use.
    </text>
  </scrollwindow>
</window>

<!-- Create a top-level window -->
  <window width="800" height="600" ondelete="Entity::exit">

<!-- Menu bar across top -->
     <menubar >
        <menu label="File">
           <menuitem label="Save" onselect="save_annote" />
           <menuitem label="Exit" onselect="Entity::exit" />
        </menu>
        <menu label="Help">
           <menuitem label="Readme" onselect="show_readme" />
           <menuitem label="About" onselect="show_about" />
        </menu>
     </menubar>

<!-- Main horizontal alignment panel for the DocuWHR window. --> 
    <halign expand="true">

<!-- Create the file list in the leftmost panel -->
      <list name="files" width="100" onselect="load_source_file">
      </list>

<!-- Create the source code panel.  This is a scrolled window with the ability to edit the text directly inside. --> 
      <hpane expand="true">
      <scrollwindow expand="true" width="500">
        <text name="source" editable="true"> </text>
      </scrollwindow>

<!-- Create the Annotation panel.  Scrolled window, editable text. --> 
      <scrollwindow expand="true" width="200">
        <text name="annote" editable="true"> </text>
      </scrollwindow>
      </hpane>

    </halign>

<!-- Button box at the bottom of the window.  --> 
    <halign>
      <button label="Add Annotation" onclick="add_annote"/>
      <button label="Save Files" onclick="save_annote"/>
      <entry name="paster" width="100"/>

<!-- ### CUSTOMIZE ### -->
      <label text="Open Comment: "/>
      <entry name="open" text="&lt;!--" width="64" />

      <label text="Close Comment: "/>
      <entry name="close" text="--&gt;" width="64" />

      <label text="Commenter's Initials" />
      <entry name="initials" text="DCB" />
    </halign>
  </window>

<!-- Create the stupid-ass "About" box -->
  <window title="About DocuWHR" name="About" visible="false" 
          width="348" height="86" ondelete="hide_window">
      <label text="Document WareHousing Repository v1.0alpha3" />
      <label text="Written by Matt Wimer and David Camden-Britton" />
      <label text="Thanks to Cylant Technology, LLC for the cycles" />
      <label text="" />
      <label text="Thank you for using DocuWHR!" />
  </window>



<!-- Start the Perl routines that do the work in this program -->
  <perl><![CDATA[

### CUSTOMIZE ###
### Directory where the files to be edited reside.  This should be something parsed at run-time in the future, or better yet, a tree widget.  But not today.

  $dir = $ENV{HOME}."/entity";

### Run this at the startup of DocuWHR to create the listing of files to be edited 
  sub load_dir
  {

    my $filelist = enode("list.files");

### CUSTOMIZE ###
    my @files = split ("\n", `cd $dir; /bin/ls *.e`);
    my $file;

    my $codefile;

    foreach $file (@files)
    {
      $filelist->new_child("label", "file" => $file, "text" => $file);
    }
  } ### End load_dir

###  Load the source file that has been clicked on.  At the same time, load up the annotation file that corresponds to this particular file. 
  sub load_source_file
  {
#    print "Loading Source File\n";
    my $filenode = shift(@_);

    my $filename = $filenode->attrib("file");

    ###codefile GOBAL###
    $codefile = "$dir/$filename";

    my $data = `cat $codefile`;
    enode("text.source")->set_data($data);

### %2: DCB% 
    $data = `cat $codefile.dwh`;
    enode("text.annote")->set_data($data);
  }  ### End load_source_file
 
  my $num;
  sub add_annote
  {
    ### Grab the data within the text entry boxes for Open, close and Initials.
    ### This will then be inserted into the Pasting box and appended to the annotation file.
### %3: DCB% 
    my $open = enode("entry.open")->attrib("text");
    my $close = enode("entry.close")->attrib("text");
    my $Initials = enode("entry.initials")->attrib("text");

    my $entry = enode("entry.paster");
    $num++;
    my $hash = "$open %$num: $Initials% $close";
    $entry->attrib("text" => $hash);
    enode("text.annote")->append_data("\n$hash\n");
  } ### End add_annote

  sub save_annote
  {
    ### Grab data from the source and annotation panels
    my $source = enode("text.source")->get_data();
    my $annote = enode("text.annote")->get_data();

    ### Feel the power of shell commands.  Make a backup of the Source file for safety.
    `cp $codefile $codefile.bak`;

    ### Note, this is a destructive open, the contents are vaporized.

    open(CODE, ">$codefile");

    print CODE $source;
    close CODE;

    open(ANNOTE, ">$codefile.dwh");
    print ANNOTE $annote;
    close ANNOTE;
  } ### End save_annote

  sub hide_window
  {
    my $window= shift;
    $window->attrib("visible" => "false");
  }
  sub show_about
  {
    my $window= enode("window.About");
    $window->attrib("visible" => "true");
  }

  sub show_readme
  {
    my $window= enode("window.README");
    $window->attrib("visible" => "true");
  }

  load_dir();
  ]]></perl>
</object>

