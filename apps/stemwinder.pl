
sub hide_window
{
    my ($node) = @_;
    $node->attrib("visible" => "false");
}

sub show_window
{
    my ($node) = @_;
    my $window = $node->attrib ("arg");
    enode ($window)->attrib ("visible" => "true");
}

sub expand_tree
{
    my ($calling_node) = @_;
    
    #print ("calling node is $calling_node\n");
    
    my $dest_node = enode ($calling_node->attrib("dest-node"));
    
    #print ("destination node is $dest_node\n");

    # Clean out old info.
    my @nodes = $calling_node->children ();
    my $node;
    
    # Go through and look for <tree> elements to delete.
    foreach $node (@nodes) {
	if ($node->type() eq "tree") {
	    $node->delete();
	    #print ("deleting $node\n");
	}
    }
    
    # Get a list of nodes at the destination
    @nodes = $dest_node->children ();
    my ($xml, $title, $name, $extra);
    
    # And generate tree entries for each one
    foreach $node (@nodes) {
	#print ("working on $dest_node child $node\n");
	$title = $node->attrib ("title");
	
	if (defined ($title)) {
	    $extra = "($title)";
	} else {
	    undef ($extra);
	}
	
	$name = $node->basename ();

	$xml = qq!<tree ondrop="tree_drop" dest-node="$node" onbuttonpress="popup_context"> 
	              <label xalign="0" text="$name $extra"/>
	          </tree>!;
	$calling_node->append_xml ($xml);
    }
}

sub tree_drop
{
    my ($node) = @_;
    print ("Something got dropped on tree item $node\n");
}

sub properties
{
    my ($calling_node) = @_;
    
    # Get the destination node that this entry is referring to.
    my $dest_node = enode ($calling_node->attrib ("dest-node"));

    # Get attribute list object happening
    my $att_list = enode ("list.attributes");
    
    # Delete all the current list entries
    $att_list->delete_children ();

    # Get a list of all supported attributes
    my @list = $dest_node->supported_attribs ();
    
    # Sort alphabetically
    my @slist = sort @list;

    # Generate a list of properties this node supports
    my ($attrib, $xml);
    foreach $attrib (@slist) {
	$xml .= qq!<halign attrib-name="$attrib" dest-node="$dest_node"> 
	             <label text="$attrib"/> 
		   </halign>
		  !;
    }
    
    # And append list items to list
    $att_list->append_xml ($xml);
    
    #
    # Now we do the same for the set attributes list
    #
    my $set_att = enode ("list.set_attributes");
    $set_att->delete_children ();
    my @list = $dest_node->get_set_attribs ();
    my @slist = sort @list;
    
    my ($attrib, $xml);
    foreach $attrib (@slist) {
	$xml .= qq!<halign attrib-name="$attrib" dest-node="$dest_node"> 
	             <label text="$attrib"/> 
		   </halign>
		  !;
    }
    
    # And append list items to list
    $set_att->append_xml ($xml);
    
    # Clean out property details panel
    enode ("frame.attrib_details")->delete_children ();
}

sub view_detailed_property
{
    my ($calling_node) = @_;
    
    # Get the destination node that this entry is referring to.
    my $dest_node = enode ($calling_node->attrib ("dest-node"));
    
    # And attribute in question
    my $attrib = $calling_node->attrib ("attrib-name");
    
    # where this xml is going..
    my $dest = enode ("frame.attrib_details");
    $dest->delete_children ();

    # Query for attribute information 
    my ($description, $value_desc, $possible_values) =
        $dest_node->attrib_info ($attrib);

    my $value = $dest_node->attrib ($attrib);

    # print ("$description, $value_desc, $possible_values, $value\n");
    
    my $description_line = qq!<hwrapalign border="4" hspacing="5"> !;
    my $word;
    @words = split (/\s/, $description);
    foreach $word (@words) {
	$description_line .= qq!<label text="$word"/>!;
    }
    
    $description_line .= "</hwrapalign>";

    # and stick it into the frame
    my $xml = qq!<frame border="4" title="Description">
	           $description_line
		 </frame>
		 <frame border="4" title="Value Description">
		   <valign border="4">
		     <label border="4" xalign="0" text="Type: $value_desc"/>
		     <label xalign="0" text="Possible: $possible_values"/>
		   </valign>
		 </frame>
		 <frame border="4" title="Current Value">
		   <entry border="4" name="attrib_value" dest-node="$dest_node" attrib-name="$attrib" 
		    text="$value" onenter="set_property"/>
		 </frame>
		!;
    
    $dest->append_xml ($xml);
}

sub set_property
{
    my ($node) = enode ("entry.attrib_value");
    
    my $dest_node = enode ($node->attrib ("dest-node"));
    my $attr = $node->attrib ("attrib-name");
    my $value = $node->attrib ("text");
    
    #print ("Setting $node $attr to $value\n");
    # Set target node property
    $dest_node->attrib ($attr, $value);
}


# Context menu stuff 
my $popup_on_path;

sub popup_context
{
    ($popup_on_path, $button) = @_;
    
    if ($button == 3) {
	enode ("menu.context")->attrib (popup => true);
    }
}

sub menu_view_data
{
    my $target = enode ($popup_on_path->attrib ("dest-node"));
    
    my $data = $target->get_data ();
    enode ("text.view")->set_data ($data);

    my $window = enode ("window.text");
    $window->attrib ("title" => "Data for $target", "visible" => "true");
}

sub menu_view_xml_tree
{
    my $target = enode ($popup_on_path->attrib ("dest-node"));
    
    my $xml = $target->get_xml ();
    enode ("text.view")->set_data ($xml);

    my $window = enode ("window.text");
    $window->attrib ("title" => "XML Tree for $target", "visible" => "true");
}

sub menu_delete_tree
{
    my $target = enode ($popup_on_path->attrib ("dest-node"));
    $target->delete ();
    $popup_on_path->delete ();
}

sub menu_new_child
{
    my ($node) = @_;
    my $target = enode ($popup_on_path->attrib ("dest-node"));
    my $type = $node->attrib ("label");
    my $new_node = $target->new_child (lc ($type));
}

#print ("Done loading stemwinder code!\n");

1;

