#!/usr/local/bin/entity

<object name="StemWinder">
  
  <!-- Context Menu -->
  <menu name="context" label="Menu">
    <menuitem onselect="menu_view_data" label="View Data"/>
    <menuitem onselect="menu_delete_tree" label="Delete Tree"/>
    <menuitem onselect="menu_view_xml_tree" label="View XML Tree"/>
    <menu name="insert_element" label="New Child...">
      <menuitem onselect="menu_new_child" label="Button"/>
      <menuitem onselect="menu_new_child" label="Valign"/>
      <menuitem onselect="menu_new_child" label="Halign"/>
      <menuitem onselect="menu_new_child" label="Frame"/>
      <menuitem onselect="menu_new_child" label="Window"/>
    </menu>
  </menu>
  
  <!-- Window for viewing XML and data -->
  <window name="text" position="center" ondelete="hide_window" visible="false">
    <scrollwin border="4" expand="true" width="500" height="400">
      <text name="view"> </text>
    </scrollwin>
  </window>
  
  <!-- Elements window -->
  <window name="elements" title="Entity XML Elements" visible="false">
    <vwrapbox>
      <button label="Button" dragdata-text="button"/>
      <button label="Window" dragdata-text="window"/>
      <button label="Vertical Align" dragdata-text="valign"/>
      <button label="Horizontal Alignment" dragdata-text="halign"/>
      <button label="List" dragdata-text="list"/>
    </vwrapbox>
  </window>

  <!-- Main window -->
  <window name="StemWinder" position="center" ondelete="perl:Entity::exit" title="StemWinder">
    <menubar>
      <menu label="Windows">
        <menuitem onselect="show_window" arg="window.elements" label="Elements Window"/>
      </menu>
    </menubar>
    <halign expand="true">
      <frame title="Entity XML Tree" border="3" expand="true">
        <valign expand="true" border="3">
	  <scrollwin expand="true" width="250" height="500" border="4">  
	    <tree name="top" 
	          selection-type="single"
                  onselect="properties"
                  onexpand="expand_tree">
	      <tree dest-node="//">
                <label xalign="0" text="/ (root node)"/>
              </tree>
	    </tree>
	  </scrollwin>
        </valign>
      </frame>
     <valign expand="true">
       <frame border="4" title="Supported Attributes" expand="true">
         <scrollwin border="4" expand="true" width="160">
           <list expand="true" name="attributes" 
	    onselect="view_detailed_property"/>
         </scrollwin>
       </frame>
     
       <frame border="4" title="Set Attributes" expand="true">
         <scrollwin border="4" expand="true" width="160">
           <list expand="true" name="set_attributes" 
	    onselect="view_detailed_property"/>
         </scrollwin>
       </frame>
     </valign>
     <frame expand="true"
	    border="4" 
            name="attrib_details"
            title="Attribute Details"
            width="200" />
   </halign>
  </window>
  <perl>
	Entity::require ("stemwinder.pl");
  </perl>
</object>

