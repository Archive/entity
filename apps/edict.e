#!/usr/local/bin/entity

<object>
  <window ondelete="perl:Entity::exit" title="Edict" position="center">
    <object dragable="true">  
      <frame title="Lookup Word" border="3">
        <halign border="3" ondrop="got_drop">
          <entry name="in" onenter="lookup_word"/>
	  <button label="Lookup" onclick="lookup_word"/>
	</halign>
      </frame>
      <queued-io 
        name="src"
        onnewdata="got_line" 
      />
      <window ondelete="close_window" name="popup" 
       visible="false" position="center">
	  <scrollwin width="600" height="300" expand="true">
	    <text name="definition" expand="true">
	    </text>
	  </scrollwin>
	  <button label="Close" expand="true" fill="false" border="2"
	   onclick="close_window"/>
      </window>
      <perl>
      <![CDATA[
      use Socket;
      
      sub close_window
      {
        enode ("window.popup")->attrib (visible => false);
      }
      
      sub got_drop
      {
        my ($target, $data) = @_;
	print ("got dnd on $target, $data\n");
	
	$entry = enode ("entry.in");
	$entry->attrib (text => "$data");
	lookup_word ($entry);
      }
      
      sub lookup_word
      {
        my ($frm) = @_;
	
	my $word = $frm->attrib ("text");
	
	my ($remote, $iaddr, $paddr, $proto, $line);
	my $hostname = "128.52.39.7";
	my $port = 2627;
	
	$iaddr = inet_aton ($hostname) || die "no host: $hostname $!";
	$paddr = sockaddr_in ($port, $iaddr);
	
	$proto = getprotobyname('tcp');
	socket(SOCK, PF_INET, SOCK_STREAM, $proto)  || die "socket: $!";
	connect(SOCK, $paddr) || die "connect: $!";
	
	$header = "DEFINE $word\n";
	
	my $src = enode ("queued-io.src");
	
	# Setup fd for src
	$src->attrib ("fd" => fileno (SOCK));
	
	# Send header
	$src->write ($header);

	enode ("text.definition")->set_data ("");
	enode ("window.popup")->attrib (visible => true);
      }
      
      sub got_line
      {
        my ($node, $data, $size) = @_;
	
	if ($size == 0) {
	  close ($node->attrib ("fd"));
          $node->attrib("fd" => -1);
	  return;
	}
	enode ("text.definition")->append_data ("$data");
    }
  
  ]]>
  </perl>

    </object>
  </window>
  
</object>
