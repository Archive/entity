      use Socket;
      
      sub close_window
      {
        enode ("window.popup")->attrib (visible => false);
      }
      
      sub got_drop
      {
        my ($target, $data) = @_;
	print ("got dnd on $target, $data\n");
	
	$entry = enode ("entry.in");
	$entry->text="$data";
	lookup_word ($entry);
      }
      
      sub lookup_word
      {
        my ($frm) = @_;
	
	my $word = $frm->attrib ("text");
	
	my ($remote, $iaddr, $paddr, $proto, $line);
	my $hostname = "128.52.39.7";
	my $port = 2627;
	
	$iaddr = inet_aton ($hostname) || die "no host: $hostname $!";
	$paddr = sockaddr_in ($port, $iaddr);
	
	$proto = getprotobyname('tcp');
	socket(SOCK, PF_INET, SOCK_STREAM, $proto)  || die "socket: $!";
	connect(SOCK, $paddr) || die "connect: $!";
	
	$header = "DEFINE $word\n";
	
	syswrite (SOCK, $header, length ($header));
	
	my $src = enode ("queued-io.src");
	$src->attrib ("fd" => fileno (SOCK));
	enode ("text.definition")->set_data ("");
	enode ("window.popup")->attrib (visible => true);
      }
      
      sub got_line
      {
        my ($node, $data, $size) = @_;
	
	if ($size == 0) {
	  close ($node->attrib ("fd"));
          $node->attrib("fd" => -1);
	  return;
	}
	enode ("text.definition")->append_data ("$data");
    }
  
