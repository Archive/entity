#include <glib.h>
#include <string.h>
#include "xml-tree.h"
#include "xml-node.h"
#include <gqueue.h>
#include "elements.h"
#include "entity.h"
#include "edebug.h"



void
xml_tree_render (XML_Node * node)
{
  XML_Node *topnode;
  XML_Node *curnode;
  XML_Node *parentnode;
  GSList *child;
  GQueue *q;
  GQueue *cq;
  gint count = 0;

  if (node)
    topnode = node;
  else
    topnode = xml_node_root ();

  /* g_print ("topnode path is '%s'\n", path); */

  /* for now we'll just silently ignore this */
  if (!topnode)
    return;

  q = g_queue_create ();
  cq = g_queue_create ();

  g_queue_push_tail (q, NULL);

  /* now that we have the node, we have to keep walking through until we
     render everything. */
  curnode = topnode;
  child = curnode->children;
  g_queue_push_tail (cq, child);
  
  /* Insure top node is rendered.. */
  element_render (curnode);

  while (TRUE)
    {
      while (child)
	{
	  /* save the current parent */
	  g_queue_push_tail (q, curnode);

	  /* save the current child */
	  g_queue_push_tail (cq, child);

	  /* delve into next child */
	  curnode = child->data;

	  edebug ("xml-tree", "Renderinging %s\n", curnode->name->str);

	  element_render (curnode);
	  count++;

	  /* find first child and descend.. if we are to render
	     children. */
	  if (curnode->flags & XML_NODE_NO_RENDER_CHILDREN)
	    child = NULL;
	  else
	    child = curnode->children;
	}

      /* when we end up here, it means we've descended all the way down a fork.
         Now we climb back out and set it to the next child if it's found */
      parentnode = g_queue_pop_tail (q);

      /* if we've returned back beyond the top, we've completed the run. */
      if (parentnode == NULL)
	{
	  g_queue_free (q);
	  g_queue_free (cq);
	  edebug ("xml-tree", "rendered %d nodes", count);
	  return;
	}
      /* g_print ("leaving %s, parenting to %s\n", curnode->name, parentnode->name); */
      element_parent (parentnode, curnode);

      /* find the next child in the parents list */
      child = g_queue_pop_tail (cq);

      if (child)
	child = child->next;

      curnode = parentnode;

    }
}

/* this one has to be done long hand too because it needs to 
   let the renderer destroy it's side of things before the node
   can be freed */

/* topnode is the node to start at, free_nodes is boolean.  Used
   to control the freeing of the nodes vs destroying of their
   visual counterparts. */
void
xml_tree_delete_in (XML_Node * topnode, gint free_nodes)
{
  XML_Node *curnode;
  XML_Node *parentnode;
  GSList *child;
  GQueue *q;
  GQueue *cq;
  GSList *destroy_list = NULL;
  GSList *free_list = NULL;
  GSList *tmp;
  gint count = 0;

  /* for now we'll just silently ignore this */
  if (!topnode)
    return;

  q = g_queue_create ();
  cq = g_queue_create ();

  g_queue_push_tail (q, NULL);

  /* walk through gathering a list of things to delete */
  curnode = topnode;
  child = curnode->children;
  g_queue_push_tail (cq, child);

  while (TRUE)
    {
      while (child)
	{
	  /* save the current parent */
	  g_queue_push_tail (q, curnode);

	  /* save the current child */
	  g_queue_push_tail (cq, child);

	  /* delve into next child */
	  curnode = child->data;

	  destroy_list = g_slist_prepend (destroy_list, curnode);

	  count++;

	  /* find first child and descend */
	  child = curnode->children;
	}

      /* when we end up here, it means we've descended all the way down a fork.
         Now we climb back out and set it to the next child if it's found */
      parentnode = g_queue_pop_tail (q);

      /* if we've returned back beyond the top, we've completed the run. */
      if (parentnode == NULL)
	{
	  g_queue_free (q);
	  g_queue_free (cq);

	  tmp = destroy_list;
	  while (tmp)
	    {
	      curnode = tmp->data;
	      /* g_print ("delete working on %s\n", curnode->name); */
	      xml_node_destroy (curnode);
	      tmp = tmp->next;
	    }

	  g_slist_free (destroy_list);

	  edebug ("xml-tree", "destroyed %d nodes", count);
	  tmp = free_list;

	  if (free_nodes)
	    {
	      while (tmp)
		{
		  curnode = tmp->data;
		  xml_node_free (curnode);
		  tmp = tmp->next;
		}
	    }

	  g_slist_free (free_list);

	  return;
	}

      /* g_print ("leaving %s, parenting to %s\n", curnode->name, parentnode->name); */
      /* save the nodes that need to be deleted for later */
      free_list = g_slist_prepend (free_list, curnode);

      /* find the next child in the parents list */
      child = g_queue_pop_tail (cq);

      if (child)
	{
	  child = child->next;
	}

      curnode = parentnode;

    }
}


void
xml_tree_delete (XML_Node * node, gint free_nodes)
{
  XML_Node *parent;
  parent = node->parent;
  
  if (!node)
    return;

  xml_tree_delete_in (node, free_nodes);
  xml_node_destroy (node);

  if (free_nodes)
    xml_node_free (node);
  
  /* We want to destroy dangling <instance> tags */
  if (parent && g_str_equal (parent->element, "instance"))
    {
      /* Should probly check to make sure they don't have any
	 other children first.. */
      xml_node_free (parent);
    }
}

void
xml_tree_move (XML_Node * src_node, XML_Node * dest_node)
{
  XML_Node *parent_node;

  if (!src_node || !dest_node)
    return;

  xml_tree_delete (src_node, FALSE);

  /* you have to yourself in the parent and make sure the
     reference is removed */
  parent_node = xml_node_parent (src_node);
  parent_node->children = g_slist_remove (parent_node->children, src_node);

  src_node->parent = NULL;

  /* now we link this sucker in to the destination node */
  dest_node->children = g_slist_append (dest_node->children, src_node);

  /* and setup the parent links properly */
  src_node->parent = dest_node;

  /* and finally, do a render tree on it */
  xml_tree_render (dest_node);
}


/* Copy a complete tree */
void
xml_tree_copy (XML_Node * src_node, XML_Node * dest_node_parent)
{
  XML_Node *curnode;
  XML_Node *curdest;
  XML_Node *parentnode;
  GSList *child;
  GQueue *q;
  GQueue *cq;
  GQueue *dest_q;
  GSList *free_list = NULL;
  gint count = 0;

  /* for now we'll just silently ignore this */
  if (!src_node || !dest_node_parent)
    return;

  q = g_queue_create ();
  cq = g_queue_create ();
  dest_q = g_queue_create ();

  g_queue_push_tail (q, NULL);

  /* now that we have the node, we have to keep walking through until we
     render everything. */
  curnode = src_node;
  child = curnode->children;
  g_queue_push_tail (cq, child);

  curdest = dest_node_parent;
  g_queue_push_tail (dest_q, curdest);

  curdest = xml_node_append_copy (curnode, curdest);

  while (TRUE)
    {
      while (child)
	{
	  /* save the current parent */
	  g_queue_push_tail (q, curnode);

	  /* save the current child */
	  g_queue_push_tail (cq, child);

	  /* delve into next child */
	  curnode = child->data;

	  g_queue_push_tail (dest_q, curdest);
	  curdest = xml_node_append_copy (curnode, curdest);

	  count++;

	  /* find first child and descend */
	  child = curnode->children;
	}

      /* when we end up here, it means we've descended all the way down a fork.
         Now we climb back out and set it to the next child if it's found */
      parentnode = g_queue_pop_tail (q);
      curdest = g_queue_pop_tail (dest_q);

      /* if we've returned back beyond the top, we've completed the run. */
      if (parentnode == NULL)
	{
	  edebug ("xml-tree", "copied %d nodes - rendering from %s", count,
		  xml_node_get_path (dest_node_parent));
	  xml_tree_render (dest_node_parent);
	  return;
	}

      /* g_print ("leaving %s, parenting to %s\n", curnode->name, parentnode->name); */
      /* save the nodes that need to be deleted for later */
      free_list = g_slist_prepend (free_list, curnode);

      /* find the next child in the parents list */
      child = g_queue_pop_tail (cq);

      if (child)
	{
	  child = child->next;
	}

      curnode = parentnode;

    }
}


static void
xml_node_foreach_atts_append_str (EBuf *key,
				  EBuf *value, gpointer user_data)
{
  GString *str = user_data;

  /* Dont save atts that start with '_' */
  if (!strncmp (key->str, "_", strlen ("_")))
    return;
  
  /* No saving names with values that start with '_' */
  if ( (!strncmp (key->str, "name", strlen ("name"))) && value &&
       (!strncmp (value->str, "_", strlen ("_"))) )
    return;
    
  g_string_append_c (str, ' ');
  g_string_append (str, key->str);
  g_string_append (str, " = \"");
  g_string_append (str, value->str);
  g_string_append (str, "\"");
}

static gint
xml_node_append_text (XML_Node * curnode, GString * strtree)
{
  GString *data;

  g_string_append_c (strtree, '<');
  g_string_append (strtree, curnode->element);

  if (curnode->atts)
    g_hash_table_foreach (curnode->atts, xml_node_foreach_atts_append_str,
			  strtree);

  data = element_get_data (curnode);
  if (data)
    {
      g_string_append (strtree, ">");
      
      if (xml_node_data_is_cdata (curnode))
	{
	  g_string_append (strtree, "<![CDATA[");
	  g_string_append (strtree, data->str);
	  g_string_append (strtree, "]]>");
	}
      else
	g_string_append (strtree, data->str);
    }
  else
    {
      g_string_append (strtree, "/>");
      return (FALSE);
    }
  return (TRUE);
}


gchar *
xml_tree_get_xml (XML_Node * topnode)
{
  XML_Node *curnode;
  XML_Node *parentnode;
  GSList *child;
  GQueue *q;
  GQueue *cq;
  gint count = 0;
  GString *strtree;
  gint level = 0;
  GString *data;
  
  strtree = g_string_sized_new (2048);

  if (!topnode)
    topnode = xml_node_root ();

  q = g_queue_create ();
  cq = g_queue_create ();

  g_queue_push_tail (q, NULL);

  /* now that we have the node, we have to keep walking through until we
     render everything. */
  curnode = topnode;

  xml_node_append_text (curnode, strtree);

  child = curnode->children;
  g_queue_push_tail (cq, child);


  while (TRUE)
    {
      while (child)
	{
	  /* save the current parent */
	  g_queue_push_tail (q, curnode);

	  /* save the current child */
	  g_queue_push_tail (cq, child);

	  /* delve into next child */
	  curnode = child->data;
	  level++;

	  xml_node_append_text (curnode, strtree);

	  count++;

	  /* find first child and descend */
	  child = curnode->children;
	}

      /* when we end up here, it means we've descended all the way down a fork.
         Now we climb back out and set it to the next child if it's found */
      parentnode = g_queue_pop_tail (q);

      level--;

      /* g_print ("leaving %s, parenting to %s\n", curnode->name, parentnode->name); */
      data = element_get_data (curnode);
      
      if (data) 
	{
	  g_string_append (strtree, "</");
	  g_string_append (strtree, curnode->element);
	  g_string_append (strtree, ">");
	}

      /* if we've returned back beyond the top, we've completed the run. */
      if (parentnode == NULL)
	{
	  g_queue_free (q);
	  g_queue_free (cq);
	  edebug ("xml-tree", "printed %d nodes", count);
	  return (strtree->str);
	}


      /* find the next child in the parents list */
      child = g_queue_pop_tail (cq);


      if (child)
	child = child->next;

      curnode = parentnode;
    }
}
