#include "config.h"
#include <glib.h>
#include <stdlib.h>
#include <stdarg.h>

static GHashTable *config_hash = NULL;

void
econfig_init (void)
{
  if (config_hash == NULL)
    config_hash = g_hash_table_new (g_str_hash, g_str_equal);
}

void
econfig_set_attr (gchar * attr, gchar * value)
{
  g_hash_table_insert (config_hash, attr, value);
}

gint
econfig_is_set (gchar * attr)
{
  gchar *value;

  value = g_hash_table_lookup (config_hash, attr);
  if (value)
    return (TRUE);
  else
    return (FALSE);
}

gchar *
econfig_get_attr (gchar * attr)
{
  gchar *value;

  value = g_hash_table_lookup (config_hash, attr);
  return (value);
}

