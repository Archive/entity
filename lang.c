#include <glib.h>
#include <string.h>
#include <stdlib.h>

#include "entity.h"
#include "perl-embed.h"
#include "entity.h"

static GHashTable *languages = NULL;
static gint unique_namespace_id = 0;

#define LANG_SEPERATOR ":"
#define LANG_DEFAULT   "perl"


/* Populate both data and intdata so that the languges that need
   strings don't have to. */
GSList *
lang_push_int (GSList * arg_list, gint arg)
{
  gchar *string;
  LangArg *argst;

  argst = g_new0 (LangArg, 1);
  string = g_malloc (20);

  g_snprintf (string, 20, "%d", arg);

  argst->type = LANG_INT;
  argst->data = string;
  argst->size = strlen (string);
  argst->intdata = arg;

  arg_list = g_slist_append (arg_list, argst);

  return (arg_list);
}

GSList *
lang_push_str (GSList * arg_list, gchar * arg)
{
  gchar *string;
  LangArg *argst;

  argst = g_new0 (LangArg, 1);
  if (!arg)
    arg = "";

  string = g_strdup (arg);

  argst->type = LANG_STRING;
  argst->data = string;
  argst->size = strlen (string);
  /* probably souldn't need this, but it can't hurt. MW */
  argst->intdata = atoi (string);

  arg_list = g_slist_append (arg_list, argst);

  return (arg_list);
}


/* Added binary data to the args list. */
GSList *
lang_push_data (GSList * arg_list, gchar * arg, gint size)
{
  gchar *string;
  LangArg *argst;

  argst = g_new0 (LangArg, 1);

  if (size > 0)
    {
      string = g_memdup (arg, size);
      argst->type = LANG_BINSTRING;
      argst->data = string;
      argst->size = size;
    }
  else
    {
      argst->type = LANG_BINSTRING;
      argst->data = NULL;
      argst->size = 0;
    }
  
  arg_list = g_slist_append (arg_list, argst);

  return (arg_list);
}

void
lang_arg_free (LangArg * arg)
{
  if (!arg)
    return;

  if (arg->type != LANG_NODE)
    g_free (arg->data);

  g_free (arg);
}

/* Add a XML_Node* to the args list*/
GSList *
lang_push_node (GSList * arg_list, XML_Node * node)
{
  LangArg *argst;
  argst = g_new0 (LangArg, 1);

  argst->type = LANG_NODE;
  argst->data = node;

  arg_list = g_slist_append (arg_list, argst);

  return (arg_list);
}

EBuf*
lang_call_function (XML_Node * calling_node, gchar * function,
		    gchar * fmt, ...)
{
  va_list ap;
  gchar *cur;			/* Current format. */

  GSList *arg_list = NULL;

  /* Types of data. */
  /* Binary string uses both string and inter. */
  char *string;
  int inter;
  EBuf* ebuffer;
  XML_Node *node = NULL;

  /* float flt; */
  /* double dbl; */

  if (!function)
    return NULL;			/* return peacefully. */

  va_start (ap, fmt);

  for (cur = fmt; *cur != '\0'; cur++)
    {
      edebug ("lang", "*cur = %c", *cur);

      if (*cur == 'n')		/* XML_Node * */
	{
	  node = va_arg (ap, XML_Node *);
	  arg_list = lang_push_node (arg_list, node);
	}
      else if (*cur == 'e')	/* EBuf* */
	{
	  ebuffer = va_arg (ap, EBuf *);
	  arg_list = lang_push_data (arg_list, ebuffer->str, ebuffer->len);
	}
      else if (*cur == 's')	/* string */
	{
	  string = va_arg (ap, char *);
	  arg_list = lang_push_str (arg_list, string);
	}
      else if (*cur == 'i')	/* int */
	{
	  inter = va_arg (ap, int);
	  arg_list = lang_push_int (arg_list, inter);
	}
      else if (*cur == 'b')	/* binary string */
	{
	  string = va_arg (ap, char *);
	  inter = va_arg (ap, int);
	  arg_list = lang_push_data (arg_list, string, inter);
	}
      /*... */
    }
  va_end (ap);

  return lang_call_function_with_list (calling_node, function, arg_list);
}

/* register a language dispatch function..  language_name is the
   prefix of the language (eg "perl" for perl:some_func) and then
   the dispatch or eval function */
void				/*Freed MW */
lang_register (gchar * language_name, LangDispatchFunc func)
{
  if (!languages)
    languages = g_hash_table_new (g_str_hash, g_str_equal);

  edebug ("lang", "registered language '%s'", language_name);

  g_hash_table_insert (languages, language_name, func);
}

/* utility function to make it easier to deal with namespaces. */
gchar *				/*Freed MW */
lang_namespace_get (gchar * language, XML_Node * calling_node)
{
  XML_Node *objnode;
  EBuf *namespace;
  EBuf *namespace_attr;
  
  namespace_attr = ebuf_new_sized (200);
  
  ebuf_append_cstring (namespace_attr, "__");
  ebuf_append_cstring (namespace_attr, language);
  ebuf_append_cstring (namespace_attr, "-namespace");
  
  edebug ("lang", "Checking object node for %s", calling_node->name->str);

  /* Don't do a find_parent if we're at the object node */
  if (g_str_equal (calling_node->element, "object"))
    objnode = calling_node;
  else
    objnode = xml_node_find_parent (calling_node, "object");
  
  namespace = xml_node_get_attr (objnode, namespace_attr->str);

  /* if one doesn't already exist, we have to create one */
  if (!namespace)
    {
      namespace = ebuf_new_sized (20);
      ebuf_sprintf (namespace, "namespace%d", unique_namespace_id++);
      
      xml_node_set_attr_quiet (objnode,
			       namespace_attr->str, namespace);
      
      edebug ("lang", "Created new %s namespace - %s", language, namespace->str);
    }

  ebuf_free (namespace_attr);
  edebug ("lang", "returning namespace '%s'", namespace->str);
  return (namespace->str);
}

/* This now looks up the language needed and dispatches the appropriate
   function to deal with it. */

EBuf*				/*Freed MW */
lang_call_function_with_list (XML_Node * calling_node,
			      gchar * function_name, GSList * arg_list)
{
  EBuf* retbuf = NULL;          /* Send to the callback from the handler. */
  gchar *language_type;
  gchar *tmp;
  gint index;
  gint lang_is_specified = FALSE;
  LangDispatchFunc dispatch_func;

  language_type = g_strdup (function_name);	/* Freed MW */

  /* search for a : */
  if ((tmp = strstr (language_type, ":")))
    {
      /* make sure it's not a double :: as in some namespace thingies */
      index = tmp - language_type;

      if (language_type[index + 1] != '\0' && language_type[index + 1] != ':')
	{
	  /* we should now be certain that it's a single : and 
	     denotes a language type */
	  language_type[index] = '\0';
	  lang_is_specified = TRUE;

	  function_name = &language_type[index + 1];
	}
    }

  if (!lang_is_specified)
    {
      XML_Node *objnode;
      gchar *tmp;

      /* First we check for a default setting in parent <object> */
      objnode = xml_node_find_parent (calling_node, "object");
      tmp = xml_node_get_attr_str (objnode, "default-lang");

      g_free (language_type);	/* going to be reset. */

      if (tmp)
	language_type = g_strdup (tmp);
      else
	language_type = g_strdup (LANG_DEFAULT);
    }

  edebug ("lang", "language type '%s', function '%s'\n",
	  language_type, function_name);

  /* grab the lang dispatch function */
  dispatch_func = g_hash_table_lookup (languages, language_type);

  if (dispatch_func)
    {
      retbuf = dispatch_func (calling_node, function_name, arg_list);
      g_slist_free (arg_list);	/* Free List Here. */
    }
  else
    {
      g_warning ("Failed to find dispatch function for language '%s'",
		 language_type);
    }
  g_free (language_type);

  return retbuf;
}
