#include <string.h>
#include <ctype.h>

#include <glib.h>

#include "eutils.h"
#include "ebuffer.h"


#define EBUF_STRUCT_ALLOC 1000
#define EBUF_CHUNK_SMALL_ALLOC 1000
#define EBUF_CHUNK_MEDIUM_ALLOC 100

#define EBUF_CHUNK_SMALL_SIZE 30
#define EBUF_CHUNK_MEDIUM_SIZE 256



static EMemChunk *ebuf_struct_chunk_admin = NULL;
static EMemChunk *ebuf_small_chunk_admin = NULL;
static EMemChunk *ebuf_medium_chunk_admin = NULL;

static gchar *
ebuf_small_chunk_alloc (void)
{
  gchar *chunk;
  
  if (!ebuf_small_chunk_admin)
    ebuf_small_chunk_admin = eutils_memchunk_admin_new (EBUF_CHUNK_SMALL_SIZE, 
							EBUF_CHUNK_SMALL_ALLOC);
  chunk = eutils_memchunk_alloc (ebuf_small_chunk_admin);
  
  return (chunk);
}    

static void
ebuf_small_chunk_free (gchar *chunk)
{
  eutils_memchunk_free (ebuf_small_chunk_admin, chunk);}

static gchar *
ebuf_medium_chunk_alloc (void)
{
  gchar *chunk;
  
  if (!ebuf_medium_chunk_admin)
    ebuf_medium_chunk_admin = eutils_memchunk_admin_new (EBUF_CHUNK_MEDIUM_SIZE, 
							 EBUF_CHUNK_MEDIUM_ALLOC);
  chunk = eutils_memchunk_alloc (ebuf_medium_chunk_admin);
  
  return (chunk);
}    

static void
ebuf_medium_chunk_free (gchar *chunk)
{
  eutils_memchunk_free (ebuf_medium_chunk_admin, chunk);
}

/* Free chunk associated with buf */
static void
ebuf_chunk_free (EBuf *buf)
{
  g_return_if_fail (buf != NULL);

  switch (buf->type)
    {
    case EBUF_CHUNK_SMALL:
      ebuf_small_chunk_free (buf->str); 
      break;
    
    case EBUF_CHUNK_MEDIUM:
      ebuf_medium_chunk_free (buf->str);
      break;

    case EBUF_CHUNK_LARGE:
      g_free (buf->str);
      break;

    default:
    }
}

static gint
nearest_power (gint num)
{
  gint n = 1;

  while (n < num)
    n <<= 1;

  return n;
}

static void
ebuf_maybe_expand (EBuf* buf, gint size)
{
  gint total;
  gchar *tmpbuf;
  
  total = buf->len + size;
  
  if (total >= EBUF_CHUNK_MEDIUM_SIZE - 2)
    {
      if (total >= buf->alloc)
	{
	  gint new_size;
	  
	  /* Special case to allow resizing via realloc on large chunks */
	  if (buf->type == EBUF_CHUNK_LARGE)
	    {
	      new_size = nearest_power (buf->len + size + 1);
	      buf->str = g_realloc (buf->str, new_size);
	      buf->alloc = new_size;
	    }
	  else
	    {
	      new_size = nearest_power (buf->len + size + 1);
	      tmpbuf = g_malloc (new_size);
	      memcpy (tmpbuf, buf->str, buf->len);
	      ebuf_chunk_free (buf);
	      
	      buf->str = tmpbuf;
	      buf->type = EBUF_CHUNK_LARGE;
	      buf->alloc = new_size;
	    }
	}
    }
  else if (total >= EBUF_CHUNK_SMALL_SIZE - 2)
    {
      /* Make sure we're not just growing in the same pool, or one larger */
      if ( (buf->type != EBUF_CHUNK_MEDIUM) && (buf->type != EBUF_CHUNK_LARGE) )
	{
	  tmpbuf = ebuf_medium_chunk_alloc ();
	  memcpy (tmpbuf, buf->str, buf->len);
	  ebuf_chunk_free (buf);
	  
	  buf->str = tmpbuf;
	  buf->type = EBUF_CHUNK_MEDIUM;
	  buf->alloc = EBUF_CHUNK_MEDIUM_SIZE;
	}
    }
  else
    {
      /* Make sure we're not just growing in the same pool, or one larger */
      if (buf->type == EBUF_CHUNK_NONE)
	{
	  tmpbuf = ebuf_small_chunk_alloc ();
	  memcpy (tmpbuf, buf->str, buf->len);
	  ebuf_chunk_free (buf);
	  
	  buf->str = tmpbuf;
	  buf->type = EBUF_CHUNK_SMALL;
	  buf->alloc = EBUF_CHUNK_SMALL_SIZE;
	}
    }
}


/* Allocate a new SMALL EBuf */
EBuf *
ebuf_new (void)
{
  EBuf *newbuf;
  
  if (!ebuf_struct_chunk_admin)
    ebuf_struct_chunk_admin = eutils_memchunk_admin_new (sizeof (EBuf), EBUF_STRUCT_ALLOC);
  
  newbuf = eutils_memchunk_alloc (ebuf_struct_chunk_admin);
  newbuf->type = EBUF_CHUNK_NONE;
  newbuf->len = 0;
  
  ebuf_maybe_expand (newbuf, 2);
  
  return (newbuf);
}

EBuf *
ebuf_new_sized (guint size)
{
  EBuf *newbuf;
  
  if (!ebuf_struct_chunk_admin)
    ebuf_struct_chunk_admin = eutils_memchunk_admin_new (sizeof (EBuf), EBUF_STRUCT_ALLOC);
  
  newbuf = eutils_memchunk_alloc (ebuf_struct_chunk_admin);
  newbuf->type = EBUF_CHUNK_NONE;
  newbuf->len = 0;
  
  ebuf_maybe_expand (newbuf, size);
  
  return (newbuf);
}

EBuf *
ebuf_new_with_int (gint val)
{
  EBuf *newbuf;
  
  if (!ebuf_struct_chunk_admin)
    ebuf_struct_chunk_admin = eutils_memchunk_admin_new (sizeof (EBuf), EBUF_STRUCT_ALLOC);
  
  newbuf = eutils_memchunk_alloc (ebuf_struct_chunk_admin);
  newbuf->type = EBUF_CHUNK_NONE;
  newbuf->len = 0;
  
  ebuf_sprintf (newbuf, "%d", val);
  
  return (newbuf);
}

EBuf *
ebuf_new_with_cstring (gchar *str)
{
  EBuf *newbuf;
  
  if (!ebuf_struct_chunk_admin)
    ebuf_struct_chunk_admin = eutils_memchunk_admin_new (sizeof (EBuf), EBUF_STRUCT_ALLOC);
  
  newbuf = eutils_memchunk_alloc (ebuf_struct_chunk_admin);
  newbuf->type = EBUF_CHUNK_NONE;
  newbuf->len = 0;
  
  ebuf_append_cstring (newbuf, str);
  
  return (newbuf);
}

EBuf *
ebuf_new_with_data (gchar *str, gint len)
{
  EBuf *newbuf;
  
  if (!ebuf_struct_chunk_admin)
    ebuf_struct_chunk_admin = eutils_memchunk_admin_new (sizeof (EBuf), EBUF_STRUCT_ALLOC);
  
  newbuf = eutils_memchunk_alloc (ebuf_struct_chunk_admin);
  newbuf->type = EBUF_CHUNK_NONE;
  newbuf->len = 0;
  
  ebuf_append_data (newbuf, str, len);
  
  return (newbuf);
}

EBuf *
ebuf_new_with_ebuf (EBuf *buf)
{
  EBuf *newbuf;
  
  if (!ebuf_struct_chunk_admin)
    ebuf_struct_chunk_admin = eutils_memchunk_admin_new (sizeof (EBuf), EBUF_STRUCT_ALLOC);
  
  newbuf = eutils_memchunk_alloc (ebuf_struct_chunk_admin);
  newbuf->type = EBUF_CHUNK_NONE;
  newbuf->len = 0;
  
  ebuf_append_ebuf (newbuf, buf);
  
  return (newbuf);
}


void
ebuf_free (EBuf *buf)
{
  g_return_if_fail (buf != NULL);
  
  ebuf_chunk_free (buf);
  eutils_memchunk_free (ebuf_struct_chunk_admin, buf);
}

void
ebuf_set_to_cstring (EBuf *buf, gchar *str)
{
  gint len;
  gint total;
  
  g_return_if_fail (buf != NULL);
  g_return_if_fail (str != NULL);
 
  len = strlen (str);
  
  total = len - buf->len;
  ebuf_maybe_expand (buf, total);
  
  strcpy (buf->str, str);
  buf->len = len;
}

void
ebuf_set_to_ebuf (EBuf *buf, EBuf *val)
{
  gint total;
  
  g_return_if_fail (buf != NULL);
  g_return_if_fail (val != NULL);
 
  total = val->len - buf->len;
  ebuf_maybe_expand (buf, total);
  
  memcpy (buf->str, val->str, val->len);
  buf->len = val->len;
  /* This should just be buf->len nor buf->len +1. MW */
  buf->str[buf->len] = '\0';
}


void
ebuf_append_cstring (EBuf *buf, gchar *str)
{
  gint len;
  
  g_return_if_fail (buf != NULL);
  g_return_if_fail (str != NULL);
 
  len = strlen (str);
  ebuf_maybe_expand (buf, len);
  
  strcpy (buf->str + buf->len, str);
  buf->len += len;
  buf->str[buf->len + 1] = '\0';
}

void
ebuf_prepend_cstring (EBuf *buf, gchar *str)
{
  gint len;
  
  g_return_if_fail (buf != NULL);
  g_return_if_fail (str != NULL);
 
  len = strlen (str);
  ebuf_maybe_expand (buf, len);

  g_memmove (buf->str + len, buf->str, buf->len);
  strncpy (buf->str, str, len);
  buf->len += len;
  /* insurance */
  buf->str[buf->len + 1] = '\0';
}

void
ebuf_prepend_char (EBuf *buf, gchar c)
{
  g_return_if_fail (buf != NULL);
 
  ebuf_maybe_expand (buf, 1);
  
  g_memmove (buf->str + 1, buf->str, buf->len);
  buf->str[0] = c;
  buf->len += 1;
  /* insurance */
  buf->str[buf->len + 1] = '\0';
}

void
ebuf_append_data (EBuf *buf, gchar *str, gint len)
{
  g_return_if_fail (buf != NULL);
  g_return_if_fail (str != NULL);
  
  ebuf_maybe_expand (buf, len);
  
  memcpy (&buf->str[buf->len], str, len);
  buf->len += len;
  
  /* Still need insurance I guess.. */
  buf->str[buf->len + 1] = '\0';
}

void
ebuf_append_ebuf (EBuf *buf, EBuf *newbuf)
{
  g_return_if_fail (buf != NULL);
  g_return_if_fail (newbuf != NULL);
  
  ebuf_maybe_expand (buf, newbuf->len);
  
  memcpy (&buf->str[buf->len], newbuf->str, newbuf->len);
  buf->len += newbuf->len;
  
  /* Still need insurance I guess.. */
  buf->str[buf->len + 1] = '\0';
}

void
ebuf_insert_cstring (EBuf *buf,
		     gint pos,
		     gchar *val)
{
  gint len;

  g_return_if_fail (buf != NULL);
  g_return_if_fail (val != NULL);
  g_return_if_fail (pos >= 0);
  g_return_if_fail (pos <= buf->len);

  len = strlen (val);
  ebuf_maybe_expand (buf, len);

  g_memmove (buf->str + pos + len, buf->str + pos, buf->len - pos);
  strncpy (buf->str + pos, val, len);
  buf->len += len;
  buf->str[buf->len] = 0;
}

void
ebuf_insert_data (EBuf *buf,
		  gint pos,
		  gchar *val,
		  gint len)
{
  g_return_if_fail (buf != NULL);
  g_return_if_fail (val != NULL);
  g_return_if_fail (pos >= 0);
  g_return_if_fail (pos <= buf->len);

  ebuf_maybe_expand (buf, len);

  g_memmove (buf->str + pos + len, buf->str + pos, buf->len - pos);
  memcpy (buf->str + pos, val, len);
  buf->len += len;
  buf->str[buf->len + 1] = 0;
}

void
ebuf_erase (EBuf *buf,
	    gint pos,
	    gint len)
{
  g_return_if_fail (buf != NULL);
  g_return_if_fail (len >= 0);
  g_return_if_fail (pos >= 0);
  g_return_if_fail (pos <= buf->len);
  g_return_if_fail (pos + len <= buf->len);

  if (pos + len < buf->len)
    g_memmove (buf->str + pos, buf->str + pos + len, buf->len - (pos + len));
  buf->len -= len;
  buf->str[buf->len] = 0;
}

void
ebuf_truncate (EBuf *buf, gint len)
{
  g_return_if_fail (buf != NULL);

  buf->len = len;
  buf->str[len] = 0;
}

/* Lifted from GString.c */
static void
ebuf_sprintfa_int (EBuf *buf,
		   gchar *fmt,
		   va_list args)
{
  gchar *tmp;

  tmp = g_strdup_vprintf (fmt, args);
  ebuf_append_cstring (buf, tmp);
  g_free (tmp);
}

void
ebuf_sprintf (EBuf *buf,
	      gchar *fmt,
	      ...)
{
  va_list args;

  va_start (args, fmt);
  ebuf_sprintfa_int (buf, fmt, args);
  va_end (args);
}


void
ebuf_stats (void)
{
  g_print ("For EBuf structs:\n");
  eutils_memchunk_stats (ebuf_struct_chunk_admin);
  g_print ("For small buffers:\n");
  eutils_memchunk_stats (ebuf_small_chunk_admin);
  g_print ("For medium buffers:\n");
  eutils_memchunk_stats (ebuf_medium_chunk_admin);
}

void
ebuf_down (EBuf *buf)
{
  guchar *c;

  g_return_if_fail (buf != NULL);

  c = buf->str;
  while (*c)
    {
      *c = tolower (*c);
      c++;
    }
}

void
ebuf_up (EBuf *buf)
{
  guchar *c;

  g_return_if_fail (buf != NULL);

  c = buf->str;
  while (*c)
    {
      *c = toupper (*c);
      c++;
    }
}


gint
ebuf_equal_ebuf (EBuf *buf1, EBuf *buf2)
{
  return strcmp ((const gchar*) buf1->str, (const gchar*) buf2->str) == 0;
}

gint
ebuf_equal_str (EBuf *buf1, gchar *str)
{
  return strcmp (buf1->str, str) == 0;
}

gint
ebuf_equal_strcase (EBuf *buf1, gchar *str)
{
  return g_strcasecmp (buf1->str, str) == 0;
}

gint
ebuf_equal_ebufcase (EBuf *buf1, EBuf *buf2)
{
  return g_strcasecmp (buf1->str, buf2->str) == 0;
}

guint
ebuf_hash (EBuf *buf)
{
  if (!buf || !buf->str)
    return (0);
  return (g_str_hash (buf->str));
}


