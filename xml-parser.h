#ifndef __XML_PARSER_H__
#define __XML_PARSER_H__

/* Returns top node of new tree, or NULL on failure */
XML_Node *
xml_parse_file (XML_Node *top_node, gchar * filename);

/* Returns top node of new tree, or NULL on failure */
XML_Node *
xml_parse_string (XML_Node * top_node, gchar * xml);

gchar *
xml_parser_get_current_filename (void);


#endif /* __XML_PARSER_H__ */
