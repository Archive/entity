#include <glib.h>
#include <unistd.h>
#include "eutils.h"
#include "edebug.h"

/* Should be able to do this with user_data, but I think 
   I had problems when I tried it. */
static GSList *key_list = NULL;

static void
eutils_foreach_attr_append_to_list (gpointer key,
				    gpointer value, gpointer user_data)
{
  key_list = g_slist_prepend (key_list, key);
}

/* list of currently set keys, appended to list, or NULL
   to start new list. */
GSList *
eutils_key_list (GHashTable * table, GSList * list)
{
  key_list = list;

  if (!table)
    return (list);

  g_hash_table_foreach (table, eutils_foreach_attr_append_to_list, NULL);

  return (key_list);
}

gint
eutils_file_exists (gchar *filename)
{
  g_return_val_if_fail (filename != NULL, FALSE);
  
  return (access (filename, F_OK) == 0);
}

gchar *
eutils_file_search (gchar *filename)
{
  gchar *path;
  
  edebug ("eutils", "file search, checking '%s'", filename);
  /* First check current working dir/absolute path */
  if (eutils_file_exists (filename))
    return (g_strdup (filename));
  
  /* Now home dir
     TODO: This should be grabbed from econfig in the future */
  path = g_strconcat (g_get_home_dir (), "/entity/system/elib/", filename, NULL); 
  edebug ("eutils", "file search, checking '%s'", path);
  if (eutils_file_exists (path))
    return (path);
  g_free (path);
  
  /* System dir */
  path = g_strconcat (DATADIR, "/elib/", filename, NULL);
  edebug ("eutils", "file search, checking '%s'", path);
  if (eutils_file_exists (path))
    return (path);
  g_free (path);

  return (NULL);
}

/* Create a new memchunk struct */
EMemChunk *
eutils_memchunk_admin_new (guint chunk_size, guint alloc_num)
{
  EMemChunk *chunk;
  
  chunk = g_new0 (EMemChunk, 1);
  
  chunk->size = chunk_size;
  chunk->alloc_num = alloc_num;

  return (chunk);
}

/* Allocate a new chunk */
void *
eutils_memchunk_alloc (EMemChunk *chunk)
{
  void *buf;
  void *newbuf;
  gint i;
  GSList *tmp;

  /* Make sure we have free chunks around */
  if (!chunk->free_list) 
    {
      buf = g_malloc0 (chunk->size * chunk->alloc_num);
      for (i = 0; i < chunk->alloc_num; i++) {
	chunk->free_list = g_slist_prepend (chunk->free_list, buf + (chunk->size * i));
      }
#ifdef EMEMCHUNK_PROFILE
      chunk->number_of_allocated_chunks += chunk->alloc_num;
#endif
    }

  /* Grab top one of list */
  newbuf = chunk->free_list->data;
  
  /* Clean up list */
  tmp = chunk->free_list;
  chunk->free_list = chunk->free_list->next;
  tmp->next = NULL;
  g_slist_free (tmp);
  
#ifdef EMEMCHUNK_PROFILE
  chunk->number_of_used_chunks++;
#endif
  
  return (newbuf);
}

/* Free a chunk */
void
eutils_memchunk_free (EMemChunk *chunk, void *buf)
{
  memset (buf, 0, chunk->size);
  
  /* Give to the pool for reuse */
  chunk->free_list = g_slist_prepend (chunk->free_list, buf);

#ifdef EMEMCHUNK_PROFILE
  chunk->number_of_freed_chunks++;
#endif
  
}

void
eutils_memchunk_stats (EMemChunk *chunk)
{
  if (!chunk)
    {
      g_print ("  Not initialized\n");
      return;
    }
#ifdef EMEMCHUNK_PROFILE
  g_print ("  Number of freed chunks: %d\n", chunk->number_of_freed_chunks);
  g_print ("  Number of used chunks: %d\n", chunk->number_of_used_chunks);
  g_print ("  Number of allocated chunks: %d\n", chunk->number_of_allocated_chunks);
  g_print ("  Total memory allocated: %d bytes\n", chunk->number_of_allocated_chunks * chunk->size);
#endif
}



