#!/usr/bin/perl

%replacements = (
		 
		 "%fixed%" => 
		 "This attribute only has an effect during the rendering of this element,\n".
		 "and changes will not be represented after it is rendered.",
		 
		 "%widget%" => 
		 "In addition to those below, this element accepts all appropriate widget attributes."
		 
		 );

sub get_version
{
    open (VER, "../configure.in");
    my @lines = <VER>;
    
    foreach $line (@lines) {
	if ($line =~ /AM_INIT_AUTOMAKE/) {
	    my ($ver) = $line =~ /\(.*, (.*?)\)/;
	    return ($ver);
	}
    }
}

sub save_docs
{
    my ($docs, $filename) = @_;
    
    my @formats = keys (%$docs);
    
    foreach $format (@formats) {
	print ("Writing $filename.$format\n");
	open (OUT, ">$filename.$format");
	print OUT $docs->{$format};
	close (OUT);
    }
}
	

sub gen_perl_api_docs
{
    my @files = @_;
    my @lines;
    my $isdoc;
    my $ver = get_version;
    my %docs;

    $docs{'html'} = "<H1>Perl API Reference (for Entity version $ver)</H1>\n";
    $docs{'text'} = "Perl API Reference (for Entity version $ver)\n";
    
    foreach $file (@files) {
	
	open (PERL, $file) or die "Unable to open $file";
	@lines = <PERL>;

	undef ($isdoc);

	foreach $line (@lines) {
	    $isdoc = 1 if ($line =~ /\/\*--/);
	    undef ($isdoc) if ($line =~ /\*\//);
	    
	    if (defined ($isdoc)) {
		
		# Get rid of perl comments
		$line =~ s/^\#//;
		
		$text = $line;
		$html = $line;
		
		# Fix up entities
		$html =~ s/>/&gt;/;
		$html =~ s/</&lt;/;
		
		# Make a nice seperator
		$html =~ s/\/\*--/\<HR\>/;
		
		# Remove any spaces at beginning.
		$html =~ s/^ +//;
		
		# ret to BR.
		$html =~ s/\n/\<BR\>\n/;
		$docs{'html'} .= $html;
		
		# Remove leading spaces.
		$text =~ s/^ +//;
		
		# Make a nice seperator
		$text =~ s/\/\*--/----------------------------------------------------------------/;
		$docs{'text'} .= $text;
	    }
	}
    }
    return (%docs);
}


sub gen_tag_reference
{
    my @spots = @_;
    my @files;
    my @lines;
    my $isdoc;
    my $ver = get_version;
    my %docs;
    my @tmpfiles;

    foreach $spot (@spots) {
	open (LIST, "/bin/ls -1 $spot|");
	@tmpfiles = <LIST>;
	chomp @tmpfiles;
	push @files, @tmpfiles;
    }

    $docs{'html'} = "<H1>XML Tag Reference (for Entity version $ver)</H1>\n";
    $docs{'text'} = "XML Tag Reference (for Entity version $ver)\n";
    
    my $is_description;

    foreach $file (@files) {
	open (PERL, $file) or die "Unable to open $file";
	@lines = <PERL>;

	undef ($isdoc);
	
	foreach $line (@lines) {
	    $isdoc = 1 if ($line =~ /\/\*--/);
	    undef ($isdoc) if ($line =~ /\*\//);
	    
	    if (defined ($isdoc)) {
		my @reps = keys (replacements);
		foreach $rep (@reps) {
		    $line =~ s/$rep/$replacements{$rep}/;
		}

		$text = $line;
		$html = $line;

		# Fix up entities
		$html =~ s/>/&gt;/g;
		$html =~ s/</&lt;/g;
		
		$html =~ s/\/\*--element/\<HR\>/;
		$html =~ s/\/\*--attr/\<BR\>/;
		
		# Remove any spaces at beginning.
		$html =~ s/^ +//;
		
		# ret to BR.
		$html =~ s/\n/\<BR\>\n/;
		$docs{'html'} .= $html;

		#### text generation

		# Remove leading spaces.
		$text =~ s/^ +//;
		
		$text =~ s/\/\*--element/-------------------------------------------------------------/;
		$text =~ s/\/\*--attr/--\n/;
		$docs{'text'} .= $text;
	    }
	}
    }
    return (%docs);
}

my %docs = gen_perl_api_docs ("../perl-embed.c", "../Entity.pl");
save_docs (\%docs, "perl-api");

my %docs = gen_tag_reference ("../gtk-renderers/*rend*.c", 
			      "../gtk-renderers/gtk-common.c");
save_docs (\%docs, "gtk-tag-reference");




