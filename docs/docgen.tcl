#!/bin/sh
# FILE: "/home/joze/pub/gnome/entity-0.3.7/docs/docgen.tcl"
# LAST MODIFICATION: "Sun Oct 10 15:20:38 1999 (joze)"
# (C) 1999 by Johannes Zellner, <johannes@zellner.org>
# $Id$
# vim:set ft=tcl: \
exec tclsh "$0" "$@"

set files [exec find .. -name {*.[ch]}]

proc filter file {
 #* <a name=set_attr>Entity::set_attr</a> path attribute value
	global data name_attr
	set in [open $file r]
	if {0 == $in} {error [list unable to open $file]}
	set target ""
	while {-1 != [gets $in line]} {
		if [regexp {\*/} $line] {
			set target ""
			continue
		}
		if {"" != $target} {
			regsub "^\[ \t\]\*\\\*" $line "" line
			if [regexp -nocase {<a *name= *"?([^"> ]*)"?[ >]} $line all name] {
				# "
				lappend name_attr($target) [string trim $name]
			}
			lappend data($target) $line
		}
		if [regexp {/\*:(.*)} $line all target] {
			set target [string trim $target]
		}
	}
	close $in
}

proc html {file data {name_attr ""}} {
	regsub {.html} file "" title
	set description $title
	set out [open $file w]
	if {0 == $out} {error [list unable to open $file for writing]}
	puts $out [subst {<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html lang="en">
<head>
    <title>$title</title>
    <link rev=Made href="mailto:entity@netidea.com">
    <link rel=Start href="./">
    <meta name="description" content="$description">
    <link rel=StyleSheet href="entity.css" type="text/css">
</head>

<body lang=de>
	}]
	if {"" != $name_attr} {
		set new ""
		foreach name $name_attr {
			append new " <a href=#$name>$name</a> |" 
		}
		regsub {\|$} $new "" new
		puts $out [concat [string trim $new]<p>]
	}
	puts $out [join $data \n]
	puts $out {
<address>
  <hr noshade size=1>
  <a href="mailto:entity@netidea.com">entity@netidea.com</a>
</address>
</body> 
</html> 
	}
	close $out
}

foreach file $files {
	filter $file
}

foreach name [array names data] {
	if [info exists name_attr($name)] {
		html $name $data($name) $name_attr($name)
	} else {
		html $name $data($name)
	}
}
