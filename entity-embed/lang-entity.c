/* I had a code-dream last night that lead me to believe i should 
 * add the entity lang renderers and language bindings.  I know 
 * its funny but i have code-dreams all the time.  Funny thing 
 * is when they feel right the code i create is always good.  MW
 */

#include <glib.h>
#include <errno.h>
#include "edebug.h"
#include "xml-node.h"
#include "xml-tree.h"
#include "erend.h"
#include <stdio.h>
#include <stdlib.h>
#include <elements.h>
#include <xml-node.h>
#include <xml-tree.h>
#include <xml-parser.h>
#include <lang.h>

/* Rational:
 * What am i thinking?  I'm thinking that we will be able to create
 * some helper function that people can call when they want something
 * simple done.  So mostly entity:<func> with be helper functions.
 */

static GHashTable* entity_func_ht;

typedef gint (*EntityFunc) (XML_Node* calling_node, 
                                gchar *function, GSList *args);
static gint
lang_entity_delete
        (XML_Node* calling_node, gchar *function, GSList *args)
{
  LangArg* arg_node;
  XML_Node* node;
  arg_node = (LangArg*)args->data;

  if (LANG_NODE == arg_node->type)
    {
      node = (XML_Node*) arg_node->data;
      xml_tree_delete(node, TRUE);
    }

  return TRUE;
}


static gint 
lang_entity_hide
        (XML_Node* calling_node, gchar *function, GSList *args)
{
  LangArg* arg_node;
  XML_Node* node;
  arg_node = (LangArg*)args->data;

  if (LANG_NODE == arg_node->type)
    {
      node = (XML_Node*) arg_node->data;
      xml_node_set_attr_str (node, "visible", "false");
    }

  return TRUE;
}

static gint
lang_entity_hide_window
        (XML_Node* calling_node, gchar *function, GSList *args)
{
  LangArg* arg_node;
  XML_Node* node;
  arg_node = (LangArg*)args->data;
  
  if (LANG_NODE == arg_node->type)
    { 
      node = (XML_Node*) arg_node->data;
      node = xml_node_find_parent (node, "window");

      if (!node)
        return TRUE;

      xml_node_set_attr_str (node, "visible", "false");
    } 

  return TRUE;
}

static gint
lang_entity_exit
        (XML_Node* calling_node, gchar *function, GSList *args)
{
  LangArg* arg_node;
  XML_Node* node;
  arg_node = (LangArg*)args->data;

  if (LANG_NODE == arg_node->type)
    {
      node = (XML_Node*) arg_node->data;
      node = xml_node_find_parent(node, "object");

      if (!node)
        return TRUE;

      xml_tree_delete (node, TRUE);
    }
  return TRUE;
}

static gint
lang_entity_show_args
        (XML_Node* calling_node, gchar *function, GSList *args)
{
  LangArg* arg;
  GSList* tmp;
  XML_Node* node;
  gchar* path;
  gint arg_int; 
  gchar* arg_string;

  for(tmp=args; tmp; tmp = tmp->next)
  {
    arg = (LangArg*) tmp->data;
    if (LANG_NODE == arg->type)
    {
      node = (XML_Node*) arg->data;
      path = xml_node_get_path(node);
      fprintf(stderr, "entity:%s node = %s\n", function, path);
      g_free(path);
    }
    else if (LANG_INT == arg->type)
    {
      arg_int = (gint) arg->data;
      fprintf(stderr, "entity:%s int = %i\n", function, arg_int);
    }
    else if (LANG_STRING == arg->type)
    {
      arg_string = arg->data;
      fprintf(stderr, "entity:%s string = %s\n", function, arg_string);
    }
    else if (LANG_BINSTRING == arg->type)
    {
      fprintf(stderr, "entity:%s <binary data(not shown)>\n", function);
    }
  }

  return TRUE;
}


static gint
entity_function_execute (XML_Node* calling_node, gchar *function, GSList *args)
{ 
  EntityFunc func;

  func = g_hash_table_lookup(entity_func_ht, function);

  if (!func)
    return TRUE;

  /* Just pass on the function... */
  return func(calling_node, function, args);
}

void entity_lang_init(void)
{
  lang_register("entity", entity_function_execute);
  lang_register("e", entity_function_execute);

  entity_func_ht = g_hash_table_new (g_str_hash, g_str_equal);

  /* Setup the function string -> code ht. */
  g_hash_table_insert(entity_func_ht, "delete",         lang_entity_delete);
  g_hash_table_insert(entity_func_ht, "exit",           lang_entity_exit);
  g_hash_table_insert(entity_func_ht, "hide",           lang_entity_hide);
  g_hash_table_insert(entity_func_ht, "hide_window",   lang_entity_hide_window);
  g_hash_table_insert(entity_func_ht, "show_args",      lang_entity_show_args);
}

