#include <glib.h>
#include <gmodule.h>
#include <stdio.h>
#include <stdlib.h>
#include <elements.h>
#include <xml-node.h>
#include <xml-tree.h>
#include <xml-parser.h>
#include <dlfcn.h>
#include <lang.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include "md5.h"
#include "edebug.h"

static char *codedir;
static int link_in_so (char *so, XML_Node * node);


static char *stdheaders = "                                     \
#include <stdio.h>                                              \
#include <stdlib.h>                                             \
#include <glib.h>                                               \
  char *xml_node_get_attr (void *node, char *str);              \
#define c_attrib(x) xml_node_get_attr(node, x)                  \
"; 

static GHashTable *c_functions_ht = NULL;

gint check_checksum (char *obj, char *ver, char *code)
{
  char *csumfile;		/*File with the checksum info for this code block. */
  void *code_digest;
  char *file_digest[16];
  MD5_CTX context;
  int fd;
  int ret;


  /*Find the md5 checksum of the code. */
  MD5Init (&context);
  MD5Update (&context, code, strlen (code));
  code_digest = MD5Final (&context);

  csumfile = g_strconcat (codedir, "/.sum/", obj, ver, NULL);
  edebug ("c-embed", "sumfile = %s\n", csumfile);

  /*Open the file */
  fd = open (csumfile, O_RDWR, 0640);
  ret = read (fd, file_digest, 16);
  if (ret < 16)
    {
      close (fd);
      fd = creat (csumfile, 0640);
      if (!fd)
	exit (1);
      g_free (csumfile);
      write (fd, code_digest, 16);
      close (fd);
      return FALSE;
    }
  g_free (csumfile);

  if (memcmp (file_digest, code_digest, 16) == 0)
    return TRUE;		/*Sums are the same. */

  lseek (fd, 0, SEEK_SET);
  write (fd, code_digest, 16);

  return FALSE;
}

gchar *
c_compile_str_get (XML_Node * node, char *tag)
{
  char *str = NULL;
  char *temp;
  GSList *list;
  XML_Node *child;
  for (list = node->children; list; list = list->next)
    {
      child = (XML_Node *) list->data;
      if (strcmp (child->element, tag) != 0 || !child->data)
	continue;
      temp = g_strconcat (child->data->str, str, NULL);
      if (str)
	g_free (str);
      str = temp;
    }
  if (!str)
    str = "";
  return (str);

}

/* get version info string from version.. just replace '.' with ':' */
static gchar *
to_version_info (gchar * ver)
{
  gchar *ver_info;
  gint i;
  gint len;
  ver_info = g_strdup (ver);

  len = strlen (ver_info);
  for (i = 0; i < len; i++)
    {
      if (ver_info[i] == '.')
	ver_info[i] = ':';
    }
  return (ver_info);
}

/* Render all the c-code tags.
 * Code that has been modified will be recompiled and relinked into
 * .so files.   These .so are then dlopen'ed and functions that have
 * been exported to entity using the <c-function> tag will then
 * added to the c_functions_ht so that they can be called from entity.
 */
static void
c_node_render (XML_Node * node)
{

  gchar *obj;			/* Object name, append .so for the lib name. */
  gchar *ver;			/* Added to the lib name, for multiple versions. */
  gchar *ver_info;		/* For libtool versioning */
  gchar *includes;		/* Text inside of <c-includes>. */
  gchar *libs;			/* Text inside of <c-libs>. */

  gchar *obj_cmd;		/* Commands that are ran to compile the c. */
  gchar *so_cmd;

  gchar *so;			/* Full path to library once compiled. */
  gchar *data;

  FILE *fp;

  if (!node || !node->data)
    {
      g_warning ("What?  why did i get this node then?\n");
      return;
    }

  obj = xml_node_get_attr (node, "object");
  if (!obj)
    obj = "libdefault";		/* This will be really SLOW!!! because each new 
				   object will have to be compiled. FIXME */
  else
    obj = g_strconcat ("lib", obj, NULL);	/* libtool needs the object to 
						   start with a lib. */

  ver = xml_node_get_attr (node, "version");
  if (!ver)
    ver = "0";
  ver_info = to_version_info (ver);


  /* build the so name for later g_module_open () */
  so = g_strconcat (codedir, "/.libs/", obj, ".so.", ver, NULL);

  includes = c_compile_str_get (node, "c-includes");
  libs = c_compile_str_get (node, "c-libs");

  /*This is the data that is checksummed, we need all od this so that
   * the relink and recompile happen if only the <c-[libs|includes]
   * are changed in the xml.
   */
  data = g_strconcat (includes, libs, node->data->str, NULL);

  if (check_checksum (obj, ver, data) == FALSE && 1)
    {
      char *tempfile = NULL;	/* File created to hold the c code. */

      tempfile = g_strconcat (codedir, "entity.c", NULL);	/* FIXME */
      if (!tempfile)
	{
	}

      /* Open temp file. */
      fp = fopen (tempfile, "w");
      if (!fp)
	{
	}

      fprintf (fp, "%s", stdheaders);
      fprintf (fp, "%s", node->data->str);
      fclose (fp);


      obj_cmd = g_strconcat ("libtool --mode=compile gcc ",
			     includes, " ", tempfile,
			     " `gtk-config --cflags` -c -o ",
			     codedir, ".objects/", obj, ver, ".lo", NULL);
      so_cmd = g_strconcat ("libtool --mode=link gcc ",
			    libs, " ", "-version-info ", ver_info, " ",
			    codedir, ".objects/", obj, ver, ".lo",
			    " -rpath /usr/lib  -o ",
			    codedir, obj, ".la", NULL);

      g_free (ver_info);

      edebug ("c-embed", "%s", obj_cmd);
      edebug ("c-embed", "%s", so_cmd);
      edebug ("c-embed", "%s", so);

      /* Rebuild if checksum is different */
      if (system (obj_cmd))
	{
	  g_warning ("C-code was not recompiled!\n");
	}
      if (system (so_cmd))
	{
	  g_warning ("C-code was not relinked!\n");
	}

      g_free (obj_cmd);
      g_free (so_cmd);
      g_free (tempfile);
    }
  g_free (data);

  link_in_so (so, node);
  g_free (so);
}


static int
link_in_so (gchar * modpath, XML_Node * node)
{
  gchar *function = NULL;
  GModule *module;
  void (*func) (GSList *);
  GSList *list;			/* List of children. */
  XML_Node *child;

  module = g_module_open (modpath, G_MODULE_BIND_LAZY);

  if (!module)
    {
      g_warning
	("Error loading dynamic library '%s': may have undefined references!\n",
	 modpath);
      return (1);
    }

  for (list = node->children; list; list = list->next)
    {
      child = (XML_Node *) list->data;

      if (strcmp (child->element, "c-function") != 0)
	continue;

      function = xml_node_get_attr (child, "name");
      if (!function && strlen (function) == 0)
	continue;

      g_module_symbol (module, function, (void **) &func);

      if (!func)
	{
	  g_warning ("Function '%s' not in code.\n", function);
	}
      else
	{
	  g_hash_table_insert (c_functions_ht, function, func);
	}
    }

  return (0);
}

int
c_call (char *function, XML_Node * node, GSList * args)
{
  void (*funct) (XML_Node *, GSList *);
  funct = g_hash_table_lookup (c_functions_ht, function);
  if (funct)
    {
      funct (node, args);
      return TRUE;
    }
  else
    {
      g_warning ("Function (%s) not defined.\n", function);
      return FALSE;
    }
}

static gint
c_function_execute (XML_Node * calling_node, gchar * function, GSList * args)
{
  void (*funct) (XML_Node *, GSList *);

  funct = g_hash_table_lookup (c_functions_ht, function);

  if (funct && args)
    {
      funct (args->data, args->next);
      return TRUE;
    }
  else
    {
      g_warning ("Function (%s) not defined.\n", function);
      return FALSE;
    }
}

void
c_init (void)
{
  Element *element;
  char *sumdir;
  char *libdir;

  c_functions_ht = g_hash_table_new (g_str_hash, g_str_equal);

  element = g_new0 (Element, 1);
  element->render_func = c_node_render;
  element->destroy_func = NULL;
  element->tag = "c-code";

  codedir = g_strconcat (g_get_home_dir (), "/entity/c-code/", NULL);
  sumdir = g_strconcat (codedir, "/.sum", NULL);
  libdir = g_strconcat (codedir, "/.objects", NULL);

  if (mkdir (codedir, 0750) == -1 && errno != EEXIST)
    {
      g_error ("Can't create %s, no c-code tags can be rendered!\n", codedir);
      exit (1);
    }

  if (mkdir (sumdir, 0750) == -1 && errno != EEXIST)
    {
      g_error ("Can't create %s, no c-code tags can be rendered!\n", sumdir);
      exit (1);
    }

  if (mkdir (libdir, 0750) == -1 && errno != EEXIST)
    {
      g_error ("Can't create %s, no c-code tags can be rendered!\n", libdir);
      exit (1);
    }

  g_free (sumdir);
  g_free (libdir);

  element_register (element);

  lang_register ("c-code", c_function_execute);
  lang_register ("c", c_function_execute);
}
