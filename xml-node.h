#ifndef __XML_NODE_H__
#define __XML_NODE_H__

#include "xmlparse.h"
#include "eutils.h"
#include "ebuffer.h"
#include <glib.h>

typedef enum
{
  XML_NODE_RENDERED = 1 << 0,
  XML_NODE_PARENTED = 1 << 1,
  XML_NODE_NO_RENDER_CHILDREN = 1 << 2
}
XML_Node_Flags;


typedef struct _XML_Node XML_Node;

struct _XML_Node
{
  /* List of child nodes, keep tail pointer arround for fast appends */
  GSList *children;
  GSList *children_tail;
  
  /* parent node. */
  XML_Node *parent;

  /* This node's tag */
  gchar *element;

  /* Node name, this is a concatination of element.name_value */
  EBuf *name;

  /* data after this tag up to the next tag - breaks are inserted with \0's
   * but data->len is equal to true string length. */
  GString *data;

  /* If the data was taken from a CDATA section, remember this here */
  gint data_is_cdata;

  /* XML attr/value pairs */
  GHashTable *atts;

  /* This will contain any information needed for passing
     to child nodes for rendering modules. 
     eg. this holds the GtkWidget * for tags that
     deal with widgets.
     Use erend_set_entity_data () and erend_get_entity_data ()
     for interface,
   */
  GHashTable *entity_data;

  /* gpointer user_data; */

  /* For internal use to keep track of state. */
  gint flags;
};


void xml_node_init (void);

XML_Node *xml_node_root (void);

/*
gchar *
xml_node_set_attributes (gchar *path, char **atts);
*/

gchar *
xml_node_get_path (XML_Node * node);

void 
xml_node_set_data (XML_Node * node, XML_Char * data);

GString *
xml_node_get_data (XML_Node * node);

gint 
xml_node_data_is_cdata (XML_Node * node);

void 
xml_node_append_data (XML_Node * node, XML_Char * data);

void 
xml_node_append_cdata (XML_Node * node, XML_Char * data);


EBuf *
xml_node_get_attr (XML_Node * node, gchar *attr);

gchar *
xml_node_get_attr_str (XML_Node * node, gchar *attr);

void
xml_node_set_attr (XML_Node * node, gchar *attr, EBuf *value);

void
xml_node_set_attr_quiet (XML_Node * node, gchar *attr, EBuf *value);

void
xml_node_set_attr_str (XML_Node * node, gchar *attr, gchar *value);

void
xml_node_set_attr_str_quiet (XML_Node * node, gchar *attr, gchar *value);

/* return parent node */
XML_Node *
xml_node_parent (XML_Node * node);

void 
xml_node_free (XML_Node * node);

void 
xml_node_destroy (XML_Node * node);

XML_Node *
xml_node_new (gchar *element);

XML_Node *
xml_node_new_child (XML_Node* parent, gchar* tag, gchar* name, GSList* atts);

gchar * 
xml_node_set_name (XML_Node *node, gchar *name);

gchar *
xml_node_unique_name (gchar *name);

void
xml_node_set_parent (XML_Node *node, XML_Node *parent);

void
xml_node_remove_from_parent (XML_Node *node);


#include "xml-node-utils.h"

#endif /* __XML_NODE_H__ */
