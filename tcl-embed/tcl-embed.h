/* -*- C -*-
 * FILE: "/home/joze/tmp/entity-0.3.7-/tcl-embed/tcl-embed.h"
 * LAST MODIFICATION: "Fri Oct  8 10:19:34 1999 (joze)"
 * 1999 by Johannes Zellner, <johannes@zellner.org>
 * $Id$
 */

#ifndef __TCL_H__
#define __TCL_H__

void
execute_tcl_code(XML_Node *calling_node, gchar *code);

void
execute_tcl_function (XML_Node *calling_node, gchar *function, GSList *args);


void 
tcl_init (void);


#endif /* __TCL_H__ */













