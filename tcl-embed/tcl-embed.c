/* -*- C -*-
 * FILE: "/home/joze/pub/gnome/entity-0.3.7/tcl-embed/tcl-embed.c"
 * LAST MODIFICATION: "Sun Oct 10 15:15:37 1999 (joze)"
 * 1999 by Johannes Zellner, <johannes@zellner.org>
 * $Id$
 * vim:set ts=4:
 */

#include "config.h"

#ifdef USE_TCL


#include <tcl.h>
/* sorry, EXTERN was defined in tcl.h, remove this definition */
#ifdef EXTERN
#   undef EXTERN
#endif

#include <assert.h>
#include <string.h>
#include <gtk/gtk.h>
#include "elements.h"
#include "xml-node.h"
#include "xml-tree.h"
#include "xml-parser.h"
#include "lang.h"
#include "tcl-embed.h"
#include "edebug.h"


/**
 * forward declarations
 */
int tcl_set_attr(ClientData, Tcl_Interp*, int, char**);
int tcl_get_attr(ClientData, Tcl_Interp*, int, char**);
int tcl_set_data(ClientData, Tcl_Interp*, int, char**);
int tcl_get_data(ClientData, Tcl_Interp*, int, char**);
int tcl_find(ClientData, Tcl_Interp*, int, char**);
int tcl_find_parent(ClientData, Tcl_Interp*, int, char**);
int tcl_delete_tree(ClientData, Tcl_Interp*, int, char**);
int tcl_current_app(ClientData, Tcl_Interp*, int, char**);
int tcl_ls(ClientData, Tcl_Interp*, int, char**);
int tcl_delete_tree_in(ClientData, Tcl_Interp*, int, char**);
int tcl_append_xml(ClientData, Tcl_Interp*, int, char**);
int tcl_get_xml(ClientData, Tcl_Interp*, int, char**);
int tcl_exit(ClientData, Tcl_Interp*, int, char**);
int tcl_quit(ClientData, Tcl_Interp*, int, char**);

/**
 * globals
 */
static Tcl_Interp* Interp;
static XML_Node *current_tcl_calling_node = NULL;

typedef struct tclcmd {
	char* name;
	int (*fun)(ClientData, Tcl_Interp*, int, char**);
} tclcmd;

#define TCL_NAMESPACE "::Entity::"
#define TCLNEWCMD(x) { TCL_NAMESPACE #x, tcl_ ## x }

tclcmd tclcmds[] = {
	TCLNEWCMD(set_attr),
	TCLNEWCMD(get_attr),
	TCLNEWCMD(set_data),
	TCLNEWCMD(get_data),
	TCLNEWCMD(find),
	TCLNEWCMD(find_parent),
	TCLNEWCMD(delete_tree),
	TCLNEWCMD(current_app),
	TCLNEWCMD(ls),
	TCLNEWCMD(delete_tree_in),
	TCLNEWCMD(append_xml),
	TCLNEWCMD(get_xml),
	TCLNEWCMD(exit),
	TCLNEWCMD(quit),
	{ (char*) NULL, 0 }
};


/**
 * === LIFECYCLE ===
 */


/**
 * create a tcl interpreter and
 * install the tcl commands.
 *
 * @ref tcl_init()
 */
static void
tcl_create_new_interp(void)
{
	tclcmd* cmd;

	/* create a tcl interpreter */
	assert((Interp = Tcl_CreateInterp()));

	assert(TCL_OK == Tcl_Init(Interp));
	/* create the tcl commands */
	for (cmd = tclcmds; cmd->name; cmd++) {
		Tcl_CreateCommand(Interp, cmd->name, cmd->fun,
			(ClientData) NULL, (Tcl_CmdDeleteProc*) NULL);
	}

	/* export all commands */
	Tcl_Eval(Interp, "namespace eval " TCL_NAMESPACE " {namespace export *}");
}

/**
 * return a namespace, which corresponds to `calling_node'
 * or create a new one, if it doesn't exist yet.
 *
 * @return gchar* namespace
 * @see get_perl_namespace()
 */
static gchar*
get_tcl_namespace (XML_Node* calling_node)
{
  return ( lang_namespace_get ("tcl", calling_node) );
}

/**
 * evaluate tcl code in a tcl namespace which
 * corresponds to the `calling_node'. `code' is
 * typcially the stuff between the begin <tcl>
 * and end </tcl> tags and must be a valid tcl
 * script.
 *
 * @see execute_perl_code()
 */
void
execute_tcl_code(XML_Node *calling_node, gchar *code)
{
	static GString* tcl_cmd = NULL;
	gchar* namespace;
	int status;

	if (tcl_cmd == NULL) {
		tcl_cmd = g_string_sized_new(1024);
	}
	g_string_truncate(tcl_cmd, 0);

	current_tcl_calling_node = calling_node;
	namespace = get_tcl_namespace(calling_node);

	g_string_append(tcl_cmd, "namespace eval ");
	g_string_append(tcl_cmd, namespace);
	g_string_append(tcl_cmd, " {\n");
	g_string_append(tcl_cmd, code);
	g_string_append(tcl_cmd, "\n}");

	edebug ("tcl-embed", "evaluating tcl script:\n%s\n", tcl_cmd->str);

	status = Tcl_Eval(Interp, tcl_cmd->str);
	if (TCL_OK != status) {
		g_warning(Tcl_GetStringResult(Interp));
	}
}

static void
tcl_node_render (XML_Node *node)
{
	execute_tcl_code (node, node->data->str);
}

static void
tcl_node_destroy (XML_Node *node)
{
	return;
}

void
execute_tcl_function (XML_Node* calling_node, gchar* function, GSList* args)
{
	static GString* tcl_cmd = NULL;
	GSList* GSLp;
	int status = TCL_OK;

	if (tcl_cmd == NULL) {
		tcl_cmd = g_string_sized_new (1024);
	}

	current_tcl_calling_node = calling_node;

	/* create a command inside the namespace
	 * corresponding to the calling node */
	g_string_truncate (tcl_cmd, 0);
	g_string_append(tcl_cmd, "namespace eval ");
	g_string_append(tcl_cmd, get_tcl_namespace(calling_node));
	g_string_append(tcl_cmd, " {\n");
	g_string_append(tcl_cmd, function);

	for (GSLp = args; GSLp; GSLp = GSLp->next) {
		gchar* arg_str = GSLp->data;
		g_string_append(tcl_cmd, " ");
		g_string_append(tcl_cmd, arg_str);
		g_free(arg_str);
	}

	/* append a closing brace */
	g_string_append(tcl_cmd, "\n}");
	g_slist_free(args);

	edebug ("tcl-embed", "Created full qualified tcl command:\n%s\n", tcl_cmd->str);
	
	status = Tcl_Eval(Interp, tcl_cmd->str);

	if (TCL_OK != status) {
		g_warning(Tcl_GetStringResult(Interp));
	}
	/* return 0; */ /* XXX */
}

/**
 * create a the <tcl> tag.
 */
void 
tcl_init(void)
{
	Element *element;

	/* initialize tcl interpreter */
	tcl_create_new_interp();

	/* Register tcl as a tag type */
	element = g_malloc0 (sizeof (Element));
	element->render_func = tcl_node_render;
	element->destroy_func = tcl_node_destroy;
	element->tag = "tcl";

	element_register (element);
	
	/* register tcl language */
	lang_register ("tcl", execute_tcl_function);
	
	/* g_print ("Tcl registration complete.\n"); */
}

void
tcl_end(void)
{
    if (!Tcl_InterpDeleted(Interp)) {
        Tcl_DeleteInterp(Interp);
    }
}

void
tcl_reload(void)
{
   tcl_end();
   tcl_init();
}


/**
 * === ERROR MESSAGES ===
 */

int
tclErrPath(Tcl_Interp* interp, char* name, gchar* path)
{
	Tcl_AppendResult(interp, TCL_NAMESPACE, name,
		": node '", path, "' not found.", (char*) 0);
	return TCL_ERROR;
}

int
tclErrWrongNumArgs(Tcl_Interp* interp, char* name)
{
	Tcl_AppendResult(interp, TCL_NAMESPACE, name,
		": Wrong # of arguments.", (char*) 0);
	return TCL_ERROR;
}

/* === */

/*: tcl-api-reference.html
 *
 * <a name=set_attr class=entry>Entity::set_attr path attribute value</a>
 *
 * <dl>
 *     <dt>path</dt><dd>
 *         target xml node.
 *     </dd>
 *     <dt>attribute</dt><dd>
 *         the attribute of path which should be modified.</dd>
 *     </dd>
 *     <dt>value</dt><dd>
 *         the new value of attribute.
 *     </dd>
 * </dl>
 * 
 * set_attr modifies an attribute of a xml element.
 * <div class=returns>
 *   returns nothing.
 * </div>
 */
int
tcl_set_attr(ClientData clientData, Tcl_Interp* interp, int argc, char** argv)
{
	gchar* path;
	gchar* attr;
	gchar* value;
	XML_Node* node;

	if (4 != argc) {
		return tclErrWrongNumArgs(interp, "set_attr");
	}

	path = argv[1];
	attr = argv[2];
	value = argv[3];

	node = xml_node_fancy_find(current_tcl_calling_node, path);

    if (!node) {
		return tclErrPath(interp, "get_attr", path);
	}

    xml_node_set_attr(node, attr, value);
	return TCL_OK;
}

/*: tcl-api-reference.html
 *
 * <a name=get_attr class=entry>Entity::get_attr path attribute</a>
 *
 * <dl>
 *     <dt>path</dt><dd>
 *         target xml node.
 *     </dd>
 *     <dt>attribute</dt><dd>
 *         the attribute of path whose value should be retrieved
 *     </dd>
 * </dl>
 *
 * <div class=returns>
 *   returns the value of the <code>path</code>'s attribute
 *   <code>attr</code>.
 * </div>
 */
int
tcl_get_attr(ClientData clientData, Tcl_Interp* interp, int argc, char** argv)
{
    gchar* path;
    gchar* attr;
    gchar* value;
    XML_Node* node;

	if (3 != argc) {
		return tclErrWrongNumArgs(interp, "get_attr");
	}

	path = argv[1];
	attr = argv[2];

    node = xml_node_fancy_find(current_tcl_calling_node, path);

    if (!node) {
		return tclErrPath(interp, "get_attr", path);
	}

    value = xml_node_get_attr(node, attr);

    if (!value) {
        Tcl_AppendResult(interp, TCL_NAMESPACE,
			"get_attr: unable to get value.", (char*) 0);
        return TCL_ERROR;
    }

	Tcl_AppendResult(interp, value, (char*) 0);
	return TCL_OK;
}

/*: tcl-api-reference.html
 *
 * <a name=set_data class=entry>Entity::set_data path string</a>
 *
 * <dl>
 *     <dt>path</dt><dd>
 *         target xml node.
 *     </dd>
 *     <dt>string</dt><dd>
 *         new data value of path
 *     </dd>
 * </dl>
 * 
 * set_data modifies the data value of a xml element.
 *
 * <div class=returns>
 *   returns nothing.
 * </div>
 */
int
tcl_set_data(ClientData clientData, Tcl_Interp* interp, int argc, char** argv)
{
    gchar* path;
    gchar* data;
    XML_Node* node;

	if (3 != argc) {
		return tclErrWrongNumArgs(interp, "set_data");
	}

	path = g_strdup(argv[1]);
	data = g_strdup(argv[2]);

    node = xml_node_fancy_find(current_tcl_calling_node, path);

    if (!node) {
		return tclErrPath(interp, "set_data", path);
	}

    xml_node_set_data(node, data);

	return TCL_OK;
}

/*: tcl-api-reference.html
 *
 * <a name=get_data class=entry>Entity::get_data path</a>
 *
 * <dl>
 *     <dt>path</dt><dd>
 *         target xml node.
 *     </dd>
 * </dl>
 * 
 * <div class=returns>
 *   returns the data value of an xml path.
 * </div>
 */
int
tcl_get_data(ClientData clientData, Tcl_Interp* interp, int argc, char** argv)
{
    gchar* path;
    XML_Node* node;
	GString* str;

	if (2 != argc) {
		return tclErrWrongNumArgs(interp, "get_data");
	}

	path = argv[1];

    node = xml_node_fancy_find(current_tcl_calling_node, path);

    if (!node) {
		return tclErrPath(interp, "get_data", path);
	}

    str = xml_node_get_data(node);
	Tcl_AppendResult(interp, str->str, (char*) 0);

	return TCL_OK;
}

/*: tcl-api-reference.html
 *
 * <a name=find class=entry>Entity::find path name</a>
 *
 * <dl>
 *     <dt>path</dt><dd>
 *         target xml node.
 *     </dd>
 *     <dt>name</dt><dd>
 *         the full name of the node you're after
 *     </dd>
 * </dl>
 *
 * <div class=returns>
 *   returns ??? TODO
 * </div>
 */
int
tcl_find(ClientData clientData, Tcl_Interp* interp, int argc, char** argv)
{
    gchar* path;
    gchar* name;
    XML_Node* node;

	if (3 != argc) {
		return tclErrWrongNumArgs(interp, "find");
	}

	path = argv[1];
	name = argv[2];

    node = xml_node_fancy_find(current_tcl_calling_node, path);

    if (!node) {
		return tclErrPath(interp, "find", path);
	}

	node = xml_node_fancy_find(node, name);

    if (!node) {
		return tclErrPath(interp, "find", path);
	}

	path = xml_node_get_path(node);

	if (path) {
		Tcl_AppendResult(interp, path, (char*) 0);
	} else {
		/**
		 * TODO: should an error message be left
		 *       in the interp's result space ?
		 */
	}

	return TCL_OK;
}

/*: tcl-api-reference.html
 *
 * <a name=find_parent class=entry>Entity::find_parent start_path type</a>
 *
 * <dl>
 *     <dt>start_path</dt><dd>
 *         the path to begin the search at.
 *     </dd>
 *     <dt>type</dt><dd>
 *         the node type to find.
 *     </dd>
 * </dl>
 *
 * <div class=returns>
 *   returns the name of the parent.
 * </div>
 */
int
tcl_find_parent(ClientData clientData, Tcl_Interp* interp, int argc, char** argv)
{
    gchar* path;
    gchar* name;
    XML_Node* node;
    XML_Node* parent_node;

	if (3 != argc) {
		return tclErrWrongNumArgs(interp, "find_parent");
	}

	path = argv[1];
	name = argv[2];

    node = xml_node_fancy_find(current_tcl_calling_node, path);

    if (!node) {
		return tclErrPath(interp, "find_parent", path);
	}

	path = xml_node_get_path(node);

	if (path) {
		Tcl_AppendResult(interp, path, (char*) 0);
	}

	parent_node = xml_node_find_parent_by_type(node, name);

	if (parent_node) {
		Tcl_AppendResult(interp, xml_node_get_path(parent_node), (char*) 0);
	} else {
		/**
		 * TODO: should an error message be left
		 *       in the interp's result space ?
		 */
	}

	return TCL_OK;
}

/*: tcl-api-reference.html
 *
 * <a name=delete_tree class=entry>Entity::delete_tree path</a>
 *
 * <dl>
 *     <dt>path</dt><dd>
 *         target xml node.
 *     </dd>
 * </dl>
 *
 * <div class=returns>
 *   returns nothing.
 * </div>
 */
int
tcl_delete_tree(ClientData clientData, Tcl_Interp* interp, int argc, char** argv)
{
    gchar* path;
    XML_Node* node;

	if (2 != argc) {
		return tclErrWrongNumArgs(interp, "delete_tree");
	}

	path = argv[1];

    node = xml_node_fancy_find(current_tcl_calling_node, path);

    if (node) {
		xml_tree_delete(node, TRUE);
	} else {
		/**
		 * TODO: should an error message be left
		 *       in the interp's result space ?
		 */
	}

	return TCL_OK;
}

/*: tcl-api-reference.html
 *
 * <a name=current_app class=entry>Entity::current_app</a>
 *
 * <div class=returns>
 *   returns the name of the current application.
 * </div>
 */
int
tcl_current_app(ClientData clientData, Tcl_Interp* interp, int argc, char** argv)
{
	XML_Node* appnode;

	if (1 != argc) {
		return tclErrWrongNumArgs(interp, "current_app");
	}

	appnode = xml_node_find_parent_by_type(current_tcl_calling_node, "app");

	if (appnode) {
		Tcl_AppendResult(interp, xml_node_get_path(appnode), (char*) 0);
	} else {
		/**
		 * TODO: should an error message be left
		 *       in the interp's result space ?
		 */
	}

	return TCL_OK;
}

/*: tcl-api-reference.html
 *
 * <a name=ls class=entry>Entity::ls path</a>
 *
 * <dl>
 *     <dt>path</dt><dd>
 *         target xml node.
 *     </dd>
 * </dl>
 *
 * <div class=returns>
 *   returns an array of the paths to the nodes inside the specified node.
 * </div>
 */
int
tcl_ls(ClientData clientData, Tcl_Interp* interp, int argc, char** argv)
{
    gchar* path;
	GSList* list;
	GSList* tmp;
    XML_Node* node;

	if (2 != argc) {
		return tclErrWrongNumArgs(interp, "ls");
	}

	path = argv[1];

    node = xml_node_fancy_find(current_tcl_calling_node, path);

    if (!node) {
#if 0
		return tclErrPath(interp, "ls", path);
#endif
		/**
		 * TODO: XS_Entity_ls just returns here.
		 */
		return TCL_OK;
	}

	list = xml_node_ls(node);

	for (tmp = list; tmp; tmp = tmp->next) {
		Tcl_AppendElement(interp, tmp->data);
	}

	return TCL_OK;
}

/*: tcl-api-reference.html
 *
 * <a name=delete_tree_in class=entry>Entity::delete_tree_in path</a>
 *
 * <dl>
 *     <dt>path</dt><dd>
 *         target xml node.
 *     </dd>
 * </dl>
 *
 * delete the XML inside the given path,
 * but does not delete the specified node.
 *
 * <div class=returns>
 *   returns nothing.
 * </div>
 */
int
tcl_delete_tree_in(ClientData clientData, Tcl_Interp* interp, int argc, char** argv)
{
    gchar* path;
    XML_Node* node;

	if (2 != argc) {
		return tclErrWrongNumArgs(interp, "delete_tree_in");
	}

	path = argv[1];

    node = xml_node_fancy_find(current_tcl_calling_node, path);

    if (node) {
		xml_tree_delete_in(node, TRUE);
	} else {
		/**
		 * TODO: should an error message be left
		 *       in the interp's result space ?
		 */
	}

	return TCL_OK;
}

/*: tcl-api-reference.html
 *
 * <a name=append_xml class=entry>Entity::append_xml parent_path name</a>
 *
 * <dl>
 *     <dt>path</dt><dd>
 *         the path to the parent node that
 *         will contain the specified xml.
 *     </dd>
 *     <dt>name</dt><dd>
 *         a string containing proper xml to be appended.
 *     </dd>
 * </dl>
 *
 * <div class=returns>
 *   returns nothing.
 * </div>
 */
int
tcl_append_xml(ClientData clientData, Tcl_Interp* interp, int argc, char** argv)
{
    gchar* path;
    gchar* xml;
    XML_Node* node;

	if (3 != argc) {
		return tclErrWrongNumArgs(interp, "append_xml");
	}

	path = argv[1];
	xml = argv[2];

    node = xml_node_fancy_find(current_tcl_calling_node, path);

    if (!node) {
		return tclErrPath(interp, "append_xml", path);
	}

	xml_parse_string(node, xml);
	xml_tree_render(node);

	return TCL_OK;
}

/*: tcl-api-reference.html
 *
 * <a name=get_xml class=entry>Entity::get_xml top_path</a>
 *
 * <dl>
 *     <dt>top_path</dt><dd>
 *         the path to the top XML node to
 *         begin generating xml text at.
 *     </dd>
 * </dl>
 *
 * <div class=returns>
 *   returns the xml of top_path.
 * </div>
 */
int
tcl_get_xml(ClientData clientData, Tcl_Interp* interp, int argc, char** argv)
{
    gchar* path;
    gchar* xml;
    XML_Node* node;

	if (2 != argc) {
		return tclErrWrongNumArgs(interp, "get_xml");
	}

	path = argv[1];

    node = xml_node_fancy_find(current_tcl_calling_node, path);

    if (!node) {
		return tclErrPath(interp, "get_xml", path);
	}

	xml = xml_tree_get_xml(node);
	Tcl_AppendResult(interp, xml, (char*) 0);

	return TCL_OK;
}

/*: tcl-api-reference.html
 *
 * <a name=exit class=entry>Entity::exit</a>
 *
 * terminate the current application.
 *
 * <div class=returns>
 *   does not return.
 * </div>
 */
int
tcl_exit(ClientData clientData, Tcl_Interp* interp, int argc, char** argv)
{
	XML_Node* appnode;

	if (1 != argc) {
		return tclErrWrongNumArgs(interp, "exit");
	}

	appnode = xml_node_find_parent_by_type(current_tcl_calling_node, "app");

	if (appnode) {
		xml_tree_delete(appnode, TRUE);
	} else {
		/**
		 * TODO: should an error message be left
		 *       in the interp's result space ?
		 */
	}

	return TCL_OK;
}

/*: tcl-api-reference.html
 *
 * <a name=quit class=entry>Entity::quit</a>
 *
 * terminate entity.
 *
 * <div class=returns>
 *   does not return.
 * </div>
 */
int
tcl_quit(ClientData clientData, Tcl_Interp* interp, int argc, char** argv)
{
	gtk_main_quit();
	/* NOTREACHED */
	return TCL_OK; /* keep the compiler happy */
}

#endif /* USE_TCL */
